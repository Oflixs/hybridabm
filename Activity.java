/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

public class Activity
{
   private static final long serialVersionUID = 1L;
   
   public String type = "";
   public String name = "";
   public Calendar from = null;
   public Calendar to = null;
   public int timePerWeek = 0;
   public String timeDist = "";
   public double sigma = 0.0;
   public List<Integer> activeDays = new ArrayList<Integer>();
   public Boolean allDays = false;
   public int maxHoursPerDay = 0;
   public int noFlexTime = 0;
   public double fractPop = 0.0;
   public int priority = 0;
   public double malariaRisk = 0.0;


   public AmaSim sim;
   public SimState state;

   //====================================================
   public Activity(SimState pstate)
   {
       sim = (AmaSim)pstate;
       state = pstate;
   }

   //====================================================
   public void print()
   {

       System.out.println(" ");
       System.out.println("-- Activity   Print resume --");
       System.out.println("Type: " +  type  + " Name: " + name);
       System.out.println("From: " +  from.getTime() + " To: " + to.getTime());
       System.out.println("Time per week: " +  timePerWeek);
       System.out.println("Time Distribution: " +  timeDist);
       System.out.println("Sigma: " +  sigma);
       System.out.print("Active Days: ");
       for(int i = 0; i < activeDays.size(); i++)
       {
           System.out.print(activeDays.get(i) + " ");
       }
       System.out.println("");
       if(allDays)System.out.println("All days: yes");
       else System.out.println("All days: no");
       System.out.println("Max Hours per day:" + maxHoursPerDay);
       System.out.println("Fractrion of the Population: " + fractPop);
       System.out.println("Priority: " + priority);

   }

   //====================================================
   public void printShort()
   {

       System.out.println("-- Activity   Print resume --");
       System.out.println("Type: " +  type  + " Name: " + name);

   }



//============================================================   
}
