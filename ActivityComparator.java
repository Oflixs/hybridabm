/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.util.*;
import sim.engine.*;
import sim.util.*;
import sim.field.network.*;

public class ActivityComparator implements Comparator, java.io.Serializable
{
   private static final long serialVersionUID = 1L;

   //----------------------------------------------------------
   public ActivityComparator()
   {
   }

   //----------------------------------------------------------
   public int compare(Object ao1, Object ao2)
   {

      Activity a1 = ((Activity)ao1);
      Activity a2 = ((Activity)ao2);
 
      if(a1.priority > a2.priority)
          return 1;
      else if(a1.priority < a2.priority)
          return -1;
      else
          return 0;
   }


}
