/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

public class AgeSegment
{
   private static final long serialVersionUID = 1L;

   public String  name = "";
   public int  ageMax = 0;
   public int  ageMin = 0;
   public int  numSleepHours = 0;
   public Activity activity;

   public AmaSim sim;
   public SimState state;

   //====================================================
   public AgeSegment(SimState pstate)
   {
       sim = (AmaSim)pstate;
       state = pstate;
   }

   //====================================================
   public void print()
   {
       System.out.println(" ");
       System.out.println("-- Age segment Print Resume -");
       System.out.println("Name: " +  name);
       System.out.println("Age Min: " +  ageMin);
       System.out.println("Age Max: " +  ageMax);
   }



//============================================================   
}
