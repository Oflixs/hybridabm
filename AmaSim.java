/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import sim.engine.*;
import sim.util.*;
import java.text.DecimalFormat;
import sim.field.grid.*;
import sim.field.network.*;
import java.util.ArrayList;
import java.util.List;


import sim.io.geo.*;
import sim.field.geo.GeomVectorField;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.planargraph.Node;
import com.vividsolutions.jts.geom.Geometry;

import sim.util.geo.GeomPlanarGraph;
import sim.util.geo.MasonGeometry;
import sim.util.geo.GeometryUtilities;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

import sim.portrayal.grid.*;

public class AmaSim extends SimState
{
   private static final long serialVersionUID = 1L;

   boolean debug = true;

   static String outDir = "";
   static String global_input_file = "";
   static String doAnalysis = "";

   String outDirCom = "";
   String global_input_fileCom = "";

   String outPath = "";
   String outFileNumbering = "";

   String monthDate = "";

   //For hybrid analysis
   public static Boolean hybridAnalysis = false;
   public HybridAnalysis hAnalysis = null;

   //For parallel code 
   static Boolean singleRun = false;
   public static Boolean calibration = false;
   public Community community = null;
   public static String testCommunityName = "";

   boolean first_run = true;
   boolean show_graph = false;
   public WriteStats stats_writer;
   public boolean gridSetupDone = false;
   public double cutFactor = 1.0;
   public int equilibPeriod = 0;
   public int equilibPeriodCounter = 0;
   public boolean equilibration = false;

   public static boolean stats_stop = false;
   public static int n_step_stats;
   public static boolean stats = false;
   public int stats_job = -1;
   public int upgradeCharts = -1;
   
   public static long runTime = 0;
   public static double  timeTimeSteps = 0.0;
   public static int  timeTimeStepsCount = 0;

   public double  totNumOfmosquitos = 0.0;
   public int  totNumOfmosquitosCount = 0;

   public ReadNapoRiverLevels readRiver;
   public List<List<Double>> nanayLevels = new ArrayList<List<Double>>();
   HashMap<Calendar, Double> dailyNapoLevel = new HashMap<Calendar, Double>();
   HashMap<Calendar, Double> dailySoilMoisture = new HashMap<Calendar, Double>();
   public double  maxNapoLevel  = 0.0;
   public double  minNapoLevel  = 0.0;
   public double  oldPMaxLevel  = 0.0;
   public double  deltaLevel    = 0.0;
   public double  oldRiverLevel = 0.0;
   public int     numRiverMacrolevels = 0;

   public ReadMalariaPrevalence readMalaria;
   public List<List<Double>> malariaPrevalence = new ArrayList<List<Double>>();
   HashMap<Calendar, Double> dailyMalariaPrevalence = new HashMap<Calendar, Double>();
   public double  maxMalariaPrevalence = 0.0;
   public double  minMalariaPrevalence = 0.0;
   public double  oldMalariaPrevalence = 0.0;

   //-------------------------------------------------
   public static int num_step;
   public static int num_job = 1;
   public static boolean withDates = false;

   public WriteXlsOutput writeXls;
   public List<Object[]> weeklyData = new ArrayList<Object[]>();
   public int weeklyDataCounter = 1;

   public double weeklyMalariaCases = 0.0;
   public double weeklyMalariaCasesFalciparum = 0.0;
   public double weeklyMalariaCasesVivax = 0.0;

   public double monthlyMalariaCases = 0.0;
   public double monthlyMalariaCasesFalciparum = 0.0;
   public double monthlyMalariaCasesVivax = 0.0;

   public double HmonthlyIncidence = 0.0;
   public double HmonthlyIncidenceFalciparum = 0.0;
   public double HmonthlyIncidenceVivax = 0.0;

   public double yearlyMalariaCases = 0.0;
   public double yearlyMalariaCasesFalciparum = 0.0;
   public double yearlyMalariaCasesVivax = 0.0;

   public double HyearlyIncidence = 0.0;
   public double HyearlyIncidenceFalciparum = 0.0;
   public double HyearlyIncidenceVivax = 0.0;

   public double weeklyNumHumanInfected = 0.0;
   public int weeklyHourStats = 0;
   public int weeklyDayStats = 0;
   public double weeklyAvgTemp = 0.0;
   public double weeklyNumRainDays = 0.0;
   public double weeklyAvgNumBitesPerHuman = 0.0;
   public double weeklyAvgFractBitesOnAnimals = 0.0;
   public double weeklyAvgFractInfectiousMosquitos = 0.0;
   public double totNumDailyBites = 0.0;
   public double totNumDailyBitesAnimals = 0.0;
   public double weeklyAvgMosquitoLifeSpan = 0.0;
   public double avgDailyFractionBitesOnAnimals = 0.0;
   public int weeklyAvgMosquitoLifeSpanCount = 0;
   public double weeklyAvgSporogonicFalciparum = 0.0;
   public int weeklyAvgSporogonicFalciparumCounter = 0;
   public double weeklyAvgSporogonicVivax = 0.0;
   public int weeklyAvgSporogonicVivaxCounter = 0;
   public double weeklyTimeToGetBlood = 0.0;
   public int weeklyTimeToGetBloodCounter = 0;
   public double weeklyTimeToLayEggs = 0.0;
   public int weeklyTimeToLayEggsCounter = 0;
   public double     weeklyNumFailBite = 0;
   public double     weeklyNumBite = 0;
   public double     weeklyNumBiteAnimal = 0;
   public double     weeklyAvgTimeToSymptoms = 0;
   public int     weeklyAvgTimeToSymptomsCounter = 0;

   public int mosquitoGridWidth  = 60;
   public int mosquitoGridHeight = 60;

   public int geoGridWidth  = 60;
   public int geoGridHeight = 60;

   public SparseGrid2D mosquitoGrid  = new SparseGrid2D(mosquitoGridWidth, mosquitoGridHeight);
   public SparseGrid2D humanGrid  = new SparseGrid2D(geoGridWidth, geoGridHeight);
   public SparseGrid2D householdGrid = new SparseGrid2D(geoGridWidth, geoGridHeight);
   public ObjectGrid2D geoGrid     = new ObjectGrid2D(geoGridWidth, geoGridHeight);
   public ObjectGrid2D elevationGrid = new ObjectGrid2D(geoGridWidth, geoGridHeight);
   public ObjectGrid2D pixelMosquitosGrid     = new ObjectGrid2D(geoGridWidth, geoGridHeight);
   public SparseGrid2D socialPlacesGrid = new SparseGrid2D(geoGridWidth, geoGridHeight);
   public SparseGrid2D farmGrid = new SparseGrid2D(geoGridWidth, geoGridHeight);

   public List<MeteoData> meteoData = new ArrayList<MeteoData>();
   public List<MeteoData> meteoDataComplete = new ArrayList<MeteoData>();

   public List<MalariaData> malariaData = new ArrayList<MalariaData>();

   public Bag mosquito_bag  = new Bag();
   public Bag household_bag = new Bag();
   public Bag human_bag     = new Bag();

   public MosquitosGenerator mosGen;
   public HouseholdsGenerator hhGen;
   public HumansGenerator hGen;
   public InitializeLayers initLayer;
   public InitializePixelMosquitosGrid initPixelMosquitosGrid;
   public Utils utils;
   public BicubicInterpolator bcubucInterpolator;
   public BilinearInterpolator blinearInterpolator;

   public Calendar startDate = null; 
   public Calendar endDate   = null; 

   public String resSheet   = "results"; 

   public int analysisShp = 0;
   public int doWriteShp = 0;
   public int analysisMosHotSpots = 0;

   //Geo Layes variables ----------------------------
   //public double numMosPerRiverBankMeter = 0;
   public double maxNumbAdultperArea = 0;
   public double numMosHighLowWaterRatio = 0;
   public double propOvopositionEmergentAdults = 0.0;
   public double avgRiverBankLengthPerPixelHigh = 0;
   public double avgRiverBankLengthPerPixelLow = 0;
   public Bag riverBankPixels = new Bag();
   public int numRiverBankPixelsHigh = 0;
   public double maxElevationHighWater = 0;
   public double minElevationHighWater = 0;
   public Bag waterChangingCps = new Bag();

   public Bag LDASCps = new Bag();

   public List<String> places = new ArrayList<String>();
   public List<String> healthPost = new ArrayList<String>();

   //Plasmodia variables -----------------------------
   public Plasmodium Falciparum;
   public Plasmodium Vivax;
   public Plasmodium bothPlasmodia;

   //Mosquitos variables ------------------------------
   public int numExistedMosquitos = 1;
   public int numSporogonicMosquitosFalciparum = 1;
   public int numSporogonicMosquitosVivax = 1;
   public int eggDevelopmentTime = 0;
   public double totalSporogonicFalciparum  = 0;
   public double totalSporogonicVivax       = 0;
   public int totalLifespan     = 0;
   public double probFailBite   = 0;
   public double randomWalkWeight   = 0;
   public double totalNumBite     = 0;
   public double totalNumBiteAnimal     = 0;
   public double avgNumDailyBitePerHuman     = 0;
   public int numDays     = 0;
   public double bitingHours[] = new double[24];
   public double monthlyMalariaIncidence[] = new double[12];
   public double malariaMonthlyCases = 0.0;
   public List<Double> malariaIncidenceSerie = new ArrayList<Double>();
   public int month = 0;
   public double aquaticStageTime = 0.0;
   public double probBloodMealperPixel = 0.0;
   public double probBloodMealperPixelAroundHumans = 0.0;

   public List<List<CoverPixel>> mosTrajs = new ArrayList<List<CoverPixel>>();

   //public int startingNumMosquitos = 0;
   public int restingTimeAfterBite = 0;
   public int restingTimeAfterLayEggs = 0;
   public int bitingPeak = 0;
   public int bitingPeakDev = 0;
   public List<Double> matingProbDist = new ArrayList<Double>();
   public double mosquitoDailySurvivalProbabilityRain = 0.0;
   public double mosquitoDailySurvivalProbabilityNoRain = 0.0;
   public double minRainWetDay = 0.0;
   public double rainSurvFact  = 0.0;
   public double timeToGetBlood = 0.0;
   public double timeToLayEggs = 0.0;
   public int numTimeToGetBlood = 0;
   public int numTimeToLayEggs = 0;

   //Humans    variables ------------------------------
   public double cAgeDepForceOfInfection = 0.0;
   public double kAgeDepForceOfInfection = 0.0;
   public double fractHumanTreated = 0.0;
   public double fractHumanAsymptomatic = 0.0;
   public double fractHumanAsymptomaticV = 0.0;
   public double fractHumanAsymptomaticF = 0.0;
   public int    asymptoSuper = 0;
   public double probFailMalariaProtection = 0;
   public double HIncidence = 0;
   public double HIncidenceVivax = 0;
   public double HIncidenceFalciparum = 0;
   public int    malariaProtection = 0;
   public int    hIdent = 0;
   public int    houseIdent = 0;
   public double protectionActivationTreshold = 0;
   public double fractPeopleAddProtection = 0;
   public double probMosquitoDieBitingProtectedPerson = 0.0;
   public double protectionPeriodFact = 0;
   public double setProtOff = 0.0;
   public double riverPatch = 0.0;
   public double minRiver = 0.0;
   public List<Double> avgPeoplePerHousehold = new ArrayList<Double>();
   public Bag ageSegments = new Bag();
   public Bag activities = new Bag();
   public Bag farmPlaces = new Bag();
   static int numHoursHumanProtectedHighRisk = 0;
   static int numHumanProtectedHighRisk = 0;

   //To mix and move people --------------------------------
   static int humanLocalMovement = 0;
   static int humanMixingType = 0;
   static int highRiskWhat =  0;
   static int numExcludedHotspots = 0;
   static int numPeopleProtectedForever = 0;

   static int numHumanForever = 0;

   //Obsolete -----
   static double fractPeopleMoveAround = 1.0;
   //Obsolete -----

   //From input now ------
   public double probBloodMealFromOutdoorHuman = 0.0;

   //Cover pixel to be use when a human agent goes out to work
   public CoverPixel outsideCp = null;

   public Bag dailyActivitiesPlaces = new Bag(); 

   //Dev = 6 central = 14 corresponds to human movement from
   //6h00 to 21h00
   
   //public int humanMovingPeak = 15;
   //public int humanMovingPeakDev = 7;

   //Questo di test
   public int humanMovingPeak = 15;
   public int humanMovingPeakDev = 7;

   //public int humanMovingPeak = 12;
   //public int humanMovingPeakDev = 0;

   public double humanWeightRoad = 0.95;
   public double humanWeightHousehold = 0.3;
   public double humanNoWeight = 0.1;

   public int humanProtectedEverywhere = 0;

   HashMap<String, Double> ageStructure = new HashMap<String, Double>();

   public double   noMosHouseRadiusLow = 0.0;
   public double   noMosHouseRadiusHigh = 0.0;
   public boolean  noMosHouse = false;

   public double vivaxTimeRecurrence = 0.0;
   public double vivaxRiskRecurrence = 0.0;

   public List<Double> weeklyHIncidenceList  = new ArrayList<Double>();
   public List<Double> weeklyHIncidenceListF = new ArrayList<Double>();
   public List<Double> weeklyHIncidenceListV = new ArrayList<Double>();

   public List<Integer> numMosquitosList = new ArrayList<Integer>();

   public double   fractProtectedPeople = 0.0;

   //Geo variables --------------------------------
   //public final int GEO_WIDTH = 100; 
   //public final int GEO_HEIGHT = 100; 
   public GeomVectorField homes = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField low_water = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField high_water = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField study_area = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField elevation = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField rioCutted_10mBuffer = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField socialPlaces = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField highRisk = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField farms = new GeomVectorField(geoGridWidth,geoGridHeight);
   public GeomVectorField roads = new GeomVectorField(geoGridWidth,geoGridHeight);

   public Envelope globalMBR;
   public double mosquitoCellSize = 0;;
   public double geoCellSize = 0;;
   public double globalMBRWidth;

   public double globalMBRHeight;
   public double globalMBRMinX;
   public double globalMBRMinY;

   public double numPixelsCoveredByWater = 0;
   public double numProductivePixels = 0;

   public double maxAdultEmergent = 0;

   public WriteShp writeShp = null;

   //Chart variables ---------------------------------
   public org.jfree.data.xy.XYSeries num_infected_households_series;  

   //Initialization optimization bags
   public Bag clusters = new Bag();
   public Bag cpClusters = new Bag();
   public int nCpPerCluster = 2500;

   public int statsGlobal = 0;
   public int statsGlobal2 = 0;

   public HashMap<Integer, List<Double>> householdsBites = new HashMap<Integer, List<Double>>();
   public HashMap<Integer, Integer> householdsBitesNorm = new HashMap<Integer, Integer>();

   //True is yes periodic boudary conditions
   static Boolean pbcMosquito = false;

   //To start Hybrid densiti calculations
   public Boolean hybridDensity = true;
   public HybridInterface hInterface = null;
   public Bag hybridPixelsToChange = new Bag();
   public Boolean hybridExample = false; //To test hybrid module

   //====================================================
   public   AmaSim(long seed)
   {
      super(seed);
   }

   //====================================================
   public  void start()
   {

      //System.out.println ("start amasim0");
      super.start();
      //System.out.println (global_input_file);
      //System.exit(0);


      if(singleRun)
      {
          if(!calibration)
          {
              global_input_fileCom = "paramsFiles/" + testCommunityName  + "/" + global_input_file;
              outPath = "sim/app/AmaSim/outputs/" + testCommunityName + "/" + outDirCom + "/";
          }
          else if(calibration)
          {
              global_input_fileCom = "paramsFiles/" + testCommunityName + "/calibration/"  + global_input_file;
              outPath = "sim/app/AmaSim/outputs/" + testCommunityName + "/calibration/" + outDirCom + "/";
          }

          System.out.println ("Community name:   " + testCommunityName);
      }
      else
      {
          global_input_fileCom = "paramsFiles/" + community.name + "/" + global_input_file; 

         outPath = "sim/app/AmaSim/outputs/" + outDirCom + "/";

         //System.out.println ("outPath:   " + outPath);
         //System.out.println ("Community name:   " + community.name);
         //System.exit(0);
      }


      if(1 != 1)
      {
          String outFile =  outDir + "simulation.out";
          System.out.println ("Dump output file: " + outFile);
          //System.exit(0);
       
          try{
              File file = new File(outFile);
              FileOutputStream fis = new FileOutputStream(file);
              PrintStream out = new PrintStream(fis);
              System.setOut(out);
              //System.out.println("First Line");
              //System.out.println("Second Line");
          }
          catch(FileNotFoundException e)
          {
              System.out.println(e);
          }
      }

       System.out.println ("======== SimulationStarts ======================");
       //System.exit(0);

/*
      if(!doAnalysis.equals(""))
      {

          //DoAnalysisPixelUniversal daPixelsPixelUniversal = new DoAnalysisPixelUniversal(this);
          //daPixelsPixelUniversal.read("Pixel Human", "pixelHuman");
          //System.exit(0);

          DoAnalysis da = new DoAnalysis(this);
          da.read(this);
          //System.exit(0);

          if(analysisShp == 1)
          {
              DoAnalysisHouseholds dahh = new DoAnalysisHouseholds(this);
              dahh.read(this);

              DoAnalysisHumans dah = new DoAnalysisHumans(this);
              dah.read(this);
              //System.exit(0);

              DoAnalysisPixelsHuman daPixelsHuman = new DoAnalysisPixelsHuman(this);
              daPixelsHuman.read(this);

              DoAnalysisPixelsHumanTime daPixelsHumanTime = new DoAnalysisPixelsHumanTime(this);
              daPixelsHumanTime.read(this);

              //System.exit(0);

              DoAnalysisPixelsMosquito daPixelsMosquito = new DoAnalysisPixelsMosquito(this);
              daPixelsMosquito.read(this);

              //System.exit(0);

              DoAnalysisShp daShp = new DoAnalysisShp(this);
              daShp.read(this);

          }

          System.out.println ("---- Simulation Analysis complete -------");
          System.exit(0);
      }
*/



      //Read the input file -----------------------------
      ReadInput input = new ReadInput(this);
      //System.exit(0);

      analysisShp = input.readInt("analysisShp");
      doWriteShp = input.readInt("doWriteShp");
      analysisMosHotSpots = input.readInt("analysisMosHotSpots");
      //System.exit(0);

      //System.out.println (global_input_file);
      //System.out.println (global_input_fileCom);
      //System.out.println (doAnalysis);
      //System.exit(0);


      readRiver = new ReadNapoRiverLevels(this);
      readMalaria = new ReadMalariaPrevalence(this);


      num_step = input.readInt("num_step");
      num_job =  Integer.parseInt(input.readIntString("num_job", "integer"));

      humanLocalMovement  = input.readInt("humanLocalMovement");
      humanMixingType  = input.readInt("humanMixingType");

      numExcludedHotspots  = input.readInt("numExcludedHotspots");

      highRiskWhat = input.readInt("highRiskWhat");

      numPeopleProtectedForever = input.readInt("numPeopleProtectedForever");




      probBloodMealFromOutdoorHuman = input.readDouble("probBloodMealFromOutdoorHuman");

      equilibPeriod = input.readInt("equilibPeriod") * 24;
      if(equilibPeriod != 0)equilibration = true;
      //System.out.println (equilibPeriod);
      //System.exit(0);

      weeklyData.add(new Object[] {
          "Date", 

          //Humans Statisitcs -----------
          "Observed Malaria Prevalence",

          "Malaria Prevalence (%)",

          //"Approx. Weekly Malaria Incidence", 
          //"Approx. weekly Vivax Malaria Incidence", 
          //"Approx. weekly Falciparum Malaria Incidence", 

          "Monthly Malaria Incidence", 
          "Monthly Vivax Malaria Incidence", 
          "Monthly Falciparum Malaria Incidence", 

          "Yearly Malaria Incidence", 
          "Yearly Vivax Malaria Incidence", 
          "Yearly Falciparum Malaria Incidence", 

          "Num of humans infected this week", 
          "Average Number of Bite per human per day", 
          "Average Fraction of bites on animals per day", 
          "Fract of susceptibles humans", 
          "Fract of infectious humans",
          "Fract of humans infected by P. Falciparum",
          "Fract of humans infected by P. Vivax",
          "Avg weekly time to symptoms",
          "Fract Human taking precautin against malaria",
          "Num of Households infected",
          "Fract households that registered at least one malaria case",
          "Fract humans that got at least one malaria case",
          "Fraction of human infection blocking for hotspots",
          "Tot num of humans",


          //Mosquitos Statisitcs -----------
          "Number of mosquitos in this time step", 
          "Average number of mosquitos", 
          "Avg Fract of infectiuos mosquitoes", 
          "Avg Lifespan Mosquitos", 
          "Average Falciparum sporogonic cycle duration",
          "Average Vivax sporogonic cycle duration", 
          "Average gonotrophic cycle (days)", 
          "Average time to get blood (days)", 
          "Average time to lay eggs (days)", 
          "Fract weekly fail bites",

          //Environmental Statistics--------
          "Napo River Level", 
          "Average Temperature", 
          "Average daily Rain fall (mm/m2)", 
          "Number pixels covered by water",
          "Number pixels producing adult mosquitos",
          "Max number emergent adults per pixel"
          //"Density of mosquitos generated per High Water area"
          //"Old mosquitos fract"
      });

      if(input.exists("startDate") && input.exists("endDate"))
      {
         startDate =  input.readCalendar("startDate");
         //startDate = startDate.replace("-", "");
         endDate   =  input.readCalendar("endDate");
         //endDate   = endDate.replace("-", "");
         withDates = true;
      }

      //System.out.println (endDate.getTime());
      //System.exit(0);
      //System.out.println (startDate);

      //Geo Layes variables ----------------------------
      maxNumbAdultperArea = input.readDouble("maxNumbAdultperArea");
      numMosHighLowWaterRatio = input.readDouble("numMosHighLowWaterRatio");
      propOvopositionEmergentAdults = input.readDouble("propOvopositionEmergentAdults");
      mosquitoCellSize = input.readDouble("mosquitoCellSize");
      geoCellSize = input.readDouble("geoCellSize");

      //Mosquitos parameters ------------------------
      //startingNumMosquitos = input.readInt("startingNumMosquitos");
      eggDevelopmentTime = input.readInt("eggDevelpmentTime");
      restingTimeAfterBite = input.readInt("restingTimeAfterBite");
      restingTimeAfterLayEggs = input.readInt("restingTimeAfterLayEggs");
      bitingPeak = input.readInt("bitingPeak");
      bitingPeakDev = input.readInt("bitingPeakDev");
      matingProbDist = input.readListDouble("matingProbDist");
      input.checkDist(matingProbDist, "matingProbDist");
      mosquitoDailySurvivalProbabilityRain = input.readDouble("mosquitoDailySurvivalProbabilityRain");
      mosquitoDailySurvivalProbabilityNoRain = input.readDouble("mosquitoDailySurvivalProbabilityNoRain");
      minRainWetDay = input.readDouble("minRainWetDay");
      rainSurvFact = input.readDouble("rainSurvFact");
      probFailBite = input.readDouble("probFailBite");
      probBloodMealperPixel = input.readDouble("probBloodMealperPixel");
      probBloodMealperPixelAroundHumans = input.readDouble("probBloodMealperPixelAroundHumans");
      randomWalkWeight = input.readDouble("randomWalkWeight");
      //bitingHours[0] = new Double[23];
      for(int i = 0; i < 24; i++){bitingHours[i] = 0.0;}
      for(int i = 0; i < 12; i++){monthlyMalariaIncidence[i] = 0.0;}
      timeToGetBlood = 0.0;
      timeToLayEggs = 0.0;
      aquaticStageTime = input.readDouble("aquaticStageTime") * 24;


      //Human     parameters ------------------------
      cAgeDepForceOfInfection = input.readDouble("cAgeDepForceOfInfection");
      kAgeDepForceOfInfection = input.readDouble("kAgeDepForceOfInfection");
      fractHumanTreated = input.readDouble("fractHumanTreated");
      fractHumanAsymptomatic = input.readDouble("fractHumanAsymptomatic");
      fractHumanAsymptomaticV = input.readDouble("fractHumanAsymptomaticV");
      fractHumanAsymptomaticF = input.readDouble("fractHumanAsymptomaticF");
      numRiverMacrolevels = input.readInt("numRiverMacrolevels");
      probFailMalariaProtection = input.readDouble("probFailMalariaProtection");
      malariaProtection = input.readInt("malariaProtection") * 24;
      protectionPeriodFact = input.readDouble("protectionPeriodFact");
      setProtOff = input.readDouble("setProtOff");
      riverPatch = input.readDouble("riverPatch");
      minRiver = input.readDouble("minRiver");
      avgPeoplePerHousehold = input.readListDouble("avgPeoplePerHousehold");
      fractProtectedPeople = input.readDouble("fractProtectedPeople");
      probMosquitoDieBitingProtectedPerson = input.readDouble("probMosquitoDieBitingProtectedPerson");
      //System.exit(0);
      
      humanProtectedEverywhere = input.readInt("humanProtectedEverywhere");

      noMosHouseRadiusLow = input.readDouble("noMosHouseRadiusLow");
      noMosHouseRadiusHigh = input.readDouble("noMosHouseRadiusHigh");
      if(noMosHouseRadiusLow != 0.0  && noMosHouseRadiusHigh != 0.0) noMosHouse = true;

      vivaxTimeRecurrence = input.readDouble("vivaxTimeRecurrence") * 24;
      vivaxRiskRecurrence = input.readDouble("vivaxRiskRecurrence");

      fractPeopleAddProtection  = input.readDouble("fractPeopleAddprotection");
      protectionActivationTreshold = input.readDouble("protectionActivationTreshold");
      System.out.println ("protectionActivationThreshols: " + protectionActivationTreshold);
      //System.exit(0);

      //Read the names of inhabitated places included in the simulation
      //area and the names of the health posts closests to the 
      //simulation area
      places = input.readPlaces("places");
      healthPost = input.readPlaces("healthPost");
      //System.exit(0);

      //Stats parameters -----------------------------
      n_step_stats =  Integer.parseInt(input.readIntString("stats", "integer"));
      String sstop =  input.readIntString("stats", "string");
      upgradeCharts = input.readInt("upgradeCharts");
      if(n_step_stats  != 0)
      {
          stats = true;
      }
      if(sstop.equals("stop"))
      {
          stats_stop = true;
      }


      //Setup the outputs ------------------------
      stats_writer = new WriteStats(this);
      writeXls = new WriteXlsOutput(this);
      if(doAnalysis.equals(""))writeXls.writeInput(this);
      writeShp = new WriteShp(this);
      //System.exit(0);

      //start all the simulation routines---------
      startAll startAll = new startAll(this);
      //Start repeat at 1.0 with ordering 10
      //Global schedule ordering (low order first)
      //0 startAll
      //1 environment
      //2humans
      //3mosquitoes
      //....
      //9 writeStats
      //10 statistics
      this.schedule.scheduleRepeating(1.0, 0, startAll);

      if(!doAnalysis.equals(""))
      {
          DoAnalysis da = new DoAnalysis(this);
          da.read(this);
          //System.exit(0);

          if(analysisShp == 1)
          {

              DoAnalysisHouseholds dahh = new DoAnalysisHouseholds(this);
              dahh.read(this);

              DoAnalysisHumans dah = new DoAnalysisHumans(this);
              dah.read(this);
              //System.exit(0);

              DoAnalysisPixelUniversal daPixelsPixelUniversal = new DoAnalysisPixelUniversal(this);
              daPixelsPixelUniversal.read("Pixel Human", "pixelHuman");
              //System.exit(0);

              DoAnalysisPixelsHumanTime daPixelsHumanTime = new DoAnalysisPixelsHumanTime(this);
              daPixelsHumanTime.read(this);

              //System.exit(0);

              //Also for Mosquitoes in principle the DoAnalysisPixelUniversal
              //class can be used. But it is not tested.
              DoAnalysisPixelsMosquito daPixelsMosquito = new DoAnalysisPixelsMosquito(this);
              daPixelsMosquito.read(this);

              //System.exit(0);

              DoAnalysisShp daShp = new DoAnalysisShp(this);
              daShp.read(this);


          }

          System.out.println ("---- Simulation Analysis complete -------");
          System.exit(0);
      }



      //double startTime = 0.0;
      //schedule.scheduleOnce(startTime, startAll);

      //-Series-----------------------------------
      num_infected_households_series = new org.jfree.data.xy.XYSeries("Number of Infected Households", false);
      //System.exit(0);
 


   }

   //====================================================
   public void finish()
   {

   }

   //====================================================
   public static void main(String[] args)
   {

       singleRun = true;

       if(args.length < 4)
       {
           System.out.println ("The arguments in input line must to be > 3 in number");
           System.exit(0);
       }

       global_input_file = args[0];
       outDir = args[1];
       testCommunityName = args[2];
       if(args[3].equals("calibration"))calibration = true;

       if(args.length > 4)
       {
           if(args[4].equals("doAnalysis"))doAnalysis = args[4];
       }
       //System.out.println (global_input_file);
       //System.out.println (doAnalysis);
       //System.exit(0);

       SimState state = new AmaSim(System.currentTimeMillis());
       //state.start();

       AmaSim sim = (AmaSim)state;
       
       sim.outDirCom = outDir;


       // MyModel is our SimState subclass
       for(int job = 0; job < num_job; job++)
       {
           state.start();
           do
           {
               long t1 = System.currentTimeMillis();
               //print_output(state);
               //System.out.println (state.schedule.getTime());
               //System.out.println (state.schedule.time());
               //System.out.println (job);

               if (!state.schedule.step(state)) break;
               //Write Stats and Stop if required----------
               if(stats_stop & ((state.schedule.getTime() + 1 ) % (n_step_stats + 1) == 0))
               {
                   //System.out.println (state.schedule.getTime());
                   //System.out.println (n_step_stats);
                   System.out.println("Steps at Stop for Stats: " + state.schedule.getTime() + " Time: " + state.schedule.time());
                   break;
               }

               //Checkpoint waiting for jts serialization
               //if (state.schedule.getTime() % 8640 == 0)
               //if(state.writeToCheckpoint(new File("out." + state.schedule.getTime() + ".checkpoint")) == null)
               //{
               //   System.out.println ("Null----");
               //   System.exit(0);
               //
               //}

               long t2 = System.currentTimeMillis();
               runTime = t2 - t1;
               timeTimeSteps = timeTimeSteps + runTime;
               timeTimeStepsCount++; 


           }
           while(state.schedule.getTime() < num_step - 1);

           int jobs = job + 1;
           System.out.println ("----------------------------------------");
           System.out.println ("----------------------------------------");
           System.out.println ("Terminated Job number: " + jobs);
           System.out.println ("----------------------------------------");


           //Close the Job ----------------------------
           sim.stats_job++;
       }
       System.out.println ("===============================================");
       System.out.println ("===============================================");
       System.out.println ("Simulation Ends afer " + num_job + " jobs");
       System.out.println ("Num steps of every Job: " + num_step);
       System.out.println ("===============================================");
       System.exit(0);
   }



}

