/*
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import com.vividsolutions.jts.geom.Geometry;

import sim.engine.*;
import sim.display.*;
import sim.portrayal.grid.*;
import sim.portrayal.simple.*;
import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;


import java.io.*;
import java.util.*;
import java.util.ArrayList;

import sim.portrayal.network.*;
import sim.util.media.chart.*;
import sim.util.*;

import sim.portrayal.DrawInfo2D;

import sim.portrayal.geo.GeomVectorFieldPortrayal;
import sim.portrayal.geo.GeomPortrayal;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.portrayal.simple.RectanglePortrayal2D;

import sim.util.geo.MasonGeometry;
import sim.util.geo.GeometryUtilities;

public class AmaSimWithUI extends GUIState
{
   private static final long serialVersionUID = 1L;
   public Display2D display;
   public JFrame displayFrame;

   public static String gInput = "";
   public static String oDir = "";
   public static String doA = "";
   public static String tComm = "";

   public static  Boolean cal = false;
   public static  Boolean srun = false;


   //Geo Portrayals
   public GeomVectorFieldPortrayal homesPortrayal = new GeomVectorFieldPortrayal(true);
   public GeomVectorFieldPortrayal low_waterPortrayal = new GeomVectorFieldPortrayal(true);
   public GeomVectorFieldPortrayal high_waterPortrayal = new GeomVectorFieldPortrayal(true);
   public GeomVectorFieldPortrayal rioCutted_10mBufferPortrayal = new GeomVectorFieldPortrayal(true);


   SparseGridPortrayal2D householdPortrayal  = new SparseGridPortrayal2D();
   SparseGridPortrayal2D farmPortrayal  = new SparseGridPortrayal2D();
   SparseGridPortrayal2D socialPlacesPortrayal  = new SparseGridPortrayal2D();
   SparseGridPortrayal2D mosquitoPortrayal  = new SparseGridPortrayal2D();
   SparseGridPortrayal2D humanPortrayal  = new SparseGridPortrayal2D();
   ObjectGridPortrayal2D geoGridPortrayal = new ObjectGridPortrayal2D();
   ObjectGridPortrayal2D mosquitoGridPortrayal = new ObjectGridPortrayal2D();
   ObjectGridPortrayal2D elevationPortrayal = new ObjectGridPortrayal2D();
//
    public AmaSimWithUI() { super(new AmaSim(System.currentTimeMillis())); }
    public AmaSimWithUI(SimState state) { super(state); }

    // allow the user to inspect the model
    public Object getSimulationInspectedObject() { return state; }  // non-volatile

    public static String getName() { return "AmaSim"; }

    AmaSim sim;

   //====================================================
    public void init(Controller controller)
    {
        super.init(controller);
        sim = (AmaSim)state;

        display = new Display2D(904,428,this); // at 400x400, we've got 4x4 per array position

        displayFrame = display.createFrame();
        controller.registerFrame(displayFrame);
        displayFrame.setVisible(true);
        display.setScale(0.95);

        //display.attach(homesPortrayal, "Homes");
        //display.attach(low_waterPortrayal, "Water");
        //display.attach(high_waterPortrayal, "Water");
        //display.attach(mosquitoPortrayal,"Mosquitos");
        //display.attach(humanPortrayal,"Human");


       //-To Chart-----------------------------------------


    }
 
 
   //====================================================
    public void setupPortrayals()
    {
        AmaSim sim = (AmaSim)state;

        homesPortrayal.setField(sim.homes);
        //homesLabelPortrayal b = new homesLabelPortrayal(new GeomPortrayal(Color.DARK_GRAY,true), Color.BLUE);
        //homesPortrayal.setPortrayalForAll(new OvalPortrayal2D(Color.BLUE, 3.0));
        homesPortrayal.setPortrayalForAll(new DrawHomesPortrayal() );
        //homesPortrayal.setPortrayalForAll(new GeomPortrayal(Color.CYAN,true));

        low_waterPortrayal.setField(sim.low_water);
        low_waterPortrayal.setPortrayalForAll(new GeomPortrayal(Color.CYAN,true));

        rioCutted_10mBufferPortrayal.setField(sim.rioCutted_10mBuffer);
        rioCutted_10mBufferPortrayal.setPortrayalForAll(new GeomPortrayal(Color.BLUE,true));

        high_waterPortrayal.setField(sim.high_water);
        high_waterPortrayal.setPortrayalForAll(new GeomPortrayal(Color.BLUE,true));
        //high_waterPortrayal.setPortrayalForAll(new HWPortrayal() );


        elevationPortrayal.setField(sim.elevationGrid);
        elevationPortrayal.setPortrayalForAll(new DrawHomesPortrayal() );

        //Mosquitos ---------------------------------------------
        mosquitoPortrayal.setField((sim.mosquitoGrid));
        DrawMosquitosPortrayal dmp = new DrawMosquitosPortrayal();
        mosquitoPortrayal.setPortrayalForAll( dmp );

        //Human  ------------------------------------------------
        humanPortrayal.setField((sim.humanGrid));
        DrawHumanPortrayal dmpH = new DrawHumanPortrayal();
        humanPortrayal.setPortrayalForAll( dmpH );

        //Households---------------------------------------------
        householdPortrayal.setField((sim.householdGrid));
        householdPortrayal.setPortrayalForAll( new DrawHouseholdsPortrayal() );

        //Farm      ---------------------------------------------
        farmPortrayal.setField((sim.farmGrid));
        farmPortrayal.setPortrayalForAll( new DrawFarmPortrayal() );

        //social    ---------------------------------------------
        //socialPlacesPortrayal.setField((sim.socialPlacesGrid));
        //socialPlacesPortrayal.setPortrayalForAll( new DrawSocialPlacesPortrayal() );

        //Cover Layers ------------------------------------------
        //sim.readLayer = new InitializeLayers(state);
        //sim.readLayer.initAll(state);

        geoGridPortrayal.setField(sim.geoGrid);
        geoGridPortrayal.setPortrayalForAll( new DrawGeoLayersPortrayal() );

        mosquitoGridPortrayal.setField(sim.pixelMosquitosGrid);
        mosquitoGridPortrayal.setPortrayalForAll( new DrawMosquitoLayersPortrayal() );

        display.reset();
        display.setBackdrop(Color.WHITE);
        display.repaint();
    }

    
   //====================================================
    public void start()
    {

        //System.out.println ("StarttttttttUIIIIIIIIIIIIIIII");
        final AmaSim sim = (AmaSim)state;

        sim.global_input_file = gInput;
        sim.outDir = oDir;
        sim.doAnalysis = doA;

        sim.testCommunityName = tComm;

        sim.singleRun = srun;
        sim.calibration = cal;

        super.start();  // set up everything but replacing the display
        // set up our portrayals
        setupPortrayals();

        //display.attach(homesPortrayal, "Homes", -0.2, -0.9, true);
        //display.attach(waterPortrayal, "Water", -0.2, -0.9, true);

        //display.attach(high_waterPortrayal, "high Water", 0, 0, true);
        //display.attach(elevationPortrayal, "elevation", 0, 0, true);
        
        display.attach(homesPortrayal, "Homes", 0, 0, true);
        //display.attach(low_waterPortrayal, "low Water", 0, 0, true);
        display.attach(geoGridPortrayal,"GeoLayers");
        display.attach(mosquitoGridPortrayal,"mosquitoLayer");
        //display.attach(rioCutted_10mBufferPortrayal, "Rio Cutted", 0, 0, true);

        display.attach(socialPlacesPortrayal,"Social Places");
        display.attach(farmPortrayal,"Farms");
        display.attach(householdPortrayal,"Households");
        display.attach(mosquitoPortrayal,"Mosquitos");
        display.attach(humanPortrayal,"Human");

        //final AmaSim sim = (AmaSim)state;

/*
        //---------------------------------------------------
        scheduleImmediateRepeat(true, new Steppable()
           {
              public void step(SimState state)
                 {

                    final AmaSim sim = (AmaSim)state;
                                                                            
                    int x = (int)state.schedule.time(); 
                                                 
                    // now add the data
                    if (x >= state.schedule.EPOCH && x < state.schedule.AFTER_SIMULATION & (x % 168 == 0)){

                    }
                  }
            });

        //To chart ================================
        //--------------------------------------------
*/

    }
            
   //====================================================
    public void load(SimState state)
    {
        super.load(state);
        // we now have new grids.  Set up the portrayals to reflect that
        setupPortrayals();
    }

       
   //====================================================
    public void quit()
    {
        super.quit();
        
        // disposing the displayFrame automatically calls quit() on the display,
        // so we don't need to do so ourselves here.
        if (displayFrame!=null) displayFrame.dispose();
        displayFrame = null;  // let gc
        display = null;       // let gc
    }



    //====================================================
    class DrawHomesPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
               if(object == null){return;}
               color = new Color(0, 0, 0);
               //System.out.println ("Homes draw");

               Rectangle2D.Double draw = info.draw;
               scale = 0.8;
               final double width = draw.width * scale;
               final double height = draw.height * scale;
   
               final int x = (int) (draw.x - width / 2.0);
               final int y = (int) (draw.y - height / 2.0);
               //System.out.println (x + " " + y);
               //
               final int w = (int) (width);
               final int h = (int) (height);
   
               graphics.setColor(color);
   
               graphics.fillRect(x, y, w, h);

               //System.exit(0);
        }
    }

   //====================================================
    public static void main(String[] args)
    {
       new AmaSimWithUI().createController();

       srun = true;

       if(args.length < 4)
       {
           System.out.println ("The arguments in input line must to be > 3 in number");
           System.exit(0);
       }

       gInput = args[0];
       oDir  = args[1];
       tComm = args[2];
       if(args[3].equals("calibration"))cal = true;

       if(args.length > 4)
       {
          if(args[4].equals("doAnalysis"))doA = args[4];
       }

       AmaSimWithUI simWithUI = new AmaSimWithUI();
       //Console console = new Console(simWithUI);
       //console.setVisible(true);
       //System.out.println ("MainUI");
    }


    //====================================================
    class DrawMosquitoLayersPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            CoverPixel pixel = (CoverPixel)object;

            if(pixel == null)
            {
                //System.out.println ("pixel null");
                return;
            }
            //else System.out.println ("pixel not null");

            double diff = pixel.nMosquitos/(sim.mosquitoCellSize * sim.mosquitoCellSize);
            diff = diff - sim.hInterface.mosquitoesBlood[pixel.xcor * sim.mosquitoGridHeight + pixel.ycor];
            diff = diff/pixel.nMosquitos/(sim.mosquitoCellSize * sim.mosquitoCellSize);
            diff = diff * diff;
            diff = Math.sqrt(diff);


            scale = 1.0;
            //scale = 0.80;


            //System.out.println (elev);
            scale = 1.0;
            int num = (int)Math.round(diff * 250);
            if(num < 0)num = 0;
            if(num > 250)num = 250;
            color = new Color(num, num, num);
            //color = new Color(0, 0, 0);
            //color = new Color(255, 255, 255);

            Rectangle2D.Double draw = info.draw;
            final double width = draw.width * scale;
            final double height = draw.height * scale;

            final int w = (int) (width/1.0);
            final int h = (int) (height/1.0);

            final int x = (int) (draw.x - w / 1.0);
            final int y = (int) (draw.y - h / 1.0);

            graphics.setColor(color);

            graphics.fillRect(x, y, w, h);


        }


    }

    //====================================================
    class DrawHumanPortrayal extends OvalPortrayal2D
    {
        Color  color;

        public DrawHumanPortrayal()
        {
        
        }

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            Human h = (Human)object;

            double infected = h.getInfected();
            boolean infectious = h.getInfectious();

            //if(h.getIsProtected())
            //{
            //   paint = new Color(250, 0, 0);
            //}
            //else if(!h.getIsProtected())
            //{
            //   paint = new Color(250, 0, 0);
            //}

            if(h.getIsProtected())
            {
               paint = new Color(0, 250, 0);
            }
            else if(!h.getIsProtected())
            {
               paint = new Color(0, 250, 0);
            }



            /*
            if(h.job == null)
            {
               paint = new Color(100, 100, 100);
            }
            else
            {
                if(h.job.name.equals("farmer"))paint = new Color(0, 204, 0);
                else if(h.job.name.equals("other"))paint = new Color(102, 51, 0);
                else if(h.job.name.equals("other manual"))paint = new Color(204, 102, 0);
                else if(h.job.name.equals("shop keeper"))paint = new Color(255, 255, 0);
                else if(h.job.name.equals("office"))paint = new Color(255, 0, 0);
                else if(h.job.name.equals("fisherman"))paint = new Color(255, 0, 255);
                else if(h.job.name.equals("miner"))paint = new Color(255, 0, 255);
                else if(h.job.name.equals("transport"))paint = new Color(255, 0, 255);
                else if(h.job.name.equals("woodcutter"))paint = new Color(255, 0, 255);
            
            }
*/


            //if(h.getInfected() > 0)
            //{
            //   paint = new Color(255, 100, 0);
            //}
            //else
            //{
            //   paint = new Color(255, 255, 0);
            //}
            scale = 2.0;
            super.draw(object, graphics, info);


        }

    }

    //====================================================
    class DrawFarmPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            //System.out.println ("Hoseholsss");
            Place p  = (Place)object;

            int xcor = p.cp.getXcor();
            int ycor = p.cp.getYcor();

            color = new Color(0, 0, 0);

            scale = 8.0;

            Rectangle2D.Double draw = info.draw;
            final double width = draw.width * scale;
            final double height = draw.height * scale;

            final int w = (int) (width/1.0);
            final int h = (int) (height/1.0);

            final int x = (int) (draw.x - w / 1.0);
            final int y = (int) (draw.y - h / 1.0);

            graphics.setColor(color);

            graphics.fillRect(x, y, w, h);

        }
    }

 
    //====================================================
    class DrawSocialPlacesPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            //System.out.println ("Hoseholsss");
            Place p  = (Place)object;

            int xcor = p.cp.getXcor();
            int ycor = p.cp.getYcor();

            color = null;
            if(p.type.equals("Church"))color = new Color(255, 255, 255);
            else if(p.type.equals("Health post"))color = new Color(255, 0, 0);
            else if(p.type.equals("School"))color = new Color(102, 204, 0);
            else if(p.type.equals("Leisure Place"))color = new Color(153, 153, 255);
            else if(p.type.equals("Sport Field"))color = new Color(0, 153, 76);

            scale = 8.0;

            Rectangle2D.Double draw = info.draw;
            final double width = draw.width * scale;
            final double height = draw.height * scale;

            final int w = (int) (width/1.0);
            final int h = (int) (height/1.0);

            final int x = (int) (draw.x - w / 1.0);
            final int y = (int) (draw.y - h / 1.0);

            graphics.setColor(color);

            graphics.fillRect(x, y, w, h);

        }
    }



    //====================================================
    class DrawHouseholdsPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;


        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            //System.out.println ("Hoseholsss");
            Household hh  = (Household)object;

            double incidence = 0;

            if(hh.getWeeksCounter() != 0)
            {
               incidence = hh.getAvgMalariaIncidence() / hh.getWeeksCounter();
            }
            if(incidence <= 0)incidence = 0;
            if(incidence >= 255)incidence = 255;
            if(incidence > 0.00001)incidence = 1.0;
            //System.out.println ("Hoseholsss incidence : " + incidence);

            int xcor = hh.getXpos();
            int ycor = hh.getYpos();

            int col = (int)(255 * incidence);

            //System.out.println ("col " + col);

            //color = new Color(col, 0, 0);
            color = new Color(51, 153, 0);

            scale = 3.0;

            Rectangle2D.Double draw = info.draw;
            final double width = draw.width * scale;
            final double height = draw.height * scale;

            final int w = (int) (width/1.0);
            final int h = (int) (height/1.0);

            final int x = (int) (draw.x - w / 1.0);
            final int y = (int) (draw.y - h / 1.0);

            graphics.setColor(color);

            graphics.fillRect(x, y, w, h);

        }
    }


    //====================================================
    class DrawMosquitosPortrayal extends OvalPortrayal2D
    {
        Color  color;

        public DrawMosquitosPortrayal()
        {
        
        }

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            Mosquito mos = (Mosquito)object;

            //if(mos.getNoNulliparous())
            if(mos.getInfected() > 0)
            {
               paint = new Color(255, 0, 0);
            }
            else
            {
               paint = new Color(0, 0, 0);
            }
            scale = 0.7;
            super.draw(object, graphics, info);


        }

    }



    //====================================================
    class DrawGeoLayersPortrayal extends RectanglePortrayal2D
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {
            CoverPixel pixel = (CoverPixel)object;

            int isWater = pixel.getIsWater();
            Boolean isRiverBank = pixel.getIsRiverBank(state);

            int xcor = pixel.getXcor();
            int ycor = pixel.getYcor();

            scale = 1.0;
            //scale = 0.80;


            //boolean belev = true;

            //if(pixel.getIsWaterLow() == 1)
            if(pixel.getIsWaterHigh() == 1)
            {               
               //System.out.println (elev);
               scale = 1.0;
               color = new Color(0, 0, 250);
               //color = new Color(0, 0, 0);
               //color = new Color(255, 255, 255);

               Rectangle2D.Double draw = info.draw;
               final double width = draw.width * scale;
               final double height = draw.height * scale;
   
               final int w = (int) (width/1.0);
               final int h = (int) (height/1.0);
   
               final int x = (int) (draw.x - w / 1.0);
               final int y = (int) (draw.y - h / 1.0);
   
               graphics.setColor(color);
   
               graphics.fillRect(x, y, w, h);
            }
            if(pixel.getIsWaterLow() == 1)
            {               
               //System.out.println (elev);
               scale = 1.0;
               color = new Color(0, 250, 250);
               //color = new Color(0, 0, 0);
               //color = new Color(255, 255, 255);

               Rectangle2D.Double draw = info.draw;
               final double width = draw.width * scale;
               final double height = draw.height * scale;
   
               final int w = (int) (width/1.0);
               final int h = (int) (height/1.0);
   
               final int x = (int) (draw.x - w / 1.0);
               final int y = (int) (draw.y - h / 1.0);
   
               graphics.setColor(color);
   
               graphics.fillRect(x, y, w, h);
            }

            if(pixel.getIsWater() == 1)
            {               
               //System.out.println (elev);
               scale = 1.0;
               color = new Color(250, 0, 250);
               //color = new Color(0, 0, 0);
               //color = new Color(255, 255, 255);

               Rectangle2D.Double draw = info.draw;
               final double width = draw.width * scale;
               final double height = draw.height * scale;
   
               final int w = (int) (width/1.0);
               final int h = (int) (height/1.0);
   
               final int x = (int) (draw.x - w / 1.0);
               final int y = (int) (draw.y - h / 1.0);
   
               graphics.setColor(color);
   
               graphics.fillRect(x, y, w, h);
            }





            if(1 == 2 && pixel.getIsProductive())
            {               
               //System.out.println (elev);
               scale = 1.0;
               color = new Color(250, 0, 250);
               //color = new Color(0, 0, 0);
               //color = new Color(255, 255, 255);

               Rectangle2D.Double draw = info.draw;
               final double width = draw.width * scale;
               final double height = draw.height * scale;
   
               final int w = (int) (width/1.0);
               final int h = (int) (height/1.0);
   
               final int x = (int) (draw.x - w / 1.0);
               final int y = (int) (draw.y - h / 1.0);
   
               graphics.setColor(color);
   
               graphics.fillRect(x, y, w, h);
            }








        }
    }

    //====================================================
    class HWPortrayal extends GeomPortrayal
    {
        String what;
        Color  color;

        public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
        {

            //System.out.println ("Hoseholsss");
            MasonGeometry mg  = (MasonGeometry)object;
            what = mg.getStringAttribute("CUBIERTA");
            System.out.println (what);

            if(what.equals("AGUA"))
            {
               color = new Color(0, 0 ,255);
   
               graphics.setColor(color);
            }

        }
    }


}

