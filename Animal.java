/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 


//----------------------------------------------------
public class Animal implements Steppable
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;

   //====================================================
   public Stoppable stopper;

   public String species = "";
   public String getSpecies(){return species;};
   public void setSpecies(String y){species = y;};

   int age = 0;
   public Integer getAge(){return age;};
   public void setAge(int y){age = y;};

   //Malaria state variables----------------------------
   public Plasmodium plasmodium = null;
   public Plasmodium getPlasmodium(){return plasmodium;};
   public void setPlasmodium(Plasmodium y){plasmodium = y;};

   //infected < 0 susceptible
   //infected = 0 became infectiuos
   //infected > 0 infected
   public double infected = -100;
   public Double getInfected(){return infected;};
   public void setInfected(double y){infected = y;};

   public boolean infectious = false;
   public Boolean getInfectious(){return infectious;};
   public void setInfectious(boolean y){infectious = y;};

   //====================================================
   public Animal(SimState state, CoverPixel cp)
   {
       sim = (AmaSim)state;

       setAge(0);
   }

   //====================================================
   public void step(SimState state)
   {
       sim = (AmaSim)state;


   }


}


