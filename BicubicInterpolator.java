/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import sim.util.geo.MasonGeometry;


//----------------------------------------------------
public class BicubicInterpolator 
{
   //Doc and sources from : 
   //http://www.paulinternet.nl/?page=bicubic
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;

   private double a00, a01, a02, a03;
   private double a10, a11, a12, a13;
   private double a20, a21, a22, a23;
   private double a30, a31, a32, a33;

   public double x = 0.0;
   public double y = 0.0;

   //====================================================
   public BicubicInterpolator(AmaSim psim)
   {
       sim = psim;
   }


   //====================================================
   public double getValue (double px, double py) 
   {
      x = px;
      y = py;
     
      double[][] p = getP();
      updateCoefficients(p);

      double x2 = x * x;
      double x3 = x2 * x;
      double y2 = y * y;
      double y3 = y2 * y;

      return (a00 + a01 * y + a02 * y2 + a03 * y3) +
          (a10 + a11 * y + a12 * y2 + a13 * y3) * x +
          (a20 + a21 * y + a22 * y2 + a23 * y3) * x2 +
          (a30 + a31 * y + a32 * y2 + a33 * y3) * x3;
   }
   
   //====================================================
   public void updateCoefficients (double[][] p) 
   {
      a00 = p[1][1];
      a01 = -.5*p[1][0] + .5*p[1][2];
      a02 = p[1][0] - 2.5*p[1][1] + 2*p[1][2] - .5*p[1][3];
      a03 = -.5*p[1][0] + 1.5*p[1][1] - 1.5*p[1][2] + .5*p[1][3];
      a10 = -.5*p[0][1] + .5*p[2][1];
      a11 = .25*p[0][0] - .25*p[0][2] - .25*p[2][0] + .25*p[2][2];
      a12 = -.5*p[0][0] + 1.25*p[0][1] - p[0][2] + .25*p[0][3] + .5*p[2][0] - 1.25*p[2][1] + p[2][2] - .25*p[2][3];
      a13 = .25*p[0][0] - .75*p[0][1] + .75*p[0][2] - .25*p[0][3] - .25*p[2][0] + .75*p[2][1] - .75*p[2][2] + .25*p[2][3];
      a20 = p[0][1] - 2.5*p[1][1] + 2*p[2][1] - .5*p[3][1];
      a21 = -.5*p[0][0] + .5*p[0][2] + 1.25*p[1][0] - 1.25*p[1][2] - p[2][0] + p[2][2] + .25*p[3][0] - .25*p[3][2];
      a22 = p[0][0] - 2.5*p[0][1] + 2*p[0][2] - .5*p[0][3] - 2.5*p[1][0] + 6.25*p[1][1] - 5*p[1][2] + 1.25*p[1][3] + 2*p[2][0] - 5*p[2][1] + 4*p[2][2] - p[2][3] - .5*p[3][0] + 1.25*p[3][1] - p[3][2] + .25*p[3][3];
      a23 = -.5*p[0][0] + 1.5*p[0][1] - 1.5*p[0][2] + .5*p[0][3] + 1.25*p[1][0] - 3.75*p[1][1] + 3.75*p[1][2] - 1.25*p[1][3] - p[2][0] + 3*p[2][1] - 3*p[2][2] + p[2][3] + .25*p[3][0] - .75*p[3][1] + .75*p[3][2] - .25*p[3][3];
      a30 = -.5*p[0][1] + 1.5*p[1][1] - 1.5*p[2][1] + .5*p[3][1];
      a31 = .25*p[0][0] - .25*p[0][2] - .75*p[1][0] + .75*p[1][2] + .75*p[2][0] - .75*p[2][2] - .25*p[3][0] + .25*p[3][2];
      a32 = -.5*p[0][0] + 1.25*p[0][1] - p[0][2] + .25*p[0][3] + 1.5*p[1][0] - 3.75*p[1][1] + 3*p[1][2] - .75*p[1][3] - 1.5*p[2][0] + 3.75*p[2][1] - 3*p[2][2] + .75*p[2][3] + .5*p[3][0] - 1.25*p[3][1] + p[3][2] - .25*p[3][3];
      a33 = .25*p[0][0] - .75*p[0][1] + .75*p[0][2] - .25*p[0][3] - .75*p[1][0] + 2.25*p[1][1] - 2.25*p[1][2] + .75*p[1][3] + .75*p[2][0] - 2.25*p[2][1] + 2.25*p[2][2] - .75*p[2][3] - .25*p[3][0] + .75*p[3][1] - .75*p[3][2] + .25*p[3][3];
   }


   //====================================================
   public double[][] getP()
   {
       //Here is supposed that the elevation points are distributed
       //on a squared lattice with extension greater that the simulation 
       //area: no border corrections
       //x and y are geo coordinates not grid coordinates.

   
        
       double[][] p = new double[4][4];
       int height = sim.elevationGrid.getHeight();
       int width = sim.elevationGrid.getWidth();
       int i0 = -100;
       int j0 = -100;
       MasonGeometry mgElev;
       Point point;
       double xx = 0.0;
       double yy = 0.0;

       //System.out.println(x + " "  + y);
       //System.exit(0);
       //System.out.println("---------------------");

all:
       for (int i = 0; i < width; i++)
       {

          for (int j = 0; j < height; j++)
          {

             mgElev = (MasonGeometry)sim.elevationGrid.get(i, j);
             point = (Point)mgElev.getGeometry();

             if(point.getX() >= x && i0 == -100)
             {
                 i0 = i;
                 MasonGeometry pmgElev = (MasonGeometry)sim.elevationGrid.get(i-1, j);
                 Point ppoint = (Point)pmgElev.getGeometry();
                 double diff = point.getX() - ppoint.getX();

                 xx  = (x - ppoint.getX())/diff;
             }
             if(point.getY() >= y && j0 == -100)
             {
                 j0 = j;
                 MasonGeometry pmgElev = (MasonGeometry)sim.elevationGrid.get(i, j-1);
                 Point ppoint = (Point)pmgElev.getGeometry();
                 double diff = point.getY() - ppoint.getY();

                 yy  = (y - ppoint.getY())/diff;
             }

             if(i0 > -100 && j0 > -100){break all;}
          }
       }

       //System.out.println("i0: " + i0 + " j0: " + j0);
       //System.out.println("xx: " + xx + " yy: " + yy);
       //System.exit(0);
       x = xx;
       y = yy;

       i0 = i0 - 2;
       j0 = j0 - 2;

       for (int i = 0; i < 4; i++)
       {
          for (int j = 0; j < 4; j++)
          {

             //System.out.println("  ");
             //System.out.println("Point in elev grid. i: " + (i + i0) + " j: " + (j + j0));
             mgElev = (MasonGeometry)sim.elevationGrid.get((i + i0), (j + j0));

             //if(mgElev == null)
             //{
             //  System.out.println("MasGeom null");
             //  System.exit(0);
             //}

             p[i][j] = (double)mgElev.getIntegerAttribute("GRID_CODE");
          }
       }



       return p;
   }

}


