/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import sim.util.geo.MasonGeometry;


//----------------------------------------------------
public class BilinearInterpolator 
{
   //Doc and sources from : 
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;

   private double a00, a01, a02, a03;
   private double a10, a11, a12, a13;
   private double a20, a21, a22, a23;
   private double a30, a31, a32, a33;

   public double x = 0.0;
   public double y = 0.0;

   //====================================================
   public BilinearInterpolator(AmaSim psim)
   {
       sim = psim;
   }


   //====================================================
   public double getValue (double px, double py) 
   {
      x = px;
      y = py;
     
      double[][] p = getP();
      updateCoefficients(p);

      return a00 + a10 * x + a01 * y + a11 * x * y;
   }
   
   //====================================================
   public void updateCoefficients (double[][] p) 
   {
      a00 = p[0][0];
      a10 = p[1][0] - p[0][0];
      a01 = p[0][1] - p[0][0];
      a11 = p[0][0] - p[1][0] - p[0][1] + p[1][1];

   }


   //====================================================
   public double[][] getP()
   {
       //Here is supposed that the elevation points are distributed
       //on a squared lattice with extension greater that the simulation 
       //area: no border corrections
       //x and y are geo coordinates not grid coordinates.

       double[][] p = new double[2][2];
       int height = sim.elevationGrid.getHeight();
       int width = sim.elevationGrid.getWidth();
       int i0 = -100;
       int j0 = -100;
       MasonGeometry mgElev;
       Point point;
       double xx = 0.0;
       double yy = 0.0;

       //System.out.println(x + " "  + y);
       //System.exit(0);
       //System.out.println("---------------------");

all:
       for (int i = 0; i < width; i++)
       {

          for (int j = 0; j < height; j++)
          {

             mgElev = (MasonGeometry)sim.elevationGrid.get(i, j);
             point = (Point)mgElev.getGeometry();

             //System.out.println(point.getX());
             //System.out.println(x);

             if(point.getX() >= x && i0 == -100)
             {
                 i0 = i - 1;
                 MasonGeometry pmgElev = (MasonGeometry)sim.elevationGrid.get(i-1, j);
                 Point ppoint = (Point)pmgElev.getGeometry();
                 double diff = point.getX() - ppoint.getX();

                 xx  = (x - ppoint.getX())/diff;
             }
             if(point.getY() >= y && j0 == -100)
             {
                 j0 = j - 1;
                 MasonGeometry pmgElev = (MasonGeometry)sim.elevationGrid.get(i, j-1);
                 Point ppoint = (Point)pmgElev.getGeometry();
                 double diff = point.getY() - ppoint.getY();

                 yy  = (y - ppoint.getY())/diff;
             }

             if(i0 > -100 && j0 > -100){break all;}
          }
       }

       //System.out.println("i0: " + i0 + " j0: " + j0);
       //System.out.println("xx: " + xx + " yy: " + yy);
       //System.exit(0);
       x = xx;
       y = yy;

       //i0 = i0 - 2;
       //j0 = j0 - 2;

       for (int i = 0; i < 2; i++)
       {
          for (int j = 0; j < 2; j++)
          {

             //System.out.println("  ");
             //System.out.println("Point in elev grid. i: " + (i + i0) + " j: " + (j + j0));
             mgElev = (MasonGeometry)sim.elevationGrid.get((i + i0), (j + j0));

             //if(mgElev == null)
             //{
             //  System.out.println("MasGeom null");
             //  System.exit(0);
             //}

             p[i][j] = (double)mgElev.getIntegerAttribute("GRID_CODE");
          }
       }

       return p;
   }

}


