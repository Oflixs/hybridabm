/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;

import sim.util.geo.MasonGeometry;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Envelope;

public class Cluster implements Steppable
{
   private static final long serialVersionUID = 1L;

   int xcor        = 0;
   int ycor        = 0;

   double normx        = 0.0;
   double normy        = 0.0;

   public AmaSim sim;

   public int getXcor(){return xcor;};
   public int getYcor(){return ycor;};

   public Bag coverPixels           = new Bag();
   public Bag highWaters            = new Bag();
   public Bag lowWaters             = new Bag();
   public Bag excludedWaters        = new Bag();
   public Bag mosquitoCoverPixels   = new Bag();

   public Polygon square = null;

   //====================================================
   public Cluster(final SimState state, int Pxcor, int Pycor, Polygon psquare)
   {

      sim = (AmaSim)state;
      xcor = Pxcor;
      ycor = Pycor;

      normx = (double)xcor/sim.geoGridWidth;
      normy = (double)ycor/sim.geoGridHeight;

      if(normx > 1)normx = 1;
      if(normy > 1)normy = 1;

      //System.out.println (xcor);
      //System.out.println (ycor);

      //System.out.println ("---------");
      //System.out.println (normx);
      //System.out.println (normy);

      square = psquare;
      square = psquare;

   }

   //====================================================
   public void step(final SimState state)
   {
   }
   



//============================================================   
}
