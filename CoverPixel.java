/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;

import sim.util.geo.MasonGeometry;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Envelope;

public class CoverPixel implements Steppable
{
   private static final long serialVersionUID = 1L;

   int xcor        = 0;
   int ycor        = 0;

   public AmaSim sim;

   public int getXcor(){return xcor;};
   public int getYcor(){return ycor;};

   int isWater = 0;
   public int  getIsWater(){return isWater;};
   public void setIsWater(int w)
   {
       if(isWater == 0 && w == 1)
       {
           setIsProductiveTimer(((int)Math.round(sim.aquaticStageTime))); 
       }

       isWater = w;
   }

   int isWaterHigh = 0;
   public int  getIsWaterHigh(){return isWaterHigh;};
   public void setIsWaterHigh(int w){isWaterHigh = w;};

   int isRiverExcluded = 0;
   public int  getIsRiverExcluded(){return isRiverExcluded;};
   public void setIsRiverExcluded(int w){isRiverExcluded = w;};

   int isWaterLow = 0;
   public int  getIsWaterLow(){return isWaterLow;};
   public void setIsWaterLow(int w){isWaterLow = w;};

   Boolean isRiverBank = false;
   public Boolean  getIsRiverBank(SimState state)
   {
       Bag neigs = this.getMooreNeighbors(state, "noShuffle", "geo");

       isRiverBank = false;
       for(int i = 0; i < neigs.size(); i++)
       {
           CoverPixel cpNeig = ((CoverPixel)neigs.objs[i]);
           if(cpNeig.getIsWater() == 0 && this.getIsWater() == 1)
           {
               isRiverBank = true;
               break;
           }
 
 
       }

       return isRiverBank;
   }
   public void setIsRiverBank(Boolean w){isRiverBank = w;}
  

   int isDryHigh = 0;
   public int  getIsDryHigh(){return isDryHigh;};
   public void setIsDryHigh(int w){isDryHigh = w;};

   long nOvipositions = 0;
   public long  getNOvipositions(){return nOvipositions;};
   public void setNOvipositions(long w){nOvipositions = w;};
   public void increaseNOvipositions(){nOvipositions++;};

   long nBites = 0;
   long nDiedMosForHuman = 0;

   long nMosquitos = 0;
   public long  getNMosquitos(){return nMosquitos;};
   public void setNMosquitos(long w){nMosquitos = w;};
   public void increaseNMosquitos(){nMosquitos++;};

   long nMosquitosBlood = 0;
   public long  getNMosquitosBlood(){return nMosquitosBlood;};
   public void setNMosquitosBlood(long w){nMosquitosBlood = w;};
   public void increaseNMosquitosBlood(){nMosquitosBlood++;};

   long nMosquitosWater = 0;
   public long  getNMosquitosWater(){return nMosquitosWater;};
   public void setNMosquitosWater(long w){nMosquitosWater = w;};
   public void increaseNMosquitosWater(){nMosquitosWater++;};

   double mosDensity = 0.0;

   int isDone = 0;
   public int  getIsDone(){return isDone;};
   public void setIsDone(int w){isDone = w;};

   public MasonGeometry masonGeometry = null;
   public void setMasonGeometry(MasonGeometry mg){masonGeometry = mg;};
   public MasonGeometry getMasonGeometry(){return masonGeometry;};

   public Bag linkPixels = new Bag();
   public void setLinkPixels(Bag mg){linkPixels = mg;};
   public Bag getLinkPixels(){return linkPixels;};

   public String type = null;
   public void setType(String mg){type = mg;};
   public String getType(){return type;};

   public double oldDeltaLevel = 0.0;
   public void setOldDeltaLevel(double mg){oldDeltaLevel = mg;};
   public double getOldDeltaLevel(){return oldDeltaLevel;};

   public double riverBankLength = 0.0;
   public void setRiverBankLength(double mg){riverBankLength = mg;};
   public double getRiverBankLength(){return riverBankLength;};

   public double riverBankLengthHigh = 0.0;
   public void setRiverBankLengthHigh(double mg){riverBankLengthHigh = mg;};
   public double getRiverBankLengthHigh(){return riverBankLengthHigh;};

   public double riverBankLengthLow = 0.0;
   public void setRiverBankLengthLow(double mg){riverBankLengthLow = mg;};
   public double getRiverBankLengthLow(){return riverBankLengthLow;};

   public double spotify = 0.0;
   public void setSpotify(double mg){spotify = mg;};
   public double getSpotify(){return spotify;};

   public double elevation = -10000;
   public void setElevation(double mg){elevation = mg;};
   public double getElevation(){return elevation;};

   public boolean containsHousehold = false;
   public void setContainsHousehold(boolean mg){containsHousehold = mg;};
   public boolean getContainsHousehold(){return containsHousehold;};

   public Bag households = new Bag();
   public void setHouseholds(Bag mg){households = mg;};
   public Bag getHouseholds(){return households;};

   //this is used to build the mosquitos pixel grid -----
   public boolean containsBreeding = false;
   public void setContainsBreeding(boolean mg){containsBreeding = mg;};

   int isRiverBankHigh = 0;
   public int  getIsRiverBankHigh(){return isRiverBankHigh;};
   public void setIsRiverBankHigh(int w){isRiverBankHigh = w;};

   int isRiverBankLow = 0;
   public int  getIsRiverBankLow(){return isRiverBankLow;};
   public void setIsRiverBankLow(int w){isRiverBankLow = w;};

   int isProductiveTimer = -300;
   public int  getIsProductiveTimer(){return isProductiveTimer;};
   public void setIsProductiveTimer(int w){isProductiveTimer = w;};

   boolean isProductive = false;
   public boolean getIsProductive(){return isProductive;};
   public void setIsProductive(boolean w){isProductive = w;};

   boolean isBufferNoMos = false;
   public boolean getIsBufferNoMos(){return isBufferNoMos;};
   public void setIsBufferNoMos(boolean w){isBufferNoMos = w;};

   public Stoppable stopper;

   public double mosRest = 0.0;
   public void setMosRest(double mg){mosRest = mg;};
   public double getMosRest(){return mosRest;};

   public List<Integer> eggsOvoposited = new ArrayList<Integer>();
   public List<Integer> getEggsOvoposited(){return eggsOvoposited;}; 
   public void setEggsOvoposited(int nEggs)
   {
       //int len = (int)(sim.aquaticStageTime / 24);
       eggsOvoposited.remove(0);
       //eggsOvoposited.set((len - 1), nEggs);
       eggsOvoposited.add(nEggs);
   }

   int nEggs = 0;
   public int  getNEggs(){return nEggs;};
   public void setNEggs(int w){nEggs = w;};
   public void addEgg(){nEggs++;};

   public Bag clusters = new Bag();

   public Polygon square = null;

   public int macrolevel = 0;
   public double macrolevelMax = -10000.0;
   public double macrolevelMin = 10000.0;

   int isRoad = 0;
   public int  getIsRoad(){return isRoad;};
   public void setIsRoad(int w){isRoad = w;};

   public Bag people = new Bag();
   public Bag getPeople(){return people;};
   public void setPeople(Bag p){people = p;};

   public void removeHuman(Human h)
   {
       //if(!people.contains(h))
       //{
       //    System.out.println ("The CoverPixel does not contain the human");
       //    System.exit(0);
       //}
       people.remove(h);
   }

   public void addHuman(Human h)
   {
       if(!people.contains(h))
       people.add(h);
   }


   int accumulatedNPeople = 0;
   public int  getAccumulatedNPeople(){return accumulatedNPeople;};
   public void setAccumulatedNPeople(int w){accumulatedNPeople = w;};
   public void addAccumulatedNPeople(){accumulatedNPeople++;};

   int nMalariaCasesF = 0;
   public int  getNMalariaCasesF(){return nMalariaCasesF;};
   public void setNMalariaCasesF(int w){nMalariaCasesF = w;};
   public void increaseNMalariaCasesF(){nMalariaCasesF++;};

   int nMalariaCasesV = 0;
   public int  getNMalariaCasesV(){return nMalariaCasesV;};
   public void setNMalariaCasesV(int w){nMalariaCasesV = w;};
   public void increaseNMalariaCasesV(){nMalariaCasesV++;};


   int nMalariaCasesFMosquito = 0;
   public int  getNMalariaCasesFMosquito(){return nMalariaCasesFMosquito;};
   public void setNMalariaCasesFMosquito(int w){nMalariaCasesFMosquito = w;};
   public void increaseNMalariaCasesFMosquito(){nMalariaCasesFMosquito++;};

   int nMalariaCasesVMosquito = 0;
   public int  getNMalariaCasesVMosquito(){return nMalariaCasesVMosquito;};
   public void setNMalariaCasesVMosquito(int w){nMalariaCasesVMosquito = w;};
   public void increaseNMalariaCasesVMosquito(){nMalariaCasesVMosquito++;};

   int isHighRisk = 0;
   public int  getIsHighRisk(){return isHighRisk;};
   public void setIsHighRisk(int w){isHighRisk = w;};

   public List<List<Object>> timeMalariaCounts = new ArrayList<List<Object>>();
   public List<List<Object>> timePopCounts = new ArrayList<List<Object>>();

   public List<Object> tmpTimeCases = new ArrayList<Object>();
   public List<Object> tmpTimePop = new ArrayList<Object>();
   public String oldMonth = "";

   public double popCounts = 0.0;
   public double malariaCounts = 0.0;

   public double soilMoisture = 0.0;

   long accumulatedNMosquitoes = 0;
   public long  getAccumulatedNMosquitoes(){return accumulatedNMosquitoes;};
   public void setAccumulatedNMosquitoes(long w){accumulatedNMosquitoes = w;};
   public void addAccumulatedNMosquitoes(){accumulatedNMosquitoes++;};

   long accumulatedNMosquitoesLookingForBlood = 0;
   public long  getAccumulatedNMosquitoesLookingForBlood(){return accumulatedNMosquitoesLookingForBlood;};
   public void setAccumulatedNMosquitoesLookingForBlood(long w){accumulatedNMosquitoesLookingForBlood = w;};
   public void addAccumulatedNMosquitoesLookingForBlood(){accumulatedNMosquitoesLookingForBlood++;};

   long accumulatedNMosquitoesBloodFeeded = 0;
   public long  getAccumulatedNMosquitoesBloodFeeded(){return accumulatedNMosquitoesBloodFeeded;};
   public void setAccumulatedNMosquitoesBloodFeeded(long w){accumulatedNMosquitoesBloodFeeded = w;};
   public void addAccumulatedNMosquitoesBloodFeeded(){accumulatedNMosquitoesBloodFeeded++;};
   //====================================================
   public CoverPixel(final SimState state, int Pxcor, int Pycor, Polygon psquare)
   {

      sim = (AmaSim)state;
      xcor = Pxcor;
      ycor = Pycor;

      square = psquare;

      isWater     = 0;
      isRiverBank = false;
      elevation   = 0.0;
      isDryHigh   = 0;

      //this.stopper = sim.schedule.scheduleRepeating(1.0, 1, this);
      this.stopper = sim.schedule.scheduleRepeating(this);

      //Initialize ovoposited eggs
      int len = (int)(sim.aquaticStageTime / 24);
      for(int i = 0;  i < (len - 1); i++)
      {
         eggsOvoposited.add(0);
      }

   }

   //====================================================
   public void step(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

      isProductiveTimer--;
      if(isWater == 1)
      {
         if(isProductiveTimer == 0) isProductive = true; 
         else if(isProductiveTimer > 0 ) isProductive = false; 
      }
      if(isWater == 0)
      {
          isProductive = false; 
          isProductiveTimer = -300;
      }


      if(!sim.equilibration && !sim.monthDate.equals(oldMonth))
      {
         if(malariaCounts == 0)
         {
             popCounts = 0.0;
            
         }
         else
         {

             tmpTimeCases = new ArrayList<Object>();
             tmpTimePop = new ArrayList<Object>();
             oldMonth = sim.monthDate;

             tmpTimeCases.add(oldMonth);
             tmpTimeCases.add(malariaCounts);

             tmpTimePop.add(oldMonth);
             tmpTimePop.add(popCounts);

             if(!oldMonth.equals(""))
             {
                 timeMalariaCounts.add(tmpTimeCases);
                 timePopCounts.add(tmpTimePop);
             }

             popCounts = 0.0;
             malariaCounts = 0.0;
         }
      }


   }
   
   //====================================================
   public Bag getVonNeumannNeighbors(final SimState state, String grid)
   {
      final AmaSim sim = (AmaSim)state;
      //VanNeumann neighbors-----------------------
      Integer coors[] = {0, 0};
      Bag neig = new Bag();
      CoverPixel tcp;

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);


      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);


      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor + 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor - 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      return neig;

   }

   //====================================================
   public Boolean checkCoorsPbc(int i ,int j)
   {
       int height = sim.mosquitoGridHeight;
       int width  = sim.mosquitoGridWidth;

       if(
               i < 0
               || i >= width
               || j < 0
               || j >= height
         )return true;
       else return false;
       
   }

   //====================================================
   public Bag getMooreNeighborsNoPbcMosquito(final SimState state, String shuffle, String grid)
   {
      final AmaSim sim = (AmaSim)state;
      //VanNeumann neighbors-----------------------
      Integer coors[] = {0, 0};
      Bag neig = new Bag();
      CoverPixel tcp;

      Polygon squareNoPbc = null;
      int height = sim.mosquitoGridHeight;
      int width  = sim.mosquitoGridWidth;
      //CoverPixel noPbc = new CoverPixel(state, i, j, square);

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);


      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor + 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor - 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor + 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor - 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor + 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor - 1;
      if(checkCoorsPbc(coors[0], coors[1]))
      {
          tcp = new CoverPixel(state, coors[0], coors[1], square);
          tcp.setType("noPbc");
          tcp.stopper.stop();         
      }
      else tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      if(shuffle.equals("doShuffle"))
      {
          neig.shuffle(state.random);
      }

      return neig;

   }




   //====================================================
   public Bag getMooreNeighbors(final SimState state, String shuffle, String grid)
   {
      final AmaSim sim = (AmaSim)state;
      //VanNeumann neighbors-----------------------
      Integer coors[] = {0, 0};
      Bag neig = new Bag();
      CoverPixel tcp;

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);


      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor + 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor;
      coors[1] = ycor - 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor + 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor + 1;
      coors[1] = ycor - 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor + 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      //------------------------------------------
      coors[0] = xcor - 1;
      coors[1] = ycor - 1;
      coors = sim.utils.pbc(state, coors, "toroidals", grid);
      tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], grid);
      neig.add(tcp);

      if(shuffle.equals("doShuffle"))
      {
          neig.shuffle(state.random);
      }

      return neig;

   }

   //====================================================
   public boolean pixelContainsHouseholds(SimState state)
   {
      final AmaSim sim = (AmaSim)state;
      Bag households = sim.householdGrid.getObjectsAtLocation(
              this.getXcor(), this.getYcor()
              );

      if(households != null && households.size() > 0)return true;
      else return false;
   }

   //====================================================
   public boolean containsHumans()
   {
       if(people.size() > 0)return true;
       else return false;
   }

   //====================================================
   public Human getHuman(SimState state)
   {
      int iran = state.random.nextInt(people.size() - 1);
      return (Human)people.get(iran);
   }

   //====================================================
   public CoverPixel getContainsBreeding(SimState state)
   {
      //input is a mosquito pixel 
      Bag linkMCp = this.getLinkPixels();

      linkMCp.shuffle(state.random);

      for(int i =  0; i < linkMCp.size(); i++)
      {
          CoverPixel gcp = (CoverPixel)linkMCp.objs[i];

          if(gcp.getIsWaterHigh() == 0) continue;
          if(gcp.getIsRiverExcluded() == 1) continue;

          //if(gcp.getIsWater() == 1 && gcp.getIsWaterLow() == 0)
          if(gcp.getIsWater() == 1) return gcp;
      }
      return null;
   
   }

   //====================================================
   public Boolean isWaterChanging()
   {
       if(!sim.hybridExample)
       {
           if(this.getIsRiverExcluded() == 0
                   && this.getIsWaterLow() == 0
                   //if(cp.getIsDryHigh() == 1)continue;
                   && this.getIsWaterHigh() == 1)return true;
           else return false;
       }
       else
       {
           //Pitxi patch
           if(isWaterHigh == 1)return true;
           else return false;
       }

   }

   public void addTimePopCounts()
   {
        popCounts++;
   }

   public void addTimeMalariaCounts()
   {
        malariaCounts++;
   }

//============================================================   
}
