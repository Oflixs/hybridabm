/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.DateUtil;

import java.text.ParseException;

public class DoAnalysis
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean first = true;

    boolean noWrite = false;

    public String file_name;
    public List<String> titles = new ArrayList<String>();
    public List<List<Object>> data = new ArrayList<List<Object>>();

    public String outFile = "";


    int equilibration = -1000;

    String startDate = "";
    String endDate = "";
    int daysDiff = 0;
    double weeksDiff = 0.0;
    double equilWeeks = 0.0;

    int numFiles = 0;

    int numZeroFiles = 0;

    //====================================================
    public DoAnalysis(AmaSim sim)
    {
       outFile = sim.outPath + "avgData_" + sim.outDirCom  +  ".xls";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile)
    {
      int stats  = 0;
      int numRowsRead = 0;

       //Patch !~!~~!!~~!~!~!~! Bho!
       //sim.da = this;

      rFile = sim.outPath + rFile;
      System.out.println (rFile);
      System.out.println (sim.outDirCom);
 
      try{
             Workbook workbook = WorkbookFactory.create(new FileInputStream(rFile) );

	     if(workbook.getNumberOfSheets() < 2)
	     {
		     System.out.println ("numSheets in the output workbook too little =  " + workbook.getNumberOfSheets());
		     return;
		     //System.exit(0);
	     }
  
             Sheet sheet = workbook.getSheet("results");


             //Sheet sheet = workbook.getSheet("80");

             List<Object> rData   = new ArrayList<Object>();
             List<List<Object>> dataF   = new ArrayList<List<Object>>();
             titles = new ArrayList<String>();
  
rows:             
             for(Row row : sheet)
             { 
                 numRowsRead++;
                 rData   = new ArrayList<Object>();
                 stats = 0;
                 for(Cell cell : row)
                 { 
                     stats++;

                     //String string = Integer.toString((int)cell.getNumericCellValue());

                     //System.out.println ("-----------------------");
                     //System.out.println (cell.getCellType());
                     //System.exit(0);

                     if(numRowsRead == 1 && cell.getCellType()  == 1)
                     {
                         titles.add(cell.getRichStringCellValue().getString() ); 
                         continue;
                     }

                     switch (cell.getCellType()) {
                         case Cell.CELL_TYPE_STRING:
                             rData.add(cell.getRichStringCellValue().getString() );
                             break;
                         case Cell.CELL_TYPE_NUMERIC:
                             rData.add((Object)cell.getNumericCellValue());
                             break;
                         default:
                             rData.add(0.0);
                             //System.out.println ("Error cell value:"  + cell.getCellType());

                             //System.exit(0);
                     }
                 }

                 if(rData.size() == 0)
                 {
                     //numRowsRead--;
                     continue rows;
                 }

                 //if(stats != 30)
                 //{
                 //   System.out.println ("Num col: " + stats);
                 //}
                 dataF.add(rData);

                 //System.out.println (rData.get(0));
 
             }

             //System.out.println ("Num row from read : " + numRowsRead);

             int numRowsTheo = sheet.getLastRowNum() - 1;

             //System.out.println ("NumRows read = " + numRowsRead);
             //System.out.println ("NumRows theo = " + numRowsTheo);

             //System.out.println ("NumWeeks from input file = " + (equilWeeks + weeksDiff));

             if(numRowsRead < (equilWeeks + weeksDiff - 3)
             || numRowsTheo < (equilWeeks + weeksDiff - 3 )
             || numRowsRead > (equilWeeks + weeksDiff + 3 )
             || numRowsTheo > (equilWeeks + weeksDiff + 3 ))
             {

                 System.out.println ("Num rows not equals num weeks in input file");
                 System.out.println ("------------- File is ignored ---------");
                 return;
                 //System.exit(0);
             }
             else System.out.println ("------------- File is processed ---------");

             numFiles++;

             if(first)
             {

                int len = dataF.size();
                //System.out.println("Len : " + len);
                //System.exit(0);

                double numZ = 0;
                for(int i = 0; i < len; i++)
                {
                    data.add(dataF.get(i)); 

                    if(i > 2)numZ = numZ + (double)dataF.get(i).get(3);


                    //if(i == 0)
                    //{
                    //    System.out.println(dataF.get(i).get(0));
                    //}
                }
                if(numZ == 0.0)
                {
                    System.out.println ("Only zero incidence file ---------");
                    numZeroFiles++;
                }
                else System.out.println ("Not only zero incidence file ---------");

             }
             else
             {
                //System.out.println("---------");
                sumAverages(sim, dataF); 
             }

             first = false;
             //System.exit(0);
  
          }
          catch(FileNotFoundException e)
              {
                 System.out.println(e);
              }
          catch(IOException e)
              {
                 System.out.println(e);
              }
          catch(InvalidFormatException e)
              {
                 System.out.println(e);
              }
 
 
    }

    //====================================================
    public void calcAverages(AmaSim sim, int numFiles)
    {

        System.out.println("================================================");
        System.out.println ("calculating averages ..........................");
        System.out.println(" ");

        List<Object> dataLine = new ArrayList<Object>();
        List<Object> line_tmp = new ArrayList<Object>();
        List<List<Object>> data_tmp = new ArrayList<List<Object>>();

        Object obj_tmp;
        Object objData;

        double d_data = 0;

        Object obj;
       
        int len = data.size();
    
        for(int i = 0; i < len; i++)
        {
            dataLine = data.get(i);
            line_tmp = new ArrayList<Object>();

            int lenLine = dataLine.size();
            //System.out.println(lenLine);

            for(int j = 0; j < lenLine; j++)
            {
               objData = (Object)dataLine.get(j); 
               obj_tmp = new Object();

               if(objData instanceof String)
               {
                  obj_tmp = objData; 
               }
               else if(objData instanceof Double)
               {
                  d_data = (Double)objData;
                  //System.out.println(d_data);
                  d_data = d_data / numFiles;
                  obj_tmp = (Object)d_data;
               }

               line_tmp.add(obj_tmp);

               //System.out.println(numFiles);
               //System.out.println(obj_tmp);
               //System.exit(0);
            }
            //System.exit(0);

            data_tmp.add(line_tmp);
        }

        data = data_tmp;
        //System.exit(0);
    }


    //====================================================
    public void sumAverages(AmaSim sim, List<List<Object>> list)
    {
        List<Object> readLine = new ArrayList<Object>();
        List<Object> dataLine = new ArrayList<Object>();
        List<Object> line_tmp = new ArrayList<Object>();
        List<List<Object>> data_tmp = new ArrayList<List<Object>>();

        Object obj_tmp;
        Object objRead;
        Object objData;

        double d_read = 0;
        double d_data = 0;

        Object obj;
       
        int len = list.size();
        if(len != data.size())
        {
            System.out.println("sumAvg in DoAnalysis returns For len...");
            //Patch!!!!  
            numFiles--;
            return;
        }
 
    
        double sumF = 0.0;
        for(int i = 0; i < len; i++)
        {
            dataLine = data.get(i);
            readLine = list.get(i);
            line_tmp = new ArrayList<Object>();

            int readLinen = readLine.size();
            int dataLinen = dataLine.size();
            //if(dataLinen != 30 || readLinen != 30)
            //{
            //   System.out.println("dataLine: " + dataLinen);
            //   System.out.println("readLine: " + readLinen);
            //}

            if(readLinen != dataLinen) return;


            int lenLine = readLinen;
            for(int j = 0; j < lenLine; j++)
            {
               objData = (Object)dataLine.get(j); 
               objRead = (Object)readLine.get(j); 
               obj_tmp = new Object();

               if(objData instanceof String)
               {
                  obj_tmp = objData; 
               }
               else if(objData instanceof Double)
               {
                  d_read = (Double)objRead;
                  d_data = (Double)objData;
                  d_data = d_data + d_read;
                  obj_tmp = (Object)d_data;
               }

               line_tmp.add(obj_tmp);

               if(j ==3)sumF = sumF + (double)objRead;

               //if(i == 0)
               //{
               //    System.out.println("Obj tmp =" + obj_tmp);
               //}
               //System.exit(0);
            }
            //System.exit(0);

            data_tmp.add(line_tmp);
        }

        if(sumF == 0.0)
        {
            System.out.println ("Only zero incidence file ---------");
            numZeroFiles++;
        }
        else System.out.println ("Not nly zero incidence file ---------");

        data = new ArrayList<List<Object>>();
        data = data_tmp;

    }

 
    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs  ........................");
      System.out.println(" ");
 
      File theDir = new File(sim.outPath);
      String [] directoryContents = theDir.list();
 

      String rFile = "";
 
      for(String fileName: directoryContents) 
      {
          //System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);

          //System.out.println (words.length);
 
          if(words.length < 2)continue;
          if(!words[1].equals("xls"))continue;
 
          delims = "_";
          words = words[0].split(delims);
 
          int len = words.length;
 
          if(!words[0].equals("output"))continue;
          System.out.println ("================================================");
 
          rFile = sim.outPath + fileName;
          readInput(sim, rFile);

          readFile(sim, fileName);
 
          System.out.println ("File number " + numFiles + " done ...........");
          //if(numFiles == 3)break;

      }
      //System.exit(0);



      calcAverages(sim, numFiles); 

      writeToFile(sim);

      writeInput(sim);

      System.out.println("Tot Num files: " + numFiles); 
      System.out.println("Num Zero incidence files: " + numZeroFiles); 
      
      System.out.println("First part analysis done   ..... ");
    }

    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the averages file.");
      System.out.println(" ");
      System.out.println("outFile: " + outFile);

      try{

          FileInputStream file = new FileInputStream(outFile);

          HSSFWorkbook workbook = new HSSFWorkbook(file);

          HSSFSheet sheet = workbook.getSheet("Input File");
          //If the sheet !exists a new sheet is created --------- 
          if(sheet == null)sheet = workbook.createSheet("Input File");

          workbook.setSheetOrder("Input File", 1);

          int rownum = 0;
          int size = list.size();

          for(int i = 0; i < size; i++)
          {
              rownum++;
              Row row = sheet.createRow(rownum);

              String line = list.get(i);

              Cell cell = row.createCell(0);
              cell.setCellValue((String)line);

          }

          try {

              FileOutputStream out = 
                  new FileOutputStream(new File(outFile));

              try{
                  //FileOutputStream out = new FileOutputStream(out_file, true);

                  workbook.write(out);
              }
              finally
              {
                  out.flush();
                  out.close();
                  System.out.println("Input params written in the file successfully..");
              }



          } catch (FileNotFoundException e) {
              e.printStackTrace();
          } catch (IOException e) {
              e.printStackTrace();
          }

      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }

    //====================================================
    public void writeToFile(AmaSim sim) 
    {
       System.out.println("================================================");
       System.out.println ("Writing  averages .............................");
       System.out.println(" ");
 
       HSSFWorkbook workbook = new HSSFWorkbook();

       HSSFSheet sheet = workbook.createSheet("results");
 
       Cell cell = null;
 
       int lastRow = sheet.getLastRowNum();
       int cellnum = 0;
       //lastRow++;

       List<Object> line_tmp = new ArrayList<Object>();
 
       //System.out.println("Last row:" + lastRow);
       //lastRow++;
 

       //writes titles ----------------------------------
       int size = titles.size();
       Row row = sheet.createRow(lastRow++);
       cellnum = 0;
 
       for(int i = 0; i < size; i++)
       {
           String title = titles.get(i);
           //System.out.println(title);
           
           cell = row.createCell(cellnum++);

           cell.setCellType(1);
           cell.setCellValue(title);
       }

       
       size = data.size();
       //System.out.println(size);
       //System.exit(0);

       for(int i = 1; i < size; i++)
       {

           cellnum = 0;
           row = sheet.createRow(lastRow++);
 
           line_tmp = data.get(i);

           //System.out.println("------------");
           //System.out.println(line_tmp.size());
           //System.exit(0);
 
           for (Object obj : line_tmp) 
           {
              //System.out.println("------------");
              cell = row.createCell(cellnum++);
 
              if(obj instanceof String)
              {
                 cell.setCellType(1);
                 cell.setCellValue((String)obj);
                 //System.out.println(obj);
                 //System.out.println("String....");
              }
              else if(obj instanceof Double)
              {
                 cell.setCellType(0);
                 cell.setCellValue((Double)obj);
                 //System.out.println("Double....");
              }
           }
       }
       //System.exit(0);
 

       try{
           File out_file = new File(outFile);

           FileOutputStream out = new FileOutputStream(out_file, false);

           try{
               //FileOutputStream out = new FileOutputStream(out_file, true);

               workbook.write(out);
           }
           finally
           {
               out.flush();
               out.close();
           }
       }
       catch (IOException ex){
           ex.printStackTrace();
       }

       System.out.println("Analysis data written successfully.....");

       try {
           //Thread.sleep(5000);
           Thread.sleep(50);
       } catch(InterruptedException ex) {
           Thread.currentThread().interrupt();
       }

      System.out.println(" ");
      System.out.println ("Analysis outputs completed ............");
      System.out.println("================================================");


    }
 


//==================================================
public void readInput(AmaSim sim, String xlsFile)
{
    FileInputStream inputFile =  null;
    System.out.println ("Reading input from file:  " + xlsFile);
    try{
        inputFile =  new FileInputStream(xlsFile);

        Workbook workbook = WorkbookFactory.create(inputFile);

        if(workbook.getNumberOfSheets() < 2)
        {
            System.out.println ("numSheets in the output workbook too little =  " + workbook.getNumberOfSheets());
            return;
            //System.exit(0);
        }

        Sheet sheet = workbook.getSheet("Input File");
        if(sheet == null)return;
        Row row;
        Cell cell = null;
        String delims = "[ ]+";
        double val;
        int int_val = 0;
        double cat;
        String string = "";
        boolean write = false;

        int numRows = sheet.getPhysicalNumberOfRows();
        //System.out.println ("Num rows: " + numRows);
        //System.exit(0);

        String oldDate = "";
        Boolean start = false;
        int stats = 0;

        for(int i = 1; i < numRows; i++)
        { 
            write = false;
            row = sheet.getRow(i);

            cell = row.getCell(0);

            string = cell.getStringCellValue();

            if(string.contains("equilibPeriod")
                    && !string.contains("#"))
            {
                //System.out.println("Line: " + string);
                String[] words = string.split(delims);
                if(words[0].equals("equilibPeriod") && words.length == 2)
                {
                    //System.out.println("words0: " + words[0]);
                    //System.out.println("words1: " + words[1]);
                    equilibration = Integer.parseInt(words[1]);
                    //System.out.println("equilibration in read input: " + equilibration);
                }
                else
                {
                    System.out.println("Problem in the genotype input file equlibration");
                    System.exit(0);
                }
            }


            if(string.contains("startDate")
                    && !string.contains("#"))
            {
                //System.out.println("Line: " + string);
                String[] words = string.split(delims);
                if(words[0].equals("startDate") && words.length == 2)
                {
                    startDate = words[1];
                }
                else
                {
                    System.out.println("Problem in the genotype input file startDate");
                    System.exit(0);
                }
            }

            if(string.contains("endDate")
                    && !string.contains("#"))
            {
                //System.out.println("Line: " + string);
                String[] words = string.split(delims);
                if(words[0].equals("endDate") && words.length == 2)
                {
                    endDate = words[1];
                }
                else
                {
                    System.out.println("Problem in the genotype input file endDate");
                    System.exit(0);
                }
            }



        }
    }
    catch(FileNotFoundException e)
    {
        System.out.println(e);
    }
    catch(IOException e)
    {
        System.out.println(e);
    }
    catch(InvalidFormatException e)
    {
        System.out.println(e);
    }
    finally
    {
        try {
            if(inputFile != null){inputFile.close();}
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Getting the dates
    //System.out.println ("startDate: " + startDate);
    //System.out.println ("endDate: " + endDate);
    //System.out.println ("equilibration: " + equilibration);


    DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    Calendar startCal = Calendar.getInstance();
    try{
        startCal.setTime(format.parse(startDate));
    }
    catch (ParseException e)
    {//catch exception if any
        System.err.println("error: " + e.getMessage());
    }

    Calendar endCal = Calendar.getInstance();
    try{
        endCal.setTime(format.parse(endDate));
    }
    catch (ParseException e)
    {//catch exception if any
        System.err.println("error: " + e.getMessage());
    }

    //System.out.println (startCal.getTime());
    //System.out.println (endCal.getTime());

    long startMilli = startCal.getTimeInMillis();
    long endMilli    = endCal.getTimeInMillis();

    daysDiff = (int)( ( endMilli - startMilli ) / (1000 * 60 * 60 * 24));
    daysDiff++;

    //System.out.println ("Days run: " + daysDiff);

    weeksDiff = (double)daysDiff/7;

    //System.out.println ("Days equilibration: " + equilibration);

    equilWeeks = (double)equilibration/7;

    //System.out.println ("Weeks between start and end date: " + weeksDiff);
    //System.out.println ("Weeks equilibration: " + equilWeeks);

    //System.exit(0);
}











}
