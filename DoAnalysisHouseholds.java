/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.DateUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bbn.openmap.proj.coords.UTMPoint;
import com.bbn.openmap.proj.coords.LatLonPoint;
import com.vividsolutions.jts.geom.Point;

import java.net.URL;

import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;

import sim.util.geo.MasonGeometry;

public class DoAnalysisHouseholds
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean first = true;

    public String file_name;
    public List<String> titles = new ArrayList<String>();
    public List<List<Object>> data = new ArrayList<List<Object>>();
    public List<Double> nMalCases = new ArrayList<Double>();

    public String outFile = "";
    public String sheetName = "";

    public List<Double> id = new ArrayList<Double>();
    public List<Double> numPeople = new ArrayList<Double>();
    public List<Double> xCor = new ArrayList<Double>();
    public List<Double> yCor = new ArrayList<Double>();
    public List<Double> nCasesFalciparum = new ArrayList<Double>();
    public List<Double> nCasesVivax = new ArrayList<Double>();

    public int numFiles = 0;
    public int statsGlob = 0;

    public HashMap<CoverPixel, List<Object>> cps = new HashMap<CoverPixel, List<Object>>();


    //====================================================
    public DoAnalysisHouseholds(AmaSim sim)
    {
        outFile = sim.outPath + "avgData_" + "Households"  +  ".xls";
        sheetName = "Households";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile)
    {
        int stats  = 0;
        int numRow = 0;

        rFile = sim.outPath + rFile;
        System.out.println ("in File: " + rFile);

        try{
            Workbook workbook = WorkbookFactory.create(new FileInputStream(rFile) );

            if(workbook.getNumberOfSheets() < 2)
            {
                System.out.println ("numSheets in the output workbook too little =  " + workbook.getNumberOfSheets());
                return;
                //System.exit(0);
            }

            numFiles++;

            Sheet sheet = workbook.getSheet(sheetName);

            List<Object> rData   = new ArrayList<Object>();
            List<List<Object>> dataF   = new ArrayList<List<Object>>();
            titles = new ArrayList<String>();

            for(Row row : sheet)
            { 
                numRow++;
                rData   = new ArrayList<Object>();
                stats = 0;
                for(Cell cell : row)
                { 
                    stats++;

                    if(numRow == 1)
                    {
                        titles.add(cell.getRichStringCellValue().getString() ); 
                        continue;
                    }

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            rData.add(cell.getRichStringCellValue().getString() );
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            rData.add((Object)cell.getNumericCellValue());
                            break;
                        default:
                            System.out.println ("Error cell value:"  + cell.getCellType());

                    }

                }

                //System.out.println ("--------------");
                if(rData.size() > 1)
                {
                    List<Object> line_tmp = new ArrayList<Object>();
                    List<Object> oldData = new ArrayList<Object>();
                    List<Object> tmp = new ArrayList<Object>();
                    double d = 0.0;
                    double d1 = 0.0;
                    double d2 = 0.0;

                    double dindex = (double)rData.get(0);
                   int index = (int)Math.round(dindex);

                    Household hh = (Household)sim.household_bag.get(index - 1);
                    CoverPixel cpH = hh.cpPosition;

                    tmp = new ArrayList<Object>();

                    if(cps.containsKey(cpH))
                    {
                        oldData = cps.get(cpH);

                        tmp.add(oldData.get(0));
                        //System.out.println (oldData.get(0));

                        d = (double)oldData.get(1);
                        d = d +(double)rData.get(1); 
                        tmp.add(d);

                        tmp.add(oldData.get(2));
                        tmp.add(oldData.get(3));

                        d1 = (double)oldData.get(4);
                        d = d1 +(double)rData.get(4); 
                        tmp.add(d);

                        d2 = (double)oldData.get(5);
                        d = d2 +(double)rData.get(5);; 
                        tmp.add(d);

                        d = (double)oldData.get(6);
                        d = d + d1 + d2;
                        tmp.add(d);

                        d = (double)oldData.get(7);
                        //System.out.println (d);
                        d = d + 1;
                        tmp.add(d);

                        cps.put(cpH, tmp); 
                    }
                    else
                    {
                        //System.out.println (rData.get(0));

                        d = (double)rData.get(0); 
                        tmp.add(d);

                        d = (double)rData.get(1); 
                        tmp.add(d);

                        d = (double)rData.get(2); 
                        tmp.add(d);

                        d = (double)rData.get(3); 
                        tmp.add(d);

                        d1 = (double)rData.get(4); 
                        tmp.add(d1);

                        d = (double)rData.get(5); 
                        tmp.add(d);

                        d = d + d1;
                        tmp.add(d);

                        tmp.add(1.0);

                        cps.put(cpH, tmp); 
                    }


                }
            }


        }
        catch(FileNotFoundException e)
        {
            System.out.println(e);
        }
        catch(IOException e)
        {
            System.out.println(e);
        }
        catch(InvalidFormatException e)
        {
            System.out.println(e);
        }

        //System.out.println(statsGlob);
        //System.exit(0);

    }

    //====================================================
    public void calcAverages(AmaSim sim)
    {

        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (CoverPixel key : cps.keySet()) 
        {
            tmp = new ArrayList<Object>();

            List<Object> oldData = cps.get(key);
            //System.out.println(" value: " + oldData.get(7));

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d/(double)oldData.get(7);
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d/(double)oldData.get(7);
            tmp.add(d);

            d = (double)oldData.get(5);
            d = d/(double)oldData.get(7);
            tmp.add(d);

            d = (double)oldData.get(6);
            d = d/(double)oldData.get(7);
            tmp.add(d);

            tmp.add(oldData.get(7));

            cps.put(key, tmp); 
        }

        //printCps();
        //System.exit(0);

    }

    //====================================================
    public void printCps()
    {

        for (CoverPixel key : cps.keySet()) 
        {
            List<Object> oldData = cps.get(key);
            System.out.println ("Id: " + oldData.get(0)
                 + " Pop: " + oldData.get(1)
                 + " CorX: " + oldData.get(2)
                 + " CorY: " + oldData.get(3)
                 + " Cases: " + oldData.get(6)
                 + " Recurrence: " + oldData.get(7)
                 );

        }
 

    }

    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs  (households)............");
      System.out.println(" ");
 
      File theDir = new File(sim.outPath);
      String [] directoryContents = theDir.list();
 
      numFiles = 0;
 
      for(String fileName: directoryContents) 
      {
          //System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);
 
          if(words.length < 2)continue;
          if(!words[1].equals("xls"))continue;
 
          delims = "_";
          words = words[0].split(delims);
 
          int len = words.length;
 
          if(!words[0].equals("output"))continue;
 
          readFile(sim, fileName);
 
          System.out.println ("File number " + numFiles + " done ...........");

          first = false;
      }

      System.out.println ("Number of Files: " + numFiles);

      calcAverages(sim); 
      //System.exit(0);

      writeInput(sim);
      
      try {
             writeToFile(sim);
              
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }

      writeToSpatAn(sim);

      //writeShp(sim);

      //System.exit(0);

      System.out.println(" ");
      System.out.println ("Analysis outputs  (households) completed ............");
      System.out.println("================================================");
      System.out.println(" ");
    }

    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the averages file.");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet("Input File");
 
      int rownum = 0;
      int size = list.size();

      for(int i = 0; i < size; i++)
      {
          rownum++;
          Row row = sheet.createRow(rownum);

          String line = list.get(i);

          Cell cell = row.createCell(0);
          cell.setCellValue((String)line);

      }
 
      try {

          FileOutputStream out = 
             new FileOutputStream(new File(outFile));
          workbook.write(out);
          out.close();
          System.out.println("Excel written successfully..");
           
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }



    //====================================================
    public void writeToFile(AmaSim sim) throws IOException
    {
       System.out.println("================================================");
       System.out.println ("Writing  averages .............................");
       System.out.println(" ");
 
       FileInputStream file = new FileInputStream(outFile);
 
       HSSFWorkbook workbook = new HSSFWorkbook(file);
       HSSFSheet sheet;
 
       sheet = workbook.getSheet(sheetName);
       //If the sheet !exists a new sheet is created --------- 
       if(sheet == null)sheet = workbook.createSheet(sheetName);
 
       workbook.setSheetOrder(sheetName, 0);
 
       Cell cell = null;
 
       int lastRow = sheet.getLastRowNum();
       int cellnum = 0;
       //lastRow++;

       List<Object> line_tmp = new ArrayList<Object>();
 
       //System.out.println("Last row:" + lastRow);
       //lastRow++;
 

       //writes titles ----------------------------------
       int size = titles.size();
       Row row = sheet.createRow(lastRow++);
       cellnum = 0;
 
       for(int i = 0; i < size; i++)
       {
           String title = titles.get(i);
           
           cell = row.createCell(cellnum++);

           cell.setCellType(1);
           cell.setCellValue(title);
       }

       
       size = data.size();


       //System.out.println ("data size: " + data.size());
       //System.exit(0);

       for (CoverPixel key : cps.keySet()) 
       {

           cellnum = 0;
           row = sheet.createRow(lastRow++);
 
           line_tmp  = cps.get(key);

           //System.out.println("------------");
           //System.out.println("line size: " + line_tmp.size());
           //System.exit(0);
 
           //System.out.println("------------");
           for (Object obj : line_tmp) 
           {
              //System.out.println("------------");
              cell = row.createCell(cellnum++);

              //System.out.println("cellnum = " + cellnum + " obj: " + obj);

              cell.setCellType(0);
              cell.setCellValue((Double)obj);
           }
       }
       //System.exit(0);
 
       file.close();
  
       File out_file = new File(outFile);
 
       FileOutputStream out = new FileOutputStream(out_file);
 
       workbook.write(out);
       out.close();
       System.out.println("Analysis data written successfully.....");

       


    }

    //====================================================
    public void writeToSpatAn(AmaSim sim) 
    {

        //here regroup the data by pixel: if two households are located
        //in the same pixel the malaria count will be groupped and referenced to
        //the pixel. if not satScan get ungry/

        System.out.println("================================================");
        System.out.println ("Writing  to spatial analysis out file..........");
        System.out.println(" ");

        File newTextFileAll = null;
        FileWriter fileWriterAll = null;

        File newTextFileF = null;
        FileWriter fileWriterF = null;

        File newTextFileV = null;
        FileWriter fileWriterV = null;

        int len = 0;
        String line = "";
        double cases = 0.0;
        String hFile = "";
        int stats = 0;

        hFile = sim.outPath + "/Households";
        newTextFileAll = new File(hFile);
        if (!newTextFileAll.exists())
        {
            System.out.println("creating directory: " + hFile);
            newTextFileAll.mkdir();
        }

        hFile = sim.outPath + "/HouseholdsFalciparum";
        newTextFileF = new File(hFile);
        if (!newTextFileF.exists())
        {
            System.out.println("creating directory: " + hFile);
            newTextFileF.mkdir();
        }

        hFile = sim.outPath + "/HouseholdsVivax";
        newTextFileV = new File(hFile);
        if (!newTextFileV.exists())
        {
            System.out.println("creating directory: " + hFile);
            newTextFileV.mkdir();
        }

        String pathAll = sim.outPath + "/Households";
        String pathF = sim.outPath + "/HouseholdsFalciparum";
        String pathV = sim.outPath + "/HouseholdsVivax";


        //case file -----------------
        try{
            hFile = pathAll + "/cases.cas";
            newTextFileAll = new File(hFile);
            fileWriterAll = new FileWriter(newTextFileAll);

            hFile = pathF + "/cases.cas";
            newTextFileF = new File(hFile);
            fileWriterF = new FileWriter(newTextFileF);

            hFile = pathV + "/cases.cas";
            newTextFileV = new File(hFile);
            fileWriterV = new FileWriter(newTextFileV);


            stats = 0;
            for (CoverPixel key : cps.keySet()) 
            {
                List<Object> tmpList = cps.get(key);

                cases = ((double)tmpList.get(6));
                line = stats + " " + (Math.round(cases * 1000)) + " \n";
                fileWriterAll.write(line);

                cases = ((double)tmpList.get(4));
                line = stats + " " + (Math.round(cases * 1000)) + " \n";
                fileWriterF.write(line);

                cases = ((double)tmpList.get(5));
                line = stats + " " + (Math.round(cases * 1000)) + " \n";
                fileWriterV.write(line);

                stats++;
            }

            // force bytes to the underlying stream
            fileWriterAll.close();
            fileWriterF.close();
            fileWriterV.close();

        } catch (IOException ex) {

        }//End case file 

        //population file -----------------
        try{
            hFile = pathAll + "/pop.pop";
            newTextFileAll = new File(hFile);
            fileWriterAll = new FileWriter(newTextFileAll);

            hFile = pathF + "/pop.pop";
            newTextFileF = new File(hFile);
            fileWriterF = new FileWriter(newTextFileF);

            hFile = pathV + "/pop.pop";
            newTextFileV = new File(hFile);
            fileWriterV = new FileWriter(newTextFileV);

            stats = 0;
            for (CoverPixel key : cps.keySet()) 
            {
                List<Object> tmpList = cps.get(key);

                line = stats + " 98 " + (Math.round(1000 * (double)tmpList.get(1)/24))  +  " \n";
                fileWriterAll.write(line);
                fileWriterF.write(line);
                fileWriterV.write(line);

                stats++;
            }

            // force bytes to the underlying stream
            fileWriterAll.close();
            fileWriterF.close();
            fileWriterV.close();

        } catch (IOException ex) {

        }//End population file 

        //coords     file -----------------
        try{
            hFile = pathAll + "/cor.cor";
            newTextFileAll = new File(hFile);
            fileWriterAll = new FileWriter(newTextFileAll);

            hFile = pathF + "/cor.cor";
            newTextFileF = new File(hFile);
            fileWriterF = new FileWriter(newTextFileF);

            hFile = pathV + "/cor.cor";
            newTextFileV = new File(hFile);
            fileWriterV = new FileWriter(newTextFileV);

            stats = 0;
            for (CoverPixel key : cps.keySet()) 
            {

                Point center = key.square.getCentroid();

                //UTMPoint utm = new UTMPoint(Math.round(yCor.get(i)), Math.round(xCor.get(i)), 18, 'S');
                UTMPoint utm = new UTMPoint(Math.round(center.getY()), Math.round(center.getX()), 18, 'S');

                //System.out.println (utm.toString());

                LatLonPoint latlon = utm.toLatLonPoint();


                line = stats + " " + latlon.getLatitude() + " "  + latlon.getLongitude()  +  " \n";
                //System.out.println (line);

                fileWriterAll.write(line);
                fileWriterF.write(line);
                fileWriterV.write(line);

                //System.out.println (line);
                stats++;
            }

            // force bytes to the underlying stream
            fileWriterAll.close();
            fileWriterF.close();
            fileWriterV.close();

        } catch (IOException ex) {

        }//End coords     file 


        //System.exit(0);

    }


    // The comparators---------------------------------------
    Comparator<List<Object>> comparator_rows = new Comparator<List<Object>>() 
    {

        @Override
        public int compare(List<Object> o1, List<Object> o2) {
            //Class cls = o1.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o1.size());

            //cls = o2.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o2.size());

            double i = (double)o2.get(0);
            double j = (double)o1.get(0);
            if (i < j) 
            {
                return 1;
            } 
            else if (i > j) 
            {
                return -1;
            } 
            else 
            {
                return 0;
            }
        }

    };

    //====================================================
    public void writeShp(AmaSim sim) 
    {
        System.out.println("================================================");
        System.out.println ("Writing households to Shp  ....................");
        System.out.println(" ");

        String dirCar = "";
        if(sim.singleRun) dirCar = "input_data/" + sim.testCommunityName + "/";
        else dirCar = "input_data/" + sim.community.name  + "/";

        //file =  dir + "RIO_A.shp";
        String file = "";
        if(sim.singleRun) file = dirCar + "VIVIENDAS_2.shp";
        else file = dirCar + "VIVIENDAS_.shp";

        URL houseGeometry = AmaSim.class.getResource(file);
        //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
        //System.out.println("fdds");
        try{
            ShapeFileImporter.read(houseGeometry, sim.homes);
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            System.exit(0);
        }

        Bag homes = sim.homes.getGeometries();
        //homes.shuffle(state.random);
        int num_homes = homes.size();;

        System.out.println ("Num households: " + num_homes);
        System.out.println ("Data size: " + data.size());
        int stats = -1;

        //if(num_homes >= 235)num_homes = 235;

        for (int ii = 0; ii < num_homes; ii++)
        {
            MasonGeometry MGhome = (MasonGeometry)homes.objs[ii];

            //String name = (String)MGhome.getStringAttribute("Name");
            int inMd = 0;
            String snMd = "";
            //if(!name.equals("Casa") && !name.equals("Infraestructura"))
            //{
            //    //System.out.println ("jump");
            //    //continue;

            //    snMd = "0";
         //    inMd = 0;
         //}
         //else
         //{
             //System.out.println (stats);
             stats++;

             double nMd = (double)data.get(stats).get(4) + (double)data.get(stats).get(5);
             inMd = (int)Math.round(nMd);
             snMd = Integer.toString(inMd);
         //}

         MGhome.addIntegerAttribute("nMalCases", inMd);

       }

       String outFileShp = sim.outPath + "avgDataHouseholdsShp";

       ShapeFileExporter.write(outFileShp, sim.homes);
       //System.out.println ("outFileShp: " + outFileShp);
       //System.exit(0);




    }



}
