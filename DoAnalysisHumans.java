/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.DateUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bbn.openmap.proj.coords.UTMPoint;
import com.bbn.openmap.proj.coords.LatLonPoint;

import java.net.URL;

import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;

import sim.util.geo.MasonGeometry;

public class DoAnalysisHumans
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean first = true;

    public String file_name;
    public List<String> titles = new ArrayList<String>();
    public List<List<Object>> data = new ArrayList<List<Object>>();
    public List<Double> nMalCases = new ArrayList<Double>();

    public String outFile = "";
    public String sheetName = "";

    public List<Double> id = new ArrayList<Double>();
    public List<Double> numPeople = new ArrayList<Double>();
    public List<Double> xCor = new ArrayList<Double>();
    public List<Double> yCor = new ArrayList<Double>();
    public List<Double> nCasesFalciparum = new ArrayList<Double>();
    public List<Double> nCasesVivax = new ArrayList<Double>();

    public Boolean stopA = false;

    HashMap<String, List<Object>> counts = new HashMap<String, List<Object>>();

    //====================================================
    public DoAnalysisHumans(AmaSim sim)
    {
       outFile = sim.outPath + "avgData_" + "Humans"  +  ".xls";
       sheetName = "Human";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile)
    {
      int stats  = 0;
      int numRow = 0;

      rFile = sim.outPath + rFile;
      System.out.println ("in File: " + rFile);
 
      try{
          Workbook workbook = WorkbookFactory.create(new FileInputStream(rFile) );

	     if(workbook.getNumberOfSheets() < 2)
	     {
		     System.out.println ("numSheets in the output workbook too little =  " + workbook.getNumberOfSheets());
		     return;
		     //System.exit(0);
	     }
 
  
             if(workbook.getSheet(sheetName) == null)
             {
                 stopA = true;
                 return;
             }
             
             Sheet sheet = workbook.getSheet(sheetName);

             List<Object> rData   = new ArrayList<Object>();
             List<List<Object>> dataF   = new ArrayList<List<Object>>();
             titles = new ArrayList<String>();

row:
             for(Row row : sheet)
             { 
               numRow++;
               rData   = new ArrayList<Object>();
               stats = 0;
               for(Cell cell : row)
               { 
                 stats++;

                 //String string = Integer.toString((int)cell.getNumericCellValue());
                 
                 //System.out.println ("-----------------------");
                 //System.out.println (cell.getCellType());
                 //System.exit(0);
                 
                 if(numRow == 0)continue row;

                 if(numRow == 1)
                 {
                    titles.add(cell.getRichStringCellValue().getString() ); 
                    continue;
                 }
                 
                 switch (cell.getCellType()) {
                     case Cell.CELL_TYPE_STRING:
                         rData.add(cell.getRichStringCellValue().getString() );
                         break;
                     case Cell.CELL_TYPE_NUMERIC:
                         rData.add((Object)cell.getNumericCellValue());
                         break;
                     default:
                         System.out.println ("Error cell value:"  + cell.getCellType());
                     

                 //System.exit(0);
                 }

                 //System.out.println ("-----------------------");
               }

              if(rData.size() == 0)continue;
              String aName = (String)rData.get(0); 

              List<Object> tmp = new ArrayList<Object>();

              double d = 0.0;

              if(counts.containsKey(aName))
              {
                  List<Object> oldData = counts.get(aName);

                  tmp.add(oldData.get(0));

                  d = (double)oldData.get(1);
                  d = d + (Double)rData.get(1); 
                  tmp.add(d);

                  d = (double)oldData.get(2);
                  d = d + (Double)rData.get(2); 
                  tmp.add(d);

                  d = (double)oldData.get(3);
                  d = d + (Double)rData.get(3); 
                  tmp.add(d);

                  counts.put(aName, tmp); 
              }
              else
              {
                  counts.put(aName, rData); 
              }


 
             }

             //System.exit(0);
  
  
  
          }
          catch(FileNotFoundException e)
              {
                 System.out.println(e);
              }
          catch(IOException e)
              {
                 System.out.println(e);
              }
          catch(InvalidFormatException e)
              {
                 System.out.println(e);
              }
 
 
    }

    //====================================================
    public void calcAverages(AmaSim sim, int numFiles)
    {
        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (String key : counts.keySet()) 
        {
            tmp = new ArrayList<Object>();
            //System.out.println("key: " + key + " value: " + counts.get(key));

            List<Object> oldData = counts.get(key);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d/numFiles;
            tmp.add(d);

            d = (double)oldData.get(2);
            d = d/numFiles;
            tmp.add(d);

            d = (double)oldData.get(3);
            d = d/numFiles;
            tmp.add(d);

            counts.put(key, tmp); 
        }



    }


 
    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs  (humans)................");
      System.out.println(" ");
 
      File theDir = new File(sim.outPath);
      String [] directoryContents = theDir.list();
 
      int numFiles = 0;
 
      for(String fileName: directoryContents) 
      {
          System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);
 
          if(words.length < 2)continue;
          if(!words[1].equals("xls"))continue;
 
          delims = "_";
          words = words[0].split(delims);
 
          int len = words.length;
 
          if(!words[0].equals("output"))continue;
 
          numFiles++;
          readFile(sim, fileName);
          if(stopA)return;
 
          System.out.println ("File number " + numFiles + " done ...........");

          first = false;
      }

      calcAverages(sim, numFiles); 

      writeInput(sim);
      
      try {
             writeToFile(sim);
              
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }

         //System.exit(0);

      System.out.println(" ");
      System.out.println ("Analysis outputs  (humans)...completed...............");
      System.out.println("================================================");
      System.out.println(" ");
    }

    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the averages file.");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet("Input File");
 
      int rownum = 0;
      int size = list.size();

      for(int i = 0; i < size; i++)
      {
          rownum++;
          Row row = sheet.createRow(rownum);

          String line = list.get(i);

          Cell cell = row.createCell(0);
          cell.setCellValue((String)line);

      }
 
      try {

          FileOutputStream out = 
             new FileOutputStream(new File(outFile));
          workbook.write(out);
          out.close();
          System.out.println("Excel written successfully..");
           
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }



    //====================================================
    public void writeToFile(AmaSim sim) throws IOException
    {

       System.out.println("================================================");
       System.out.println ("Writing  averages .............................");
       System.out.println(" ");
 
       FileInputStream file = new FileInputStream(outFile);
       System.out.println("Out file: " + outFile);
 
       HSSFWorkbook workbook = new HSSFWorkbook(file);
       HSSFSheet sheet;
 
       sheet = workbook.getSheet(sheetName);
       //If the sheet !exists a new sheet is created --------- 
       if(sheet == null)sheet = workbook.createSheet(sheetName);
 
       workbook.setSheetOrder(sheetName, 0);
 
       Cell cell = null;
 
       int lastRow = sheet.getLastRowNum();
       int cellnum = 0;
       //lastRow++;

       //System.out.println("Last row:" + lastRow);
       //lastRow++;

       //writes titles ----------------------------------
       int size = titles.size();
       Row row = sheet.createRow(lastRow++);
       cellnum = 0;
 
       for(int i = 0; i < size; i++)
       {
           String title = titles.get(i);
           
           cell = row.createCell(cellnum++);

           cell.setCellType(1);
           cell.setCellValue(title);
       }

       

       for (String key : counts.keySet()) 
       {

           cellnum = 0;
           row = sheet.createRow(lastRow++);
 
           List<Object> line_tmp = counts.get(key);

           //System.out.println("------------");
           //System.out.println(line_tmp.size());
           //System.exit(0);
 
           for (Object obj : line_tmp) 
           {
              cell = row.createCell(cellnum++);

              if(obj instanceof String)
              {
                 cell.setCellType(1);
                 cell.setCellValue((String)obj);
                 //System.out.println(obj);
                 //System.out.println("String....");
              }
              else if(obj instanceof Double)
              {
                 cell.setCellType(0);
                 cell.setCellValue((Double)obj);
                 //System.out.println("Double....");
              }
           }



       }
       //System.exit(0);
 
       file.close();
  
       File out_file = new File(outFile);
 
       FileOutputStream out = new FileOutputStream(out_file);
 
       workbook.write(out);

       out.close();

       System.out.println("Analysis Human data written successfully.....");
       //System.exit(0);


    }


    // The comparators---------------------------------------
    Comparator<List<Object>> comparator_rows = new Comparator<List<Object>>() 
    {

        @Override
        public int compare(List<Object> o1, List<Object> o2) {
            //Class cls = o1.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o1.size());

            //cls = o2.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o2.size());

            double i = (double)o2.get(0);
            double j = (double)o1.get(0);
            if (i < j) 
            {
                return 1;
            } 
            else if (i > j) 
            {
                return -1;
            } 
            else 
            {
                return 0;
            }
        }

    };




}
