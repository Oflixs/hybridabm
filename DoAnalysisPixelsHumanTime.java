/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.DateUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bbn.openmap.proj.coords.UTMPoint;
import com.bbn.openmap.proj.coords.LatLonPoint;

import java.net.URL;

import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;

import sim.util.geo.MasonGeometry;

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import sim.io.geo.*;
import sim.field.geo.GeomVectorField;

public class DoAnalysisPixelsHumanTime
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean first = true;

    public String file_name;
    //public List<String> titles = new ArrayList<String>();
    public String titles = "";
    public List<List<Object>> data = new ArrayList<List<Object>>();
    public List<Double> nMalCases = new ArrayList<Double>();

    public String outFile = "";
    public String sheetName = "";

    public List<Double> id = new ArrayList<Double>();
    public List<String> date = new ArrayList<String>();
    public List<Double> numPeople = new ArrayList<Double>();
    public List<Double> xCor = new ArrayList<Double>();
    public List<Double> yCor = new ArrayList<Double>();
    public List<Double> nCasesFalciparum = new ArrayList<Double>();
    public List<Double> nCases = new ArrayList<Double>();
    public List<Double> nCasesVivax = new ArrayList<Double>();

    HashMap<String, List<Object>> counts = new HashMap<String, List<Object>>();
    HashMap<String, List<Object>> countsNorm = new HashMap<String, List<Object>>();

    List<List<Object>> countsList = new ArrayList<List<Object>>();
    List<List<Object>> countsNormList = new ArrayList<List<Object>>();

    //====================================================
    public DoAnalysisPixelsHumanTime(AmaSim sim)
    {
       outFile = sim.outPath + "avgData_" + "PixelsHumanTime"  +  ".xls";
       sheetName = "PixelsHumanTime";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile)
    {
      int stats  = 0;
      int numRow = 0;
      String strLine = "";
      List<Object> tmp = new ArrayList<Object>();
      double d = 0.0;
      double d1 = 0.0;
      double d2 = 0.0;

      rFile = sim.outPath + rFile;
      System.out.println ("input File: " + rFile);
	  //System.exit(0);

      try
      {
          FileInputStream fstream = new FileInputStream(rFile);
          // get the object of datainputstream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          //read file line by line

          while ((strLine = br.readLine()) != null)   
          {
              stats++;            
              if(stats == 1)
              {
                  titles = strLine;
                  continue;
              }
              // print the content on the console
              strLine = strLine.trim();

              //System.out.println (strLine);

              String delims = " ";
              String[] words = strLine.split(delims);

              //double index = (double)Double.parseDouble(words[0]); 
              String index = words[0] + " " + words[1]; 

              tmp = new ArrayList<Object>();

              if(counts.containsKey(index))
              {
                  List<Object> oldData = counts.get(index);

                  tmp.add(oldData.get(0));

                  d = (double)oldData.get(2);
                  d = d +(double)Double.parseDouble(words[2]); 
                  tmp.add(d);

                  tmp.add(oldData.get(3));
                  tmp.add(oldData.get(4));

                  d2 = (double)oldData.get(5);
                  d = d2 +(double)Double.parseDouble(words[5]); 
                  tmp.add(d);

                  counts.put(index, tmp); 
              }
              else
              {
                  tmp.add((words[0] + " " + words[1]));
                  //System.out.println (words[0] + " " + words[1]);
                  //System.exit(0);

                  d = (double)Double.parseDouble(words[2]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[3]); 
                  tmp.add(d);

                  d1 = (double)Double.parseDouble(words[4]); 
                  tmp.add(d1);

                  d = (double)Double.parseDouble(words[5]); 
                  tmp.add(d);

                  counts.put(index, tmp); 
              }

          }

      }
      catch (Exception e)
      {//catch exception if any
          System.err.println("error: " + e.getMessage());
      }


    }
 
    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs  (Pixels Human)..........");
      System.out.println(" ");
 
      File theDir = new File(sim.outPath);
      String [] directoryContents = theDir.list();
 
      int numFiles = 0;
 
      for(String fileName: directoryContents) 
      {
          System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);

          int len = words.length;

          if(words.length < 2)continue;
          if(!words[1].equals("dat"))continue;
 
          delims = "_";
          words = words[0].split(delims);
 
          if(!words[0].equals("pixelHumanTime"))continue;
 
          numFiles++;
          readFile(sim, fileName);
 
          System.out.println ("File number " + numFiles + " done ...........");

          first = false;
      }
      //System.exit(0);

      calcAverages(sim, numFiles); 
      normalizeAverages(sim, numFiles); 

      //TreeMap sortedMap = new TreeMap(new rowComparator(counts));
      //counts.putAll(sortedMap);

      convertToLists();
      Collections.sort(countsList, new listComparator());
      Collections.sort(countsNormList, new listComparator());

      //for(int i = 0; i < countsList.size(); i++)
      //{
      //   if((double)countsList.get(i).get(6) > 0)
      //   {
      //       System.out.println(countsList.get(i).get(6));
      //   }
      //
      //}


      writeInput(sim);
      
      writeToSpatAn(sim);

      //writeShp(sim);

      //System.exit(0);
 
      System.out.println(" ");
      System.out.println ("Analysis outputs  (Pixels Human) completed ....");
      System.out.println("================================================");
      System.out.println(" ");
    }

    //====================================================
    public void convertToLists()
    {
        for (String key : counts.keySet()) 
        {

            List<Object> tmpList = counts.get(key);
            countsList.add(tmpList);

        }

        for (String key : countsNorm.keySet()) 
        {

            List<Object> tmpList = countsNorm.get(key);
            countsNormList.add(tmpList);

        }
    }

    //====================================================
    public void calcAverages(AmaSim sim, int numFiles)
    {

        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (String key : counts.keySet()) 
        {
            tmp = new ArrayList<Object>();
            //System.out.println("key: " + key + " value: " + counts.get(key));

            List<Object> oldData = counts.get(key);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d/numFiles;
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d/numFiles;
            tmp.add(d);

            counts.put(key, tmp); 
        }

    }

    //====================================================
    public void normalizeAverages(AmaSim sim, int numFiles)
    {

        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (String key : counts.keySet()) 
        {
            tmp = new ArrayList<Object>();
            //System.out.println("key: " + key + " value: " + counts.get(key));

            List<Object> oldData = counts.get(key);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d * numFiles;
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d * numFiles;
            tmp.add(d);

            countsNorm.put(key, tmp); 
        }

    }

    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the averages file.");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet("Input File");
 
      int rownum = 0;
      int size = list.size();

      for(int i = 0; i < size; i++)
      {
          rownum++;
          Row row = sheet.createRow(rownum);

          String line = list.get(i);

          Cell cell = row.createCell(0);
          cell.setCellValue((String)line);

      }
 
      try {

          FileOutputStream out = 
             new FileOutputStream(new File(outFile));
          workbook.write(out);
          out.close();
          System.out.println("Excel written successfully..");
           
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }

    //====================================================
    public void writeToSpatAn(AmaSim sim) 
    {
       System.out.println("================================================");
       System.out.println ("Writing  to spatial analysis out file Pix Human");
       System.out.println(" ");

       for (String key : counts.keySet()) 
       {
 
           List<Object> line_tmp = counts.get(key);
           List<Object> line_tmpNorm = countsNorm.get(key);

           //System.out.println("------------");
           //System.out.println(line_tmp.size());
           //System.exit(0);
 
           if((double)line_tmpNorm.get(4) == 0)continue;

           int cell = 0;
           for (Object obj : line_tmpNorm) 
           {
               cell++;

               //if(cell == 1)id.add((Double)obj);
               //System.out.println(obj);
               if(cell == 1)date.add((String)obj);
               if(cell == 2)numPeople.add((Double)obj);
               if(cell == 3)xCor.add((Double)obj); 
               if(cell == 4)yCor.add((Double)obj); 
               if(cell == 5)nCases.add((Double)obj);
           }

       }

       File newTextFileAll = null;
       FileWriter fileWriterAll = null;

       File newTextFileF = null;
       FileWriter fileWriterF = null;

       File newTextFileV = null;
       FileWriter fileWriterV = null;

       int len = 0;
       String line = "";
       int cases = 0;
       String hFile = "";

       hFile = sim.outPath + "/PixelsHumanTime";
       newTextFileAll = new File(hFile);
       if (!newTextFileAll.exists())
       {
         System.out.println("creating directory: " + hFile);
         newTextFileAll.mkdir();
       }

       String pathAll = sim.outPath + "/PixelsHumanTime";
      
       //case file -----------------
       try{
          hFile = pathAll + "/cases.cas";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          len = date.size();
          //System.out.println ("id size: " + id.size());
          
          for (int i = 0; i < len; i++)
          {
             //cases = (int)Math.round(nCasesFalciparum.get(i) + nCasesVivax.get(i));

             //System.out.println (date.get(i));

             String delims = " ";
             String[] words = date.get(i).split(delims);

             cases = (int)Math.round(nCases.get(i));
             //line = date.get(i)  + " "  + cases + " \n";
             line = words[0]  + " "  + cases + " " + words[1]  + " \n";
             fileWriterAll.write(line);

          }
          
          // force bytes to the underlying stream
          fileWriterAll.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End case file 

       //population file -----------------
       try{
          hFile = pathAll + "/pop.pop";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          len = date.size();
          
          for (int i = 0; i < len; i++)
          {
             line = date.get(i)  + " "  + Math.round(numPeople.get(i))  +  " \n";
             fileWriterAll.write(line);
          }
          
          // force bytes to the underlying stream
          fileWriterAll.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End population file 

       //coords     file -----------------
       try{
          hFile = pathAll + "/cor.cor";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          HashMap<String, String> tmp = new HashMap<String, String>();
          len = date.size();
          
          for (int i = 0; i < len; i++)
          {

             //System.out.println ("-------------");

             UTMPoint utm = new UTMPoint(Math.round(yCor.get(i)), Math.round(xCor.get(i)), 18, 'S');

             //System.out.println (utm.toString());

             LatLonPoint latlon = utm.toLatLonPoint();


             String delims = " ";
             String[] words = date.get(i).split(delims);


             line = words[0] + " " + latlon.getLatitude() + " "  + latlon.getLongitude()  +  " \n";

             if(tmp.containsKey(words[0]))continue;
             else tmp.put(words[0], line);
             //System.out.println (line);


             //System.out.println (line);
          }

          for (String key : tmp.keySet()) 
          {
              line = tmp.get(key);
              fileWriterAll.write(line);
          }

          
          // force bytes to the underlying stream
          fileWriterAll.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End coords     file 


       //System.exit(0);
    }



public class rowComparator implements Comparator, java.io.Serializable
{
   private static final long serialVersionUID = 1L;
   HashMap map;

   //----------------------------------------------------------
   public rowComparator(HashMap map)
   {
       this.map = map;
   }

   //----------------------------------------------------------
   public int compare(Object key1, Object key2) {
   {
       List<Object> o1 = (ArrayList<Object>)map.get(key1);
       List<Object> o2 = (ArrayList<Object>)map.get(key2);

       double i = (double)o1.get(4);
       double j = (double)o1.get(4);
       if (i < j) 
       {
           return 1;
       } 
       else if (i > j) 
       {
           return -1;
       } 
       else 
       {
           return 0;
       }


   }


}

}


public class listComparator implements Comparator<List<Object>>, java.io.Serializable
{   
   private static final long serialVersionUID = 1L;

   //----------------------------------------------------------
   public listComparator()
   {
   }

   //----------------------------------------------------------
   public int compare(List<Object> lo1, List<Object> lo2)
   {
       List<Object> l1 = (ArrayList<Object>)lo1;
       List<Object> l2 = (ArrayList<Object>)lo2;

 
      if((double)l1.get(4) < (double)l2.get(4))
          return 1;
      else if((double)l1.get(4) > (double)l2.get(4))
          return -1;
      else
          return 0;
   }


}

}

