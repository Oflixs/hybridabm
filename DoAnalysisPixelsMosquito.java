/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.DateUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bbn.openmap.proj.coords.UTMPoint;
import com.bbn.openmap.proj.coords.LatLonPoint;

import java.net.URL;

import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;

import sim.util.geo.MasonGeometry;

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import sim.io.geo.*;
import sim.field.geo.GeomVectorField;

public class DoAnalysisPixelsMosquito
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean first = true;

    public String file_name;
    //public List<String> titles = new ArrayList<String>();
    public String titles = "";
    public List<List<Object>> data = new ArrayList<List<Object>>();
    public List<Double> nMalCases = new ArrayList<Double>();

    public String outFile = "";
    public String sheetName = "";

    public List<Double> id = new ArrayList<Double>();
    public List<Double> numPeople = new ArrayList<Double>();
    public List<Double> xCor = new ArrayList<Double>();
    public List<Double> yCor = new ArrayList<Double>();
    public List<Double> nCasesFalciparum = new ArrayList<Double>();
    public List<Double> nCasesVivax = new ArrayList<Double>();

    HashMap<Double, List<Object>> counts = new HashMap<Double, List<Object>>();
    HashMap<Double, List<Object>> countsNorm = new HashMap<Double, List<Object>>();

    //====================================================
    public DoAnalysisPixelsMosquito(AmaSim sim)
    {
       outFile = sim.outPath + "avgData_" + "PixelsMosquito"  +  ".xls";
       sheetName = "PixelsMosquito";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile)
    {
      int stats  = 0;
      int numRow = 0;
      String strLine = "";
      List<Object> tmp = new ArrayList<Object>();
      double d = 0.0;

      rFile = sim.outPath + rFile;
      System.out.println ("input File: " + rFile);
	  //System.exit(0);

      try
      {
          FileInputStream fstream = new FileInputStream(rFile);
          // get the object of datainputstream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          //read file line by line

          while ((strLine = br.readLine()) != null)   
          {
              stats++;            
              if(stats == 1)
              {
                  titles = strLine;
                  continue;
              }
              // print the content on the console
              strLine = strLine.trim();

              //System.out.println (strLine);


              String delims = " ";
              String[] words = strLine.split(delims);

              double index = (double)Double.parseDouble(words[0]); 

              tmp = new ArrayList<Object>();

              if(counts.containsKey(index))
              {
                  List<Object> oldData = counts.get(index);

                  tmp.add(oldData.get(0));

                  d = (double)oldData.get(1);
                  d = d +(double)Double.parseDouble(words[1]); 
                  tmp.add(d);

                  tmp.add(oldData.get(2));
                  tmp.add(oldData.get(3));

                  d = (double)oldData.get(4);
                  d = d +(double)Double.parseDouble(words[4]); 
                  tmp.add(d);

                  d = (double)oldData.get(5);
                  d = d +(double)Double.parseDouble(words[5]); 
                  tmp.add(d);

                  counts.put(index, tmp); 
              }
              else
              {
                  d = (double)Double.parseDouble(words[0]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[1]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[2]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[3]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[4]); 
                  tmp.add(d);

                  d = (double)Double.parseDouble(words[5]); 
                  tmp.add(d);

                  counts.put(index, tmp); 
              }

          }

      }
      catch (Exception e)
      {//catch exception if any
          System.err.println("error: " + e.getMessage());
      }


    }

    //====================================================
    public void readData(AmaSim sim, List<Object> rData)
    {
        double index = (double)rData.get(0);
        double d  = 0.0;

        List<Object> tmp = new ArrayList<Object>();

        if(counts.containsKey(index))
        {
            tmp = new ArrayList<Object>();

            List<Object> oldData = counts.get(index);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d + (double)rData.get(1);
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d + (double)rData.get(4);
            tmp.add(d);

            d = (double)oldData.get(5);
            d = d + (double)rData.get(5);
            tmp.add(d);

            counts.put(index, tmp); 

        
        }
        else
        {
          counts.put(index, rData);  
        }
         

    }

 
    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs  (Pixels Mosquito)..........");
      System.out.println(" ");
 
      File theDir = new File(sim.outPath);
      String [] directoryContents = theDir.list();
 
      int numFiles = 0;
 
      for(String fileName: directoryContents) 
      {
          System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);

          int len = words.length;

          if(words.length < 2)continue;
          if(!words[1].equals("dat"))continue;
 
          delims = "_";
          words = words[0].split(delims);
 
          if(!words[0].equals("pixelMosquito"))continue;
 
          numFiles++;
          readFile(sim, fileName);
 
          System.out.println ("File number " + numFiles + " done ...........");

          first = false;
      }
      //System.exit(0);

      calcAverages(sim, numFiles); 
      normalizeAverages(sim, numFiles); 

      writeInput(sim);
      
      //try {
      //       writeToFile(sim);
      //        
      //   } catch (FileNotFoundException e) {
      //       e.printStackTrace();
      //   } catch (IOException e) {
      //       e.printStackTrace();
      //   }

         writeToSpatAn(sim);

         writeShp(sim);

         //System.exit(0);



 
      System.out.println(" ");
      System.out.println ("Analysis outputs  (Pixels Mosquito) completed ....");
      System.out.println("================================================");
      System.out.println(" ");
    }

    //====================================================
    public void calcAverages(AmaSim sim, int numFiles)
    {

        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (Double key : counts.keySet()) 
        {
            tmp = new ArrayList<Object>();
            //System.out.println("key: " + key + " value: " + counts.get(key));

            List<Object> oldData = counts.get(key);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d/numFiles;
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d/numFiles;
            tmp.add(d);

            d = (double)oldData.get(5);
            d = d/numFiles;
            tmp.add(d);

            counts.put(key, tmp); 
        }

    }

    //====================================================
    public void normalizeAverages(AmaSim sim, int numFiles)
    {

        double d  = 0.0;
        List<Object> tmp = new ArrayList<Object>();

        for (Double key : counts.keySet()) 
        {
            tmp = new ArrayList<Object>();
            //System.out.println("key: " + key + " value: " + counts.get(key));

            List<Object> oldData = counts.get(key);

            tmp.add(oldData.get(0));
            
            d = (double)oldData.get(1);
            d = d * numFiles;
            tmp.add(d);

            tmp.add(oldData.get(2));
            tmp.add(oldData.get(3));

            d = (double)oldData.get(4);
            d = d * numFiles;
            tmp.add(d);

            d = (double)oldData.get(5);
            d = d * numFiles;
            tmp.add(d);

            countsNorm.put(key, tmp); 
        }

    }



    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the averages file.");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet("Input File");
 
      int rownum = 0;
      int size = list.size();

      for(int i = 0; i < size; i++)
      {
          rownum++;
          Row row = sheet.createRow(rownum);

          String line = list.get(i);

          Cell cell = row.createCell(0);
          cell.setCellValue((String)line);

      }
 
      try {

          FileOutputStream out = 
             new FileOutputStream(new File(outFile));
          workbook.write(out);
          out.close();
          System.out.println("Excel written successfully..");
           
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }


/*
    //====================================================
    public void writeToFile(AmaSim sim) throws IOException
    {
       System.out.println("================================================");
       System.out.println ("Writing  averages .............................");
       System.out.println(" ");
 
       FileInputStream file = new FileInputStream(outFile);
 
       HSSFWorkbook workbook = new HSSFWorkbook(file);
       HSSFSheet sheet;
 
       sheet = workbook.getSheet(sheetName);
       //If the sheet !exists a new sheet is created --------- 
       if(sheet == null)sheet = workbook.createSheet(sheetName);
 
       workbook.setSheetOrder(sheetName, 0);
 
       Cell cell = null;
 
       int lastRow = sheet.getLastRowNum();
       int cellnum = 0;
       //lastRow++;

       //System.out.println("Last row:" + lastRow);
       //lastRow++;

       //writes titles ----------------------------------
       int size = titles.size();
       Row row = sheet.createRow(lastRow++);
       cellnum = 0;
 
       for(int i = 0; i < size; i++)
       {
           String title = titles.get(i);
           
           cell = row.createCell(cellnum++);

           cell.setCellType(1);
           cell.setCellValue(title);
       }

       

       for (Double key : counts.keySet()) 
       {


           cellnum = 0;
           row = sheet.createRow(lastRow++);
 
           List<Object> line_tmp = counts.get(key);
           List<Object> line_tmpNorm = countsNorm.get(key);

           //System.out.println("------------");
           //System.out.println(line_tmp.size());
           //System.exit(0);
 
           for (Object obj : line_tmp) 
           {
              cell = row.createCell(cellnum++);

              if(obj instanceof String)
              {
                 cell.setCellType(1);
                 cell.setCellValue((String)obj);
                 //System.out.println(obj);
                 //System.out.println("String....");
              }
              else if(obj instanceof Double)
              {
                 cell.setCellType(0);
                 cell.setCellValue((Double)obj);
                 //System.out.println("Double....");
              }
           }

           cellnum = 0;
           for (Object obj : line_tmpNorm) 
           {
              cell = row.createCell(cellnum++);

              if(cellnum == 1)id.add((Double)obj);
              if(cellnum == 2)numPeople.add((Double)obj);
              if(cellnum == 3)xCor.add((Double)obj); 
              if(cellnum == 4)yCor.add((Double)obj); 
              if(cellnum == 5)nCasesFalciparum.add((Double)obj);
              if(cellnum == 6)nCasesVivax.add((Double)obj); 

           }


       }
       //System.exit(0);
 
       file.close();
  
       File out_file = new File(outFile);
 
       FileOutputStream out = new FileOutputStream(out_file);
 
       workbook.write(out);
       out.close();
       System.out.println("Analysis data written successfully.....");

       


    }
*/

    //====================================================
    public void writeToSpatAn(AmaSim sim) 
    {
       System.out.println("================================================");
       System.out.println ("Writing  to spatial analysis out file..........");
       System.out.println(" ");


       for (Double key : counts.keySet()) 
       {
 
           List<Object> line_tmp = counts.get(key);
           List<Object> line_tmpNorm = countsNorm.get(key);

           //System.out.println("------------");
           //System.out.println(line_tmp.size());
           //System.exit(0);
 
           if(!((double)line_tmpNorm.get(4) > 0 || (double)line_tmpNorm.get(5) > 0))continue;

           int cell = 0;
           for (Object obj : line_tmpNorm) 
           {
               cell++;

               if(cell == 1)id.add((Double)obj);
               if(cell == 2)numPeople.add((Double)obj);
               if(cell == 3)xCor.add((Double)obj); 
               if(cell == 4)yCor.add((Double)obj); 
               if(cell == 5)nCasesFalciparum.add((Double)obj);
               if(cell == 6)nCasesVivax.add((Double)obj); 

           }


       }
 


       File newTextFileAll = null;
       FileWriter fileWriterAll = null;

       File newTextFileF = null;
       FileWriter fileWriterF = null;

       File newTextFileV = null;
       FileWriter fileWriterV = null;

       int len = 0;
       String line = "";
       int cases = 0;
       String hFile = "";
       int idi = 0;

       hFile = sim.outPath + "/PixelsMosquito";
       newTextFileAll = new File(hFile);
       if (!newTextFileAll.exists())
       {
         System.out.println("creating directory: " + hFile);
         newTextFileAll.mkdir();
       }

       hFile = sim.outPath + "/PixelsMosquitoFalciparum";
       newTextFileF = new File(hFile);
       if (!newTextFileF.exists())
       {
         System.out.println("creating directory: " + hFile);
         newTextFileF.mkdir();
       }

       hFile = sim.outPath + "/PixelsMosquitoVivax";
       newTextFileV = new File(hFile);
       if (!newTextFileV.exists())
       {
         System.out.println("creating directory: " + hFile);
         newTextFileV.mkdir();
       }

       String pathAll = sim.outPath + "/PixelsMosquito";
       String pathF = sim.outPath + "/PixelsMosquitoFalciparum";
       String pathV = sim.outPath + "/PixelsMosquitoVivax";
      
       //case file -----------------
       try{
          hFile = pathAll + "/cases.cas";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          hFile = pathF + "/cases.cas";
          newTextFileF = new File(hFile);
          fileWriterF = new FileWriter(newTextFileF);

          hFile = pathV + "/cases.cas";
          newTextFileV = new File(hFile);
          fileWriterV = new FileWriter(newTextFileV);

          len = id.size();
          System.out.println ("id size: " + id.size());
          
          for (int i = 0; i < len; i++)
          {
             //cases = (int)Math.round(nCasesFalciparum.get(i) + nCasesVivax.get(i));

             //System.out.println (id.get(i));

             cases = (int)Math.round(nCasesVivax.get(i) + nCasesFalciparum.get(i) );
             idi = (int)Math.round(id.get(i));
             line = idi + " " + cases + " \n";
             fileWriterAll.write(line);

             cases = (int)Math.round(nCasesFalciparum.get(i) );
             idi = (int)Math.round(id.get(i));
             line = idi + " " + cases + " \n";
             fileWriterF.write(line);

             cases = (int)Math.round(nCasesVivax.get(i) );
             idi = (int)Math.round(id.get(i));
             line = idi + " " + cases + " \n";
             fileWriterV.write(line);
          }
          
          // force bytes to the underlying stream
          fileWriterAll.close();
          fileWriterF.close();
          fileWriterV.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End case file 

       //population file -----------------
       try{
          hFile = pathAll + "/pop.pop";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          hFile = pathF + "/pop.pop";
          newTextFileF = new File(hFile);
          fileWriterF = new FileWriter(newTextFileF);

          hFile = pathV + "/pop.pop";
          newTextFileV = new File(hFile);
          fileWriterV = new FileWriter(newTextFileV);

          len = id.size();
          
          for (int i = 0; i < len; i++)
          {
             idi = (int)Math.round(id.get(i));
             line = idi + " 11 " + Math.round(numPeople.get(i))  +  " \n";
             fileWriterAll.write(line);
             fileWriterF.write(line);
             fileWriterV.write(line);
          }
          
          // force bytes to the underlying stream
          fileWriterAll.close();
          fileWriterF.close();
          fileWriterV.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End population file 

       //coords     file -----------------
       try{
          hFile = pathAll + "/cor.cor";
          newTextFileAll = new File(hFile);
          fileWriterAll = new FileWriter(newTextFileAll);

          hFile = pathF + "/cor.cor";
          newTextFileF = new File(hFile);
          fileWriterF = new FileWriter(newTextFileF);

          hFile = pathV + "/cor.cor";
          newTextFileV = new File(hFile);
          fileWriterV = new FileWriter(newTextFileV);

          len = id.size();
          
          for (int i = 0; i < len; i++)
          {

             //System.out.println ("-------------");

             UTMPoint utm = new UTMPoint(Math.round(yCor.get(i)), Math.round(xCor.get(i)), 18, 'S');

             //System.out.println (utm.toString());

             LatLonPoint latlon = utm.toLatLonPoint();


             idi = (int)Math.round(id.get(i));
             line = idi + " " + latlon.getLatitude() + " "  + latlon.getLongitude()  +  " \n";
             //System.out.println (line);

             fileWriterAll.write(line);
             fileWriterF.write(line);
             fileWriterV.write(line);

             //line = idi + " " + Math.round(xCor.get(i)) + " "  + Math.round(yCor.get(i))  +  " \n";
             //System.out.println (line);
          }
          
          // force bytes to the underlying stream
          fileWriterAll.close();
          fileWriterF.close();
          fileWriterV.close();

        } catch (IOException ex) {
                 System.out.println(ex);
            
        }//End coords     file 


       //System.exit(0);
          
    }


    // The comparators---------------------------------------
    Comparator<List<Object>> comparator_rows = new Comparator<List<Object>>() 
    {

        @Override
        public int compare(List<Object> o1, List<Object> o2) {
            //Class cls = o1.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o1.size());

            //cls = o2.getClass();
            //System.out.println("The type of the object is: " + cls.getName());
            //System.out.println("len: " + o2.size());

            double i = (double)o2.get(0);
            double j = (double)o1.get(0);
            if (i < j) 
            {
                return 1;
            } 
            else if (i > j) 
            {
                return -1;
            } 
            else 
            {
                return 0;
            }
        }

    };

    //====================================================
    public void writeShp(AmaSim sim) 
    {
       System.out.println("================================================");
       System.out.println ("Writing pixels mosquito to Shp  ..................");
       System.out.println(" ");

       String dirCar = "";
       if(sim.singleRun) dirCar = "input_data/" + sim.testCommunityName + "/";
       else dirCar = "input_data/" + sim.community.name  + "/";

       String file = "";
       if(sim.singleRun) file = dirCar + "pixelsMosquito" + sim.testCommunityName  + ".shp";
       else file = dirCar + "pixelsMosquito" + sim.community.name  + ".shp";

       GeomVectorField pixels = new GeomVectorField(sim.geoGridWidth, sim.geoGridHeight);

       for (Double key : counts.keySet()) 
       {
           List<Object> line_tmp = counts.get(key);
           Coordinate coords  = new Coordinate((double)line_tmp.get(2), (double)line_tmp.get(3));
           Point point = new GeometryFactory().createPoint(coords);

           MasonGeometry MGdata = new MasonGeometry(point);

           int inMd = 0;
           String snMd = "";

           double nMd = (double)line_tmp.get(4) + (double)line_tmp.get(5);
           inMd = (int)Math.round(nMd/(double)line_tmp.get(1));
           snMd = Double.toString(nMd);
           //System.out.println (snMd);

           MGdata.addDoubleAttribute("nMalCases", nMd/(double)line_tmp.get(1));

           if(nMd > 0)
           {
               //System.out.println (nMd + " " + inMd);
               pixels.addGeometry(MGdata);
           }

       }

    String outFileShp = sim.outPath + "avgDataPixelsMosquito";

    ShapeFileExporter.write(outFileShp, pixels);


    //System.out.println ("outFileShp: " + outFileShp);
    //System.exit(0);


    }



}
