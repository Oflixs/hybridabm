/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import java.io.PrintStream;
import java.net.URL;
import java.nio.charset.Charset;

import com.hexiong.jdbf.DBFReader;
import com.hexiong.jdbf.DBFWriter;
import com.hexiong.jdbf.JDBFException;
import com.hexiong.jdbf.JDBField;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;

import sim.field.geo.GeomVectorField;
import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;
import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Geometry;


import org.apache.poi.ss.usermodel.DateUtil;

public class DoAnalysisShp
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    boolean firstGeo = true;
    boolean firstMos = true;

    public String file_name;
    public List<String> titlesGeo = new ArrayList<String>();
    public List<String> titlesMos = new ArrayList<String>();

    Bag dataGeo = new Bag();
    Bag dataMos = new Bag();

    public int maxNCpGeo = -10000;
    public int maxNCpMos = -10000;

    public String outFile = "";
    public String outFileLocal = "";
    public String outFileGeo = "";
    public String outFileMos = "";
    public String outFileLocalGeo = "";
    public String outFileLocalMos = "";
    public String outDir = "";
    public String outDirLocal = "";

    public String templateFileDBFGeo = "";
    public String templateFileSHPGeo = "";
    public String templateFileSHXGeo = "";

    public String templateFileDBFMos = "";
    public String templateFileSHPMos = "";
    public String templateFileSHXMos = "";

    public List<String> colNames      = new ArrayList<String>();
    public List<String> colNamesGeo   = new ArrayList<String>();
    public List<String> colNamesMos   = new ArrayList<String>();

    //====================================================
    public DoAnalysisShp(AmaSim sim)
    {
       outDir = sim.outPath + "OutShps/";
       outFile = outDir  + "avgData_" + sim.outDir;

       outDirLocal = "outputs/" + sim.testCommunityName + "/" + sim.outDirCom + "/OutShps/";

       outFileLocal = outDirLocal  + "avgData_" + sim.outDir;

       outFileLocalGeo = outFileLocal + "Geo";
       outFileLocalMos = outFileLocal + "Mos";
    }

    //====================================================
    public void readFile(AmaSim sim, String rFile, String type)
    {
        rFile = outDirLocal + rFile;
        System.out.println("Processing File: " + rFile + "  ");
        System.out.println(" ");

        GeomVectorField cps = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        GeomVectorField toWrite = new GeomVectorField(sim.geoGridWidth, sim.geoGridHeight);

        String outFileSHP = "sim/app/AmaSim/" + rFile;
        //System.out.println(outFileSHP);
        //System.out.println(templateFileSHPGeo);

        URL geometry = AmaSim.class.getResource(rFile);
        //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
        //System.out.println("fdds");
        try{
            ShapeFileImporter.read(geometry, cps);
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            System.exit(0);
        }


        Bag cpsG = cps.getGeometries();
        int len = cpsG.size();;
        double dValue = 0.0;
        int iValue = 0;

        System.out.println("Len shp Geo: " + len);
        for (int i = 0; i < len; i++)
        {
            if(i % 1000 == 0)System.out.println(i);
            MasonGeometry MGcps = (MasonGeometry)cpsG.objs[i];

            if(type.equals("Geo"))
            {

                int contains = dataContains(dataGeo, MGcps);
                if(contains < 0)
                {
                    //System.out.println("geoData NOT contains mg");

                    dataGeo.add(MGcps);
                }
                else
                {
                    //System.out.println("geoData contains mg");

                    MasonGeometry mg = (MasonGeometry)dataGeo.get(contains);

                    dValue = (double)mg.getDoubleAttribute("elevation") 
                        + (double)MGcps.getDoubleAttribute("elevation");
                    mg.addDoubleAttribute("elevation", dValue);

                    iValue = (int)mg.getIntegerAttribute("id");
                    mg.addIntegerAttribute("id", iValue);

                    iValue = (int)mg.getIntegerAttribute("nOvipos") 
                        + (int)MGcps.getIntegerAttribute("nOvipos");
                    mg.addIntegerAttribute("nOvipos", iValue);

                    iValue = (int)mg.getIntegerAttribute("nMalCases") 
                        + (int)MGcps.getIntegerAttribute("nMalCases");
                    mg.addIntegerAttribute("nMalCases", iValue);

                    iValue = (int)MGcps.getIntegerAttribute("buffer");
                    mg.addIntegerAttribute("buffer", iValue);

                    dataGeo.set(contains, mg);

                }
            }
            else if(type.equals("Mos"))
            {
                int contains = dataContains(dataMos, MGcps);
                if(contains < 0)
                {
                    //System.out.println("mosData NOT contains mg");
                    
                    dataMos.add(MGcps);
                }
                else
                {
                    //System.out.println("mosData contains mg");

                    MasonGeometry mg = (MasonGeometry)dataMos.get(contains);

                    iValue = (int)mg.getIntegerAttribute("id");
                    mg.addIntegerAttribute("id", iValue);

                    iValue = (int)mg.getIntegerAttribute("NMosqu") 
                        + (int)MGcps.getIntegerAttribute("NMosqu");
                    mg.addIntegerAttribute("NMosqu", iValue);

                    dataMos.set(contains, mg);

                }
            }
        }

        //System.exit(0);

 
    }

    //====================================================
    public void calcAverages(AmaSim sim, int nFiles, String type)
    {
        double dValue = 0.0;
        int iValue = 0;
        int len = 0;
        if(type.equals("Geo")) len = dataGeo.size(); 
        else if(type.equals("Mos")) len = dataMos.size(); 
        MasonGeometry mg = null;

        for (int i = 0; i < len; i++)
        {
            //System.out.println(ii);
            if(type.equals("Geo"))
            {
                mg = (MasonGeometry)dataGeo.get(i);

                dValue = (double)mg.getDoubleAttribute("elevation")/nFiles;
                mg.addDoubleAttribute("elevation", dValue);

                iValue = (int)mg.getIntegerAttribute("id");
                mg.addIntegerAttribute("id", iValue);

                iValue = (int)mg.getIntegerAttribute("nOvipos");
                dValue = (double)iValue/nFiles;
                mg.addDoubleAttribute("nOvipos", dValue);

                iValue = (int)mg.getIntegerAttribute("nMalCases");
                dValue = (double)iValue/nFiles;
                mg.addDoubleAttribute("nMalCases", dValue);

                dataGeo.set(i, mg);
            }
            else if(type.equals("Mos"))
            {
                mg = (MasonGeometry)dataMos.get(i);

                iValue = (int)mg.getIntegerAttribute("id");
                mg.addIntegerAttribute("id", iValue);

                iValue = (int)mg.getIntegerAttribute("NMosqu");
                dValue = (double)iValue/nFiles;
                mg.addDoubleAttribute("NMosqu", dValue);

                dataMos.set(i, mg);
            }
        }
    }



    //====================================================
    public Integer dataContains(Bag data, MasonGeometry mg)
    {

       for(int i = 0; i < data.size(); i++)
       {
           MasonGeometry mgRead = (MasonGeometry)data.get(i);

           int dataId = mgRead.getIntegerAttribute("id");
           int mgId = mg.getIntegerAttribute("id");

           if(dataId == mgId)return i;
       
       }

       return -1;
    }

    //====================================================
    public void read(AmaSim sim)
    {
      System.out.println("================================================");
      System.out.println ("Analyzing the outputs (Shps)...................");
      System.out.println(" ");

      File theDir = new File(outDir);
      String [] directoryContents = theDir.list();
      //System.out.println (theDir);
 
      int numFilesGeo = 0;
      int numFilesMos = 0;
 
      for(String fileName: directoryContents) 
      {
          //System.out.println (fileName);
          String delims = "\\.";
          String[] words = fileName.split(delims);

          //System.out.println (words.length);
 
          if(words.length < 2)continue;


          //if(!words[1].equals("dbf"))continue;
 
          delims = "_";
          String[] words2 = words[0].split(delims);

          int len = words2.length;
 
          if(!words2[0].equals("geoShp") && !words2[0].equals("mosShp"))continue;
 

          if(words2[0].equals("geoShp"))
          {
              if(words[1].equals("shp"))
              {
                  templateFileSHPGeo = outDirLocal + fileName;
                  readFile(sim, fileName, "Geo");
                  numFilesGeo++;
                  System.out.println ("File number Geo " + numFilesGeo + " done ...........");
              }
          }
          else if(words2[0].equals("mosShp"))
          {
              if(words[1].equals("shp"))
              {
                  templateFileSHPMos = outDirLocal + fileName;
                  readFile(sim, fileName, "Mos");
                  numFilesMos++;
                  System.out.println ("File number Mos " + numFilesMos + " done ...........");
              }
          }
      }

      calcAverages(sim, numFilesGeo, "Geo"); 
      calcAverages(sim, numFilesMos, "Mos"); 

      //System.exit(0);

      try {
             writeToFile("Geo");
             writeToFile("Mos");
              
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }

 
      System.out.println(" ");
      System.out.println ("Analysis outputs  (Shp) completed ............");
      System.out.println("================================================");
      System.out.println(" ");
    }


    //====================================================
    public void writeToFile(String type) throws IOException
    {
        GeomVectorField toWrite = new GeomVectorField(60, 60);
        Bag data = null;

        String outFileSHP = "";

        if(type.equals("Geo"))
        {
            outFileSHP = "sim/app/AmaSim/" + outFileLocalGeo;
            data = dataGeo;
        }
        else if(type.equals("Mos"))
        {
            outFileSHP = "sim/app/AmaSim/" + outFileLocalMos;
            data = dataMos;
        }

        for(int i = 0 ; i < data.size(); i++)
        {
            MasonGeometry mg = (MasonGeometry)data.get(i);
            toWrite.addGeometry(mg);

            //if(mg == null)System.out.println(i);
        }

        System.out.println(outFileSHP);
        ShapeFileExporter.write(outFileSHP, toWrite);
    }



}
