/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;



//----------------------------------------------------
public class Household implements Steppable
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;


   //====================================================
   public Stoppable stopper;

   int xpos;
   int ypos;
   public Integer getXpos(){return xpos;};
   public Integer getYpos(){return ypos;};
   public void setXpos(int x){xpos = x;};
   public void setYpos(int y){ypos = y;};

   public Point geoPoint = null;
   public Point getGeoPoint(){return geoPoint;};
   public void setGeoPoint(Point p){geoPoint = p;};

   public Bag people = new Bag();
   public Bag getPeople(){return people;};
   public void setPeople(Bag p){people = p;};

   public Bag residents = new Bag();
   public Bag getResidents(){return residents;};
   public void setResidents(Bag p){residents = p;};

   public void removeHuman(Human h)
   {
       //if(!people.contains(h))
       //{
       //    System.out.println ("The Household does not contain the human");
       //    System.exit(0);
       //}
       people.remove(h);
   }
   public void addHuman(Human h)
   {
       if(!people.contains(h))
       people.add(h);
   }

   //Malaria state variables----------------------------
   public Integer getNumPeople(){return people.size();};

   public MasonGeometry masonGeometry = null;
   public void setMasonGeometry(MasonGeometry mg){masonGeometry = mg;};
   public MasonGeometry getMasonGeometry(){return masonGeometry;};

   public CoverPixel cpPosition = null;
   public void setCpPosition(CoverPixel mg){cpPosition = mg;};
   public CoverPixel getCpPosition(){return cpPosition;};

   public int identity = 0;
   public Integer getIdentity(){return identity;};
   public void setIdentity(int y){identity = y;};

   public int weeklyMalariaCases = 0;
   public Integer getWeeklyMalariaCases(){return weeklyMalariaCases;};
   public void setWeeklyMalariaCases(int y){weeklyMalariaCases = y;};
   public void increaseWeeklyMalariaCases(){weeklyMalariaCases++;};

   public int weeksCounter = 0;
   public Integer getWeeksCounter(){return weeksCounter;};
   public void setWeeksCounter(int y){weeksCounter = y;};
   public void increaseWeeksCounter(){weeksCounter++;};

   public double avgMalariaIncidence = 0;
   public double getAvgMalariaIncidence(){return avgMalariaIncidence;};
   public void setAvgMalariaIncidence(double y){avgMalariaIncidence = y;};
   public void increaseAvgMalariaIncidence(double y){avgMalariaIncidence = avgMalariaIncidence + y;};

   public int numFalciparumCases = 0;
   public Integer getNumFalciparumCases(){return numFalciparumCases;};
   public void setNumFalciparumCases(int y){numFalciparumCases = y;};
   public void increaseNumFalciparumCases(){numFalciparumCases++;};

   public int numVivaxCases = 0;
   public Integer getNumVivaxCases(){return numVivaxCases;};
   public void setNumVivaxCases(int y){numVivaxCases = y;};
   public void increaseNumVivaxCases(){numVivaxCases++;};

   public boolean gotMalaria = false;
   public boolean getGotMalaria(){return gotMalaria;};
   public void setGotMalaria(boolean y){gotMalaria = y;};

   //isOccupied = true means that domestic animals live around
   public Boolean isOccupied = false;

   //----------------------------------
   public Integer getNumInfectedPeople()
   {
       int stats = 0;
       for(int i = 0; i < people.size(); i++)
       {
           Human human = ((Human)people.objs[i]);
           if(human.getHInfected()){stats++;}
       }
       return stats;
   }

   //----------------------------------
   public Integer getNumInfectiousPeople()
   {
       int stats = 0;
       for(int i = 0; i < people.size(); i++)
       {
           Human human = ((Human)people.objs[i]);
           if(human.getHInfectious()){stats++;}
       }
       return stats;
   }

   //----------------------------------
   public Double getFractInfectedPeople()
   {
       double fip;
       fip = (double)getNumInfectedPeople() + (double)getNumInfectiousPeople();
       fip = fip / (double)getNumPeople();
       return fip;
   }

   //====================================================
   public Household(SimState state, CoverPixel gcp, Point pp)
   {
       sim = (AmaSim)state;

       cpPosition = gcp;

       geoPoint = pp;

       //this.stopper = sim.schedule.scheduleRepeating(this);
       
       sim.houseIdent++;
       identity = sim.houseIdent;
   }

   //====================================================
   public void step(SimState state)
   {
       sim = (AmaSim)state;
   }

   //====================================================
   public void generatePeople(SimState state)
   {
      int numPeople = 0;
      int stats = 1;

      double random = state.random.nextGaussian() * sim.avgPeoplePerHousehold.get(1) + sim.avgPeoplePerHousehold.get(0); 

      numPeople = (int)Math.round(random);
      if(numPeople <= 0)numPeople = 1;

      //System.out.println ("Num people: " + numPeople); 

      while(stats <= numPeople)
      {
          Boolean protect = false;
          Boolean move = false;

          double ran = state.random.nextDouble();
          if(ran <= sim.fractProtectedPeople)protect = true;

          ran = state.random.nextDouble();
          if(ran <= sim.fractPeopleMoveAround)move = true;

          Human h = new Human(state, cpPosition, this);

          residents.add(h);

          isOccupied = true;

          if(protect)h.setIsProtected(true);
          if(protect)h.setIsProtectedInTheHousehold(true);
          if(move)h.setIsMovible(true);

          stats++;
      }

   }


   //====================================================
   public void malariaInOneYeaToHousehold(SimState state)
   {
       int numMax = 4000;
       Bag nPeople = people;
       nPeople.shuffle(state.random);

       int stats = 1;
       //System.out.println ("Num people: " + people.size()); 
       for(int i = 0; i < nPeople.size(); i++)
       {
           Human human = ((Human)nPeople.objs[i]);
           if(human.getMalariaInOneYear() && stats < numMax)
           {
              human.setMIOYOn();
              stats++;
           }
       }

   
   }




}


