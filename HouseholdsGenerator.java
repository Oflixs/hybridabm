/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.Geometry;

public class HouseholdsGenerator implements Steppable
{
   private static final long serialVersionUID = 1L;

   //====================================================
   public HouseholdsGenerator(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

   }


   //====================================================
   public void  setUpOneMalaria(final SimState state)
   {
       final AmaSim sim = (AmaSim)state;

       Bag hh = sim.household_bag;
       for(int i = 0; i < hh.size(); i++)
       {
           Household  h = ((Household )hh.objs[i]);
           
           double ran = state.random.nextDouble();

           Bag people = h.getPeople();

           if(ran < 0.5)
           {
              for(int j = 0; j < people.size(); j++)
              {
                  Human human = ((Human)people.objs[j]);

                  human.setMIOYOn();
              }
           }
           else
           {
              for(int j = 0; j < people.size(); j++)
              {
                  Human human = ((Human)people.objs[j]);

                  human.setMIOYOff();
              }

           
           }

       }



   }
   
   //====================================================
   public void initHh(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

      System.out.println("================================================");
      System.out.println("Initialize households  and humans...............");
      System.out.println(" ");

      int stats = 0;


      Bag homes = sim.homes.getGeometries();
      //homes.shuffle(state.random);
      int num_homes = homes.size();;

      System.out.println ("Num households: " + num_homes);
      
      //if(num_homes >= 235)num_homes = 235;

      for (int ii = 0; ii < num_homes; ii++)
      {
         MasonGeometry MGhome = (MasonGeometry)homes.objs[ii];
         Point pHome = (Point)MGhome.getGeometry();

         //String name = (String)MGhome.getStringAttribute("Name");
         //if(!name.equals("Casa") && !name.equals("Infraestructura"))continue;

         //System.out.println("Household coordinates:" + pHome.getX() + ", " + pHome.getY()); 
         //System.out.println("Stats: " + stats);

         stats++;

         Integer[] coords  = sim.utils.getDiscreteCoordinatesPoint(pHome, "geo");


         int inx = (int)Math.floor((coords[0]  / 500)); 
         int iny = (int)Math.floor((coords[1]  / 500)); 

         inx = 0;
         iny = 0;

         //System.out.println("intx: " + inx + " iny: " + iny);
                  

         CoverPixel cp = sim.utils.getCoverPixelFromCoords(state, coords[0] + inx, coords[1] + iny, "geo");

         
         if(cp.getIsWater() == 1)
         {
            //System.out.println ("You have got a house in the water");
            //System.out.println ("Problems.....");
            //System.exit(0);
         }

         //System.out.println("----------------------");
         //System.out.println(coords[0]  + " " + coords[1]);

         generateHousehold(state, cp, pHome);
      }

      if(sim.noMosHouse)noMosHouse(state);

      /*
      //Tocheck
      int stats = 0;
      int stats1 = 0;

      for(int i = 0; i < sim.household_bag.size(); i++)
      {

        Household hh = ((Household)sim.household_bag.objs[i]);

        CoverPixel cp = hh.getCpPosition();
        Point pp = hh.getGeoPoint();


       double x = pp.getX();
       double y = pp.getY();



       //System.out.println ("Num households: " + num_homes);

       //if(num_homes >= 235)num_homes = 235;

       for (int ii = 0; ii < num_homes; ii++)
       {
           MasonGeometry MGhome = (MasonGeometry)homes.objs[ii];
           Point pHome = (Point)MGhome.getGeometry();


           double xh = pHome.getX();
           double yh = pHome.getY();

           if(cp.square.contains(pHome))
           {
               stats1++;
               System.out.println ( "Contains");
           
           }

           if(x == xh && y == yh)
           {
               System.out.println ( "here equals!!!!");
               stats++;
               break;

           }



       }






      }
      System.out.println ("stats: " + stats);
      System.out.println ("stats1: " + stats1);
      */


      System.out.println (sim.household_bag.size() + " households generated");
      if(sim.household_bag.size() < 0)
      {
          System.out.println ("Something is wrong with your households sph");
          System.exit(0);
      }
      //System.exit(0);

   }


   //====================================================
   public void step(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

   }

   //====================================================
   public void generateHousehold(final SimState state, CoverPixel cp, Point pp)
   {
      final AmaSim sim = (AmaSim)state;

  
      Household h = new Household(state, cp, pp);
      //System.out.println("Households: " + cp.getXcor() + " " + cp.getYcor());

      sim.householdGrid.setObjectLocation(h,
              sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[0],
              sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[1]
              );
      sim.household_bag.add(h);
   }

   //====================================================
   public void noMosHouse(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

      Bag cps = sim.geoGrid.elements();
      int num_pixels = cps.size();

      CoverPixel cpH = null;
      CoverPixel cp = null;

      int lenH = sim.household_bag.size();
      Household hh = null;



      double maxElev = -100000;
      double minElev =  100000;
      double elevation = 0.0;

      for(int i =  0; i < num_pixels; i++)
      {
         cp = ((CoverPixel)cps.objs[i]);

         MasonGeometry mg = cp.getMasonGeometry();
         Polygon square = (Polygon)mg.getGeometry();

         for(int j = 0; j < lenH; j++)
         {
             hh = ((Household)sim.household_bag.objs[j]);
   
             cpH = hh.getCpPosition();
    
             MasonGeometry mgH = cpH.getMasonGeometry();
             Polygon squareH = (Polygon)mgH.getGeometry();
             Point centerH = squareH.getCentroid();
   
             Geometry bufferH = centerH.buffer(sim.noMosHouseRadiusHigh);
   
             if(bufferH.covers(square))
             {
                cp.setIsBufferNoMos(true); 
                cp.setIsWater(0); 
                //if(cp.getIsWaterLow() == 1)
                //{
                //    System.out.println("Buffer zone!!!!!");
                //}
                //System.out.println("Buffer zone!!!!!");
             }

             bufferH = centerH.buffer(sim.noMosHouseRadiusLow);

             if(bufferH.covers(square))
             {
                cp.setIsBufferNoMos(false); 
                //System.out.println("Buffer zone!!!!!");
             }
   
   
         }
      }


      for(int i =  0; i < num_pixels; i++)
      {
         cp = ((CoverPixel)cps.objs[i]);

         MasonGeometry mg = cp.getMasonGeometry();
         Polygon square = (Polygon)mg.getGeometry();

         for(int j = 0; j < lenH; j++)
         {
             hh = ((Household)sim.household_bag.objs[j]);
   
             cpH = hh.getCpPosition();
    
             MasonGeometry mgH = cpH.getMasonGeometry();
             Polygon squareH = (Polygon)mgH.getGeometry();
             Point centerH = squareH.getCentroid();
   
             Geometry bufferH = centerH.buffer(sim.noMosHouseRadiusLow);

             if(bufferH.covers(square))
             {
                cp.setIsBufferNoMos(false); 
                //System.out.println("Buffer zone!!!!!");
             }
   
   
         }
      }

          //System.exit(0);


   }




    //System.exit(0);





//============================================================   
}
