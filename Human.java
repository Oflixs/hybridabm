/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.Geometry;

//----------------------------------------------------
public class Human extends Animal 
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;
   SimState state = null;

   public Household household = null;
   public Household getHousehold(){return household;};
   public void setHousehold(Household y){household = y;};

   public boolean hInfected = false;
   public boolean getHInfected(){return hInfected;};
   public void setHInfected(boolean y){hInfected = y;};

   public boolean hInfectious = false;
   public boolean getHInfectious(){return hInfectious;};
   public void setHInfectious(boolean y){hInfectious = y;};

   public boolean treated = false;
   public boolean getTreated(){return treated;};
   public void setTreated(boolean y){treated = y;};

   public boolean malariaInOneYear = false;
   public boolean getMalariaInOneYear(){return malariaInOneYear;};
   public void setMalariaInOneYear(boolean y){malariaInOneYear = y;};

   public int malariaInOneYearTimer = -100;
   public int getMalariaInOneYearTimer(){return malariaInOneYearTimer;};
   public void setMalariaInOneYearTimer(int y){malariaInOneYearTimer = y;};

   public boolean susceptible = true;
   public boolean getSusceptible(){return susceptible;};
   public void setSusceptible(boolean y){susceptible = y;};

   public int infectiousPeriod = -100;
   public Integer getInfectiousPeriod(){return infectiousPeriod;};
   public void setInfectiousPeriod(int y){infectiousPeriod = y;};

   public int infectionTimer = -100;
   public Integer getInfectionTimer(){return infectionTimer;};
   public void setInfectionTimer(int y){infectionTimer = y;};

   public int asymptomatic = 0;
   public Integer getAsymptomatic(){return asymptomatic;};
   public void setAsymptomatic(int y){asymptomatic = y;};

   HashMap<String, Double> asymptomaticity = new HashMap<String, Double>();
   public HashMap getAsymptomaticity(){return asymptomaticity;};
   public void setAsymptomaticity(HashMap y){asymptomaticity = y;};

   public CoverPixel cpPosition = null;
   public void setCpPosition(CoverPixel mg){cpPosition = mg;};
   public CoverPixel getCpPosition(){return cpPosition;};

   public int timeToSymptoms = -100;
   public Integer getTimeToSymptoms(){return timeToSymptoms;};
   public void setTimeToSymptoms(int y){timeToSymptoms = y;};

   public int timeToCommunicability = -100;
   public Integer getTimeToCommunicability(){return timeToCommunicability;};
   public void setTimeToCommunicability(int y){timeToCommunicability = y;};

   public int timeToRecover = -100;
   public Integer getTimeToRecover(){return timeToRecover;};
   public void setTimeToRecover(int y){timeToRecover = y;};

   public int identity = 0;
   public Integer getIdentity(){return identity;};
   public void setIdentity(int y){identity = y;};

   public double probFail = 0;
   public double getProbFail(){return probFail;};
   public void setProbFail(double y){probFail = y;};

   public boolean gotMalaria = false;
   public boolean getGotMalaria(){return gotMalaria;};
   public void setGotMalaria(boolean y){gotMalaria = y;};

   public int numMaxProtection = -40000;

   public boolean vivaxRecurrence = false;
   public boolean getVivaxRecurrence(){return vivaxRecurrence;};
   public void setVivaxRecurrence(boolean y){vivaxRecurrence = y;};

   public int vivaxRecurrenceTime = -100;
   public int getVivaxRecurrenceTime(){return vivaxRecurrenceTime;};
   public void setVivaxRecurrenceTime(int y){vivaxRecurrenceTime = y;};

   public boolean isProtected = false;
   public boolean getIsProtected(){return isProtected;};
   public void setIsProtected(boolean y){isProtected = y;}

   public boolean isProtectedInTheHousehold = false;
   public boolean getIsProtectedInTheHousehold(){return isProtectedInTheHousehold;};
   public void setIsProtectedInTheHousehold(boolean y){isProtectedInTheHousehold = y;}

   public boolean isProtectedInTheHotspots = false;
   public boolean getIsProtectedInTheHotspots(){return isProtectedInTheHotspots;};
   public void setIsProtectedInTheHotspots(boolean y){isProtectedInTheHotspots = y;}

   public boolean isProtectedForever = false;
   public boolean getIsProForever(){return isProtectedForever;};
   public void setIsProtectedForever(boolean y){isProtectedForever = y;}

   public boolean isMovible = false;
   public boolean getIsMovible(){return isMovible;};
   public void setIsMovible(boolean y){isMovible = y;};

   public boolean isMoving = false;
   public boolean getIsMoving(){return isMoving;};
   public void setIsMoving(boolean y){isMoving = y;};

   HashMap<String, CoverPixel> places = new HashMap<String, CoverPixel>();
   public HashMap getPlaces(){return places;};
   public void setPlaces(HashMap y){places = y;};

   public double distSportField = 1000000.0;
   public double distLeisure = 1000000.0;
   public double distShop = 1000000.0;
   public double distChurch = 1000000.0;
   public double distSchool = 1000000.0;
   public double distGov = 100000.0;
   public double distHealth = 100000.0;

   public AgeSegment ageSegment = null;
   public AgeSegment getAgeSegment(){return ageSegment;};
   public void setAgeSegment(AgeSegment y){ageSegment = y;};

   public List<Activity> activities = new ArrayList<Activity>();
   public List<Activity> getActivities(){return activities;};
   public void setActivities(List<Activity> y){activities = y;};

   public Activity job = null;
   public Activity getJob(){return job;};
   public void setJob(Activity y){job = y;};


   //Parameter to determine the shcedule
   public HumanSchedule dailyActivities = null;
   public CoverPixel workPlace = null;
   public int numHours = 0;
   public int weekDay = 0;
   public int dayHour = 0;
   public List<Integer> activitiesNTimes = new ArrayList<Integer>();
   public double malariaRisk = 0.0;

   public double farmerProbHavingFarmOut = 0.5;
   public double otherJobProbHavingWorkOut = 0.0;
   //This is the prob of not to ber able to go to work
   public double probNotToGoWork = 0.015;

   public int numMalariaCases = 0;
   public Integer getNumMalariaCases(){return numMalariaCases;};
   public void setNumMalariaCases(int y){numMalariaCases = y;};

   public double probBiteOutdoor = 0.0;

   public Boolean writeHighRisk = false;
   public Boolean infectionBlocking = false;


   //====================================================
   public Human(SimState pstate, CoverPixel cp, Household ph)
   {
       super(pstate, cp);
       //System.out.rintln ("New Human is created ----");

       state = pstate;
       sim = (AmaSim)state;

       cpPosition = cp;

       sim = (AmaSim)state;

       this.setSpecies("Homo Sapiens");

       household = ph;

       household.addHuman(this);

       this.init(state);

       double time = 0.0;
       if(state.schedule.getTime() > 0)time = (double)state.schedule.getTime();  
       this.stopper = sim.schedule.scheduleRepeating(time, 2, this);

       sim.humanGrid.setObjectLocation(this, 
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "geo")[0],
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "geo")[1]
           );

       sim.human_bag.add(this);

       asymptomaticity.put("Falciparum", new Double(0.0));
       asymptomaticity.put("Vivax", new Double(0.0));
   
       identity = sim.hIdent;
       sim.hIdent++;

       //To setup the agent's local movement dailyActivities ---------
       //double ran = state.random.nextDouble();
       //if(ran <= sim.fractProtectedPeople)setIsProtected(true);

       //System.out.println ("Age = " + age + " " + age/8640);

       if(sim.humanLocalMovement != 0)
       {
           setupReferencePlaces(sim);

           setupAgeSegment(sim);

           setupJob(sim);
           //System.exit(0);

           setupActivities(sim);
       }
   }

   //====================================================
   public void init(SimState state)
   {
       sim = (AmaSim)state;
       
       double ran = 0.0;
       int    iran = 0;
        
       if(sim.ageStructure.size() == 0)this.setAge(state.random.nextInt(70) * 8640);
       else
       {
           ran = state.random.nextDouble();

           if(ran <= sim.ageStructure.get("0_5"))setAge(8640 * state.random.nextInt(5));

           else if(sim.ageStructure.get("0_5") <= ran 
           && ran < sim.ageStructure.get("6_12"))setAge(8640 * (6 + state.random.nextInt(6)));

           else if(sim.ageStructure.get("6_12") <= ran 
           && ran < sim.ageStructure.get("13_17"))setAge(8640 * (12 + state.random.nextInt(4)));

           else if(sim.ageStructure.get("13_17") <= ran 
           && ran < sim.ageStructure.get("18_24"))setAge(8640 * (13 + state.random.nextInt(6)));

           else if(sim.ageStructure.get("18_24") <= ran 
           && ran < sim.ageStructure.get("25_39"))setAge(8640 * (24 + state.random.nextInt(14)));

           else if(sim.ageStructure.get("25_39") <= ran 
           && ran < sim.ageStructure.get("40_55"))setAge(8640 * (39 + state.random.nextInt(15)));

           else setAge(8640 * (55 + state.random.nextInt(15)));
        
       }

       //System.out.println ("Age = " + age + " " + age/8640);

       ran = state.random.nextDouble();

       if(setAsymptomaticity(state))return;

       if(state.random.nextInt(100) < 1)
       {
          susceptible = false;

          ran = state.random.nextDouble();

          if(ran > 0.5)plasmodium = sim.Falciparum;
          else plasmodium = sim.Vivax;

          hInfected = true;

          infectionTimer = 0;
    
          ran = state.random.nextDouble();
    
          if(ran > sim.fractHumanTreated)treated = false;
          else treated = true;
    
          if(!treated)timeToRecover = 2 + state.random.nextInt(plasmodium.getHumanInfectiousPeriodNotTreated());           
    
          int numDays = plasmodium.getIntrinsicIncubation().get(1) - plasmodium.getIntrinsicIncubation().get(0);
          int ranI = state.random.nextInt(numDays + 1);
          ranI = ranI + plasmodium.getIntrinsicIncubation().get(0);
          timeToSymptoms = state.random.nextInt(ranI + 2);
          //System.out.println ("Time to symptoms: " + timeToSymptoms/24);


          timeToCommunicability = 2 + state.random.nextInt(plasmodium.getStartCommunicability());

       
       }

   }


   //====================================================
   public void step(SimState state)
   {
       super.step(state);
       sim = (AmaSim)state;


       //System.out.println ("---Human Step----------------");
       //System.out.println (cpPosition.xcor + " " + cpPosition.ycor);
       //System.out.println (sim.geoGridWidth + " " + sim.geoGridHeight);
       //System.out.println ("-----------------------------");
       //System.out.println (this.getAge() + "  mosquito age");

       if(hInfected)infectionDynamics(state);

       //See if hte human    have to die -----------------
       this.setAge(this.getAge() + 1);
       if(this.getAge() > 80 * 8640)
       {
          this.stopper.stop();         
          sim.human_bag.remove(this);
          household.removeHuman(this);

          return;
       }

       //Upgrade asymptmaticity: every hour a little fraction is lost
       double as;
       as = asymptomaticity.get("Falciparum");
       as = as - sim.Falciparum.getFractAsymptomaticityLostPerHour();
       if(as < 0.0) as = 0.0;
       asymptomaticity.put("Falciparum", as);

       as = asymptomaticity.get("Vivax");
       as = as - sim.Vivax.getFractAsymptomaticityLostPerHour();
       if(as < 0.0) as = 0.0;
       asymptomaticity.put("Vivax", as);

       //Incubation period ends -> Infectious period starts --------------
       if(infected == 0)
       {
           //System.out.println (this.getSpecies() + "  become infectious!");
           this.setInfectiousPeriod(plasmodium.getHumanInfectiousPeriodNotTreated());
           this.setInfectious(true);
       }

       //Upgrade timers----------------------------------
       infectiousPeriod--;
       malariaInOneYearTimer--;
       vivaxRecurrenceTime--;

       //The humnan is no more infectiuos: recovered state -> is now susceptible
       if(infectiousPeriod == 0)
       {
           //human gain a fract of asymptomaticity---------------------
           as = asymptomaticity.get(plasmodium.getType());
           as = as +  plasmodium.getFractAsymptomaticityGainedPerInfection();
           if(as < 0.0)as = 0.0;
           asymptomaticity.put(plasmodium.getType(), as);

           setInfectious(false);
           setPlasmodium(null);
       }

       infected--;

       if( malariaInOneYearTimer == 0 )setMIOYOff();
       if( sim.HmonthlyIncidence <= sim.setProtOff )setMIOYOff();

       //fact = 0 all full period
       //fact = 1 scaling all the time
       double fact = sim.protectionPeriodFact;
       if(malariaInOneYearTimer > fact * sim.malariaProtection)
       {
          probFail = sim.probFailMalariaProtection;
       }
       else
       {
          probFail = sim.probFailMalariaProtection * (double)( malariaInOneYearTimer / (sim.malariaProtection * fact)  );
       }

       //probFail = sim.probFailMalariaProtection;

       if( malariaInOneYearTimer < 0 && malariaInOneYear )
       {
          System.out.println ("error in Human malariainoneyear   !!!!-------");
          System.exit(0);
       }

       if(vivaxRecurrence && vivaxRecurrenceTime == 0)setupRecurrence(state);

       //To move around 
       if(sim.humanLocalMovement > 1)
       {
           getDailyActivities(sim);
           move(state);
       }
   }
   


   //====================================================
   public void setupInfectionAfterBite(SimState state, Mosquito mosquito)
   {
      double ran = state.random.nextDouble();

      if(mosquito != null && mosquito.getPlasmodium() != null)
      {
         if(ran < sim.vivaxRiskRecurrence && mosquito.getPlasmodium().getType().equals("Vivax")) 
         {
            vivaxRecurrence = true;   
   
            ran = state.random.nextDouble();
            ran = ran - 0.5;
            ran = ran / 2.5;
            vivaxRecurrenceTime = (int)Math.round(sim.vivaxTimeRecurrence * (1 + ran));   
         }
      }

      numMalariaCases++;
      
      sim = (AmaSim)state;

      gotMalaria = true;
      household.setGotMalaria(true);

      susceptible = false;
      if(mosquito != null && mosquito.getPlasmodium() != null)
      {
         setPlasmodium(mosquito.getPlasmodium());
      }
      else
      {
         plasmodium = sim.Vivax;
      }

      hInfected = true;
      infectionTimer = 0;

      ran = state.random.nextDouble();

      if(ran > sim.fractHumanTreated)treated = false;
      else treated = true;

      if(!treated)
      {
          
         ran = state.random.nextDouble();
         ran = ran - 0.5;
         ran = ran/2.5;
         timeToRecover = plasmodium.getHumanInfectiousPeriodNotTreated();           
         timeToRecover = timeToRecover + (int)Math.round(ran * timeToRecover);
      }

      
      //ran = state.random.nextDouble();
      //System.out.println ("-------------------------");
      int numDays = plasmodium.getIntrinsicIncubation().get(1) - plasmodium.getIntrinsicIncubation().get(0);
      //System.out.println ("Num days: " + numDays/24);
      int ranI = state.random.nextInt(numDays + 1);
      //System.out.println ("Num days: " + ranI/24);
      ranI = ranI + plasmodium.getIntrinsicIncubation().get(0);
      timeToSymptoms = ranI;
      //System.out.println ("Plasm: " + plasmodium.getType() + "000000000000000000000000000000000000000");
      //System.out.println ("Time to symptoms: " + timeToSymptoms);
      //System.out.println ("-------------------------");

      sim.weeklyAvgTimeToSymptoms = sim.weeklyAvgTimeToSymptoms + timeToSymptoms;
      sim.weeklyAvgTimeToSymptomsCounter++;

      ran = state.random.nextDouble();
      ran = ran - 0.5;
      ran = ran/2.5;
      timeToCommunicability = plasmodium.getStartCommunicability();
      timeToCommunicability = timeToCommunicability + (int)Math.round(ran * timeToCommunicability);

      if(numMaxProtection > 0)
      {
         setMIOYOn();
         malariaInOneYeaToHousehold(state);
      }

   }

   //====================================================
   public void setMIOYOff()
   {
      malariaInOneYear = false;
      malariaInOneYearTimer = -150;
      //System.exit(0);
   }

   //====================================================
   public void setMIOYOn()
   {
       //System.out.println ("biten!!!!!!!-------");
       //System.out.println ("Num Mlaria in one year: " + getNM(sim));
       //System.out.println ("Num people: " + getP(sim));
       malariaInOneYear = true;
       malariaInOneYearTimer = sim.malariaProtection;
       //System.out.println ("Mal in one year timer: " + malariaInOneYearTimer);
   }

   //====================================================
   public void infectionDynamics(SimState state)
   {
       infectionTimer++;

       if(timeToSymptoms == infectionTimer && treated)
       {
          double ran = state.random.nextDouble();
          ran = ran - 0.5;
          ran = ran/2.5;
          timeToRecover = plasmodium.getHumanInfectiousPeriodTreated();           
          timeToRecover = timeToRecover + infectionTimer + (int)Math.round(ran * plasmodium.getHumanInfectiousPeriodTreated());           

          //Pitxi era cosi
          //double ran = state.random.nextDouble();
          //ran = ran - 0.5;
          //ran = ran/2.5;
          //timeToRecover = infectionTimer + (int)Math.round(ran * plasmodium.getHumanInfectiousPeriodTreated());           

	  if(!sim.equilibration)
	  {
             sim.weeklyMalariaCases++;
             if(plasmodium.getType().equals("Falciparum"))sim.weeklyMalariaCasesFalciparum++;
             if(plasmodium.getType().equals("Vivax"))sim.weeklyMalariaCasesVivax++;

             sim.monthlyMalariaCases++;
             if(plasmodium.getType().equals("Falciparum"))sim.monthlyMalariaCasesFalciparum++;
             if(plasmodium.getType().equals("Vivax"))sim.monthlyMalariaCasesVivax++;

             sim.yearlyMalariaCases++;
             if(plasmodium.getType().equals("Falciparum"))sim.yearlyMalariaCasesFalciparum++;
             if(plasmodium.getType().equals("Vivax"))sim.yearlyMalariaCasesVivax++;

             household.increaseWeeklyMalariaCases();
             if(plasmodium.getType().equals("Falciparum"))household.increaseNumFalciparumCases();

             if(plasmodium.getType().equals("Vivax"))household.increaseNumVivaxCases();

	     CoverPixel cph = this.getCpPosition();
	     if(plasmodium.getType().equals("Vivax")) cph.increaseNMalariaCasesV();
	     if(plasmodium.equals("Falciparum")) cph.increaseNMalariaCasesF();
	     cph.addTimeMalariaCounts();
	  }
}

       if(timeToCommunicability == infectionTimer)hInfectious = true;

       if(timeToRecover == infectionTimer)recover(state);

   }

   //====================================================
   public void recover(SimState state)
   {
       hInfected              = false;
       hInfectious            = false;
       treated                = false;
       susceptible            = true;

       timeToSymptoms         = -100;
       timeToRecover          = -100;
       timeToCommunicability  = -100;
       infectionTimer         = -100;

       return;

   }

   //====================================================
   public void malariaInOneYeaToHousehold(SimState state)
   {
       Bag people = household.getPeople();
       people.shuffle(state.random);

       int stats = 1;
       //System.out.println ("Num people: " + people.size()); 
       for(int i = 0; i < people.size(); i++)
       {
           Human human = ((Human)people.objs[i]);
           if(!human.getMalariaInOneYear() && stats <= numMaxProtection)
           {
              human.setMIOYOn();
              stats++;
           }
       }
   }

   //====================================================
   public int getNM(SimState state)
   {
       Bag people = household.getPeople();
       people.shuffle(state.random);

       int stats = 10000;
       //System.out.println ("Num people: " + people.size()); 
       for(int i = 0; i < people.size(); i++)
       {
           Human human = ((Human)people.objs[i]);
           if(human.getMalariaInOneYear())
           {
              stats++;
           }
       }
       return stats;
   }

   //====================================================
   public int getP(SimState state)
   {
       Bag people = household.getPeople();
       people.shuffle(state.random);
       return people.size();

   }

   //====================================================
   public boolean setAsymptomaticity(SimState state)
   {
       //Extract asymptomatic humans -----------
       double ran = state.random.nextDouble();

       if(sim.fractHumanAsymptomaticF > 0.25)
       {
          System.out.println ("Fract asymptomatic Falciparum grater thant 25%");
          System.out.println ("Not possible with this code! Sorry");
          System.exit(0);
       }

       if(sim.fractHumanAsymptomaticV > 0.25)
       {
          System.out.println ("Fract asymptomatic Vivax grater thant 25%");
          System.out.println ("Not possible with this code! Sorry");
          System.exit(0);
       }

       //low asymptomaticity for both
       if(sim.fractHumanAsymptomaticF == 0.0 && sim.fractHumanAsymptomaticV == 0.0)
       {
             ran = state.random.nextDouble();

             if(ran <= sim.fractHumanAsymptomatic)
             {
                asymptomatic = 1;
                hInfectious = true;
                susceptible = false;
      
                ran = state.random.nextDouble();
                if(ran > 0.5)plasmodium = sim.Falciparum;
                else plasmodium = sim.Vivax;
      
                return true;
             }
             else return false;
       }
       else
       {
          if(sim.asymptoSuper == 1)
          {

                 //asymptomaticy to both plasmodia alowed
          

                 boolean asy = false;
                 int stats = 0;
                 //Vivax
                 ran = state.random.nextDouble();
                 if(ran <= sim.fractHumanAsymptomaticV)
                 {
                    asymptomatic = 1;
          
                    hInfectious = true;
                    susceptible = false;
                    plasmodium = sim.Vivax;
             
                    asy = true;
                    stats++;
                 }
      
               
                 //Falciparum
                 ran = state.random.nextDouble();
                 if(ran <= sim.fractHumanAsymptomaticF)
                 {
                    asymptomatic = 1;
          
                    hInfectious = true;
                    susceptible = false;
                    plasmodium = sim.Falciparum;
             
                    asy = true;
                    stats++;
                 }

                 if(stats == 2)plasmodium = sim.bothPlasmodia;
                 return asy;
          }
          else
          {
   
                 //asymptomaticy to both plasmodia not alowed
                 ran = state.random.nextDouble();
                 if(ran >= 0.5)
                 {
                    //Vivax
                    ran = state.random.nextDouble();
                    if(ran <= sim.fractHumanAsymptomaticV * 2)
                    {
                       asymptomatic = 1;
          
                       hInfectious = true;
                       susceptible = false;
                       plasmodium = sim.Vivax;
             
                       return true;
                    }
      
                 
                 }
                 else
                 {
                    //Falciparum
                    ran = state.random.nextDouble();
                    if(ran <= sim.fractHumanAsymptomaticF * 2)
                    {
                       asymptomatic = 1;
          
                       hInfectious = true;
                       susceptible = false;
                       plasmodium = sim.Falciparum;
             
                       return true;
                    }
      
       
                 
                 }
                 return false;
             }
   

       
       }

   }

   //====================================================
   public void setupRecurrence(SimState state)
   {
       Mosquito mosquito = null;
       for(int i = 0; i < sim.mosquito_bag.size(); i++)
       {
           mosquito = ((Mosquito)sim.mosquito_bag.objs[i]);
           if(mosquito.getPlasmodium() != null)
           {
              if(mosquito.getPlasmodium().getType().equals("Vivax"))break;
           }

       }

       if(mosquito == null)
       {
          plasmodium = sim.Vivax; 
       }

       setupInfectionAfterBite(state, mosquito);

       vivaxRecurrence = false;
       vivaxRecurrenceTime = -100;

   }

   //===========4000pli=========================================
   public void pr()
   {
       System.out.println ("-----------------------------");
       //System.out.println (this.getAge() + "  mosquito age");
       //if(infectious)
       //{
       //   System.out.println (infectious);
       //}



   }


//===========================================================
public void setupReferencePlaces(AmaSim sim)
{
    SimState state = (SimState)sim;

    int num_places = sim.dailyActivitiesPlaces.size();
    //System.out.println (num_places);

    Point pHuman = sim.utils.getCellCentralPoint(household.cpPosition.getXcor(), household.cpPosition.getYcor(), "geo");

    for (int ii = 0; ii < num_places; ii++)
    {
        Place place = (Place)sim.dailyActivitiesPlaces.get(ii);
         
        Point pPlace =  place.centralPoint;

        CoverPixel cp = place.cp;

        String type = place.type;
        //System.out.println (type);

        double dist = pPlace.distance(pHuman);

        //System.out.println ("dist = " + dist);
        //System.out.println ("type " + type);

        if(dist < distSportField && type.equals("Sport Field"))
        {
            distSportField = dist;
            places.put("Sport Field", cp);
        }

        if(dist < distLeisure && 
                (type.equals("Leisure Place") || type.equals("Recreacional")))
        {
            distLeisure = dist;
            places.put("Leisure Place", cp);
        }

        if(dist < distShop && 
                (type.equals("Shop") || type.equals("Tienda")))
        {
            distShop = dist;
            places.put("Shop", cp);
        }

        if(dist < distChurch && 
                (type.equals("Church") || type.equals("Iglesia")))
        {
            distChurch = dist;
            places.put("Church", cp);
        }

        if(dist < distSchool && 
                (type.equals("School") || type.equals("Escuela")))
        {
            distSchool = dist;
            places.put("School", cp);
            //System.out.println ("--------------------------------------");
            //System.exit(0);
        }

        if(dist < distGov && 
                (type.equals("Government building") || type.equals("Gob")))
        {
            distGov = dist;
            places.put("Gov", cp);
        }

        if(dist < distHealth && 
                (type.equals("Health post") || type.equals("Puesto de salud")))
        {
            distHealth = dist;
            places.put("Health Post", cp);
        }




    }


    //to randomize a little the visited places .....
    double tresh = 0.25;
    for (int ii = 0; ii < num_places; ii++)
    {
        Place place = (Place)sim.dailyActivitiesPlaces.get(ii);
        CoverPixel cp = place.cp;
        String type = place.type;
         
        double ran = state.random.nextDouble();

        if(ran < tresh && type.equals("Sport Field"))
        {
            places.put("Sport Field", cp);
        }

        if(ran < tresh && 
                (type.equals("Leisure Place") || type.equals("Recreacional")))
        {
            places.put("Leisure Place", cp);
        }

        if(ran < tresh && 
                (type.equals("Shop") || type.equals("Tienda")))
        {
            places.put("Shop", cp);
        }

        if(ran < tresh && 
                (type.equals("Church") || type.equals("Iglesia")))
        {
            places.put("Church", cp);
        }

        if(ran < tresh && 
                (type.equals("School") || type.equals("Escuela")))
        {
            places.put("School", cp);
            //System.out.println ("--------------------------------------");
            //System.exit(0);
        }

        if(ran < tresh && 
                (type.equals("Government building") || type.equals("Gob")))
        {
            places.put("Gov", cp);
        }

        if(ran < tresh && 
                (type.equals("Health post") || type.equals("Puesto de salud")))
        {
            places.put("Health Post", cp);
        }

    }



    //System.out.println("Places size: " + places.size());
    //for (String key : places.keySet()) {
    //    System.out.println("key: " + key + " value: " + places.get(key));
    //}



    //System.exit(0);



}

//===========================================================
public void move(SimState state)
{
    //if(!isMovible)return;
    if(weekDay == 0)return;

    CoverPixel oldPos = cpPosition;

    if(sim.humanLocalMovement == 2)moveAround(state);
    else if(sim.humanLocalMovement == 3)moveToFocalPoints(state);
    else if(sim.humanLocalMovement == 4)moveDailyActivities(state);

    if(sim.hybridDensity && cpPosition != oldPos)
    {
        CoverPixel mcp = (CoverPixel)cpPosition.linkPixels.get(0);
        CoverPixel mcpOld = (CoverPixel)oldPos.linkPixels.get(0);

        sim.hybridPixelsToChange.add(mcp);
        sim.hybridPixelsToChange.add(mcpOld);
    }

    if(cpPosition != null)
    {
        sim.humanGrid.setObjectLocation(this, 
                sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "geo")[0],
                sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "geo")[1]
                );
    }
    //else System.exit(0);

}

//===========================================================
public void moveAround(SimState state)
{
    if(isTimeToMove(state))
    {
        isProtected = false;

        CoverPixel nextCp = giveNextPixelRandomWalk(state);    

        goToPixel(nextCp);

        household.removeHuman(this); 

        isMoving = true;

        //sim = (AmaSim)state;
        //sim.utils.getHumansInMovement();

    }
    else
    {
        isProtected = isProtectedInTheHousehold;

        household.addHuman(this); 

        cpPosition.removeHuman(this);

        cpPosition = household.cpPosition;

        isMoving = false;

        //sim = (AmaSim)state;
        //sim.utils.getHumansInMovement();
    }
}

//===========================================================
public void goToPixel(CoverPixel cp)
{
    if(cpPosition != null)cpPosition.removeHuman(this);
    if(cp != null)
    {
        cpPosition = cp;
        cp.addHuman(this);
    }
    else
    {
        cpPosition = household.cpPosition;
        household.cpPosition.addHuman(this);
    }
}

//====================================================
//This is a classic weghted random walk===============
public CoverPixel giveNextPixelRandomWalk(SimState state)
{
    //if(cpPosition.getIsWater() == 1) System.exit(0);
    sim = (AmaSim)state;
    CoverPixel tcp;

    List<Double> weightsList = new ArrayList<Double>();

    Boolean noGood = true;
    CoverPixel rCp = null;
    while(noGood)
    {
        //convert from mosquito to geo grid coordinates --
        //System.out.println ("------------------------");
        Bag neig = cpPosition.getMooreNeighbors(state, "doShuffle", "geo");

        for(int i =  0; i < neig.size(); i++)
        {
            tcp = (CoverPixel)neig.objs[i];

            if(tcp.getIsWater() == 1)
            {
                weightsList.add(0.0);
            }
            else
            {
                if(tcp.getIsRoad() == 1)weightsList.add(sim.humanWeightRoad);
                else if(tcp.getContainsHousehold())weightsList.add(sim.humanWeightHousehold);
                else weightsList.add(sim.humanNoWeight);
            }


            //System.out.println ("weig : " + weightsList.get(i) );

            //if(tcp.getIsWater() == 1)
            //{
            //    System.out.println ("isWater " + i);
            //}
            //else
            //    System.out.println ("noWater " + i);

        }


        double sum = 0.0;
        for(int i =  0; i < weightsList.size(); i++)
        {
            sum = sum + weightsList.get(i);
        }
        //System.out.println ("sum " + sum);
        for(int i =  0; i < weightsList.size(); i++)
        {
            weightsList.set(i, weightsList.get(i)/sum);
        }

        sum = 0.0;
        double ran = state.random.nextDouble();
        for(int i =  0; i < neig.size(); i++)
        {
            tcp = (CoverPixel)neig.objs[i];
            sum = sum + weightsList.get(i);

            //System.out.println ("sum : " + sum );
            //System.out.println ("ran : " + ran );
            //System.out.println ("weig : " + weightsList.get(i) );

            if(ran <= sum)
            {
                //System.out.println ("i : " + i );
                rCp = tcp;
                break;
            }
        }
        if(rCp.getIsWater() == 0)noGood = false;

        //System.out.println ("--------");
        //System.out.println (rCp.getIsWater());
        //System.out.println (cpPosition.getIsWater());

    }

    return rCp;

}

//====================================================
public boolean isTimeToMove(SimState state)
{
    int now = (int)state.schedule.getTime();  
    //System.out.println ("now: " + (now % 24));

    for(int i =  -sim.humanMovingPeakDev; i <= sim.humanMovingPeakDev; i++)
    {
       int t = sim.humanMovingPeak + i;

       if(t < 0){t = t + 24;}
       if(t > 23){t = t - 24;}

       int day = now % 24;
       if(day == t)
       {
          //System.out.println ("Moving time!! " + (now % 24 ));
          return true;
       }
    }
    return false;
}

//===========================================================
public void goAroundHousehold()
{
    isProtected = false;

    household.removeHuman(this); 
    if(cpPosition != null)cpPosition.removeHuman(this);

    cpPosition = household.cpPosition;

    goToPixel(cpPosition);

    isMoving = true;
}


//===========================================================
public void moveDailyActivities(SimState state)
{
    //System.out.println ("Day of the week: " + weekDay);
    //System.out.println ("Hour of the day: " + dayHour);

    String actName = dailyActivities.activity.get(dayHour);
    String where = dailyActivities.where.get(dayHour);
    CoverPixel baseCp = dailyActivities.cp.get(dayHour);
    malariaRisk = dailyActivities.malariaRisk.get(dayHour);
    String type = dailyActivities.type.get(dayHour);


    if(baseCp == null && actName.equals("housewife"))
    {
        System.out.println ("ActName0: " + actName);
        System.exit(0);
    }

    //System.out.println ("--------");
    //System.out.println("x hous4: " + household.cpPosition.getXcor() + "y hous4: " + household.cpPosition.getYcor());
    //System.out.println ("ActName: " + actName);
    //System.out.println ("Where: " + where);
    //System.out.println ("Time of the day: " + dayHour);
    //System.out.println ("Day of the week: " + weekDay);
    //System.out.println ("Human age seg: " + ageSegment.name);
    //System.out.println ("Type: " + type);
    //System.out.println ("WorkPlace: " + workPlace);
    //System.out.println("cpPosition coords: " + cpPosition.getXcor() + " " +cpPosition.getYcor());
    //System.out.println("base coords: " + baseCp.getXcor() + " " + baseCp.getYcor());
    //if(job != null)System.out.println ("Job: " + job.name);
    //dailyActivities.print();

    //if(baseCp == null)
    //{
    //   System.out.println (baseCp);
    //   System.out.println ("-nulrl----");
    //   System.exit(0);
    //}


    Boolean good = false;
    if(baseCp != null)good = checkNeighborsForWater(baseCp);
    else good = false;

    CoverPixel nextCp = null;

    if(actName.equals("Sleep") && where.equals("home"))
    {
        isProtected = isProtectedInTheHousehold;

        household.addHuman(this); 

        cpPosition.addHuman(this);

        if(cpPosition != null)cpPosition.removeHuman(this);

        cpPosition = household.cpPosition;

        isMoving = false;
    }
    else if(where.equals("jobOut"))
    {
        if(sim.humanProtectedEverywhere == 0)
        {
            isProtected = false;

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            cpPosition = sim.outsideCp;

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 1)
        {
            isProtected = isProtectedInTheHousehold;   

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            cpPosition = sim.outsideCp;

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 2)goAroundHousehold();
    }
    else if(actName.equals("Free time") && where.equals("At home"))
    {
        if(sim.humanProtectedEverywhere == 0)
        {
            isProtected = false;

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            if(good)nextCp = giveNextPixeldailyActivities(state, household.cpPosition, type);
            else nextCp = household.cpPosition;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 1)
        {
            isProtected = isProtectedInTheHousehold;   

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            if(good)nextCp = giveNextPixeldailyActivities(state, household.cpPosition, type);
            else nextCp = household.cpPosition;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 2)goAroundHousehold();

    }
    else if(where.equals("jobIn"))
    {
        if(sim.humanProtectedEverywhere == 0)
        {
            isProtected = false;

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            //if(!good)System.out.println ("ActName2: " + actName);

            if(good)nextCp = nextCp = giveNextPixeldailyActivities(state, baseCp, type);
            else nextCp = sim.outsideCp;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 1)
        {
            isProtected = isProtectedInTheHousehold;   

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            if(good)nextCp = nextCp = giveNextPixeldailyActivities(state, baseCp, type);
            else nextCp = sim.outsideCp;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 2)goAroundHousehold();


    }
    else
    {
        if(sim.humanProtectedEverywhere == 0)
        {
            isProtected = false;

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            if(good)nextCp = nextCp = giveNextPixeldailyActivities(state, baseCp, type);
            else nextCp = sim.outsideCp;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 1)
        {
            isProtected = isProtectedInTheHousehold;   

            household.removeHuman(this); 

            if(cpPosition != null)cpPosition.removeHuman(this);

            if(good)nextCp = nextCp = giveNextPixeldailyActivities(state, baseCp, type);
            else nextCp = sim.outsideCp;

            goToPixel(nextCp);

            isMoving = true;
        }
        else if(sim.humanProtectedEverywhere == 2)goAroundHousehold();
    }


    if(where.equals("jobIn"))
    {
        if(job.name.equals("farmer"))probBiteOutdoor = 1.0;
        if(job.name.equals("shop keeper"))probBiteOutdoor = sim.probBloodMealFromOutdoorHuman * (1 - 0.3);
        if(job.name.equals("office"))probBiteOutdoor = sim.probBloodMealFromOutdoorHuman * (1 - 0.4);
    }
    else if(actName.equals("Free time") && where.equals("At home"))
    {
        probBiteOutdoor = sim.probBloodMealFromOutdoorHuman * (1 - 0.3);
    }
    else
    {
        probBiteOutdoor = 0.0;
    }

    if(sim.highRiskWhat == 1 || sim.highRiskWhat == 2)
    {
        //System.exit(0);

        if(cpPosition.getIsHighRisk() == 1)
        {
            //System.out.println ("  ");
            //if(job != null)System.out.println ("Job: " + job.name);
            //System.out.println ("Human age seg: " + ageSegment.name);
            //System.out.println ("ActName: " + actName);
            //System.out.println ("Where: " + where);
            //System.exit(0);

            Boolean write = true;
            if(sim.numPeopleProtectedForever != 0 
            && sim.numHumanForever >= sim.numPeopleProtectedForever 
            )write = false;

            if(write)
            {
                isProtected = true;
                if(!writeHighRisk)
                {
                    sim.numHumanProtectedHighRisk++;
                    writeHighRisk = true;
                }
                sim.numHoursHumanProtectedHighRisk++;
                if(!isProtectedForever)sim.numHumanForever++;
                isProtectedForever = true;
            }

        }
    }



    //Farmers -----------------------------------
    if(sim.highRiskWhat == 3)
    {
        if(job != null && job.name.equals("farmer"))
        {
            isProtected = true;
            if(!writeHighRisk)
            {
                sim.numHumanProtectedHighRisk++;
                writeHighRisk = true;
            }
            sim.numHoursHumanProtectedHighRisk++;
            isProtectedForever = true;
        }
    }

    if(isProtectedForever)isProtected = true;
    //if(isProtectedForever)infectionBlocking = true;

    if(!sim.equilibration)cpPosition.addAccumulatedNPeople();
    
    if(!sim.equilibration)cpPosition.addTimePopCounts();
    //System.out.println (where);



}

//====================================================
public Boolean checkNeighborsForWater(CoverPixel baseCp)
{
    Bag neig = baseCp.getMooreNeighbors(state, "doShuffle", "geo");

    if(baseCp.getIsWater() == 0)return true;

    for(int i =  0; i < neig.size(); i++)
    {
        CoverPixel tcp = (CoverPixel)neig.objs[i];
        if(tcp.getIsWater() == 0)return true;
    }

    return false;
}



//====================================================
//The human goes only to specific places in the community
public CoverPixel giveNextPixeldailyActivities(SimState state, CoverPixel baseCp, String type)
{
    //if(cpPosition.getIsWater() == 1) System.exit(0);
    //System.out.println ("ActName3: " + type);
    sim = (AmaSim)state;

    if(type.equals("Sleep"))return null;
    
    int spread = 1;

    if(baseCp == null)return null;

    if(type.equals("Leisure Place") || type.equals("Recreacional"))spread = 15;

    if(type.equals("Sport Field"))spread = 5;

    if(type.equals("School"))spread = 10;

    if(type.equals("Health post") || type.equals("Puesto de salud"))spread = 2;

    if(type.equals("Church") || type.equals("Iglesia"))spread = 3;

    if(type.equals("farmer"))spread = 20;

    if(type.equals("fisherman"))spread = 100;

    if(type.equals("transport"))spread = 100;

    if(type.equals("other") || type.equals("other manual"))spread = 2;

    if(type.equals("Free time"))spread = 4;

    if(type.equals("housewife"))spread = 6;

    //System.out.println (type);

    Boolean noGood = true;
    CoverPixel tcp = null;

    //patch stats
    int stats = 0;

    while(noGood)
    {
        stats++;
        int iranX = state.random.nextInt(spread + 1);
        int iranY = state.random.nextInt(spread + 1);
        Integer coors[] = {0, 0};

        int delta = (int)Math.round((spread -1)/2);

        //System.out.println("===================================");
        //System.out.println("Comm name: " + sim.community.name);
        //System.out.println(sim.geoGridWidth + " " + sim.geoGridHeight);
        //System.out.println("spread: " + spread);
        //System.out.println("delta: " + delta);
        //System.out.println("x basedCp: " + baseCp.getXcor() + "y baseCp: " + baseCp.getYcor());
        //System.out.println("x hous4: " + household.cpPosition.getXcor() + "y hous4: " + household.cpPosition.getYcor());

        coors[0] = baseCp.getXcor() + iranX - delta;
        coors[1] = baseCp.getYcor() + iranY - delta;
        //System.out.println("xdelta: " + coors[0] + " ydelta: " + coors[1]);
        coors = sim.utils.pbc(state, coors, "toroidals", "geo");

        //System.out.println("x: " + coors[0] + " y: " + coors[1]);

        //System.out.println("xpbc: " + coors[0] + " ypbc: " + coors[1]);

        //System.out.println ("--------");
        //System.out.println ("Time of the day: " + dayHour);
        //System.out.println ("Day of the week: " + weekDay);
        //System.out.println ("Human age seg: " + ageSegment.name);
        //System.out.println ("Type: " + type);
        //System.out.println ("WorkPlace: " + workPlace);
        //System.out.println("cpPosition coords: " + cpPosition.getXcor() + " " +cpPosition.getYcor());
        //System.out.println("base coords: " + baseCp.getXcor() + " " + baseCp.getYcor());
        //System.out.println("x hous4: " + household.cpPosition.getXcor() + "y hous4: " + household.cpPosition.getYcor());
        //if(job != null)System.out.println ("Job: " + job.name);


        tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], "geo");

        if(tcp.getIsWater() != 1)noGood = false;
        if(stats > 15)
        {
           tcp = baseCp;
           noGood = false;
        }
    }


    return tcp;
    //return places.get("leisure");

}

//===========================================================
public void moveToFocalPoints(SimState state)
{
    if(isTimeToMove(state))
    {
        isProtected = false;

        CoverPixel nextCp = giveNextPixelFocal(state);    
        goToPixel(nextCp);

        //goToPixel(household.getCpPosition());

        household.removeHuman(this); 

        //System.out.println(household.getPeople().size());
        //if(household.getPeople().size() == 0)
        //{
        //    System.out.println ("zero people!!!");
        //    System.exit(0);
        //}



        isMoving = true;

        //sim = (AmaSim)state;
        //sim.utils.getHumansInMovement();

    }
    else
    {
        isProtected = isProtectedInTheHousehold;

        household.addHuman(this); 

        cpPosition.removeHuman(this);

        cpPosition = household.cpPosition;

        isMoving = false;

        //System.out.println ("fdwsssssssssssssssssssssssssss");
        //System.exit(0);

        //sim = (AmaSim)state;
        //sim.utils.getHumansInMovement();
    }
}

//====================================================
//The human goes only to specific places in the community
public CoverPixel giveNextPixelFocal(SimState state)
{
    //if(cpPosition.getIsWater() == 1) System.exit(0);
    sim = (AmaSim)state;

    CoverPixel cp = places.get("Leisure Place");
    //System.out.println ("Leisure: " + cp.xcor + " " + cp.ycor);

    Boolean noGood = true;
    int spread = 15;
    CoverPixel tcp = null;

    while(noGood)
    {
        int iranX = state.random.nextInt(spread + 1);
        int iranY = state.random.nextInt(spread + 1);
        Integer coors[] = {0, 0};

        int delta = (int)Math.round((spread -1)/2);

        coors[0] = cp.getXcor() + iranX - delta;
        coors[1] = cp.getYcor() + iranY - delta;
        coors = sim.utils.pbc(state, coors, "toroidals", "geo");
        tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], "geo");

        if(tcp.getIsWater() != 1)noGood = false;
    }

    return tcp;
    //return places.get("leisure");

}


//===========================================================
public void setupAgeSegment(AmaSim sim)
{
    //System.out.println ("------------------------------");

    for(int i = 0; i < sim.ageSegments.size(); i++)
    {
       AgeSegment as = (AgeSegment)sim.ageSegments.get(i);


       if(age/8640 >= as.ageMin && age/8640 <= as.ageMax)
       {
           //System.out.println ("Age = " + age/8640 + " " + as.ageMin + " " + as.ageMax);
           ageSegment = as;
           return;
       }
    }
    System.out.println ("No age segment Age = " + age/8640);
    System.exit(0);
}

//===========================================================
public void getDailyActivitiesMove(AmaSim sim)
{
    SimState state = (SimState)sim;

    weekDay = sim.utils.getDayOfWeek(sim);
    dayHour = sim.utils.getDayHour(sim);

    dailyActivities = new HumanSchedule(state);
    resetWeeklyActivities();

    //Here order is importan denoting the agent dailyActivities priorities:
    //work first then sleep and then others activities
    getWork();
    getSleep();
    getOtherActivities();
    completeDailyActivities();

    writeHighRisk = false;

    //System.out.println ("Day of the week: " + weekDay);
    //System.out.println ("Human age seg: " + ageSegment.name);
    //dailyActivities.print();
    ////System.exit(0);

}



//===========================================================
public CoverPixel getRandomCp(AmaSim sim)
{
    SimState state = (SimState)sim;

    Boolean noGood = true;
    CoverPixel tcp = null;

    while(noGood)
    {
        int iranX = state.random.nextInt(sim.geoGridWidth);
        int iranY = state.random.nextInt(sim.geoGridHeight);
        Integer coors[] = {0, 0};

        coors[0] = iranX;
        coors[1] = iranY;

        coors = sim.utils.pbc(state, coors, "toroidals", "geo");
        tcp = sim.utils.getCoverPixelFromCoords(state, coors[0], coors[1], "geo");

        if(tcp.getIsWater() != 1)noGood = false;
    }

    return tcp;

}

//===========================================================
public void getDailyActivities(AmaSim sim)
{
    SimState state = (SimState)sim;

    weekDay = sim.utils.getDayOfWeek(sim);
    dayHour = sim.utils.getDayHour(sim);

    if(dayHour == 0)
    {
        dailyActivities = new HumanSchedule(state);
        resetWeeklyActivities();

        //Here order is importan denoting the agent dailyActivities priorities:
        //work first then sleep and then others activities
        getWork();
        getSleep();
        getOtherActivities();
        completeDailyActivities();

        writeHighRisk = false;

        //System.out.println ("Day of the week: " + weekDay);
        //System.out.println ("Human age seg: " + ageSegment.name);
        //dailyActivities.print();
        ////System.exit(0);

    }
}

//===========================================================
public void getWork()
{
    double ran = 0.0;

    if(job != null && job.name.equals("unemployed"))return;

    if(job != null && job.activeDays.contains(weekDay))
    {
        //System.out.println ("The agent goes to work --");
        //System.out.println ("Job : " + job.name);
        //System.out.println ("hour : " + job.to.get(Calendar.HOUR_OF_DAY));

        ran = state.random.nextDouble();
        if(ran >= probNotToGoWork)
        {
            int from = job.from.get(Calendar.HOUR_OF_DAY);
            int to = job.to.get(Calendar.HOUR_OF_DAY);

            int iToDay = to;
            int iFromDay = from;

            numHours = getHourDiff(from, to);

            //System.out.println ("from : " + iFromDay);
            //System.out.println ("to : " + iToDay);
            //System.out.println ("num hours Work xx : " + numHours);

            int iran = 0;

            if(numHours >= job.maxHoursPerDay)
            {

                Boolean noGood = true;
                while(noGood)
                {
                    if(job.sigma == 0)break;

                    numHours = getHourDiff(iFromDay, iToDay);

                    double gauss = Math.abs(state.random.nextGaussian() * job.sigma);
                    //System.out.println ("gauss: " + gauss);

                    if(gauss >= numHours)continue;

                    iran = state.random.nextInt(2);

                    //System.out.println ("iran: " + iran);

                    if(iran == 0)
                    {
                        iFromDay = (int)(Math.round(iFromDay + gauss + 24) % 24);

                    }
                    else if(iran == 1)
                    {
                        iToDay = (int)(Math.round(iToDay - gauss + 24) % 24);
                    }

                    //System.out.println ("iFromDay : " + iFromDay);
                    //System.out.println ("iToDay : " + iToDay);
                    //System.out.println ("num hours Work  : " + numHours);

                    numHours = getHourDiff(iFromDay, iToDay);

                    if(numHours <= job.maxHoursPerDay)noGood = false;
                }

            }

            //System.out.println ("from : " + iFromDay);
            //System.out.println ("to : " + iToDay);
            //System.out.println ("num hours Work  : " + numHours);
            //System.out.println (" ");


            if(iFromDay > iToDay)
            {
                for(int i = iFromDay; i < 24; i++)
                {
                    if(!dailyActivities.activity.get(i).equals(""))continue;

                    completeScheduleJob(i);

                }
                for(int i = 0; i < iToDay + 1; i++)
                {
                    if(!dailyActivities.activity.get(i).equals(""))continue;

                    completeScheduleJob(i);

                }
            }
            else
            {
                for(int i = iFromDay; i < iToDay + 1; i++)
                {
                    //System.out.println ("i: " + i);
                    if(!dailyActivities.activity.get(i).equals(""))continue;

                    completeScheduleJob(i);

                }
            }

            //System.exit(0);

        }
        else
        {
            //System.out.println ("Unemplyement let the worker at home");
        }

    }//Job selection end -------------------------

}

//===========================================================
public void setupJob(AmaSim sim)
{
    SimState state = (SimState)sim;

    double ran = state.random.nextDouble();

    if(ageSegment.name.equals("retired"))return;
    if(ageSegment.name.equals("child"))return;

    double sum = 0.0;

    ran = state.random.nextDouble();

    for(int i = 0; i < sim.activities.size(); i++)
    {
       Activity aa = (Activity)sim.activities.get(i);
       if(!aa.type.equals("job"))continue;

       sum = sum + aa.fractPop;
       if(ran < sum)
       {
           job = aa;
           //System.out.println ("Job name: " + job.name);
           if(job.name.equals("farmer"))
           {
               getFarm();
               //System.exit(0);
           }
           else if(job.name.equals("office"))
           {

               ran = state.random.nextDouble();
               if(ran >= 0.5)
               {

                   if(places.get("Gov") != null)workPlace = places.get("Gov");
                   else workPlace = household.cpPosition;
               }
               else
               { 
                   if(places.get("Health Post") != null)workPlace = places.get("Health Post");
                   else workPlace = household.cpPosition;
               }
           }
           else if(job.name.equals("shop keeper"))
           {
               if(places.get("Shop") != null)workPlace = places.get("Shop");
               else workPlace = household.cpPosition;
           }
           else if(job.name.equals("other") || job.name.equals("other manual"))
           {
               int numH = sim.household_bag.size();
               int iran = state.random.nextInt(numH);
               Household h = (Household)sim.household_bag.get(iran);
               if(ran > otherJobProbHavingWorkOut)workPlace = h.cpPosition;
           }
           else if(job.name.equals("craftsman"))
           {
               int numH = sim.household_bag.size();
               int iran = state.random.nextInt(numH);
               Household h = (Household)sim.household_bag.get(iran);
               if(ran > otherJobProbHavingWorkOut)workPlace = h.cpPosition;
           }
           else if(job.name.equals("unemployed"))
           {
               workPlace = household.cpPosition;
           }
           else if(job.name.equals("housewife"))
           {
               workPlace = household.cpPosition;
           }

           
           return;
       }
    }
}

//===========================================================
public void setupJobOnlyPosition(AmaSim sim)
{
    SimState state = (SimState)sim;

    double ran = state.random.nextDouble();

    workPlace = null;

    if(ageSegment.name.equals("retired"))return;
    if(ageSegment.name.equals("child"))return;

    if(job.name.equals("farmer"))
    {
        getFarm();
        //System.exit(0);
    }
    else if(job.name.equals("office"))
    {

        ran = state.random.nextDouble();
        if(ran >= 0.5)
        {

            if(places.get("Gov") != null)workPlace = places.get("Gov");
            else workPlace = household.cpPosition;
        }
        else
        { 
            if(places.get("Health Post") != null)workPlace = places.get("Health Post");
            else workPlace = household.cpPosition;
        }
    }
    else if(job.name.equals("shop keeper"))
    {
        if(places.get("Shop") != null)workPlace = places.get("Shop");
        else workPlace = household.cpPosition;
    }
    else if(job.name.equals("other") || job.name.equals("other manual"))
    {
        int numH = sim.household_bag.size();
        int iran = state.random.nextInt(numH);
        Household h = (Household)sim.household_bag.get(iran);
        if(ran > otherJobProbHavingWorkOut)workPlace = h.cpPosition;
    }
    else if(job.name.equals("craftsman"))
    {
        int numH = sim.household_bag.size();
        int iran = state.random.nextInt(numH);
        Household h = (Household)sim.household_bag.get(iran);
        if(ran > otherJobProbHavingWorkOut)workPlace = h.cpPosition;
    }
    else if(job.name.equals("unemployed"))
    {
        workPlace = household.cpPosition;
    }
    else if(job.name.equals("housewife"))
    {
        workPlace = household.cpPosition;
    }

}



//===========================================================
public void setupActivities(AmaSim sim)
{

    //System.out.println ("---");
    //System.out.println ("-" + ageSegment.name + "-");
    for(int i = 0; i < sim.activities.size(); i++)
    {
       Activity aa = (Activity)sim.activities.get(i);
       if(aa.type.equals("job"))continue;

       //System.out.println (ageSegment.name);
       //System.out.println ("-" + aa.name + "-");

       if(aa.type.equals(ageSegment.name))
       {
           double ran = state.random.nextDouble();
           if(ran <= aa.fractPop )
           {
               activities.add(aa);
           }
           else
           {
               //System.out.println ("Not included----");
           }
           //System.out.println ("Preso----");
           //System.out.println (ageSegment.name);
           //System.out.println (aa.type + " " + aa.name);
       }

    }

    Collections.sort(activities, new ActivityComparator());

    for(int i = 0 ; i < activities.size(); i++)activitiesNTimes.add(0);

    //for(int i = 0 ; i < activities.size(); i++)
    //{
    //   Activity aa = (Activity)activities.get(i);

    //   System.out.println ("i: " + aa.priority + " " + aa.name);
    //}

}

//===========================================================
public void completeScheduleJob(int i)
{
    dailyActivities.activity.set(i, job.name);
    dailyActivities.malariaRisk.set(i, job.malariaRisk);
    dailyActivities.type.set(i, job.name);

    if(job.name.equals("woodcutter")
            || job.name.equals("fisherman")
            || job.name.equals("miner")
            || job.name.equals("transport")
      )
    {
        dailyActivities.where.set(i, "jobOut");
        dailyActivities.cp.set(i, sim.outsideCp);
    }

    if(job.name.equals("farmer") && workPlace == null)
    {
        dailyActivities.where.set(i, "jobOut");
        dailyActivities.cp.set(i, sim.outsideCp);
    }
    else if(job.name.equals("farmer") && workPlace != null)
    {
        dailyActivities.cp.set(i, workPlace);
        dailyActivities.where.set(i, "jobIn");
        //System.exit(0);
    }

    if(job.name.equals("other manual") && workPlace == null)
    {
        dailyActivities.where.set(i, "jobOut");
        dailyActivities.cp.set(i, sim.outsideCp);
    }
    else if(job.name.equals("other manual") && workPlace != null)
    {
        dailyActivities.cp.set(i, workPlace);
        dailyActivities.where.set(i, "jobIn");
    }

    if(job.name.equals("other") && workPlace == null)
    {
        dailyActivities.where.set(i, "jobOut");
        dailyActivities.cp.set(i, sim.outsideCp);
    }
    else if(job.name.equals("other") && workPlace != null)
    {
        dailyActivities.cp.set(i, workPlace);
        dailyActivities.where.set(i, "jobIn");
    }

    if(job.name.equals("housewife"))
    {
        dailyActivities.cp.set(i, workPlace);
        dailyActivities.where.set(i, "jobIn");
    }

    if(job.name.equals("craftsman"))
    {
        dailyActivities.cp.set(i, workPlace);
        dailyActivities.where.set(i, "jobIn");
    }



    //int iran = state.random.nextInt(2);
    //if(job.name.equals("transport") && iran == 0)
    //{
    //    dailyActivities.where.set(i, "jobOut");
    //    dailyActivities.cp.set(i, null);
    //}
    //else if(job.name.equals("transport") && iran == 1)
    //{
    //    dailyActivities.cp.set(i, workPlace);
    //    dailyActivities.where.set(i, "jobIn");
    //}

    if(job.name.equals("shop keeper"))
    {
        dailyActivities.where.set(i, "jobIn");
        dailyActivities.cp.set(i, workPlace);
    }

    if(job.name.equals("office"))
    {
        dailyActivities.where.set(i, "office");
        dailyActivities.cp.set(i, workPlace);
    }

    //System.out.println ("Work Where:  " + dailyActivities.where.get(i));

}

//===========================================================
public void getSleep()
{
    //Selects the sleep hours ------------------------------
    Activity sleep = getSleepActivity();
    //System.out.println ("hour : " + sleep.to.get(Calendar.HOUR_OF_DAY));
    //sleep.print();

    int from = sleep.from.get(Calendar.HOUR_OF_DAY);
    int to = sleep.to.get(Calendar.HOUR_OF_DAY);

    int iFromDay = from;
    int iToDay = to;

    numHours = getHourDiff(from, to);

    int iran = 0;

    if(numHours >= sleep.maxHoursPerDay)
    {
        Boolean noGood = true;
        while(noGood)
        {
            numHours = getHourDiff(iFromDay, iToDay);

            double gauss = Math.abs(state.random.nextGaussian() * sleep.sigma);
            if(gauss >= numHours)continue;

            iran = state.random.nextInt(2);

            if(iran == 0)
            {
                if(iFromDay < iToDay)iFromDay = (int)(Math.round(iFromDay + gauss/2 + 24) % 24);
                else iFromDay = (int)(Math.round(iFromDay - gauss/2 + 24) % 24);

            }
            else if(iran == 1)
            {
                if(iToDay > iFromDay)iToDay = (int)(Math.round(iToDay - gauss/2 + 24) % 24);
                else iToDay = (int)(Math.round(iToDay + gauss/2 + 24) % 24);
            }

            numHours = getHourDiff(iFromDay, iToDay);
            if(numHours <= sleep.maxHoursPerDay)noGood = false;
        }
    }

    //System.out.println ("from : " + iFromDay);
    //System.out.println ("to : " + iToDay);
    //System.out.println ("num hours Sleep  : " + numHours);
    //System.out.println (" ");
    
    //System.exit(0);

    if(iFromDay > iToDay)
    {
        for(int i = iFromDay; i < 24; i++)
        {
            if(!dailyActivities.activity.get(i).equals(""))continue;

            dailyActivities.activity.set(i, sleep.name);
            dailyActivities.malariaRisk.set(i, sleep.malariaRisk);
            dailyActivities.where.set(i, "home");
            dailyActivities.cp.set(i, household.cpPosition);
            dailyActivities.type.set(i, sleep.name);
        }
        for(int i = 0; i < iToDay + 1; i++)
        {
            if(!dailyActivities.activity.get(i).equals(""))continue;

            dailyActivities.activity.set(i, sleep.name);
            dailyActivities.malariaRisk.set(i, sleep.malariaRisk);
            dailyActivities.where.set(i, "home");
            dailyActivities.cp.set(i, household.cpPosition);
            dailyActivities.type.set(i, sleep.name);
        }
    }
    else
    {
        for(int i = iFromDay; i < iToDay + 1; i++)
        {
            if(!dailyActivities.activity.get(i).equals(""))continue;

            dailyActivities.activity.set(i, sleep.name);
            dailyActivities.malariaRisk.set(i, sleep.malariaRisk);
            dailyActivities.where.set(i, "home");
            dailyActivities.cp.set(i, household.cpPosition);
            dailyActivities.type.set(i, sleep.name);
        }
    }

    //dailyActivities.print();

    //System.out.println ("end getSleep");
    //System.exit(0);

}



 
//===========================================================
public void getOtherActivities()
{
    for(int ia = 0; ia < activities.size(); ia++)
    {
        Activity activity = activities.get(ia);

        if(activity.name.equals("Sleep"))continue;

        if(!activity.activeDays.contains(weekDay))continue;

        if(activitiesNTimes.get(ia) > activity.timePerWeek)continue;

        if(places.get(activity.name) == null)
        {
            //System.out.println (activity.name);
            //System.out.println (places);
            //System.exit(0);
            continue;
        }

        int from = activity.from.get(Calendar.HOUR_OF_DAY);
        int to = activity.to.get(Calendar.HOUR_OF_DAY);

        double toDay   = (state.random.nextGaussian() * activity.sigma);
        double fromDay = (state.random.nextGaussian() * activity.sigma);

        fromDay = ((from + fromDay + 24) % 24);
        toDay = ((to + toDay + 24) % 24);

        int iToDay = (int)Math.round(toDay);
        int iFromDay = (int)Math.round(fromDay);

        iToDay = (iToDay % 24);
        iFromDay = (iFromDay % 24);

        numHours = getHourDiff(iFromDay, iToDay);

        int iran = 0;

        Boolean write = true;
        //if( activity.noFlexTime == 1)write = false;

        double sigma = 0.0;
        if(activity.sigma == 0)sigma = 1;
        else sigma = activity.sigma;

        if(numHours >= activity.maxHoursPerDay && write)
        {
            Boolean noGood = true;
            while(noGood)
            {
                double gauss = Math.abs(state.random.nextGaussian() * sigma);
                double delta = ((numHours + (int)Math.round(gauss)) % 24);

                iran = state.random.nextInt(2);

                numHours = getHourDiff(iFromDay, iToDay);
                if(numHours < delta)delta = delta * 0.5;

                if(iran == 0)iFromDay = (int)(Math.round(iFromDay + delta/2 + 24) % 24);
                else if(iran == 1)iToDay = (int)(Math.round(iToDay - delta/2 + 24) % 24);

                numHours = getHourDiff(iFromDay, iToDay);
                if(numHours < activity.maxHoursPerDay)noGood = false;
            }


        }

        //System.out.rintln ("from : " + iFromDay);
        //System.out.println ("to : " + iToDay);
        //System.out.println ("Activity  : " + activity.name);
        //System.out.println ("num hours Activity  : " + numHours);
        //System.out.println (" ");

        if(iFromDay > iToDay)
        {
            for(int i = iFromDay; i < 24; i++)
            {
                if(!dailyActivities.activity.get(i).equals(""))continue;

                completeScheduleActivity(i, activity, ia);

            }
            for(int i = 0; i < iToDay + 1; i++)
            {
                if(!dailyActivities.activity.get(i).equals(""))continue;

                completeScheduleActivity(i, activity, ia);

            }
        }
        else
        {
            for(int i = iFromDay; i < iToDay + 1; i++)
            {
                if(!dailyActivities.activity.get(i).equals(""))continue;

                completeScheduleActivity(i, activity, ia);

            }
        }






    }

    //System.exit(0);



    
}

//===========================================================
public void completeScheduleActivity(int i, Activity activity, int activityIndex)
{
    dailyActivities.activity.set(i, activity.name);
    dailyActivities.malariaRisk.set(i, activity.malariaRisk);
    dailyActivities.where.set(i, activity.name);
    dailyActivities.type.set(i, activity.name);

    int num = activitiesNTimes.get(activityIndex);
    activitiesNTimes.set(activityIndex, num++);

    if(activity.name.equals("Sleep"))
    {
        dailyActivities.cp.set(i, household.cpPosition);
    }
    else dailyActivities.cp.set(i, places.get(activity.name));

    //System.out.println ("Activity Where:  " + dailyActivities.where.get(i));

}



//===========================================================
public List<Activity> getPriorityList()
{
    List<Activity> list = new ArrayList<Activity>();




    return list;

}

//===========================================================
public void completeDailyActivities()
{

    for(int i = 0; i < dailyActivities.activity.size(); i++)
    {
        if(dailyActivities.activity.get(i).equals(""))
        {
            dailyActivities.activity.set(i, "Free time");
            dailyActivities.malariaRisk.set(i, 1.0);
            dailyActivities.where.set(i, "At home");
            dailyActivities.cp.set(i, household.cpPosition);
            dailyActivities.type.set(i, "Free time");

        }
    }

}

//===========================================================
public Activity getSleepActivity()
{

    Activity a = null;
    //System.out.println ("-------------------");
    //System.out.println ("age seg = " + ageSegment.name);

    for(int i = 0; i < activities.size(); i++)
    {
       a = (Activity)activities.get(i);
       //System.out.println ("Activity name = " + a.name);
       if(a.name.equals("Sleep"))
       {
           //System.out.println ("Activity get act = " + a.name);
           return a;
       }
    }
    System.out.println ("No sleep activity for this age segment.....");
    System.exit(0);

    return a;
}



//===========================================================
public int  getHourDiff(int from, int to)
{
    return ((to -from + 24) % 24);
}


//===========================================================
public void  resetWeeklyActivities()
{
    for(int i = 0; i < activities.size(); i++)
    {
       activitiesNTimes.set(i, 0);
    
    }
}

//===========================================================
public void  getFarm()
{
    Point pHuman = sim.utils.getCellCentralPoint(cpPosition.getXcor(), cpPosition.getYcor(), "geo");

    double distMin = 1000000.0;
    Place placeMin = null;
    CoverPixel cpMin = null;

    for (int ii = 0; ii < sim.farmPlaces.size(); ii++)
    {

        Place place = (Place)sim.farmPlaces.get(ii);
        if(place == null)continue;
         
        Point pPlace =  place.centralPoint;

        CoverPixel cp = place.cp;

        double dist = pPlace.distance(pHuman);

        if(dist < distMin)
        {
           distMin = dist;
           placeMin = place;
           cpMin= cp;
        }

    }

    if(cpMin != null)
    {
        //System.out.println ("Farmer In ++++++++++++++++++++++++++++++++");
        //System.exit(0);
        workPlace = cpMin;
        sim.farmPlaces.remove(placeMin);
    }
    else
    {
       workPlace = null;
    }

    //System.out.println (sim.farmPlaces.size());

    //ran = state.random.nextDouble();
    //if(ran > farmerProbHavingFarmOut)workPlace = getRandomCp(sim);
}

}
