/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

public class HumanSchedule
{
   private static final long serialVersionUID = 1L;
   
   public List<String> activity = new ArrayList<String>();
   public List<Double> malariaRisk = new ArrayList<Double>();
   public List<CoverPixel> cp = new ArrayList<CoverPixel>();
   public List<String> where = new ArrayList<String>();
   public List<String> type = new ArrayList<String>();

   public AmaSim sim;
   public SimState state;

   //====================================================
   public HumanSchedule(SimState pstate)
   {
       sim = (AmaSim)pstate;
       state = pstate;

       for(int i = 0; i < 24; i++)
       {
           activity.add("");
           malariaRisk.add(null);
           cp.add(null);
           where.add("");
           type.add("");
       }
   }

   //====================================================
   public void print()
   {
       System.out.println(" ");
       System.out.println("-- Human Daily Activities resume ---");

       int sleep = 0;
       int work = 0;
       int leisure = 0;
       int sport = 0;
       int school = 0;

       for(int i = 0; i < activity.size(); i++)
       {
           //System.out.println(" ");
           //System.out.println("-- Activity   ---------------");
           
           if(activity.get(i).equals("Sleep"))sleep++;
           if(where.get(i).equals("jobIn") || where.get(i).equals("jobOut"))work++;
           if(activity.get(i).equals("Leisure Place"))leisure++;
           if(activity.get(i).equals("Sport Field"))sport++;
           if(activity.get(i).equals("School"))school++;

           System.out.println("From " + i + " to " + (i + 1) + " Activity: " + activity.get(i) + " where: " + where.get(i) + " Type: " + type.get(i));
           //System.out.println("Cp position activity: " + cp.get(i));
       }

       System.out.println(" ");
       System.out.println("Num. hours Sleep: "+ sleep);
       System.out.println("Num. hours Work: "+ work);
       System.out.println("Num. hours Leisure: "+ leisure);
       System.out.println("Num. hours Sport: "+ sport);
       System.out.println("Num. hours School: "+ school);
   }



//============================================================   
}
