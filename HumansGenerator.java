/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.Geometry;

public class HumansGenerator 
{
   private static final long serialVersionUID = 1L;

   //====================================================
   public HumansGenerator()
   {

   }

   //====================================================
   public void generateHumans(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

      for(int i = 0; i < sim.household_bag.size(); i++)
      {
          Household hh = (Household)sim.household_bag.get(i);

          hh.generatePeople(state);
      }
      System.out.println (sim.human_bag.size() + " humans generated");

      if(sim.highRiskWhat == 4)
      {
          int stop = sim.numPeopleProtectedForever - 1;

          if(stop != 0)
          {
              sim.human_bag.shuffle(state.random);

              for(int i = 0; i < sim.human_bag.size(); i++)
              {
                  Human h = (Human)sim.human_bag.get(i);
                  h.isProtectedForever = true;
                  if(i == stop)break;
              }
          }

      }

   }


//============================================================   
}
