/*
   Copyright 2011 by Francesco Pizzitutti
   Licensed under the Academic Free License version 3.0
   See the file "LICENSE" for more information
   */

package sim.app.AmaSim;

import sim.engine.*;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;

import java.text.ParseException;

public class HybridAnalysis implements Steppable
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    AmaSim sim = null;
    SimState state = null;

    Bag mosHumanPixels = new Bag();      

    int numHumansPerPixel = 0;

    HashMap<Integer, Double> mortalityH = new HashMap<Integer, Double>();
    HashMap<Integer, Double> bites = new HashMap<Integer, Double>();

    double mH = 0.0;
    double bb = 0.0;
    
    public int statsAvg = 0;

    //====================================================
    public HybridAnalysis(AmaSim psim)
    {
        sim = psim;
        state = (SimState)sim;

        initPixelBag();

        removeHumans();
        numHumansPerPixel++;
        setNumHumansPerPixel(numHumansPerPixel);

    }

    //====================================================
    public void removeHumans()
    {
        int size = mosHumanPixels.size();
        int stats = 0;

        for(int i = 0; i < size; i++)
        {
            CoverPixel cpm = ((CoverPixel)mosHumanPixels.objs[i]);

            stats = 0;

            for(int j =0; j < cpm.households.size(); j++)
            {
                Household hh = (Household)cpm.households.get(j);
                CoverPixel hhCp = hh.cpPosition;

                for(int m = 0; m < hh.people.size(); m++)
                {
                    Human h = (Human)hh.people.get(m);
                    removeHumanFromSimulation(h);
                }
            }
        }

    }

    //====================================================
    public void initPixelBag()
    {
        //Select the pixels with human presence ---------
        Bag pixelsMosquito = sim.pixelMosquitosGrid.elements();
        int size = pixelsMosquito.size();

        Boolean first = true;

        for(int i = 0; i < size; i++)
        {
            CoverPixel cpm = ((CoverPixel)pixelsMosquito.objs[i]);

            if(cpm.containsHousehold)mosHumanPixels.add(cpm);
        }

        System.out.println ("Num of mosquito pixels containing hinabited humans: " + mosHumanPixels.size()); 

    }


    //====================================================
    public void setNumHumansPerPixel(int numH)
    {
        int size = mosHumanPixels.size();
        int stats = 0;

        for(int i = 0; i < size; i++)
        {
            CoverPixel cpm = ((CoverPixel)mosHumanPixels.objs[i]);

            stats = 0;

            for(int j =0; j < cpm.households.size(); j++)
            {
                Household hh = (Household)cpm.households.get(j);
                CoverPixel hhCp = hh.cpPosition;
hh:
                for(int m = 0; m < hh.people.size(); m++)
                {
                    Boolean protect = false;
                    Boolean move = false;

                    double ran = state.random.nextDouble();
                    if(ran <= sim.fractProtectedPeople)protect = true;

                    //if(protect)
                    //{
                    //    System.out.println("Is Protect in the household..........");
                    //    System.exit(0);
                    //}

                    ran = state.random.nextDouble();
                    if(ran <= sim.fractPeopleMoveAround)move = true;

                    Human h = new Human(state, hhCp, hh);

                    getDailyActivitiesHybridAnalysis(h);

                    hh.residents.add(h);

                    hh.isOccupied = true;

                    if(protect)h.setIsProtected(true);
                    if(protect)h.setIsProtectedInTheHousehold(true);
                    if(move)h.setIsMovible(true);
                    //System.exit(0);

                    stats++;

                    if(stats >= numH)break hh;
                }

            }
        }

        if(sim.highRiskWhat == 4)
        {
            int stop = sim.numPeopleProtectedForever - 1;

            if(stop != 0)
            {
                sim.human_bag.shuffle(state.random);

                for(int i = 0; i < sim.human_bag.size(); i++)
                {
                    Human h = (Human)sim.human_bag.get(i);
                    h.isProtectedForever = true;
                    if(i == stop)break;
                }
            }

        }



    }

    //====================================================
    public void sumAvgs()
    {
        int size = mosHumanPixels.size();
        int stats = 0;

        for(int i = 0; i < size; i++)
        {
            CoverPixel cpm = ((CoverPixel)mosHumanPixels.objs[i]);
            statsAvg++;

            mH = mH + (double)cpm.nDiedMosForHuman/cpm.nMosquitos;
            bb = bb + (double)cpm.nBites/cpm.nMosquitos;

            //System.out.println(numHumansPerPixel + " " + cpm.nDiedMosForHuman + " " + cpm.nBites + " "  + statsAvg);
            //System.out.println(numHumansPerPixel + " " + mH + " " + bb + " "  + statsAvg);

            cpm.nDiedMosForHuman = 0;
            cpm.nBites = 0;
        }
    }

    //====================================================
    public void storeAvgs()
    {
        mortalityH.put(numHumansPerPixel, (double)mH/statsAvg);
        bites.put(numHumansPerPixel, (double)bb/statsAvg);

        //System.out.println(numHumansPerPixel + " " + mH + " " + bb + " "  + statsAvg);

        mH = 0.0;
        bb = 0.0;
        statsAvg = 0;
    }

    //====================================================
    public void printRates()
    {
        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println ("Num hum per pixel -- Averages mortality human -- Bite rate ---");

        Iterator it = mortalityH.entrySet().iterator();

        while (it.hasNext()) 
        {
            Map.Entry pairs = (Map.Entry)it.next();
            Integer ii = (Integer)pairs.getKey();
            double mhh = (Double)pairs.getValue();
            double bbb = bites.get(ii);

            System.out.println (ii + " " + mhh + " " + bbb);
        }
        //System.exit(0);

        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println ("++++++++++++++++++++++++++++++++++++++++++++");

    }

    //====================================================
    public void step(SimState pstate)
    {
        state = pstate;
        int nowi = sim.utils.getTime();

        int nowPrint = (int)state.schedule.getTime();  

        sumAvgs();

        //System.out.println ("nowi: " + nowi);
        //System.out.println ("nowPrint: " + nowPrint);

        //if(nowPrint % 24 == 8760)
        if(nowPrint % 4380 == 0)
        {
            storeAvgs();
            //System.exit(0);

            removeHumans();
            numHumansPerPixel++;
            setNumHumansPerPixel(numHumansPerPixel);

            printRates();
            //System.exit(0);
        }

       int now = (int)state.schedule.getTime();  
       if(now == sim.num_step - 1)printRates();




    }

    //= Remove human form simulation ======================
    public  void removeHumanFromSimulation(Human human)
    {
        //System.out.println("Human is going to " + newSim.community.name + " from " + oldSim.community.name);

        Household oldHousehold = human.household;
        oldHousehold.removeHuman(human);

        human.cpPosition.removeHuman(human);

        sim.humanGrid.remove(human);

        sim.human_bag.remove(human);

        human.places = new HashMap<String, CoverPixel>();

        human.stopper.stop();         
    }


//===========================================================
public void getDailyActivitiesHybridAnalysis(Human human)
{
    human.dailyActivities = new HumanSchedule(state);
    human.resetWeeklyActivities();

    //Here order is importan denoting the agent dailyActivities priorities:
    //work first then sleep and then others activities
    human.getWork();
    human.getSleep();
    human.getOtherActivities();
    human.completeDailyActivities();

    human.writeHighRisk = false;

    //System.out.println ("Day of the week: " + weekDay);
    //System.out.println ("Human age seg: " + ageSegment.name);
    //dailyActivities.print();
    ////System.exit(0);

}



}
