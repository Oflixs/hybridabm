/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import com.vividsolutions.jts.geom.Polygon;

import mosDensity.*;
import com.mathworks.toolbox.javabuilder.*;

public class HybridInterface implements Steppable
{
   private static final long serialVersionUID = 1L;

   public Stoppable stopper;

   public AmaSim sim = null;
   public SimState state = null;

   public int[] water; // pixel with water that can be used as breeding site = 1, = 0 otherwise
   public double[] mh; //mortality due to bed nets
   public double[] bb; // bite rate 
   public double[] mosquitoesBlood; // density of mosquitoes searching for blood
   public double[] mosquitoesWater; // density of mosquitoes searching for water

   public double[] mosquitoesBloodInit; // density of mosquitoes searching for blood

   public int updateFreq = 24; // in timev steps that is hours

   public femDensity hybDensity;

   public int mheight = 0;
   public int mwidth  = 0;

   //Pitxi: inserire tutte le variabili che servono per inizializzare l' interfaccia alcune sono gia definite tipo sim.geoGridWidth e height e sim.mosquitoGridWidth e height altre forse mancano 

   //====================================================
   public HybridInterface(AmaSim psim)
   {
      sim = psim;
      state = (SimState)sim;

      init();
      //System.exit(0);

      //time, ordering and steppable
      this.stopper = sim.schedule.scheduleRepeating(0.0, 9, this);

      //-----------------------------------------------------------
      int gheight = sim.geoGridHeight;
      int gwidth  = sim.geoGridWidth;


   }

   //====================================================
   public void init()
   {
       mheight = sim.mosquitoGridHeight;
       mwidth  = sim.mosquitoGridWidth;
       System.out.println ("Mos Height: " + mheight + " Mos width: " + mwidth);

       int statTot = 0;

       water               = new int[mwidth * mheight];
       mh                  = new double[mwidth * mheight];
       bb                  = new double[mwidth * mheight];
       mosquitoesBlood     = new double[mwidth * mheight];
       mosquitoesWater     = new double[mwidth * mheight];
       mosquitoesBloodInit = new double[mwidth * mheight];

       //Initialize the grids ---------------------------------------
       CoverPixel cpm = null;
       Bag cps = sim.pixelMosquitosGrid.elements();
       int num_pixels = cps.size();
       for(int c = 0; c < num_pixels; c++)
       {
           cpm = ((CoverPixel)cps.objs[c]);
           updatePixel(cpm);

           int i = cpm.xcor;
           int j = cpm.ycor;

           mosquitoesBlood[i * mheight + j] = cpm.nMosquitosBlood/(sim.mosquitoCellSize * sim.mosquitoCellSize);
           mosquitoesWater[i * mheight + j] = cpm.nMosquitosWater/(sim.mosquitoCellSize * sim.mosquitoCellSize);

           mosquitoesBloodInit[i * mheight + j] = cpm.nMosquitosBlood/(sim.mosquitoCellSize * sim.mosquitoCellSize);

           statTot++;
       }

      System.out.println ("Num scanned geo pixels: " + statTot + " num mosquito pixels from sizes:" + mwidth * mheight);


       //Initialize the densities -----------------------------------
       try{
           System.out.println ("Starting the initialization od hybDensity");
           System.out.println ("new hybDensity");
           hybDensity = new femDensity();
           System.out.println ("femPDEInit");


           Object[] list = null; //mortality due to bed nets
           list = new Object[2];
           list[0] = (double)mwidth;
           list[1] = (double)mheight;

           hybDensity.femPDEInit(list);
           //hybDensity.femPDEInit((double)mwidth, (double)mheight);
           //Pitxi hourly mort prob is given fixed but in reality is 
           //T dependent --> correct and use the right formula

           System.out.println ("femInitialConditions");

           list = new Object[2];
           list[0] = mosquitoesBlood;
           list[1] = mosquitoesWater;
           
           hybDensity.femInitialConditions(list);
           //hybDensity.femInitialConditions(mosquitoesBlood, mosquitoesWater);

           System.out.println ("Initialization od hybDensity completed");
       }
       catch(Exception e)
       {
           System.out.println(e.toString());
       }


      //System.exit(0);
      // 
   }

   //====================================================
   public void updatePixel(CoverPixel cpm)
   {
       int i = cpm.xcor;
       int j = cpm.ycor;

       //System.out.println ("CovePixel type: " + cpm.type);
       //System.out.println ("i: " + i + " j: " + j);
       //System.out.println ("Mos Height: " + mheight + " Mos width: " + mwidth);

       CoverPixel cpg = null;
       Bag cps = cpm.linkPixels;
       int num_pixels = cps.size();

       Boolean waterYes = false;
       int totHumans = 0;
       int humansProt = 0;

       for(int c = 0; c < num_pixels; c++)
       {
           CoverPixel cp = ((CoverPixel)cps.objs[c]);

           if(cp.getIsWater() == 1)waterYes = true;

           if(cp.containsHumans())
           {
               for(int p = 0; p < cp.people.size(); p++)
               {
                   Human h = (Human)cp.people.get(p);
                   if(h.getIsProtected())humansProt++;
                   totHumans++;
               }
           }
       }

       if(waterYes)water[i * mheight + j] = 1;
       else     water[i * mheight + j] = 0;

       if(totHumans != 0)
       {
           mh[i * mheight + j] = sim.probMosquitoDieBitingProtectedPerson * humansProt/totHumans;
           bb[i * mheight + j] = (totHumans - humansProt)/totHumans;
       }
       else
       {
           mh[i * mheight + j] = 0; 
           bb[i * mheight + j] = 0; 
       }

   }


   //====================================================
   public void updateGrids()
   {
       System.out.println ("-------------------------------------");

       for(int i = 0; i < sim.hybridPixelsToChange.size(); i++)
       {
           System.out.println ("=== >> Upgrading a Pixel!!!! << ===");
           CoverPixel cp = (CoverPixel)sim.hybridPixelsToChange.get(i);
           //System.out.println ("CovePixel type: " + cp.type);
           updatePixel(cp);
       }

       //Pitxi: devo fare update anche della grid di mosquito o ti serve solo all' inizio e poi mai piu'?

       sim.hybridPixelsToChange = new Bag();
       //System.exit(0);
   }


   //====================================================
   public void step(final SimState state)
   {
       int now = (int)state.schedule.getTime();  
       Object[] list = null; //mortality due to bed nets
       if(now % updateFreq == 0)
       {
           updateGrids();

           double gm = 0.85/24;
           double hatTime = 15 * 24;
           double natality = 7.0 / 24;
           try{

               list = new Object[6];
               list[0] = (Object)water;
               list[1] = (Object)mh;
               list[2] = (Object)bb;
               list[3] = (Object)gm;
               list[4] = (Object)natality;
               list[5] = (Object)hatTime;


               hybDensity.femUpdate(list);
               //hybDensity.femUpdate(water, mh, bb,  gm, natality, natality, hatTime);

               list = hybDensity.femPDEStep(1);

               mosquitoesBlood = new double[mwidth*mheight];
               mosquitoesWater = new double[mwidth*mheight];

               mosquitoesBlood = (double[])list[0]; 
               mosquitoesWater = (double[])list[1]; 

           }
           catch(Exception e)
           {
               System.out.println(e.toString());
           }

          //check output content
          for(int i = 0; i < mwidth; i++)
          {
              for(int j = 0; j < mheight; j++)
              {
                  System.out.println (i + " " + j + ": " 
                          + mosquitoesWater[i * mheight + j] + " " 
                          + mosquitoesBlood[i * mheight + j] + " " 
                          + (mosquitoesBlood[i * mheight + j] - mosquitoesBloodInit[i * mheight + j]));

              }
          
          }

          //System.exit(0);

       }
       //System.exit(0);
       //

   }


   //====================================================
   public void updateMosPixels()
   {
       int mheight = sim.mosquitoGridHeight;
       int mwidth  = sim.mosquitoGridWidth;

       Bag cps = sim.pixelMosquitosGrid.elements();
       int num_pixels = cps.size();

       CoverPixel cp;

       for(int c = 0; c < num_pixels; c++)
       {
           cp = ((CoverPixel)cps.objs[c]);
           int i = cp.xcor;
           int j = cp.ycor;

           cp.mosDensity = mosquitoesBlood[i * mheight + j];//Pitxi se la variabile e' sempre quella senno' cambiarla

       }



   }

//============================================================   
}
