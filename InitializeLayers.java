/*
   Copyright 2011 by Francesco Pizzitutti
   Licensed under the Academic Free License version 3.0
   See the file "LICENSE" for more information
   */

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

import sim.field.grid.*;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.prep.*;

import sim.util.geo.MasonGeometry;

import sim.field.geo.GeomVectorField;

import java.util.ArrayList;
import java.util.List;


public class InitializeLayers implements Steppable
{

private static final long serialVersionUID = 1L;
final AmaSim sim;
final SimState state;

double cpWaterChangingMinElev = 0.0;

//====================================================
public InitializeLayers(final SimState pstate)
{
    state = pstate; 
    sim = (AmaSim)state;
}

//====================================================
public void step(final SimState state)
{
}



//====================================================
public void setupCpClusters()
{
    System.out.println("================================================");
    System.out.println("Initialize cover pixels grid ...................");
    System.out.println(" ");

    int nCpPerClusterLocal = 0;
    int clusterSide = 0;
    nCpPerClusterLocal = (int)Math.ceil(Math.sqrt(sim.nCpPerCluster));

    if(nCpPerClusterLocal % 2 == 0)clusterSide = nCpPerClusterLocal++;
    else clusterSide = nCpPerClusterLocal;
    int halfClusterSide = (int)Math.floor(clusterSide * 0.5);

    if(clusterSide % 2 == 0)clusterSide++;
    System.out.println("Cluster side: " + clusterSide);
    System.out.println("Cluster halfside: " + halfClusterSide);
    System.out.println("geoGridWidth: " + sim.geoGridWidth);
    System.out.println("geoGridHeight: " + sim.geoGridHeight);
    System.out.println("Extimated num geo grid pixels: " + sim.geoGridHeight * sim.geoGridWidth);

    //System.exit(0);
    int stati  = 0;
    int statj  = 0;
    int stats  = 0;
    int stats1 = 0;
    int stats2 = 0;
    int stats3 = 0;
    int stats4 = 0;

    for(int i = (halfClusterSide + 1); i < sim.geoGridWidth; i = i + clusterSide)
    {
        //System.out.println("i: " + i);

        for(int j = (halfClusterSide + 1) ; j < sim.geoGridHeight; j = j + clusterSide)
        {

            Point center = sim.utils.getCellCentralPoint(i, j, "geo");
            Polygon square = sim.utils.getSquareAroundPoint(center, sim.geoCellSize * clusterSide);

            Cluster cluster = new Cluster(state, i, j, square);
            sim.clusters.add(cluster);

            //Set the geoGrid cps-----------------------------------
            for(int n = i - (halfClusterSide + 1); n < i + halfClusterSide; n++)
            {
                if(n >= sim.geoGridWidth)continue;
                stati = n;

                for(int m = j - (halfClusterSide + 1); m < j + halfClusterSide; m++)
                {
                    if(m >= sim.geoGridHeight)continue;

                    statj = m;
                    //The square that corresponds to the grod coords n, m 
                    //in the geo reference siystyem is built
                    Point centerCp = sim.utils.getCellCentralPoint(n, m, "geo");
                    Polygon squareCp = sim.utils.getSquareAroundPoint(centerCp, sim.geoCellSize);
                    //System.exit(0);

                    //The coverPixel gets (i, j) as grid coordinates and
                    //the correspondingi suqare in the geo ref system 
                    //as masonGeoemtry
                    MasonGeometry mg = new MasonGeometry(squareCp);

                    CoverPixel cp = new CoverPixel(state, n, m, squareCp);
                    cp.setMasonGeometry(mg);
                    cp.setType("geo");

                    //System.out.println ("n: " + n + " m: " + m);
                    sim.geoGrid.set(
                            sim.utils.getCoordsToDisplay(state, n, m, "geo")[0],
                            sim.utils.getCoordsToDisplay(state, n, m, "geo")[1],
                            cp
                            );

                    cp.clusters.add(cluster);
                    cluster.coverPixels.add(cp);
                    stats1++;
                    //System.out.println ("Cluster");
                }
            }
                //System.exit(0);


            }
        }

        System.out.println ("stat1: " + stats1 + " stat2: " + stats2);
        System.out.println ("num of pixels so far: " + ((stati + 1)*(statj + 1)));
        System.out.println ("num of pixels created so far: " + stats1);
        //Bag cps = sim.geoGrid.elements();
        //int num_pixels = cps.size();
        //System.out.println ("num of pixels in geo grid: " + num_pixels);
        //System.out.println ("num of pixels created: " + stats);
       // System.exit(0);


        //checkClusters(state);

        int maxj = statj;
        int maxi = stati;
        stats = 0;

        //Complete the right border ---------------------------------
        if(stati < sim.geoGridWidth)
        {
            stati = 0;

            int i = (halfClusterSide + 1) + (maxi + 1);
            for(int j = (halfClusterSide + 1) ; j < sim.geoGridHeight; j = j + clusterSide)
            {
                Point center = sim.utils.getCellCentralPoint(i, j, "geo");
                Polygon square = sim.utils.getSquareAroundPoint(center, sim.geoCellSize * clusterSide);

                Cluster cluster = new Cluster(state, i, j, square);
                sim.clusters.add(cluster);

                //Set the geoGrid cps-----------------------------------
                //System.out.println ("i:" + i);
                //System.out.println ("n:" + (i - (halfClusterSide + 1) + "n max: " + (sim.geoGridWidth - 1)));
                for(int n = i - (halfClusterSide + 1); n < i + halfClusterSide; n++)
                {
                    //System.out.println (n + " ========================");
                    if(n >= sim.geoGridWidth)continue;

                    for(int m = j - (halfClusterSide + 1); m < j + halfClusterSide; m++)
                    {
                        if(m >= sim.geoGridHeight)continue;
                        //System.out.println ("n:" + n + " m: " + m);

                        //The square that corresponds to the grod coords n, m 
                        //in the geo reference siystyem is built
                        Point centerCp = sim.utils.getCellCentralPoint(n, m, "geo");
                        Polygon squareCp = sim.utils.getSquareAroundPoint(centerCp, sim.geoCellSize);

                        //The coverPixel gets (i, m) as grid coordinates and
                        //the correspondingi suqare in the geo ref system 
                        //as masonGeoemtry
                        MasonGeometry mg = new MasonGeometry(squareCp);

                        CoverPixel cp = new CoverPixel(state, n, m, squareCp);
                        cp.setMasonGeometry(mg);
                        cp.setType("geo");

                        //System.out.println (n + " " + (-m + sim.geoGridHeight - 1));
                        sim.geoGrid.set(
                                sim.utils.getCoordsToDisplay(state, n, m, "geo")[0],
                                sim.utils.getCoordsToDisplay(state, n, m, "geo")[1],
                                cp
                                );

                        cp.clusters.add(cluster);
                        cluster.coverPixels.add(cp);

                        stats2++;
                    }
                }

            }

        System.out.println("Initilization geo grid pixels ok   .............");
        
        }
        System.out.println("Aggiunti i: " + stats2);
        System.out.println ("stat1: " + stats1 + " stat2: " + stats2);
        //checkClusters(state);
        //System.exit(0);

        //Complete the bottom border ---------------------------------
        stats = 0;
        if(statj < sim.geoGridHeight)
        {
            statj = 0;

            for(int i = (halfClusterSide + 1) ; i < sim.geoGridWidth; i = i + clusterSide)
            {
     

                int j = (halfClusterSide + 1) + maxj + 1;

                Point center = sim.utils.getCellCentralPoint(i, j, "geo");
                Polygon square = sim.utils.getSquareAroundPoint(center, sim.geoCellSize * clusterSide);

                Cluster cluster = new Cluster(state, i, j, square);
                sim.clusters.add(cluster);

                //Set the geoGrid cps-----------------------------------
                //System.out.println ("j:" + j);
                //System.out.println ("m:" + (j - (halfClusterSide + 1) + "n max: " + (sim.geoGridHeight - 1)));
                for(int n = i - (halfClusterSide + 1); n < i + halfClusterSide; n++)
                {
                    if(n >= sim.geoGridWidth)continue;

                    for(int m = j - (halfClusterSide + 1); m < j + sim.geoGridHeight; m++)
                    {
                        if(m >= sim.geoGridHeight)continue;

                        //The square that corresponds to the grod coords n, m 
                        //in the geo reference siystyem is built
                        Point centerCp = sim.utils.getCellCentralPoint(n, m, "geo");
                        Polygon squareCp = sim.utils.getSquareAroundPoint(centerCp, sim.geoCellSize);

                        //The coverPixel gets (i, m) as grid coordinates and
                        //the correspondingi suqare in the geo ref system 
                        //as masonGeoemtry
                        MasonGeometry mg = new MasonGeometry(squareCp);

                        CoverPixel cp = new CoverPixel(state, n, m, squareCp);
                        cp.setMasonGeometry(mg);
                        cp.setType("geo");

                        //System.out.println ("n:" + n + " m: " + m);
                        sim.geoGrid.set(
                                sim.utils.getCoordsToDisplay(state, n, m, "geo")[0],
                                sim.utils.getCoordsToDisplay(state, n, m, "geo")[1],
                                cp
                                );

                        cp.clusters.add(cluster);
                        cluster.coverPixels.add(cp);
                        stats3++;
                    }
                }

            }
        
        }
        System.out.println("Aggiunti i: " + stats3);
        //checkClusters(state);

        //Complete the last cluster    ---------------------------------
        stati = 0;
        statj = 0;
        stats = 0;


            int i = (halfClusterSide + 1) + maxi + 1;
            int j = (halfClusterSide + 1) + maxj + 1;

            Point center = sim.utils.getCellCentralPoint(i, j, "geo");
            Polygon square = sim.utils.getSquareAroundPoint(center, sim.geoCellSize * clusterSide);

            Cluster cluster = new Cluster(state, i, j, square);
            sim.clusters.add(cluster);

            //Set the geoGrid cps-----------------------------------
            //System.out.println ("j:" + j);
            //System.out.println ("m:" + (j - (halfClusterSide + 1) + "n max: " + (sim.geoGridHeight - 1)));
            for(int n = i - (halfClusterSide + 1); n < i + halfClusterSide; n++)
            {
                if(n >= sim.geoGridWidth)continue;

                for(int m = j - (halfClusterSide + 1); m < j + sim.geoGridHeight; m++)
                {
                    if(m >= sim.geoGridHeight)continue;

                    //The square that corresponds to the grod coords n, m 
                    //in the geo reference siystyem is built
                    Point centerCp = sim.utils.getCellCentralPoint(n, m, "geo");
                    Polygon squareCp = sim.utils.getSquareAroundPoint(centerCp, sim.geoCellSize);

                    //The coverPixel gets (i, m) as grid coordinates and
                    //the correspondingi suqare in the geo ref system 
                    //as masonGeoemtry
                    MasonGeometry mg = new MasonGeometry(squareCp);

                    CoverPixel cp = new CoverPixel(state, n, m, squareCp);
                    cp.setMasonGeometry(mg);
                    cp.setType("geo");

                    //System.out.println ("n:" + n + " m: " + m);
                    sim.geoGrid.set(
                            sim.utils.getCoordsToDisplay(state, n, m, "geo")[0],
                            sim.utils.getCoordsToDisplay(state, n, m, "geo")[1],
                            cp
                            );

                    cp.clusters.add(cluster);
                    cluster.coverPixels.add(cp);
                    statj++;
                    stats4++;
                }

        }

        System.out.println("Aggiunti i: " + stats4);
        //checkClusters(state);

        System.out.println("Totale pixels creati: " + (stats1 + stats2 + stats3 + stats4));


        //System.out.println ("stati: " + stati + " statj: " + statj);
        //System.out.println ("num of pixels to be added: " + (stati + statj));

        //System.exit(0);

    }

    //====================================================
    public void checkClusters(final SimState state)
    {

        System.out.println ("=== Check Clusters ===========================");
        final AmaSim sim = (AmaSim)state;
        int stats = 0;
        Cluster cluster = null;

        //Only to check-----------------
        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();
        System.out.println ("Num pixel in geo grid: " + num_pixels);

        //clusters ---------------------
        int numClusters = sim.clusters.size();
        System.out.println ("Num of clusters: " + numClusters);

        stats = 0;
        for(int i =  0; i < numClusters; i++)
        {
            cluster = ((Cluster)sim.clusters.objs[i]);

            int numCp = cluster.coverPixels.size();

            for(int c = 0; c < numCp; c++)
            {
                CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);

                stats++;

                //if(cp.getElevation() <= 0.0)
                //System.out.println("Ma ceh cazzooooooooo! " + cp.getElevation());

            }
            if(i == (numClusters  - 0))break;

        }

 
        System.out.println ("Num pixel in clusters: " + stats);

        System.out.println ("=== Check Clusters ===========================");


        //System.exit(0);
    }


    //====================================================
    public void setWater(final SimState state)
    {
        System.out.println("Setting Pixels water state......................");
        System.out.println(" ");

        final AmaSim sim = (AmaSim)state;

        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();

        double totNumCells = sim.geoGridWidth * sim.geoGridHeight;
        System.out.println("Num pixels in geo grid: " + num_pixels);
        System.out.println("Extimated Num of pixels: " + totNumCells);
        //System.exit(0);

        int statsHigh = 0;
        int statsLow  = 0;
        int statsExcluded  = 0;

        setHighWaterShp();
        setLowWaterShp();
        setExcludedWaterShp();

        System.out.println("Setting water excluded  ....");
        setWaterExcluded();

        System.out.println("Setting water low   ........");
        setWaterLow();

        System.out.println("Setting water high  ........");
        setWaterHigh();


        //Counts the different kinds of pixels and 
        //setchanging Cp bag
        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            //if(cp.getIsWaterHigh() == 1 && cp.getIsWaterLow() == 1)
            //    System.out.println("Superposition of high and low");

            if(cp.getIsWaterHigh() == 1)statsHigh++;
            if(cp.getIsWaterLow() == 1)statsLow++;
            if(cp.getIsRiverExcluded() == 1)statsExcluded++;

            if(cp.isWaterChanging())sim.waterChangingCps.add(cp);

            //Patch waiting for the new covers
            if(cp.getIsWaterHigh() == 0 && cp.getIsWaterLow() == 1)
            {
                cp.setIsWaterLow(0);
                cp.setIsWater(0);
                cp.setIsDryHigh(0);
            }
            //End of patch

        }

        /*
        System.out.println("Num pixels in geo grid: " + num_pixels);

        double perc = Math.round((100 * statsHigh/totNumCells) * 100  );
        System.out.println(perc/100  + "% of the cells is covered by water in the High Water regime");
        System.out.println("Num pixels high water: " + statsHigh);

        perc = Math.round((100 * statsLow/totNumCells) * 100  );
        System.out.println(perc/100  + "% of the cells is covered by water in the Low Water regime");
        System.out.println("Num pixels low water: " + statsLow);

        perc = Math.round((100 * statsExcluded/totNumCells) * 100  );
        System.out.println(perc/100  + "% of tthe cells is excluded from mos prod because in the middle of the river");
        System.out.println("Num pixels excluded water: " + statsExcluded);
        System.out.println(" ");

        System.exit(0);
        */


    }




    //====================================================
    public void setWaterHigh()
    {

        //Associates geo grid cps to the water cover -------------
        int numClusters = sim.clusters.size();

        for(int i =  0; i < numClusters; i++)
        {
            //System.out.println("---------------------------------");
            //System.out.println("Water high num cluster: " + i);
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            int numWaters = cluster.highWaters.size();
            int numCp     = cluster.coverPixels.size();

            //System.out.println("Num water in cluster: " + numWaters);
            //System.out.println("Num coverPixels in cluster: " + numCp);
            if(numWaters <= 0)continue;

    water:
            for(int w = 0; w < numWaters; w++)
            {

                //System.out.println("-------");
                //System.out.println(w);
                //System.out.println("Water number: " + w);

                MasonGeometry obj = (MasonGeometry)cluster.highWaters.objs[w];
                Geometry gwater = (Geometry)obj.getGeometry();
                PreparedGeometry pwater = PreparedGeometryFactory.prepare(gwater);
    cp:
                for(int c = 0; c < numCp; c++)
                {
                    CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);
                    Polygon square = (Polygon)cp.square;
                    //if(c % 100 == 0)System.out.println("Cp number: " + c);
                    //System.out.println("Cp number: " + c);

                    //patch pitxi
                    /*
                    if(cp.getIsWaterHigh() == 1)continue;
                    if(cp.getIsWaterLow() == 1)
                    {
                        cp.setIsWaterHigh(1);
                        continue;
                    }

                    if(cp.getIsRiverExcluded() == 1)
                    {
                        cp.setIsWaterHigh(1);
                        continue;
                    }
                    */


                    //if(pwater.covers(square))
                    //{
                    //    //System.out.println("Is Water High!!!!!");
                    //    cp.setIsWaterHigh(1); 
                    //    //cp.setIsWater(1); 
                    //    //System.out.println("Cpx: " + cp.getXcor());
                    //    //System.out.println("Cpy: " + cp.getYcor());
                    //    //break cp;
                    //    continue;
                    //}

                    if(pwater.intersects(square))
                    {
                        //System.out.println("Is Water High!!!!!");
                        cp.setIsWaterHigh(1); 
                        //cp.setIsWater(1); 
                        //System.out.println("Cpx: " + cp.getXcor());
                        //System.out.println("Cpy: " + cp.getYcor());
                        //break cp;
                        continue;
                    }

                    //if(square.covers(pwater))
                    //{
                    //    //System.out.println("Is Water High!!!!!");
                    //    cp.setIsWaterHigh(1); 
                    //    //cp.setIsWater(1); 
                    //    //System.out.println("Cpx: " + cp.getXcor());
                    //    //System.out.println("Cpy: " + cp.getYcor());
                    //    //break cp;
                    //    continue;
                    //}




                }
            }


        }
    }



    //====================================================
    public void setWaterLow()
    {

        //Associates geo grid cps to the water cover -------------
        int numClusters = sim.clusters.size();

        for(int i =  0; i < numClusters; i++)
        {
            //System.out.println("---------------------------------");
            //System.out.println("Water low num cluster: " + i);
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            int numWaters = cluster.lowWaters.size();
            int numCp     = cluster.coverPixels.size();

            //System.out.println("Num water in cluster: " + numWaters);
            //System.out.println("Num coverPixels in cluster: " + numCp);
            if(numWaters <= 0)continue;

    water:
            for(int w = 0; w < numWaters; w++)
            {

                //System.out.println("-------");
                //System.out.println(w);
                //System.out.println("Water number: " + w);

                MasonGeometry obj = (MasonGeometry)cluster.lowWaters.objs[w];
                Geometry gwater = (Geometry)obj.getGeometry();
                PreparedGeometry pwater = PreparedGeometryFactory.prepare(gwater);
                //System.out.println("Area: " + pwater.getArea());
    cp:
                for(int c = 0; c < numCp; c++)
                {
                    CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);
                    Polygon square = (Polygon)cp.square;
                    //if(c % 100 == 0)System.out.println("Cp number: " + c);
                    //System.out.println("Cp number: " + c);

                    if(cp.getIsWaterLow() == 1)continue;

                    if(cp.getIsRiverExcluded() == 1)
                    {
                        cp.setIsWaterLow(1);
                        continue;
                    }

                    //if(pwater.covers(square))
                    //{
                    //    //System.out.println("Is Water Low!!!!!");
                    //    cp.setIsWater(1); 
                    //    cp.setIsWaterLow(1); 
                    //    //System.out.println("Cpx: " + cp.getXcor());
                    //    //System.out.println("Cpy: " + cp.getYcor());
                    //    //break cp;
                    //    continue;
                    //}

                    if(pwater.intersects(square))
                    {
                        //System.out.println("Is Water Low!!!!!");
                        cp.setIsWater(1); 
                        cp.setIsWaterLow(1); 
                        //System.out.println("Cpx: " + cp.getXcor());
                        //System.out.println("Cpy: " + cp.getYcor());
                        //break cp;
                        continue;
                    }

                }
            }


        }

    }

    //====================================================
    public void setWaterExcluded()
    {

        //Associates geo grid cps to the water cover -------------
        int numClusters = sim.clusters.size();

        for(int i =  0; i < numClusters; i++)
        {
            //System.out.println("---------------------------------");
            //System.out.println("Water excluded num cluster: " + i);
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            int numWaters = cluster.excludedWaters.size();
            int numCp     = cluster.coverPixels.size();

            //System.out.println("Num water in cluster: " + numWaters);
            //System.out.println("Num coverPixels in cluster: " + numCp);
            if(numWaters <= 0)continue;

    water:
            for(int w = 0; w < numWaters; w++)
            {

                //System.out.println("-------");
                //System.out.println(w);
                //System.out.println("Water number: " + w);

                MasonGeometry obj = (MasonGeometry)cluster.excludedWaters.objs[w];
                Geometry gwater = (Geometry)obj.getGeometry();
                PreparedGeometry pwater = PreparedGeometryFactory.prepare(gwater);
    cp:
                for(int c = 0; c < numCp; c++)
                {
                    CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);
                    Polygon square = (Polygon)cp.square;
                    //if(c % 100 == 0)System.out.println("Cp number: " + c);
                    //System.out.println("Cp number: " + c);

                    if(cp.getIsRiverExcluded() == 1)continue;


                    //if(pwater.covers(square))
                    //{
                    //    //System.out.println("Is Water High!!!!!");
                    //    cp.setIsRiverExcluded(1); 
                    //    //System.out.println("Cpx: " + cp.getXcor());
                    //    //System.out.println("Cpy: " + cp.getYcor());
                    //    //break cp;
                    //    continue;
                    //}

                    if(pwater.intersects(square))
                    {
                        //System.out.println("Is Water High!!!!!");
                        cp.setIsRiverExcluded(1); 
                        //System.out.println("Cpx: " + cp.getXcor());
                        //System.out.println("Cpy: " + cp.getYcor());
                        //break cp;
                        continue;
                    }

                }
            }


        }

    }



    //====================================================
    public void findDryHigh(final SimState state)
    {
        final AmaSim sim = (AmaSim)state;
        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();

        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            if(cp.getIsWaterLow() == 1)cp.setIsWater(1);    

            if(cp.getIsWaterHigh() == 0 || cp.getIsWaterLow() == 0)
                cp.setIsDryHigh(1);
        }


    }

    //====================================================
    public void findCpMaxMin(final SimState state)
    {
        System.out.println("Setting up the pixels river macrolevel");
        final AmaSim sim = (AmaSim)state;

        //double deltaLevel = (sim.maxNapoLevel - sim.minNapoLevel) / sim.numRiverMacrolevels;

        //Bag cps = sim.geoGrid.elements();
        Bag cps = sim.waterChangingCps;
        int num_pixels = cps.size();

        double maxElev = -100000;
        double minElev =  100000;
        double elevation = 0.0;

        //========================================
        int statsTot = 0;
        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);
            if(maxElev < cp.getElevation())maxElev = cp.getElevation();
            if(minElev >= cp.getElevation())minElev = cp.getElevation();

        }

        cpWaterChangingMinElev = minElev;
        System.out.println("Water changing pixels min lev: " + minElev);

        //System.exit(0);



    }

    //====================================================
    public void setupCpMacrolevel(final SimState state)
    {
        System.out.println("Setting up the pixels river macrolevel");
        final AmaSim sim = (AmaSim)state;

        int numClusters = sim.clusters.size();

        for(int i =  0; i < numClusters; i++)
        {
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);

            int numCp     = cluster.coverPixels.size();

            double tMin = 100000.0;
            double tMax = -100000.0;

            for(int c = 0; c < numCp; c++)
            {
                CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);

                if(!cp.isWaterChanging())continue;

                if(cp.getElevation() >  tMax)tMax = cp.getElevation();
                if(cp.getElevation() <= tMin)tMin = cp.getElevation();

                //if(cp.getElevation() <= 0.0)
                //System.out.println("Ma ceh cazzooooooooo! " + cp.getElevation());

            }

            //System.out.println("Max: " + tMax);
            //System.out.println("Min: " + tMin);

            for(int c = 0; c < numCp; c++)
            {
                CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);

                if(!cp.isWaterChanging())continue;

                cp.macrolevelMax = tMax;
                cp.macrolevelMin = tMin;
            }

        }

        //System.exit(0);

    }

    //====================================================
    public void setupCpMacrolevelOld(final SimState state)
    {
        System.out.println("Setting up the pixels river macrolevel");
        final AmaSim sim = (AmaSim)state;

        findCpMaxMin(state);

        double deltaLevel = (sim.maxNapoLevel - sim.minNapoLevel) / sim.numRiverMacrolevels;

        //Bag cps = sim.geoGrid.elements();
        Bag cps = sim.waterChangingCps;
        int num_pixels = cps.size();

        double maxElev = -100000;
        double minElev =  100000;
        double elevation = 0.0;

        int statsH = 0;
        int statsL = 0;
        int stats = 0;

        double avgE = 0.0;

        double tMin = 105.0;
        double tMax = 120.0;
                
        List<Double> macroLevels = new ArrayList<Double>();
        List<Integer> statsLev = new ArrayList<Integer>();


        //========================================
        int statsTot = 0;
        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            stats = 0;
            for(double l = sim.minNapoLevel; l <= sim.maxNapoLevel; l = l + deltaLevel )
            {
                if(cp.getElevation() > l + cpWaterChangingMinElev && cp.getElevation() <= l + deltaLevel + cpWaterChangingMinElev)
                {
                    cp.macrolevel = stats;
                    cp.macrolevelMax = l + deltaLevel + cpWaterChangingMinElev ;
                    cp.macrolevelMin = l + cpWaterChangingMinElev;
                    statsTot++;

                }
            
                stats++;
            }
            if(cp.getElevation() > sim.maxNapoLevel)
            System.out.println("cpelev = " + cp.getElevation() + " maxNapolev " + (sim.maxNapoLevel + cpWaterChangingMinElev));

            System.out.println("Stats = " + stats);
        }

        System.out.println("StatsTot = " + statsTot);
        System.out.println("numPixels = " + num_pixels);
        System.out.println(cpWaterChangingMinElev);
        System.exit(0);

    }


    //====================================================
    public void findWaterMinMaxElevation(final SimState state)
    {
        System.out.println("Find water min max elevation");
        final AmaSim sim = (AmaSim)state;

        //Bag cps = sim.geoGrid.elements();
        Bag cps = sim.waterChangingCps;
        int num_pixels = cps.size();

        double maxElev = -100000;
        double minElev =  100000;
        double elevation = 0.0;

        int statsH = 0;
        int statsL = 0;
        int stats = 0;

        double avgE = 0.0;

        double tMin = 105.0;
        double tMax = 116.0;

        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            //if(cp.getIsWaterHigh() == 0 && cp.getIsWaterLow() == 0)continue;
            //if(cp.getIsWaterHigh() != 1)continue;
            //if(cp.getIsRiverExcluded() == 1)continue;

            elevation = cp.getElevation();
            //System.out.println("Elevation: " + elevation);
            avgE = avgE + elevation;

            if(maxElev <= elevation){maxElev = elevation;}
            if(minElev >= elevation){minElev = elevation;}
            //System.out.println("Elevation: " + elevation);
            if(elevation > tMax)
            {
                //System.out.println("Elevation high: " + elevation);
                //System.out.println("statsH: " + statsH);
                statsH++;
            }

            if(elevation <= tMin)
            {
                //System.out.println("Elevation low: " + elevation);
                //System.out.println("statsL: " + statsL);
                statsL++;
            }
            stats++;


        }
        System.out.println(" ");
        System.out.println("Max Elevation Water Changing: " + maxElev);
        System.out.println("Min Elevation Water Changing: " + minElev);
        System.out.println(" ");
        System.out.println("Tot num of pixels: " + stats);
        System.out.println("Tot num of pixels Low: " + statsL);
        System.out.println("Tot num of pixels High: " + statsH);
        System.out.println(" ");
        //System.out.println("Between " + tMin + " and " + tMax + " you have the " + (100 * (stats - statsL - statsH)/(stats) ) + " % of the water changing pixels, Master.");

        System.out.println(" ");
        System.out.println("Avg elevation water changing pixels: " + avgE/stats);

        sim.maxElevationHighWater = maxElev;
        sim.minElevationHighWater = minElev;

        //System.exit(0);

}


    //====================================================
public void setElevation(final SimState state)
{
    System.out.println("setElevation ..................");
    final AmaSim sim = (AmaSim)state;

    //Bag cps = sim.geoGrid.elements();
    Bag cps = sim.waterChangingCps;
    int num_pixels = cps.size();
    //System.out.println("Num Pixels: " + num_pixels);

    //Envelope mbr = sim.elevation.getMBR();
    //System.out.println("elve mbr max Y: " + mbr.getMaxY());
    //mbr = sim.high_water.getMBR();
    //System.out.println("water mbr max Y: " + mbr.getMaxY());

    //System.out.println("elev grid width: " + sim.elevationGrid.getWidth());
    //System.out.println("elev grid height: " + sim.elevationGrid.getHeight());
    //System.exit(0);

    double maxElev = -100000;
    double minElev =  100000;

    //System.out.println(num_pixels);
    //System.exit(0);

    for(int i =  0; i < num_pixels; i++)
    {
        //if(i % 100 == 0)System.out.println("cp number: " + i);
        CoverPixel cp = ((CoverPixel)cps.objs[i]);

        //if(cp.getIsWaterHigh() == 0 && cp.getIsWaterLow() == 0)continue;
        //if(cp.getIsWaterHigh() != 1)continue;
        //if(cp.getIsRiverExcluded() == 1)continue;

        Point point = sim.utils.getCellCentralPoint(cp.getXcor(), cp.getYcor(), "geo");

        double elevation = sim.blinearInterpolator.getValue(point.getX(), point.getY()); 

        //if(elevation <= 0.0)System.out.println("Ma che cazzooooooooo! " + elevation);

        cp.setElevation(elevation);

        if(maxElev <= elevation){maxElev = elevation;}
        if(minElev >= elevation){minElev = elevation;}
        //System.out.println("Elevation: " + elevation);
    }
    System.out.println(" ");
    System.out.println("Max Elevation water changing: " + maxElev);
    System.out.println("Min Elevation water changing: " + minElev);

    maxElev = -100000;
    minElev =  100000;
    int height = sim.elevationGrid.getHeight();
    int width = sim.elevationGrid.getWidth();
    for (int i = 0; i < width - 1; i++)
    {

        for (int j = 0; j < height - 1; j++)
        {
            MasonGeometry mgElev = (MasonGeometry)sim.elevationGrid.get(i, j);
            //point = (Point)mgElev.getGeometry();

            double elevation = (double)mgElev.getIntegerAttribute("GRID_CODE");
            //System.out.println("Elevation: " + elevation);

            if(maxElev <= elevation){maxElev = elevation;}
            if(minElev >= elevation){minElev = elevation;}
        }
    }

    System.out.println(" ");
    System.out.println("Max Elevation from Shp file: " + maxElev);
    System.out.println("Min Elevation from Shp file: " + minElev);

    //System.exit(0);
}

//====================================================
public void setHighRisk(final SimState state)
{
    System.out.println("Setting the high risk pixels");
    int numHr = sim.highRisk.getGeometries().numObjs;
    Bag hr = sim.highRisk.getGeometries();

    Bag tmp = new Bag();

    if(sim.highRiskWhat == 2)
    {
        if(sim.numExcludedHotspots == 2)
        {
            tmp.add(hr.get(0));
            tmp.add(hr.get(4));
        }
        if(sim.numExcludedHotspots == 1)
        {
            tmp.add(hr.get(0));
        }
    }
    if(sim.highRiskWhat == 1)
    {
        if(sim.numExcludedHotspots == 2)
        {
            tmp.add(hr.get(0));
            tmp.add(hr.get(2));
        }
        if(sim.numExcludedHotspots == 1)
        {
            tmp.add(hr.get(0));
        }
    }

    hr = tmp;
    System.out.println("Num selected hotspots: " + hr.size());

    Bag cps = sim.geoGrid.elements();
    int num_pixels = cps.size();
    int stats = 0;

    System.out.println("Num Pixels: " + num_pixels);

    for(int ii = 0; ii < hr.size(); ii++)
    {
        MasonGeometry rrMg = (MasonGeometry)hr.get(ii);
        Geometry rrGeo = (Geometry)rrMg.getGeometry();

        //System.out.println(num_pixels);
        //System.out.println(hr.size());
        //System.out.println("Orig area: " + rrMg.getDoubleAttribute("Shape_Area"));
        //System.out.println("Calc area: " + rrGeo.getArea());

        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);
            Polygon square = (Polygon)cp.square;

            if(rrGeo.covers(square) || rrGeo.intersects(square))
            {
                if(cp.getIsHighRisk()  == 0)
                {
                    cp.setIsHighRisk(1);
                    stats++;
                }

                //System.out.println("High Risk!!!! ");
            }
        }

    }

    System.out.println("Number of high risk pixels: " + stats);
    //System.exit(0);

}

//====================================================
public void checkLayers(final SimState state)
{
    System.out.println("Checking Pixels water state......................");
    System.out.println(" ");

    final AmaSim sim = (AmaSim)state;

    double totNumCells = sim.geoGridWidth * sim.geoGridHeight;

    Bag cps = sim.geoGrid.elements();
    int num_pixels = cps.size();
    int statsRiverHigh = 0;
    int statsRiverLow  = 0;

    int statsHigh = 0;
    int statsLow  = 0;

    int numCellsStats  = 0;

    for(int i =  0; i < num_pixels; i++)
    {
        CoverPixel cp = ((CoverPixel)cps.objs[i]);

        if(cp.getIsWater() == 1)cp.setIsProductive(true);

        numCellsStats++;

        if(cp.getIsWaterHigh() == 1) statsHigh++;
        if(cp.getIsWaterLow() == 1) statsLow++;

    }

    System.out.println("Num Cells count: "  + numCellsStats);
    System.out.println(" ");

    System.out.println("Water high: "  + statsHigh);
    System.out.println(" ");

    System.out.println("Water Low: "  + statsLow);
    System.out.println(" ");

    System.out.println(".................................................");
    //System.exit(0);

}

//====================================================
public void setHighWaterShp()
{
    final AmaSim sim = (AmaSim)state;

    int num_waters = sim.high_water.getGeometries().numObjs;
    int numClusters = sim.clusters.size();
    //System.out.println("Num polygons in highWater: " + num_waters);
    
    //Associate every shp feature to a geo grid mega cluster of cps
    for (int ii = 0; ii < num_waters; ii++)
    {
        Bag cBag = new Bag();

        MasonGeometry mgWater = (MasonGeometry)sim.high_water.getGeometries().objs[ii];
        Geometry pwater = (Geometry)mgWater.getGeometry();

        for(int i =  0; i < numClusters; i++)
        {
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            //if(pwater.covers(square))
            if(cSquare.covers(pwater))
            {
                //System.out.println("Covers high!!!!!!!");
                cBag.add(cluster);
                cluster.highWaters.add(mgWater);
            
                break;
            }
            else if (pwater.intersects(cSquare))
            {
                //System.out.println("Overlaps high!!!!!");
                cBag.add(cluster);
                cluster.highWaters.add(mgWater);
            }
        }

        mgWater.setUserData(cBag);

    }


}



//====================================================
public void setLowWaterShp()
{
    final AmaSim sim = (AmaSim)state;

    int num_waters = sim.low_water.getGeometries().numObjs;
    int numClusters = sim.clusters.size();
    
    //Associate every shp feature to a geo grid mega cluster of cps
    for (int ii = 0; ii < num_waters; ii++)
    {
        Bag cBag = new Bag();

        MasonGeometry mgWater = (MasonGeometry)sim.low_water.getGeometries().objs[ii];

        Geometry gwater = (Geometry)mgWater.getGeometry();

        for(int i =  0; i < numClusters; i++)
        {
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            //if(gwater.covers(square))
            if(cSquare.covers(gwater))
            {
                //System.out.println("Covers !!!!!!!!!!!");
                cBag.add(cluster);
                cluster.lowWaters.add(mgWater);
                break;
            }
            else if (gwater.intersects(cSquare))
            {
                cBag.add(cluster);
                cluster.lowWaters.add(mgWater);
            }
        }
        mgWater.setUserData(cBag);
    }


}

//====================================================
public void setExcludedWaterShp()
{
    final AmaSim sim = (AmaSim)state;

    int num_waters = sim.rioCutted_10mBuffer.getGeometries().numObjs;
    int numClusters = sim.clusters.size();
    
    //Associate every shp feature to a geo grid mega cluster of cps
    for (int ii = 0; ii < num_waters; ii++)
    {
        Bag cBag = new Bag();

        MasonGeometry mgWater = (MasonGeometry)sim.rioCutted_10mBuffer.getGeometries().objs[ii];

        Geometry gwater = (Geometry)mgWater.getGeometry();

        for(int i =  0; i < numClusters; i++)
        {
            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            //if(pwater.covers(square))
            if(cSquare.covers(gwater))
            {
                //System.out.println("Covers !!!!!!!!!!!");
                cBag.add(cluster);
                cluster.excludedWaters.add(mgWater);
                break;
            }
            else if (gwater.intersects(cSquare))
            {
                cBag.add(cluster);
                cluster.excludedWaters.add(mgWater);
            }
        }
        mgWater.setUserData(cBag);
    }


}

public class mgComparator implements Comparator<Object>, java.io.Serializable
{   
   private static final long serialVersionUID = 1L;

   //----------------------------------------------------------
   public mgComparator()
   {
   }

   //----------------------------------------------------------
   public int compare(Object po1, Object po2)
   {

       MasonGeometry mg1 = (MasonGeometry)po1;
       MasonGeometry mg2 = (MasonGeometry)po2;

       double relRisk1 = (double)mg1.getDoubleAttribute("LLR");
       double relRisk2 = (double)mg2.getDoubleAttribute("LLR");
 
       if((double)relRisk1 < (double)relRisk2)
           return 1;
       else if((double)relRisk1 > (double)relRisk2)
           return -1;
       else
           return 0;
   }


}


    //====================================================
    public void findLDASPixels(final SimState state)
    {
        final AmaSim sim = (AmaSim)state;
        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();

        int num_pixelsLDAS = sim.LDASCps.size();

        Bag tmp = new Bag();

        for(int j =  0; j < num_pixelsLDAS; j++)
        {
            CoverPixel cpLDAS = ((CoverPixel)sim.LDASCps.objs[j]);
            Polygon cpSquareLDAS = cpLDAS.square;
            Point cpCenterLDAS = cpSquareLDAS.getCentroid(); 

            //if(cpCenterLDAS.getX() < 662000 && cpCenterLDAS.getY() > 9680000)
            //{
            //    System.out.println("LDASx = " + cpCenterLDAS.getX() + " LDASy" +cpCenterLDAS.getY());

            //    System.out.println("areaLDAS = " + cpSquareLDAS.getArea());
            //}
            //if(1 == 1)continue;


            for(int i =  0; i < num_pixels; i++)
            {
                CoverPixel cp = ((CoverPixel)cps.objs[i]);
                Polygon cpSquare = cp.square;
                Point cpCenter = cpSquare.getCentroid(); 

                //System.out.println("----------------------");
                //System.out.println("x = " + cpCenter.getX());
                //System.out.println("x = " + cpCenter.getX() + " y " +cpCenter.getY());
                //System.out.println("area = " + cpSquare.getArea());
                //if(1 == 1)break;


                //if(cpSquare.intersects(cpSquareLDAS))
                if(cpSquareLDAS.covers(cpSquare))
                {
                    System.out.println("One LDAS Pixel! Tot num: " + tmp.size());
                    tmp.add(cpLDAS);
                   // System.exit(0);
                    break;
                }
            }

        }

        sim.LDASCps = tmp;
        //System.exit(0);

    }



}

