/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import sim.field.grid.*;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Envelope;

import sim.util.geo.MasonGeometry;

public class InitializePixelMosquitosGrid
{
   private static final long serialVersionUID = 1L;

   //====================================================
   public InitializePixelMosquitosGrid(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;
   }


   //====================================================
   public void initAll(final SimState state)
   {
      System.out.println("================================================");
      System.out.println("Initialize mosquitos pixels grid ...............");
      System.out.println(" ");
      
      final AmaSim sim = (AmaSim)state;

      for(int i = 0; i < sim.mosquitoGridWidth; i++)
      {
         for(int j = 0; j < sim.mosquitoGridHeight; j++)
         {
             //The square that corresponds to the grod coords i, j 
             //in the mosquito reference siystyem is built
             Point center = sim.utils.getCellCentralPoint(i, j, "mosquito");
             Polygon square = sim.utils.getSquareAroundPoint(center, sim.mosquitoCellSize);

             CoverPixel cp = new CoverPixel(state, i, j, square);
             cp.setType("mosquito");

             //The coverPixel gets (i, j) as grid coordinates and
             //the correspondingi suqare in the mosquito ref system 
             //as masonGeoemtry
             MasonGeometry mg = new MasonGeometry(square);
             cp.setMasonGeometry(mg);

             //System.out.println (i + " " + (-j + sim.mosquitoGridHeight - 1));
             sim.pixelMosquitosGrid.set(sim.utils.getCoordsToDisplay(state, i, j, "mosquito")[0],
                               sim.utils.getCoordsToDisplay(state, i, j, "mosquito")[1],
                               cp
                              );

             //Find the Cluster of every mos coverPixel-------
             for(int c = 0; c < sim.clusters.size(); c++)
             {
                 Cluster cluster = ((Cluster)sim.clusters.objs[c]);
                 Polygon cSquare = (Polygon)cluster.square;

                 if(cSquare.intersects(square) || cSquare.covers(square))
                 {
                     cluster.mosquitoCoverPixels.add(cp);
                     cp.clusters.add(cluster);
                 }
             }


         }
      }
      System.out.println("Initialize mosquitos pixels grid ends ..........");
      System.out.println(" ");
   }


   //====================================================
   public void linkTwoGrids(final SimState state)
   {
       System.out.println("Linking mos pixel to geo pixels ------");
       //System.exit(0);

       final AmaSim sim = (AmaSim)state;
       Bag linksM;
       Bag linksG;

       Bag pixelsMosquito = sim.pixelMosquitosGrid.elements();
       int num_mosquitosPixels = pixelsMosquito.size();

       Bag pixelsGeo = sim.geoGrid.elements();
       int num_geoPixels = pixelsGeo.size();

       int i1 = (int)Math.round(0.25 * sim.clusters.size());
       int i2 = (int)Math.round(0.50 * sim.clusters.size());
       int i3 = (int)Math.round(0.75 * sim.clusters.size());

       for(int c = 0; c < sim.clusters.size(); c++)
       {
           Cluster cluster = ((Cluster)sim.clusters.objs[c]);

           if(i1 == c  || i2 == c  || i3== c)
           {
               double fract = Math.round(100 * c/ sim.clusters.size());
               System.out.println(100 * fract/100 + "% of geo clusters done");
           }

           for(int g = 0; g < cluster.coverPixels.size(); g++)
           {
               CoverPixel cpg = ((CoverPixel)cluster.coverPixels.objs[g]);

               MasonGeometry gmg      = cpg.getMasonGeometry();
               Polygon squareGeo = (Polygon)gmg.getGeometry();
               Point centerGeo   = (Point)squareGeo.getCentroid();

               double pgx = centerGeo.getX();
               double pgy = centerGeo.getY();

mosquito:
               for(int i =  0; i < cluster.mosquitoCoverPixels.size(); i++)
               {
                   CoverPixel cpm = ((CoverPixel)cluster.mosquitoCoverPixels.objs[i]);

                   MasonGeometry mg       = cpm.getMasonGeometry();
                   Polygon squareMosquito = (Polygon)mg.getGeometry();
                   Point centerMosquito   = (Point)squareMosquito.getCentroid();

                   double pmx = centerMosquito.getX();
                   double pmy = centerMosquito.getY();

                   if(
                           pgx <= pmx + (sim.mosquitoCellSize * 0.5)     
                           && pgx >  pmx - (sim.mosquitoCellSize * 0.5)     
                           && pgy <= pmy + (sim.mosquitoCellSize * 0.5)     
                           && pgy >  pmy - (sim.mosquitoCellSize * 0.5)     
                     )
                   {

                       //if(cpg.xcor == 200 && cpg.ycor == 74)
                       //{
                       //    System.out.println ("---Here I am ----------------");
                       //    System.out.println (cpm.xcor + " " + cpm.ycor);
                       //    System.exit(0);
                       //}

                       //geo pixel-------
                       linksG = new Bag();
                       linksG.add(cpm);
                       cpg.setLinkPixels(linksG);

                       //mosquito pixel--
                       linksM = cpm.getLinkPixels();
                       linksM.add(cpg);
                       cpm.setLinkPixels(linksM);
                       break mosquito;
                   }
               }


           }

       }

       //System.exit(0);
   }

   //====================================================
   public void setHouseholds(final SimState state)
   {
      System.out.println("setting mosquitos pixels grid households.....");

      final AmaSim sim = (AmaSim)state;

      Bag pixelsMosquito = sim.pixelMosquitosGrid.elements();
      int num_mosquitosPixels = pixelsMosquito.size();

      for(int i =  0; i < num_mosquitosPixels; i++)
      {
         CoverPixel cpm = ((CoverPixel)pixelsMosquito.objs[i]);

         cpm.setHouseholds(householdsInsidePixel(state, cpm));

         if(cpm.getHouseholds().size() == 0)cpm.setContainsHousehold(false);
         else cpm.setContainsHousehold(true);
      }
 

   }

   //====================================================
   public Bag householdsInsidePixel(SimState state, CoverPixel mcp)
   {
      final AmaSim sim = (AmaSim)state;
      //input is a mosquito pixel 
      Bag linkMCp = mcp.getLinkPixels();
      Bag households = new Bag();

      for(int i =  0; i < linkMCp.size(); i++)
      {
          CoverPixel gcp = (CoverPixel)linkMCp.objs[i];

          int x = gcp.getXcor();
          int y = gcp.getYcor();

          x = sim.utils.getCoordsFromDisplay(state, x, y, "geo")[0];
          y = sim.utils.getCoordsFromDisplay(state, x, y, "geo")[1];

          Bag gcph = sim.householdGrid.getObjectsAtLocation(x,y);

          if(gcph == null)continue;

          for(int j =  0; j < gcph.size(); j++)
          {
               households.add( (Household)gcph.objs[j] );
          }

      }
      return households;
   
   }


//============================================================   
}
