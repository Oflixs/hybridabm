/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;

import sim.engine.*;
import sim.util.*;


import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LayersDynamics implements Steppable
{
private static final long serialVersionUID = 1L;

public SimState state;
final AmaSim sim;

//====================================================
public Stoppable stopper;

//====================================================
public LayersDynamics(final SimState pstate)
{
    System.out.println("================================================");
    System.out.println("Initialize Layers Dynamics   ...................");
    System.out.println(" ");

    state = pstate;

    sim = (AmaSim)state;

    this.stopper = sim.schedule.scheduleRepeating(1.0, 1, this);

    //System.out.println("Start Date: " + sim.startDate);

    changeWaterCover(state, sim.startDate);
    //System.exit(0);

}

//====================================================
public void step(final SimState state)
{
    final AmaSim sim = (AmaSim)state;

    int nowGen = (int)state.schedule.getTime();  
    int now = sim.utils.getTime();

    if((nowGen - 6) % 24 == 0)
    {
        generateMosquitosFromHighWater(state);
    }

    if(nowGen % 24  == 0)
    {
        MeteoData md = sim.meteoData.get(now);
        //System.out.println("now: " + now);
        //md.print();
        Calendar calendar = md.getCalendar();
        //System.out.println(calendar.getTime());
                
        //To get the timeMonth date in string format
        DateFormat format = new SimpleDateFormat("MM-yyyy");
        Date date = calendar.getTime();
        sim.monthDate = format.format(date);
        //System.out.println(sim.monthDate);

        //System.exit(0);

        changeWaterCover(state, calendar);
    }

}

//====================================================
public void changeWaterCover(final SimState state, Calendar calendar)
{
    final AmaSim sim = (AmaSim)state;

    System.out.println("Change Water Cover --------------------");
    //System.out.println("Calendar date: " + calendar);

    double riverLevel = sim.dailyNapoLevel.get(calendar);

    //System.out.println("River Level from sim: " + riverLevel);
    //System.out.println("Max napo lev: " + sim.maxNapoLevel);
    //System.out.println("Min napo lev: " + sim.minNapoLevel);

    //riverLevel = (riverLevel * (sim.maxNapoLevel - sim.minNapoLevel) + sim.minNapoLevel)/( sim.maxNapoLevel - sim.minNapoLevel );

    riverLevel = (riverLevel - sim.minNapoLevel)/(sim.maxNapoLevel - sim.minNapoLevel);
    //System.out.println("Normalized River level: " + riverLevel);
    //System.exit(0);

    //riverLevel = riverLevel + sim.minRiver;

    //double deltaLevel = 0.0;

    //if(calendar.compareTo(sim.startDate) == 0)deltaLevel = 0.0;
    //else deltaLevel = riverLevel - sim.oldRiverLevel;

    System.out.println("Normalized river level: " + riverLevel);
    //System.out.println("sim: " + sim.equilibPeriod);
    changeIt(state, riverLevel);
    //if(!sim.equilibration)System.exit(0);
    sim.oldRiverLevel = riverLevel;
    //System.out.println("Change Water Cover Done----------------");
}

//====================================================
public void changeIt(final SimState state, double riverLevel)
{
    //System.out.println("Change it --------------");

    final AmaSim sim = (AmaSim)state;

    Boolean change = true;
    int stats = 0;
    int statsDried = 0;
    int statsDry = 0;

        //System.out.println("recursive stage " + stats);
        stats++;

        Bag cps = sim.waterChangingCps;
        int num_pixels = cps.size();
        System.out.println("Num pixels water changing: " + num_pixels);

        change = false;

        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            //Pitxi patch ---------
            if(sim.hybridExample && cp.getIsWaterHigh() == 1)
            {
                if(sim.hybridDensity && cp.getIsWater() != 1)
                {
                    if(cp.linkPixels.size() > 0)
                    {
                        CoverPixel mcp = (CoverPixel)cp.linkPixels.get(0);
                        sim.hybridPixelsToChange.add(mcp);
                    }
                }
                cp.setIsWater(1); 
                //System.out.println("Wetted!!");
            }
            if(sim.hybridExample && cp.getIsWaterHigh() != 1)
            {
                if(sim.hybridDensity && cp.getIsWater() != 0)
                {
                    if(cp.linkPixels.size() > 0)
                    {
                        CoverPixel mcp = (CoverPixel)cp.linkPixels.get(0);
                        sim.hybridPixelsToChange.add(mcp);
                    }
                }
                cp.setIsWater(0); 
                //System.out.println("Not Wetted!!");
            }

            if(sim.hybridExample && 1 == 1)
            {
               continue;
            }

            //if(cp.getIsWater() == 1)continue;
            //if(i % 100 == 0)System.out.println("pixel num: " + i);

            if(cp.getIsBufferNoMos())
            {
                if(sim.hybridDensity && cp.getIsWater() != 0)
                {
                    if(cp.linkPixels.size() > 0)
                    {
                        CoverPixel mcp = (CoverPixel)cp.linkPixels.get(0);
                        sim.hybridPixelsToChange.add(mcp);
                    }
                }
                cp.setIsWater(0); 
                continue;
            }

            //double level = cp.macrolevelMin + riverLevel * (cp.macrolevelMax - cp.macrolevelMin);

            //System.out.println("----------------------------");
            //System.out.println("macroLevelMin: " + cp.macrolevelMin + " macro:LEvelMax: " + cp.macrolevelMax);
            //System.out.println("pixel Level: " + cp.getElevation());
            //System.out.println("river Level: " + riverLevel);
            //System.out.println("river Level rescaled: " + (riverLevel * (cp.macrolevelMax - cp.macrolevelMin) + cp.macrolevelMin));
            //System.out.println("Level: " + level + " cp elevatioon: " + cp.getElevation());

            if(cp.getElevation() > riverLevel * (cp.macrolevelMax - cp.macrolevelMin) + cp.macrolevelMin)
            {
                if(sim.hybridDensity && cp.getIsWater() != 0)
                {
                    //System.out.println("Pixel changed cover");
                    if(cp.linkPixels.size() > 0)
                    {
                        CoverPixel mcp = (CoverPixel)cp.linkPixels.get(0);
                        sim.hybridPixelsToChange.add(mcp);
                    }
                }
                cp.setIsWater(0); 
                statsDried++;
                //System.out.println("Dried!!");
            }
            else
            {
                if(sim.hybridDensity && cp.getIsWater() != 1)
                {
                    //System.out.println("Pixel changed cover");
                    if(cp.linkPixels.size() > 0)
                    {
                        CoverPixel mcp = (CoverPixel)cp.linkPixels.get(0);
                        sim.hybridPixelsToChange.add(mcp);
                    }
                }
                cp.setIsWater(1); 
                //System.out.println("Wetted!!");
            }

            //cp.setIsWater(1); 

            //if(!cp.getIsRiverBank(state))continue;
            //if(changeNeig(cp, deltaLevel)) change = true;
              


        }


        /*
        stats = 0;

        for(int i =  0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            if(cp.getIsWater() == 1)
            {
                stats++;
            }

            if(cp.getIsWater() == 0)
            {
                statsDry++;
            }

        }

        System.out.println("Num dryed: " + statsDried);
        System.out.println("Num total: " + num_pixels);
        System.out.println("Num wet: " + stats);
        System.out.println("Num dry: " + statsDry);
        //System.exit(0);
        */
}


//Gives true if it changed some neig==================
public Boolean changeNeig(CoverPixel cp, double deltaLevel)
{
    Bag neigs = cp.getMooreNeighbors(state, "noShuffle", "geo");
    double totDeltaLevel = deltaLevel + cp.getOldDeltaLevel();

    Boolean changed = false;
    for(int i = 0; i < neigs.size(); i++)
    {
        CoverPixel cpNeig = ((CoverPixel)neigs.objs[i]);

        if(!cpNeig.isWaterChanging())continue;
        //System.out.println("Changed in water -----------------");

        if(totDeltaLevel >= 0)
        {
            if((cpNeig.getElevation() <= cp.getElevation() + totDeltaLevel)
                && cpNeig.getIsWater() == 0    
                    )
            {
                cpNeig.setIsWater(1); 
                //System.out.println("index of wetted " + i);
                if(cpNeig.getIsRiverBank(state))
                cpNeig.setOldDeltaLevel(cp.getElevation() + totDeltaLevel - cpNeig.getElevation());
                changed = true;
            }
        }
        else if(totDeltaLevel < 0)
        {
            if((cpNeig.getElevation() > cp.getElevation() + totDeltaLevel)
                && cpNeig.getIsWater() == 1    
                    )
            {
                cpNeig.setIsWater(0); 
                cp.setIsWater(0); 
                //System.out.println("index of dryed " + i);
                //System.out.println("Changed in dry -----------------");
                if(cpNeig.getIsRiverBank(state))
                cpNeig.setOldDeltaLevel(  cpNeig.getElevation() - deltaLevel - cp.getElevation());
                changed = true;
            }
        
        }
    
    }

    if(cp.getIsRiverBank(state))cp.setOldDeltaLevel(totDeltaLevel);
    else cp.setOldDeltaLevel(0);

    return changed;

}

//====================================================
public void generateMosquitosFromHighWater(final SimState state)
{
    final AmaSim sim = (AmaSim)state;
    CoverPixel cp;

    Bag pixels = sim.geoGrid.elements();
    pixels.shuffle(state.random);
    int len = pixels.size();
    double rest = 0;
    double dnumMos = 0;
    int numMos = 0;
    int stats = 0;

    sim.numProductivePixels = 0;
    sim.numPixelsCoveredByWater = 0;

    for(int i = 0; i < len; i++)
    {
        cp = ((CoverPixel)pixels.objs[i]);

        dnumMos = cp.eggsOvoposited.get(0);

        //Reset the 0 value of ovopisited engss
        int nEggs = cp.getNEggs();
        cp.setEggsOvoposited(nEggs);
        cp.setNEggs(0);

        if(!cp.getIsProductive())continue;
        if(cp.getIsWater() == 0) continue;

        if(cp.getIsWaterHigh() == 0) continue;
        if(cp.getIsRiverExcluded() == 1) continue;

        sim.numPixelsCoveredByWater++;

        if(dnumMos == 0)continue;

        sim.numProductivePixels++;

        dnumMos = dnumMos * sim.propOvopositionEmergentAdults;

        if(dnumMos > sim.maxNumbAdultperArea * sim.geoCellSize * sim.geoCellSize )
        {
            dnumMos = sim.maxNumbAdultperArea * sim.geoCellSize * sim.geoCellSize;
        }

        //System.out.println("cell area: " + sim.geoCellSize * sim.geoCellSize);
        //System.exit(0);

        if(cp.getIsWaterLow() == 1) dnumMos = dnumMos / sim. numMosHighLowWaterRatio;

        if(dnumMos < 1)
        {
            rest = cp.getMosRest();
            rest = rest + dnumMos;

            if(rest >= 1)
            {
                //System.out.println("Inside rest " + rest);
                numMos = (int)Math.round(rest);      
                //System.out.println("Inside numMos " + numMos);
                cp.setMosRest(0.0);
            }
            else 
            {
                cp.setMosRest(rest);
                continue;
            }
        }
        else
        {
            numMos = (int)Math.round(dnumMos);      
        }

        if(sim.maxAdultEmergent < numMos)sim.maxAdultEmergent = numMos;


        for(int m = 0; m < numMos; m++)
        {
            //System.out.println("----------------------");
            //System.out.println("New Mosquito");
            sim.mosGen.generateMosquito(state, cp);
            stats++;
        }

    }
    //System.out.println(stats + " Mosquitos generated");

}

//============================================================   
}
