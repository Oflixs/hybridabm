/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

public class MalariaData
{
   private static final long serialVersionUID = 1L;

   public Calendar  calendar = null;
   public Calendar  getCalendar(){return calendar;};
   public void  setCalendar(Calendar w){calendar = w;};

   public String hPost = "";
   public String place = "";

   final AmaSim sim;


   //====================================================
   public MalariaData(AmaSim psim)
   {
      sim = psim;
   }

   //====================================================
   public void print()
   {

       System.out.println("--Malaria Data Print resume --");
       System.out.println("Date: " +  calendar);

   }



//============================================================   
}
