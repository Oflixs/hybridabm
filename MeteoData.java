/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.*;
import java.util.*;

public class MeteoData
{
   private static final long serialVersionUID = 1L;

   public double  rain = 0.0;
   public double  getRain(){return rain;};
   public void    setRain(double w){rain = w;};

   public double  minT = 0.0;
   public double  getMinT(){return minT;};
   public void    setMinT(double w){minT = w;};

   public double  maxT = 0.0;
   public double  getMaxT(){return maxT;};
   public void    setMaxT(double w){maxT = w;};

   public Calendar  calendar = null;
   public Calendar  getCalendar(){return calendar;};
   public void  setCalendar(Calendar w){calendar = w;};

   public Calendar  dateCalendar = null;
   public Calendar  getDateCalendar(){return dateCalendar;};
   public void    setDateCalendar(Calendar w){dateCalendar = w;};

   //====================================================
   public MeteoData(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;
   }

   //====================================================
   public void print()
   {

       System.out.println("-- Meteo Data Print resume --");
       System.out.println("Date: " +  calendar.getTime());
       //System.out.println("DateCalendar: " +  dateCalendar.getTime());
       System.out.println("Rain (mm): " +  rain);
       System.out.println("Min T (c): " +  minT);
       System.out.println("Max T (c): " +  maxT);

   }



//============================================================   
}
