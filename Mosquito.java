/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import com.vividsolutions.jts.geom.Point;

//----------------------------------------------------
public class Mosquito extends Animal 
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;

   int restingAfterBite = -100;
   int restingAfterLayEggs = -100;
   boolean bloodFeeded = false;
   
   double sporogonic = 0.0;
   boolean sporogonicEnded;

   int matingTime = -100;

   int DD = 0;
   double incubationMinTemp = 0.0;

   String stopped = "no";

   boolean beforeMating           = true;
   public  boolean getBeforeMating(){return beforeMating;};

   boolean hostSearch             = false;
   public  boolean getHostSearch(){return hostSearch;};

   boolean breedingHabitatSearch  = false;
   public  boolean getBreedingHabitatSearch(){return breedingHabitatSearch;};

   boolean stepEnd = false;

   double timeToGetBlood = 0.0;
   double timeToLayEggs = 0.0;
   int eggDevelopmentTime = 0;

   boolean noNulliparous = false;
   public  boolean getNoNulliparous(){return noNulliparous;};

   public CoverPixel cpPosition = null;
   public void setCpPosition(CoverPixel mg){cpPosition = mg;};
   public CoverPixel getCpPosition(){return cpPosition;};

   HashMap<Household, Integer> households = new HashMap<Household, Integer>();
   public HashMap getHouseholds(){return households;};
   public void setHouseholds(HashMap y){households = y;};

   //public Boolean justArrived = true;

   public Household inHousehold = null;

   public List<CoverPixel> visitedCps = new ArrayList<CoverPixel>();
   public Boolean infectiousBite = false;
   public Boolean pbcApplied = false;

   //boolean tagged = false;

   //====================================================
   public Mosquito(SimState state, CoverPixel cpm, String grid)
   {

       super(state, cpm);
       cpPosition = cpm;
       sim = (AmaSim)state;


       if(!grid.equals("mosquito"))
       {
           System.out.println ("The mosquitos acept only mosquitos grid coordinates");
           System.exit(0);
       }

       this.setSpecies("Anopheles Darlingi");

       double time = 0.0;
       if(state.schedule.getTime() > 0)time = (double)state.schedule.getTime();  
       this.stopper = sim.schedule.scheduleRepeating(time, 3, this);

       sim.mosquitoGrid.setObjectLocation(this, 
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "mosquito")[0],
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "mosquito")[1]
           );
       sim.mosquito_bag.add(this);

       //Select the hour of the mating ------
       selectMatingTime(state);       
       beforeMating = true;

       sporogonicEnded = false;

       eggDevelopmentTime = -100;

       noNulliparous = false;

       cpPosition.nMosquitos++;
       cpPosition.nMosquitosBlood++;

   }

   //====================================================
   public void step(SimState state)
   {


      super.step(state);
      sim = (AmaSim)state;
      stepEnd = false;

      cpPosition.addAccumulatedNMosquitoes();
      if(hostSearch)cpPosition.addAccumulatedNMosquitoesLookingForBlood();
      if(breedingHabitatSearch)cpPosition.addAccumulatedNMosquitoesBloodFeeded();

      //System.out.println ("---Mosquito step-------------");
      //System.out.println (cpPosition.xcor + " " + cpPosition.ycor);

      

      //System.out.println ("-----------------------------");
      //System.out.println (this.getAge() + "  mosquito age");


      int topMos = 400000;
      if(sim.mosquito_bag.size() > topMos)
      {
        System.out.println ("Too many mosquitoes in the sim > " + topMos);
        sim.writeXls.write(state);
        sim.writeXls.writeHousehold(state);
        System.exit(0);
      }


      int now = (int)state.schedule.getTime();  
      if(now % 168 == 0)cpPosition.increaseNMosquitos(); 

      //See if the mosquito survives this hour ----------
      ifIHaveToDie(state);
      if(stepEnd) return;


      //Infection dynamics not dep on hourly activites --
      infectionDynamics(state);

      //Hourly activities no depending on infection -----

      //Upgrade and watch timers-------------------------
      timers(state);
      if(stepEnd) return;

      if(isDayTime(state)) return;

      if(beforeMating)    searchPartner(state);
      else if(hostSearch)  searchHost(state);
      else if(breedingHabitatSearch) searchBreedingHabitat(state);
      if(sim.debug)checkLogic(state); //switch off during production
      if(stepEnd) return;


   }
   
   //====================================================
   public void timers(SimState state)
   {
      restingAfterBite--;
      restingAfterLayEggs--;
      timeToGetBlood++;
      timeToLayEggs++;
      eggDevelopmentTime--;

      //See if some timers is set-----------------------
      if(restingAfterBite    >= 0
      || restingAfterLayEggs >= 0 )stepEnd = true;;
      return;
   }

   //====================================================
   public void searchPartner(SimState state)
   {
       //If recently emerge from breeding site wait for mating
       //and move as in the blood seeking mode
       move(state, "hostSearch");
       matingTime--;
       if(matingTime == 0)
       {
          beforeMating = false;
          hostSearch   = true;
          return;
       }
   }

   //====================================================
   public Household fullHousehold(SimState state)
   {
       Bag hhs = cpPosition.getHouseholds();

       if(hhs.size() == 0)return null;

       hhs.shuffle(state.random);
       for(int i = 0; i < hhs.size(); i++)
       {
           Household h = (Household)hhs.get(i);
           if(h.getPeople().size() > 0)return h;
       }

       return null;
   }

   //====================================================
   public Boolean occupiedHouseholdHere()
   {
       Bag hhs = cpPosition.getHouseholds();
       for(int i =  0; i < hhs.size(); i++)
       {
           Household hh = (Household)hhs.get(i);
           if(hh.isOccupied)return true;
       }
       return false;
   }

   //====================================================
   public Bag getPeopleInTheMosPixel(SimState state)
   {
       Bag linkPixels = this.cpPosition.getLinkPixels();
       //System.out.println (linkPixels.size());

       //linkPixels.shuffle(state.random);

       Bag people = new Bag();

       for(int i = 0; i < linkPixels.size(); i++)
       {
           CoverPixel cp = (CoverPixel)linkPixels.get(i);

           //System.out.println (cp.xcor + " " + cp.ycor);

           Bag cpPeople = cp.getPeople();

           //cpPeople.shuffle(state.random);

           for(int j = 0; j < cpPeople.size(); j++)
           {
               //System.out.println (j);
               //System.exit(0);
               Human h = (Human)cpPeople.get(j);
               people.add(h);
           }
       }
       people.shuffle(state.random);

       return people;
   }



   //====================================================
   public Household seeIfEnterInside(SimState state, Bag people)
   {
       //If mosquito does not bite an animal around the 
       //household it decides if it enters inside an household
       List<Double> weightsList = new ArrayList<Double>();

       weightsList.add((double)people.size());

       Bag hhs = cpPosition.getHouseholds();

       //if(hhs.size() > 1)
       //{
       //    System.out.rintln ("Num Houses: " + hhs.size());
       //    System.exit(0);
       //}
       
       for(int i =  0; i < hhs.size(); i++)
       {
           Household hh = (Household)hhs.get(i);
           weightsList.add((double)hh.getNumPeople());
           //System.out.println ("Num People: " + hh.getNumPeople());
       }

       double sum = 0.0;
       for(int i =  0; i < weightsList.size(); i++)
       {
           sum = sum + weightsList.get(i);
       }
       for(int i =  0; i < weightsList.size(); i++)
       {
           weightsList.set(i, weightsList.get(i)/sum);
           //System.out.println (i + " " + weightsList.get(i));
       }

       sum = 0.0;
       int index = 0;
       double ran = state.random.nextDouble();
       for(int i =  0; i < weightsList.size(); i++)
       {
           sum = sum + weightsList.get(i);
           if(ran <= sum)
           {
              index = i;
              break;
           }
       }

       //System.out.println ("index: " + index);

       if(index == 0)return null;
       else 
       {
           Household hh = (Household)hhs.get(index - 1);

           //System.out.println ("--------");
           //System.out.println ("nPeople: " + hh.people.size());

           if(hh.people.size() > 0)return hh;
           else return null;

       }


   }

   //====================================================
   public void afterBite(SimState state)
   {
       hostSearch = false;
       breedingHabitatSearch = true;
       restingAfterBite = sim.restingTimeAfterBite;
       eggDevelopmentTime = sim.eggDevelopmentTime;
       timeToLayEggs = 0.0;
       //justArrived = true;
       inHousehold = null;

       stepEnd = true; 
   }

   //====================================================
   public Boolean bite(SimState state, String what)
   {
       afterBite(state);

       if(what.equals("Animal"))
       {
           sim = (AmaSim)state;

           bloodFeeded = true;

           //Upgrade Statistics 
           sim.totalNumBiteAnimal++;
           sim.weeklyNumBiteAnimal++;
           sim.totNumDailyBitesAnimals++;
           //System.out.println ("Num bite on animals:"  + sim.totNumDailyBitesAnimals);


           int now = (int)state.schedule.getTime();  
           now = (int)now % 24;
           sim.bitingHours[now]++;

           if(noNulliparous)
           {
               sim.timeToGetBlood = sim.timeToGetBlood + timeToGetBlood;
               sim.numTimeToGetBlood++;

               sim.weeklyTimeToGetBlood = sim.weeklyTimeToGetBlood + timeToGetBlood;
               sim.weeklyTimeToGetBloodCounter++;
           }
           //System.out.println (now + " " + sim.bitingHours[now]);
       }
       else if (what.equals("Human"))
       {
       
       }

       return true;

   }

   //====================================================
   public Boolean thereArePeopleHere(SimState state)
   {
       Bag linkPixels = this.cpPosition.getLinkPixels();

       for(int i = 0; i < linkPixels.size(); i++)
       {
           CoverPixel cp = (CoverPixel)linkPixels.get(i);
           if(cp.getPeople().size() > 0)return true;
       }
       return false;
   }



   //====================================================
   public void searchHost(SimState state)
   {
       Boolean print = false;


       if(print)System.out.println ("Start mos search ---------------------");
       if(print)sim.utils.getHumansInMovement();

       //Not biting time rest and wait for the right moment
       if(!isBitingTime(state)){ stepEnd = true; return;}

       boolean occupiedHouseholdFounded = occupiedHouseholdHere();
       boolean thereArePeopleHere = thereArePeopleHere(state);

       //If not in a pixel containing households or people there is still
       //a probability to have a blood meal from animals
       //If no animal meal move to another adjacent pixel
       if(!occupiedHouseholdFounded && !thereArePeopleHere)
       {
           double ran = state.random.nextDouble();
           if(ran < sim.probBloodMealperPixel)
           {
               if(print)System.out.println ("Far Animal blood....................");
               cpPosition.nBites++;
               bite(state, "Animal");
               return;
           }

           move(state, "hostSearch");

           stepEnd = true; return;
       }

      Bag peopleInTheMosPixel = new Bag();
      if(thereArePeopleHere)peopleInTheMosPixel = getPeopleInTheMosPixel(state);
      if(inHousehold == null)inHousehold = seeIfEnterInside(state, peopleInTheMosPixel);

      if(inHousehold == null && peopleInTheMosPixel.size() > 0 )
      {
          //System.out.println ("People in the mos pixel" + peopleInTheMosPixel.size());
          //System.exit(0);
         
          if(print)System.out.println ("There are people outside Mosquito Agent");

          double ran = state.random.nextDouble();

          Human h = (Human)peopleInTheMosPixel.get(0);

          double biteProb = 0.0;
          
          if(h.probBiteOutdoor != 0.0)biteProb = h.probBiteOutdoor;
          else biteProb =  sim.probBloodMealFromOutdoorHuman;


          if(ran <= biteProb && biteHuman(state, h));
          {
              if(print)System.out.println ("Outdoor human blood.................");
              cpPosition.nBites++;
              afterBite(state);
              return;
          }
      }
      else if(inHousehold != null)
      {
          //System.out.println ("nPeople: " + inHousehold.people.size());

          if(print)System.out.println ("Inside the household Mosquito Agent");

          Human h = searchSomeoneToBiteIndoor(state);

          if(h == null)
          {
              //System.out.println ("No human inside the inHousehold");
              //System.exit(0);
              inHousehold = null;
              stepEnd = true;
              return;
          }

          if(biteHuman(state, h))
          {
              if(print)System.out.println ("Indoor human blood.................");
              cpPosition.nBites++;
              afterBite(state);
              return;
          }
      }

       //If in a pixel containing household wiht people living 
       //and not inside an household mosquito trie to bite an animal
       if(occupiedHouseholdFounded)
       {
           if(print)System.out.println ("Out of the household and with occupied house");

           double ran = state.random.nextDouble();
           if(ran < sim.probBloodMealperPixelAroundHumans)
           {
               if(print)System.out.println ("Close Animal blood................");
               cpPosition.nBites++;
               bite(state, "Animal");
               return;
           }
       }


      stepEnd = true; 
      return;

   }

   //====================================================
   public void xsearchBreedingHabitat(SimState state)
   {
       CoverPixel gcp = cpPosition.getContainsBreeding(state);
       //System.out.println ("Search for breeding habitat fuori");

       if(gcp != null && eggDevelopmentTime < 0)
       {
           //System.out.println ("Search for breeding habitat dentro");
           layEggs(state, gcp); 
           breedingHabitatSearch = false;
           hostSearch            = true;


           //if(infectious)System.out.println ("Infectious householdFounded breeding  !!!!");
           stepEnd = true; 
           return;
       }

       //Not near to breeding site -> move
       move(state, "breedingSiteSearch");
   }

   //====================================================
   public void searchBreedingHabitat(SimState state)
   {
       CoverPixel gcp = cpPosition.getContainsBreeding(state);
       //System.out.println ("Search for breeding habitat fuori");

       if(gcp != null)
       {
           //if(justArrived)
           //{
           //   justArrived = false;
           //   return;
           //}

           if(eggDevelopmentTime < 0)
           {
               //System.out.println ("Search for breeding habitat dentro");
               layEggs(state, gcp); 
               breedingHabitatSearch = false;
               hostSearch            = true;

               //if(infectious)System.out.println ("Infectious householdFounded breeding  !!!!");
               stepEnd = true; 
               //justArrived = true;
               return;
           }
       }
       else
       {
           //Not near to breeding site -> move
           move(state, "breedingSiteSearch");
       }

   }

   //====================================================
   public void infectionDynamics(SimState state)
   {
       //Upgrade the degree days if infected
       if(infected > 0)
       {
           upgradeDegreesDays(state);
           //Check if the incubation period is finish
           if(infected <= 0)
           {
              //System.out.println ("Mosquito became infectious !!!!!!");
              this.setInfectious(true);
              sporogonicEnded = true;
              infected = -1;
           }
       }
   }

   //====================================================
   public Human searchSomeoneToBiteIndoor(SimState state)
   {
       if(inHousehold == null)return null;

       if(inHousehold.getPeople().size() == 0)return null;

       Bag people = inHousehold.getPeople();

       people.shuffle(state.random);

       return ((Human) people.get(0));
   }

   //====================================================
   public Human searchSomeoneToBiteIndoor2(SimState state)
   {
       Bag hhs = cpPosition.getHouseholds();
       if(hhs.size() == 0)return null;

       hhs.shuffle(state.random);
       Bag hs = new Bag();

       for(int i =  0; i < hhs.size(); i++)
       {

           Household hh = (Household)hhs.get(i);
           Bag people = hh.getPeople();
           people.shuffle(state.random);

           for(int j =  0; j < people.size(); j++)
           {
               hs.add(people.get(j));
               
           }
       }

       hs.shuffle(state.random);
       return (Human)hs.get(0);
       
   }



   //====================================================
   public CoverPixel giveNextPixel(SimState state, String what)
   {
       sim = (AmaSim)state;
       CoverPixel tcp;

       List<Double> weightsList = new ArrayList<Double>();

       //convert from mosquito to geo grid coordinates --
       //System.out.pri89<F7>ntln ("------------------------");
       Bag neig = null;
       if(sim.pbcMosquito)neig = cpPosition.getMooreNeighbors(state, "doShuffle", "mosquito");
       else 
       {
           neig = cpPosition.getMooreNeighborsNoPbcMosquito(state, "doShuffle", "mosquito");
       }
       //---------

       for(int i =  0; i < neig.size(); i++)
       {
           tcp = (CoverPixel)neig.objs[i];
           weightsList.add(0.0);

       
           if(what == "hostSearch")
           {
             if(tcp.getContainsHousehold() || tcp.containsHumans())
             {
                 if(tcp.getContainsHousehold())
                 weightsList.add(i, (weightsList.get(i) + sim.randomWalkWeight));
                 if(tcp.containsHumans())
                 weightsList.add(i, (weightsList.get(i) + sim.randomWalkWeight));
             }
             else weightsList.add(i , 1 - sim.randomWalkWeight);
           }
           else if(what == "breedingSiteSearch")
           {
             if(tcp.getContainsBreeding(state) != null)weightsList.add(sim.randomWalkWeight);
             else weightsList.add(1 - sim.randomWalkWeight);
           }
           else
           {
              System.out.println ("Not understand what is what mosquito");
              System.exit(0);
           }
       }


       double sum = 0.0;
       for(int i =  0; i < weightsList.size(); i++)
       {
           sum = sum + weightsList.get(i);
       }
       for(int i =  0; i < weightsList.size(); i++)
       {
           weightsList.set(i, weightsList.get(i)/sum);
       }

       sum = 0.0;
       double ran = state.random.nextDouble();
       for(int i =  0; i < neig.size(); i++)
       {
           tcp = (CoverPixel)neig.objs[i];
           sum = sum + weightsList.get(i);
           if(ran <= sum)return tcp;
       }

       return cpPosition;
   
   }

   //====================================================
   public void move(SimState state, String what)
   {
       sim = (AmaSim)state;

       //Remove mos from the previous pixel
       cpPosition.nMosquitos--;

       if(what.equals("hostSearch"))cpPosition.nMosquitosBlood--;
       else if(what.equals("hostSearch"))cpPosition.nMosquitosWater--;
 
       CoverPixel oldPos = cpPosition;

       cpPosition = giveNextPixel(state, what);

       if(what.equals("hostSearch"))cpPosition.nMosquitosBlood++;
       else if(what.equals("hostSearch"))cpPosition.nMosquitosWater++;

       //add mos to current  pixel
       cpPosition.nMosquitos++;

       if(sim.hybridExample)
       {
           if(cpPosition.xcor < 0
                   || cpPosition.ycor < 0             
                   || cpPosition.xcor >= sim.mosquitoGridWidth              
                   || cpPosition.ycor >= sim.mosquitoGridHeight              
             )
           {
               die(state); 
               stepEnd = true;
               return;
           }
       }

       Point centerOld = oldPos.square.getCentroid();
       Point centerNew = cpPosition.square.getCentroid();

       double dist = centerOld.distance(centerNew);
       //System.out.println (dist);
       if(dist > 100)
       {
           //System.out.println ("Pbc applied!!!!");
           pbcApplied = true;
           //System.exit(0);
       }

       if(sim.analysisMosHotSpots == 1)
       {
           cpPosition.addAccumulatedNMosquitoes();
           //System.out.println (cpPosition.getAccumulatedNMosquitoes());
           //System.exit(0);
       }

       sim.mosquitoGrid.setObjectLocation(this, 
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "mosquito")[0],
                     sim.utils.getCoordsToDisplay(state, cpPosition.getXcor(), cpPosition.getYcor(), "mosquito")[1]
           );

       //To get the mos trav dists
       //int now = (int)state.schedule.getTime();  
       //if(now % 24 == 0)
       //{    
       //    visitedCps.add(cpPosition);
       //}


       if(sim.debug)
       {
          if(stopped.equals("si"))
          {
              System.out.println ("stopped!!!!!!!!!!!!!!!!!");
          }
       }


   }


   //====================================================
   public Household selectHousehold(SimState state)
   {
      Bag households = cpPosition.getHouseholds();
      households.shuffle(state.random);

      int num_houses = households.size();

      if(num_houses > 0)
      {
         //select an household in the pixel wehre it is

          List<Double> weightsList = new ArrayList<Double>();

          for(int i =  0; i < num_houses; i++)
          {
              Household hh = (Household)households.get(i);
              weightsList.add((double)hh.getNumPeople());
          }

          double sum = 0.0;
          for(int i =  0; i < weightsList.size(); i++)
          {
              sum = sum + weightsList.get(i);
          }
          for(int i =  0; i < weightsList.size(); i++)
          {
              weightsList.set(i, weightsList.get(i)/sum);
          }

          sum = 0.0;
          double ran = state.random.nextDouble();
          for(int i =  0; i < num_houses; i++)
          {
              Household hh = (Household)households.get(i);
              sum = sum + weightsList.get(i);
              if(ran <= sum)return hh;
          }

         //System.out.println (household.getFractInfectedPeople());
      }
      else return null;

      return null;
   }

   //====================================================
   public Human selectHumanInHousehold(SimState state, Household hh)
   {
      sim = (AmaSim)state;
      //select a person inside the selected household
      Bag hpeople = hh.getPeople();
      hpeople.shuffle(state.random);

      int num_people = hpeople.size();
      double Psum = 0.0;
      int Iage = 0; 
      double Yage = 0.0; 
      double p = 0.0;

      for(int i =  0; i < num_people; i++)
      {
         Human h = ((Human)hpeople.objs[i]);
         Iage = h.getAge();
         Yage = Iage /(365 * 24);
         p = (1 - sim.cAgeDepForceOfInfection * Math.exp(- Yage * sim.kAgeDepForceOfInfection) );

         Psum = Psum + p;
      }


      double ran = state.random.nextDouble();


      double dsup = 0.0;
      double dinf = 0.0;
      Human h = ((Human)hpeople.objs[0]);
      for(int i =  0; i < num_people; i++)
      {
         h = ((Human)hpeople.objs[i]);
         Iage = h.getAge();
         Yage = Iage /(365 * 24);
         p = (1 - sim.cAgeDepForceOfInfection * Math.exp(- Yage * sim.kAgeDepForceOfInfection) );
         p = p / Psum;
         dsup = dsup + p;

         if( p >= dinf  && p < dsup )return h;

         dinf = dinf + p;
      }


      return h;
   }

   //====================================================
   public boolean biteHuman(SimState state, Human human)
   {
       sim = (AmaSim)state;
  
       double ran = state.random.nextDouble();
       if(ran < sim.probFailBite) return false;

       if(human.getIsProtected())
       {
           ran = state.random.nextDouble();
           if(ran <= sim.probMosquitoDieBitingProtectedPerson)
           {
               cpPosition.nDiedMosForHuman++;

               //System.out.println ("Died for bed net!!!!!!!!!!!!!");
               //System.exit(0);

               //sim.statsGlobal++;
               //System.out.println (sim.statsGlobal);
               die(state); 
               stepEnd = true;
           }
           return false;
       }
       //else
       //System.out.println ("Biting !!!!! ========================");
       //human.job.print();
       //System.exit(0);

       bloodFeeded = true;

       //Upgrade Statistics 
       sim.totalNumBite++;
       sim.weeklyNumBite++;
       sim.totNumDailyBites++;

       if(inHousehold != null)
       {
          if(!households.containsValue(inHousehold)) 
          {
              households.put(inHousehold, 1);
          }
          else
          {
             int  numB = households.get(inHousehold);          
             households.put(inHousehold, (numB + 1));
          }
       
       }

       int now = (int)state.schedule.getTime();  
       now = (int)now % 24;
       sim.bitingHours[now]++;

       if(noNulliparous)
       {
          sim.timeToGetBlood = sim.timeToGetBlood + timeToGetBlood;
          sim.numTimeToGetBlood++;

          sim.weeklyTimeToGetBlood = sim.weeklyTimeToGetBlood + timeToGetBlood;
          sim.weeklyTimeToGetBloodCounter++;
          //System.out.println ("Time to get blood: " + timeToGetBlood);
          //timeToGetBlood = sim.weeklyTimeToGetBlood / sim.weeklyTimeToGetBloodCounter;
          //System.out.println ("Time to get blood avg: " + timeToGetBlood);
       }
       //System.out.println (now + " " + sim.bitingHours[now]);

       ran = state.random.nextDouble();

       //Mosquito is infectious------------------------
       if(human.getSusceptible() && this.getInfectious() )
       {
          //System.out.println ("Human susceptible and mis infectiuos");
          if(sim.debug)
          {
             if(this.getPlasmodium() == null)
             {
                System.out.println ("Infectiuos Mosquito with null Plasmodium");
                System.exit(0);
             }
          }

          if(human.getAsymptomatic() == 1)return true;

          if(this.getPlasmodium().getTEfficiencyMosquitoToHuman() >= ran)
          {
             human.setupInfectionAfterBite(state, this);

             infectiousBite = true;

             sim.malariaMonthlyCases++;
             sim.weeklyNumHumanInfected++;

             if(sim.humanLocalMovement != 0)
             {
                 //int weekDay = sim.utils.getDayOfWeek(sim);
                 //System.out.println (human.dailyActivities.activity.get(weekDay));
                 //System.out.println (human.dailyActivities.where.get(weekDay));
             }


             //System.out.println ("Human Infected !!!!!!!!!!!!!!!!!!!");
          }
       }
       //Human is infectious--------------------------
       else if(human.getHInfectious()  
               && this.getInfected() < 0 
               && !this.getInfectious()
               && !human.infectionBlocking
               )
       {

           if(sim.analysisMosHotSpots == 1)
           {
               if(human.getPlasmodium().getType().equals("Vivax"))
               {
                   if(!sim.equilibration)cpPosition.increaseNMalariaCasesVMosquito();
                   //System.out.println ("Qui!!!!!");
                   //System.exit(0);
               }
               if(human.getPlasmodium().getType().equals("Falciparum"))
               {
                   //System.out.println ("Qui!!!!!");
                   //System.exit(0);
                   cpPosition.increaseNMalariaCasesFMosquito();
                   //System.out.println (cpPosition.getNMalariaCasesFMosquito());
               }
           }

          if(sim.debug)
          {
             if(human.getPlasmodium() == null)
             {
                System.out.println ("Infectiuos Human with null Plasmodium");
                System.exit(0);
             }
          }

          Plasmodium tPlasmodium = null;
          if(human.getPlasmodium().getType().equals("bothPlasmodia"))
          {
             ran = state.random.nextDouble();
             if(ran > 0.5)tPlasmodium =  sim.Falciparum;
             else tPlasmodium = sim.Vivax;
          }
          else
          {
             tPlasmodium = human.getPlasmodium(); 
          }

          if(human.getAsymptomatic() == 1)
          {
             ran = state.random.nextDouble();
             if(tPlasmodium.getAsymptomaticTEfficiencyHumanToMosquito() >= ran)
             {
                this.setPlasmodium(tPlasmodium);
                this.setInfected(tPlasmodium.getDD());
                return true;
             }
          }
          else
          {
             ran = state.random.nextDouble();
             if(tPlasmodium.getSymptomaticTEfficiencyHumanToMosquito() >= ran)
             {
                this.setPlasmodium(tPlasmodium);
                this.setInfected(tPlasmodium.getDD());
             }
          }

       }
       return true;
   }


   //====================================================
   public boolean isBitingTime(SimState state)
   {
       boolean isTime = false;
       int now = (int)state.schedule.getTime();  
       for(int i =  - sim.bitingPeakDev; i <= sim.bitingPeakDev; i++)
       {
          int t = sim.bitingPeak + i;

          if(t < 0){t = t + 24;}
          if(t > 23){t = t - 24;}

          int day = now % 24;
          if(day == t)
          {
             //System.out.println ("biting time!! " + (now % 24 ));
             isTime = true;
             return isTime;
          }
       }
        return isTime;
   }

   //====================================================
   public void selectMatingTime(SimState state)
   {
      sim = (AmaSim)state;
      double ran = state.random.nextDouble();
      double cumD = 0.0;

      for(int i = 0; i <  sim.matingProbDist.size(); i = i + 2)
      {
         cumD = cumD + sim.matingProbDist.get(i + 1);
         if(ran < cumD)
         {
            matingTime = sim.matingProbDist.get(i).intValue();
            break;
         }
      }
   }

   //====================================================
   public boolean survive(SimState state)
   {
      int now = sim.utils.getTime();

      sim = (AmaSim)state;
      double sd = 0.0;

      MeteoData md = sim.meteoData.get(now);
      double rain = md.getRain();
      double temp = (md.getMinT() + md.getMaxT())/2;

      double expo = -(1 / (-4.4 + (1.31 * temp) - (0.03 * Math.pow(temp, 2)  )  ) );
      sd = Math.exp(expo);

      if(sim.equilibration)
      {
         double ran2 = state.random.nextDouble();
         if(ran2 > 0.7)rain = 14;
         else rain = 0;
      }

      if(rain > sim.minRainWetDay)
          sd = sd * sim.rainSurvFact;


      double ran = state.random.nextDouble();

      //System.out.println ("-----------------------------");
      //System.out.println (now);
      //System.out.println (age);
      //System.out.println (rain);
      //System.out.println ("Temp: " + temp);
      //System.out.println ("Survival prob: " + sd);
      //System.out.println (ran);
      //System.exit(0);
      if(ran <= sd)
      {
          return true;
      }
      else return false;



   }

   //====================================================
   public void updateHouseholds(SimState state)
   {

       int size = households.size();
       if(size <= 1)return;

       sim = (AmaSim)state;
       //System.out.println("--- Number of bites in households ---- ");
       List<Double> tmp = new ArrayList<Double>();

       double norm = 0.0;
       for (Household key : households.keySet()) 
       {
           int value = households.get(key);
           //System.out.println("key: " + key + " value: " + value);
           tmp.add((double)value);
           norm = norm + value;
       }

       Collections.sort(tmp);

       List<Double> tmp2 = new ArrayList<Double>();
       for(int i = 0; i < tmp.size(); i++)
       {
           double d = tmp.get(i)/norm;
           tmp2.add(d);
       }

       List<Double> read = sim.householdsBites.get(size);
       if(read == null)
       {
           read = new ArrayList<Double>();
           for(int i = 0; i < tmp2.size(); i++)
           {
               read.add(0.0);
           }
       }

       int nn = 0;
       Object nnO = (Object)sim.householdsBitesNorm.get(size);
       if(nnO != null)nn = (int)nnO;
       nn = nn + 1;
       sim.householdsBitesNorm.put(size, nn);
       

       tmp = new ArrayList<Double>();
       for(int i = 0; i < tmp2.size(); i++)
       {
           double d = tmp2.get(i);
           double dRead = read.get(i);
           dRead = dRead + d;
           tmp.add(dRead);
       }

       sim.householdsBites.put(size, tmp);


   }

   //====================================================
   public void die(SimState state)
   {
       sim = (AmaSim)state;
       //System.out.println (age);

       //Upgrade Statistics -----
       sim.numExistedMosquitos++;
       sim.totalLifespan = sim.totalLifespan + age;
       sim.weeklyAvgMosquitoLifeSpan = sim.weeklyAvgMosquitoLifeSpan + age;
       sim.weeklyAvgMosquitoLifeSpanCount++;

       updateHouseholds(state);

       if(sporogonicEnded)
       {
          //System.out.println ("Sporogonic: " + (double)sporogonic/24 + "  " + plasmodium.getType() + " days");
          if(plasmodium.getType().equals("Falciparum"))
          {
             sim.totalSporogonicFalciparum = sim.totalSporogonicFalciparum + sporogonic;
             sim.numSporogonicMosquitosFalciparum++;

             sim.weeklyAvgSporogonicFalciparum = sim.weeklyAvgSporogonicFalciparum + sporogonic;
             sim.weeklyAvgSporogonicFalciparumCounter++;
          
          }

          if(plasmodium.getType().equals("Vivax"))
          {
             sim.totalSporogonicVivax = sim.totalSporogonicVivax + sporogonic;
             sim.numSporogonicMosquitosVivax++;

             sim.weeklyAvgSporogonicVivax = sim.weeklyAvgSporogonicVivax + sporogonic;
             sim.weeklyAvgSporogonicVivaxCounter++;
          }

       }

       //System.out.println ("--------------------------------------");
       if(sim.mosquitoGrid.remove(this) == null)
       {
          System.out.println ("The mosquito that is stopping do not exists in the mosquitoGrid");
          System.exit(0);
       }
       sim.mosquito_bag.remove(this);

       //if(infectiousBite && !pbcApplied)sim.mosTrajs.add(visitedCps);
       //if(!pbcApplied)sim.mosTrajs.add(visitedCps);

       stopped = "si";
       this.stopper.stop();         
       return;
   }


   //====================================================
   public void layEggs(SimState state, CoverPixel gcp)
   {
      //System.out.println ("Lay Eggs!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      sim = (AmaSim)state;
      bloodFeeded = false;
      restingAfterLayEggs = sim.restingTimeAfterLayEggs;
      if(noNulliparous)
      {
         sim.timeToLayEggs = sim.timeToLayEggs + timeToLayEggs;
         sim.numTimeToLayEggs++;

         sim.weeklyTimeToLayEggs = sim.weeklyTimeToLayEggs + timeToLayEggs;
         sim.weeklyTimeToLayEggsCounter++;
      }
      timeToGetBlood = 0.0;

      noNulliparous = true;

      gcp.addEgg();
      gcp.increaseNOvipositions();

   }


   //====================================================
   public void upgradeDegreesDays(SimState state)
   {
      //System.out.println ("-----------------------------");
      sim = (AmaSim)state;

      int now = sim.utils.getTime();

      //System.out.println ("now: " + now);

      MeteoData md = sim.meteoData.get(now);
      double avgTemp = md.getMinT() + md.getMaxT();
      avgTemp = avgTemp/2;
      //System.out.println ("AvgTemp: " + avgTemp);
      //System.out.println ("infected: " + infected);

      avgTemp = avgTemp - plasmodium.getIncubationMinTemp();
      avgTemp = avgTemp/24;

      infected = infected - avgTemp;

      //System.out.println ("Incubation Temp: " + plasmodium.getIncubationMinTemp());
      //System.out.println ("AvgTemp/24: " + avgTemp);
      //System.out.println ("AvgTemp: " + avgTemp*24);
      //System.out.println ("infected: " + infected);
      //System.out.println ("ExtTime: " + infected/(avgTemp*24));
      //if(plasmodium.getType().equals("Vivax"))
      //{
      //   System.exit(0);
      //}

      sporogonic++;
   }


   //====================================================
   public void checkLogic(SimState state)
   {
      boolean error = false;

      if(beforeMating && hostSearch) error = true;
      if(beforeMating && breedingHabitatSearch) error = true;
      if(hostSearch && breedingHabitatSearch) error = true;

      if(error)
      {
         System.out.println ("Error in mosquito beahviour logic!!!!!");
         System.exit(0);
      }

   }

   //====================================================
   public void ifIHaveToDie(SimState state)
   {
      //See if hte mosquito have to die -----------------
      this.setAge(this.getAge() + 1);
      if((age % 24) == 0 && !survive(state)){die(state); stepEnd = true;}
      return;
   }



   //====================================================
   public boolean isDayTime(SimState state)
   {
       int now = (int)state.schedule.getTime();  
       now = (int)now % 24;

       if(now >= 7 && now <= 17)
       {
          
          //System.out.println ("NOOOOOOOO Move!");
          //System.out.println (now);
          //now = (int)state.schedule.getTime();  
          //System.out.println (now);
          return true; 
       }
       //System.out.println ("Move!");

       return false;
   }


   //====================================================
   public void pr()
   {
       System.out.println ("-----------------------------");
       //System.out.println (this.getAge() + "  mosquito age");
       //if(infectious)
       //{
       //   System.out.println (infectious);
       //}
   }


   //====================================================
   public Human selectHumanInHousehold2(SimState state, Household hh)
   {
      Bag hpeople = hh.getPeople();

      //Everybody has left the house!!!!
      if(hpeople.size() < 1)
      {
          inHousehold = null;
          return null;
      }

      hpeople.shuffle(state.random);

      Human h = ((Human)hpeople.get(0));

      return h;
   }

   //====================================================
   public void biteAnimal(SimState state)
   {
       sim = (AmaSim)state;
  
       bloodFeeded = true;

       //Upgrade Statistics 
       sim.totalNumBiteAnimal++;
       sim.weeklyNumBiteAnimal++;
       sim.totNumDailyBitesAnimals++;
       //System.out.println ("Num bite on animals:"  + sim.totNumDailyBitesAnimals);


       int now = (int)state.schedule.getTime();  
       now = (int)now % 24;
       sim.bitingHours[now]++;

       if(noNulliparous)
       {
          sim.timeToGetBlood = sim.timeToGetBlood + timeToGetBlood;
          sim.numTimeToGetBlood++;

          sim.weeklyTimeToGetBlood = sim.weeklyTimeToGetBlood + timeToGetBlood;
          sim.weeklyTimeToGetBloodCounter++;
       }
       //System.out.println (now + " " + sim.bitingHours[now]);

   }



}


