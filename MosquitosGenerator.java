/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

public class MosquitosGenerator implements Steppable
{
   private static final long serialVersionUID = 1L;

   //====================================================
   public MosquitosGenerator(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;

   }
   
   //====================================================
   public void step(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;


   }

   //====================================================
   public void generateMosquito(final SimState state, CoverPixel cp)
   {
      final AmaSim sim = (AmaSim)state;

      Bag linkCp = cp.getLinkPixels();

      //System.out.println (linkCp.size());

      CoverPixel cpm = ((CoverPixel)linkCp.objs[0]);

      //Integer[] coords = sim.utils.getCellFromGeoToMosquitoGrid(state, xpos, ypos);

      Mosquito m = new Mosquito(state, cpm, "mosquito");

      //System.out.println (sim.gridHeight);
      //System.out.println (state.random.nextInt(sim.gridHeight));
      
      //System.out.println (m.getAge() + "  age");
   }

   //====================================================
   public void killMos(final SimState state, Mosquito m)
   {
      final AmaSim sim = (AmaSim)state;

      m = null;
   }

   //Generate starting number of mosquitos===============
   public void generateMosquitosStart(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;
      CoverPixel cp;

      //if(sim.deltaLevel < 0) return;

      Bag pixels = sim.geoGrid.elements();
      pixels.shuffle(state.random);
      double rest = 0;
      double dnumMos = 0;
      //int numMos = 10000;
      int numMos = 10000;

      int numPixel = pixels.size();

      int stats = 0;

      for(int i = 0; i < numMos; i++)
      {
         int iran = state.random.nextInt(numPixel - 1);

         cp = ((CoverPixel)pixels.objs[iran]);

         sim.mosGen.generateMosquito(state, cp);
         stats++;
      }
      System.out.println(stats + " Mosquitos generated");
      //System.exit(0);

      Bag mos = sim.mosquito_bag;
      int len = mos.size();

      for(int i = 0; i < len; i++)
      {
         double ran = state.random.nextDouble();
         if(ran > 0.0085)continue;

         Mosquito m = ((Mosquito)mos.objs[i]);
         int iran = state.random.nextInt(10);
         m.setAge(iran);


         iran = state.random.nextInt(7);
         if(iran == 6)
         {
             m.setPlasmodium(sim.Falciparum);
             iran = state.random.nextInt(sim.Falciparum.getDD()) + 1;
             m.setInfected(iran);
         }
         else
         {
             m.setPlasmodium(sim.Vivax);
             iran = state.random.nextInt(sim.Vivax.getDD()) + 1;
             m.setInfected(iran);
         }

      }

   }


   //Generate starting number of mosquitos===============
   public void generateMosquitosStartEggs(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;
      CoverPixel cp;

      //if(sim.deltaLevel < 0) return;

      Bag pixels = sim.geoGrid.elements();
      pixels.shuffle(state.random);
      int len = pixels.size();
      double rest = 0;
      double dnumMos = 0;
      //int numMos = 0;
      int numMos = 10000;
      double numMosPixel = Math.round(numMos/len);
      if(1 == 1)return;

      for(int i = 0; i < len; i++)
      {
         cp = ((CoverPixel)pixels.objs[i]);

         if(cp.getIsWaterHigh() == 0) continue;
         if(cp.getIsWater() == 0) continue;

         int iran = state.random.nextInt(10);

         int elen = (int)(sim.aquaticStageTime / 24);
         for(int j = 0;  j < (elen - 1); j++)
         {
            iran = state.random.nextInt(100);
            if(iran < 7)
            {
               cp.eggsOvoposited.add(iran);
            }
            //System.out.println(iran);
         }

         iran = state.random.nextInt(100);
         if(iran < 33)
         {
             dnumMos = 1.5 * sim.propOvopositionEmergentAdults;
             numMos = (int)Math.round(dnumMos);      
             if(numMos > 0)
             {
                 for(int m = 0; m < numMos; m++)
                 {
                     //System.out.println("----------------------");
                     //System.out.println("Mosquito");
                     sim.mosGen.generateMosquito(state, cp);
                 }
             }
             else
             {
                 sim.mosGen.generateMosquito(state, cp);
             }

         }


      }

      //System.out.println(stats + " Mosquitos generated");


      Bag mos = sim.mosquito_bag;
      len = mos.size();

      for(int i = 0; i < len; i++)
      {
         double ran = state.random.nextDouble();
         if(ran > 0.008)continue;

         Mosquito m = ((Mosquito)mos.objs[i]);
         int iran = state.random.nextInt(10);
         m.setAge(iran);


         iran = state.random.nextInt(7);
         //System.out.println(iran);
         if(iran > 4)
         {
             m.setPlasmodium(sim.Falciparum);
             iran = state.random.nextInt(sim.Falciparum.getDD()) + 1;
             m.setInfected(iran);
         }
         else
         {
             m.setPlasmodium(sim.Vivax);
             iran = state.random.nextInt(sim.Vivax.getDD()) + 1;
             m.setInfected(iran);
         }

      }
      //System.exit(0);

   }

//============================================================   
}
