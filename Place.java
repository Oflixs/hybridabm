/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;

import sim.util.geo.MasonGeometry;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Envelope;

public class Place implements Steppable
{
   private static final long serialVersionUID = 1L;

   public CoverPixel cp = null;
   public Point centralPoint = null;
   public String type;
   //in meters
   public double size = 0.0;
   //in number of geo pixels
   public double iSize = 0;


   //====================================================
   public Place(CoverPixel pcp, Point pcentral, String ptype)
   {

      cp = pcp;
      centralPoint = pcentral;
      type = ptype;

   }

   //====================================================
   public void step(final SimState state)
   {
   }
   
}
