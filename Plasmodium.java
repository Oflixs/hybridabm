/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 


//----------------------------------------------------
public class Plasmodium implements Steppable
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;

   //====================================================
   public Stoppable stopper;

   String type = "";
   public String getType(){return type;};
   public void setType(String y){type = y;};

   //Malaria state variables----------------------------
   public int infected = -100;
   public Integer getInfected(){return infected;};
   public void setInfected(int y){infected = y;};

   public List<Integer> intrinsicIncubation = new ArrayList<Integer>();
   public List<Integer> getIntrinsicIncubation(){return intrinsicIncubation;};
   public void setIntrinsicIncubation(List<Integer> y){intrinsicIncubation = y;};

   public int DD = 0;
   public Integer getDD(){return DD;};
   public void setDD(int y){DD = y;};

   public double incubationMinTemp = 0;
   public Double getIncubationMinTemp(){return incubationMinTemp;};
   public void setIncubationMinTemp(double y){incubationMinTemp = y;};

   public double SymptomaticTEfficiencyHumanToMosquito = 0;
   public Double getSymptomaticTEfficiencyHumanToMosquito(){return SymptomaticTEfficiencyHumanToMosquito;};
   public void setSymptomaticTEfficiencyHumanToMosquito(double y){SymptomaticTEfficiencyHumanToMosquito = y;};

   public double AsymptomaticTEfficiencyHumanToMosquito = 0;
   public Double getAsymptomaticTEfficiencyHumanToMosquito(){return AsymptomaticTEfficiencyHumanToMosquito;};
   public void setAsymptomaticTEfficiencyHumanToMosquito(double y){AsymptomaticTEfficiencyHumanToMosquito = y;};

   public double tEfficiencyMosquitoToHuman = 0;
   public Double getTEfficiencyMosquitoToHuman(){return tEfficiencyMosquitoToHuman;};
   public void settEfficiencyMosquitoToHuman(double y){tEfficiencyMosquitoToHuman = y;};

   public int humanInfectiousPeriodNotTreated = 0;
   public Integer getHumanInfectiousPeriodNotTreated(){return humanInfectiousPeriodNotTreated;};
   public void setHumanInfectiousPeriodNotTreated(int y){humanInfectiousPeriodNotTreated = y;};

   public int humanInfectiousPeriodTreated = 0;
   public Integer getHumanInfectiousPeriodTreated(){return humanInfectiousPeriodTreated;};
   public void setHumanInfectiousPeriodTreated(int y){humanInfectiousPeriodTreated = y;};

   public double fractAsymptomaticityGainedPerInfection = 0.0;
   public Double getFractAsymptomaticityGainedPerInfection(){return fractAsymptomaticityGainedPerInfection;};
   public void setFractAsymptomaticityGainedPerInfection(double y){fractAsymptomaticityGainedPerInfection = y;};

   public double fractAsymptomaticityLostPerHour = 0.0;
   public Double getFractAsymptomaticityLostPerHour(){return fractAsymptomaticityLostPerHour;};
   public void setFractAsymptomaticityLostPerHour(double y){fractAsymptomaticityLostPerHour = y;};
   
   public int startCommunicability = 0;
   public int getStartCommunicability(){return startCommunicability;};
   public void setStartCommunicability(int y){startCommunicability = y;};

   //====================================================
   public Plasmodium(SimState state, String pType)
   {
       sim = (AmaSim)state;
       type = pType;
   }

   //====================================================
   public void init(SimState state, ReadInput input)
   {
      if(type.equals("Falciparum"))
      {
         intrinsicIncubation = input.readListInt("FalciparumIntrinsicIncubation");
         intrinsicIncubation.set(0, intrinsicIncubation.get(0)*24);
         intrinsicIncubation.set(1, intrinsicIncubation.get(1)*24);


         DD = input.readInt("FalciparumDD");
         incubationMinTemp = input.readDouble("FalciparumIncubationMinTemp");

         AsymptomaticTEfficiencyHumanToMosquito = input.readDouble("AsymptomaticFalciparumTEfficiencyHumanToMosquito");
         SymptomaticTEfficiencyHumanToMosquito = input.readDouble("SymptomaticFalciparumTEfficiencyHumanToMosquito");

         tEfficiencyMosquitoToHuman = input.readDouble("FalciparumTEfficiencyMosquitoToHuman");

         humanInfectiousPeriodTreated = input.readInt("FalciparumHumanInfectiousPeriodTreated");
         humanInfectiousPeriodNotTreated = input.readInt("FalciparumHumanInfectiousPeriodNotTreated") * 24;

         fractAsymptomaticityGainedPerInfection = input.readDouble("FalciparumFractAsymptomaticityGainedPerInfection");

         fractAsymptomaticityLostPerHour = input.readDouble("FalciparumFractAsymptomaticityLostPerHour");

         startCommunicability = input.readInt("FalciparumStartCommunicability") * 24;

      
      }

      if(type.equals("Vivax"))
      {
         intrinsicIncubation = input.readListInt("VivaxIntrinsicIncubation");
         intrinsicIncubation.set(0, intrinsicIncubation.get(0)*24);
         intrinsicIncubation.set(1, intrinsicIncubation.get(1)*24);

         DD = input.readInt("VivaxDD");
         incubationMinTemp = input.readDouble("VivaxIncubationMinTemp");

         AsymptomaticTEfficiencyHumanToMosquito = input.readDouble("AsymptomaticVivaxTEfficiencyHumanToMosquito");
         SymptomaticTEfficiencyHumanToMosquito = input.readDouble("SymptomaticVivaxTEfficiencyHumanToMosquito");


         tEfficiencyMosquitoToHuman = input.readDouble("VivaxTEfficiencyMosquitoToHuman");


         humanInfectiousPeriodTreated = input.readInt("VivaxHumanInfectiousPeriodTreated");
         humanInfectiousPeriodNotTreated = input.readInt("VivaxHumanInfectiousPeriodNotTreated") * 24;

         fractAsymptomaticityGainedPerInfection = input.readDouble("VivaxFractAsymptomaticityGainedPerInfection");

         fractAsymptomaticityLostPerHour = input.readDouble("VivaxFractAsymptomaticityLostPerHour");

         startCommunicability = input.readInt("VivaxStartCommunicability") * 24;
      }

      if(type.equals("bothPlasmodia"))
      {
          //nothing.......
      }



   }

   //====================================================
   public void step(SimState state)
   {

   }


}


