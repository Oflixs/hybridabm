/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.HashMap;

import java.text.SimpleDateFormat;
import java.text.ParseException;

import sim.util.geo.MasonGeometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.Geometry;

public class ReadDailyActivities
{
//private static final long serialVersionUID = -4554882816749973618L;
private static final long serialVersionUID = 1L;

Bag readed_xlsx = new Bag();
public Bag getReaded_xlsx() { return readed_xlsx; }

public AmaSim sim;
public SimState state;
Workbook workbook;

public int nAges = 0;
public int nActivities = 0;

//====================================================
public ReadDailyActivities(AmaSim psim)
{
    sim = psim;
    state = (SimState)psim;
}

//====================================================
public void read()
{
    AmaSim sim = (AmaSim)state;
    System.out.println("================================================");
    System.out.println ("Reading the Human Daily Activities DataBase ...");
    System.out.println(" ");

    try{
        workbook = WorkbookFactory.create(new FileInputStream("sim/app/AmaSim/input_data/dailyActivities.xls") );

        Sheet sheet = workbook.getSheetAt(0);
        Row row;
        Cell cell;
        String delims = "[ ]+";
        double val;
        int    int_val;
        double cat;
        String string = "";

        int numRows = sheet.getPhysicalNumberOfRows();
        //System.out.println ("numRows " + numRows);

        for(int i = 0; i < numRows; i++)
        { 
            row = sheet.getRow(i);
            if(row == null)continue;;

            cell = row.getCell(0);
            if(cell == null)continue;

            string = (String)(cell.getStringCellValue());
            String[] words = string.split(delims);

            if(words[0].equals("segment"))readSegment(i, words);
            else if(words[0].equals("activity"))readActivity(i, words);
            //System.exit(0);

        }

    }
    catch(FileNotFoundException e)
    {
        System.out.println(e);
    }
    catch(IOException e)
    {
        System.out.println(e);
    }
    catch(InvalidFormatException e)
    {
        System.out.println(e);
    }

    //int len = readed_xlsx.size();
    System.out.println("Num Age Segment: " + nAges);
    System.out.println("Num Activities: " + nActivities);
    //System.exit(0);
}


//====================================================
public void readSegment(int start, String[] words)
{
    AgeSegment ageSegment;

    Sheet sheet = workbook.getSheetAt(0);
    Row row;
    Cell cell;

    String string = "";
    int ivalue = 0;
    String delims = "[ ]+";

    int numRows = sheet.getPhysicalNumberOfRows();
    //System.out.println ("numRows " + numRows);


    for(int i = start + 1; i < numRows; i++)
    {
        //System.out.println ("-----------------");
        ageSegment = new AgeSegment(state);

        row = sheet.getRow(i);

        if(row == null)return;
        if(row.getFirstCellNum() == -1)return;

        cell = row.getCell(0);
        string = (String)(cell.getStringCellValue());
        ageSegment.name = string; 

        cell = row.getCell(1);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        ageSegment.ageMin = ivalue; 
        //System.out.println (ivalue);

        cell = row.getCell(2);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        ageSegment.ageMax = ivalue; 
        //System.out.println (ivalue);

        cell = row.getCell(3);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        ageSegment.numSleepHours = ivalue; 

        sim.ageSegments.add(ageSegment);
        nAges++;
        //ageSegment.print();
    }



}


//====================================================
public void readActivity(int start, String[] words)
{
    Sheet sheet = workbook.getSheetAt(0);
    Row row;
    Cell cell;
    Calendar cal;

    String string = "";
    int ivalue = 0;
    double dvalue = 0.0;
    String delims = "[ ]+";

    int numRows = sheet.getPhysicalNumberOfRows();
    //System.out.println ("numRows " + numRows);

    Activity activity;
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

    for(int i = start + 1; i < numRows; i++)
    {
        //System.out.println ("-------------------------");
        row = sheet.getRow(i);

        activity = new Activity(state);
        activity.type = words[1];

        //System.out.println ("Activity type:" + activity.type);

        if(row == null)return;
        if(row.getFirstCellNum() == -1)return;

        cell = row.getCell(0);
        if(cell == null)return;
        string = (String)(cell.getStringCellValue());
        activity.name = string; 

        try{
            cell = row.getCell(1);
            string = (String)(cell.getStringCellValue());
            cal = Calendar.getInstance();
            Date date = formatTime.parse(string);
            cal.setTime(date);
            activity.from = cal; 
        }
        catch(ParseException e)
        {
            System.out.println(e);
        }

        try{
            cell = row.getCell(2);
            string = (String)(cell.getStringCellValue());
            cal = Calendar.getInstance();
            Date date = formatTime.parse(string);
            cal.setTime(date);
            activity.to = cal; 
        }
        catch(ParseException e)
        {
            System.out.println(e);
        }

        //System.out.println ("Activity from:" + activity.from.get(Calendar.HOUR_OF_DAY));
        //System.out.println ("Activity to:" + activity.to.get(Calendar.HOUR_OF_DAY));

        //System.out.println (ivalue);

        cell = row.getCell(3);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        activity.timePerWeek = ivalue; 
        //System.out.println (ivalue);

        cell = row.getCell(4);
        string = (String)(cell.getStringCellValue());
        activity.timeDist = string; 

        cell = row.getCell(5);
        dvalue = (double)(cell.getNumericCellValue());
        activity.sigma = dvalue; 

        cell = row.getCell(6);
        string = (String)(cell.getStringCellValue());
        //System.out.println (string);
        String[] days = string.split(delims);
        int stats = 0;
        //System.out.println (days.length);
        //System.out.println (days[0]);
        for(int z = 0; z < days.length; z++)
        {
            //System.out.println ("--" + day + "--");
            stats++;
            //System.out.println(Integer.valueOf(days[z]));
            ivalue = Integer.valueOf(days[z]);
            //ivalue = Integer.parseInt(day);
            activity.activeDays.add(ivalue);
        }
        if(stats == 7)activity.allDays = true;

        cell = row.getCell(7);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        activity.maxHoursPerDay = ivalue; 

        cell = row.getCell(8);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        activity.noFlexTime = ivalue; 

        cell = row.getCell(9);
        dvalue = (double)(cell.getNumericCellValue());
        activity.fractPop = dvalue; 

        cell = row.getCell(10);
        ivalue = (int)Math.round((cell.getNumericCellValue()));
        activity.priority = ivalue; 

        cell = row.getCell(11);
        dvalue = (double)(cell.getNumericCellValue());
        activity.malariaRisk = dvalue; 

        sim.activities.add(activity);
        nActivities++;
        //activity.print();
        //System.exit(0);

    }
    //System.exit(0);
}

//====================================================
public void  initSocial()
{
    Bag places = sim.socialPlaces.getGeometries();
    int num_places = places.size();;

    System.out.println ("Num social places: " + num_places);

    for (int ii = 0; ii < num_places; ii++)
    {
        MasonGeometry MGPlace = (MasonGeometry)places.objs[ii];
        Point pPlace = (Point)MGPlace.getGeometry();

        double maxDistSport = -10000.0;
        double maxDistLeisure = -10000.0;
        double maxDistShop = -10000.0;
        double maxDistChurch = -10000.0;
        double maxDistSchool = -10000.0;
        double maxDistGovernement = -10000.0;
        double maxDistHealth = -10000.0;



    }


}


//====================================================
public void setCoverPixelsRoads()
{
    System.out.println("Finding covePixels covered by roads    ");

    int numClusters = sim.clusters.size();
    int numRoads = sim.roads.getGeometries().numObjs;

    int stats = 0;

    System.out.println("Number of road segments: " + numRoads);

roads:
    for(int r = 0; r < numRoads; r++)
    {
        //System.out.println("---------------------------------");
        //System.out.println("Road low num cluster: " + i);
        MasonGeometry mgRoad = (MasonGeometry)sim.roads.getGeometries().objs[r];
        Geometry pRoad = (Geometry)mgRoad.getGeometry();

cluster:
        for(int i =  0; i < numClusters; i++)
        {

            Cluster cluster = ((Cluster)sim.clusters.objs[i]);
            Polygon cSquare = (Polygon)cluster.square;

            if(!cSquare.intersects(pRoad))continue cluster;

            int numCp = cluster.coverPixels.size();

            //System.out.println("-------");
            //System.out.println(w);
            //System.out.println("Cluster number: " + i);

            //System.out.println("Area: " + pRoad.getArea());
cp:
            for(int c = 0; c < numCp; c++)
            {
                CoverPixel cp = ((CoverPixel)cluster.coverPixels.objs[c]);
                Polygon square = (Polygon)cp.square;
                //if(c % 100 == 0)System.out.println("Cp number: " + c);
                //System.out.println("Cp number: " + c);

                if(pRoad.intersects(square))
                {
                    //System.out.println("Is Road !!!");
                    cp.setIsRoad(1); 
                    //System.exit(0);
                    //cp.setIsRoad(1); 
                    //System.out.println("Cpx: " + cp.getXcor());
                    //System.out.println("Cpy: " + cp.getYcor());
                    //break cp;
                    stats++;
                    //continue;
                }


            }
        }


    }


    System.out.println("Number of CoverPixel intersecting roads: " + stats);


}


//====================================================
public void initializePlaces()
{

    System.out.println("Initialize Daily Activity Places ----");
    Bag socialPlaces = sim.socialPlaces.getGeometries();
    int num_places = socialPlaces.size();

    for (int ii = 0; ii < num_places; ii++)
    {
        MasonGeometry MGPlace = (MasonGeometry)socialPlaces.objs[ii];

        String type = (String)MGPlace.getStringAttribute("LABEL");

        if(type.equals("Casa"))continue;
        if(type.equals("Bridge"))continue;
        if(type.equals("Muestra de agua"))continue;

        Point pPlace = (Point)MGPlace.getGeometry();

        Integer[] coords  = sim.utils.getDiscreteCoordinatesPoint(pPlace, "geo");
        CoverPixel cp = sim.utils.getCoverPixelFromCoords(state, coords[0], coords[1], "geo");

        Place place = new Place(cp, pPlace, type);

        if(place != null)
        {
            sim.socialPlacesGrid.setObjectLocation(place,
                    sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[0],
                    sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[1]
                    );
        }

        System.out.println(type);


        if(type.equals("Leisure Place") || type.equals("Recreacional"))
        {
            place.size = 60;
            place.iSize = (int)Math.ceil(place.size / sim.geoCellSize);
        }

        if(type.equals("Sport Field"))
        {
            place.size = 30;
            place.iSize = (int)Math.ceil(place.size / sim.geoCellSize);
        }

        if(type.equals("School"))
        {
            place.size = 20;
            place.iSize = (int)Math.ceil(place.size / sim.geoCellSize);
        }

        if(type.equals("Health post") || type.equals("Puesto de salud"))
        {
            place.size = 20;
            place.iSize = (int)Math.ceil(place.size / sim.geoCellSize);
        }

        if(type.equals("Church") || type.equals("Iglesia"))
        {
            place.size = 20;
            place.iSize = (int)Math.ceil(place.size / sim.geoCellSize);
        }





        sim.dailyActivitiesPlaces.add(place);


    }


    //System.exit(0);

}


//====================================================
public void initializeFarms()
{

    System.out.println("Initialize Farms -----------");
    Bag farms = sim.farms.getGeometries();
    int num_farms = farms.size();

    for (int ii = 0; ii < num_farms; ii++)
    {
        MasonGeometry MGFarm = (MasonGeometry)farms.objs[ii];

        Point pFarm = (Point)MGFarm.getGeometry();

        Integer[] coords  = sim.utils.getDiscreteCoordinatesPointFarm(pFarm, "geo");
        CoverPixel cp = null;
        if(coords != null)cp = sim.utils.getCoverPixelFromCoords(state, coords[0], coords[1], "geo");

        Place place = new Place(cp, pFarm, "farm");

        if(cp != null)
        {
            sim.farmGrid.setObjectLocation(place,
                    sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[0],
                    sim.utils.getCoordsToDisplay(state, cp.getXcor(), cp.getYcor(), "geo")[1]
                    );
        }

        sim.farmPlaces.add(place);


    }

    //System.exit(0);

}



}

