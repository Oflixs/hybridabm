/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import sim.engine.SimState;

public class ReadMalariaPrevalence {
   private static final long serialVersionUID = 1L;


   String stats[] = new String[2];
   public String[] getStats() { return stats; }

   public String out_stats = "";

   public AmaSim sim = null;

   public String input_file = "";

   //------------------------------------------------
   public void setFile(String f)
   {
       input_file = f;
       String path = "sim/app/AmaSim/input_data/";
       input_file = path + input_file;
   }



   //====================================================
   public ReadMalariaPrevalence(AmaSim sim)
   {

   } 


   //====================================================
   public void readIncidence(AmaSim sim)
   {
      System.out.println("================================================");
      System.out.println ("Reading the Malaria incidence series...........");
      System.out.println(" ");

    try{
           Workbook workbook = WorkbookFactory.create(new FileInputStream(input_file));

           int date_index = 7;
           //place is where the person lives
           int place_index = 3;
           //healt post
           int hpost_index = 23;
           
           Sheet sheet = workbook.getSheet("MAZAN_NAPO_CORRECT");
           Row row;
           Cell cell;
           String delims = "[ ]+";
           double val;
           int    int_val;
           double cat;
           String string = "";
           String stringNew = "";
           boolean write = false;
           Date date = null;
           double level = 0.0;
           double levelT = 0.0;
           int stats = 0;
           
           int numRows = sheet.getPhysicalNumberOfRows();
           System.out.println ("numRows " + numRows);
           System.out.println ("Places: " + sim.places);

           for(int i = 1; i < numRows; i++)
           { 
              write = false;
              MalariaData md = new MalariaData(sim);

              row = sheet.getRow(i);

              //int_val = cell.getCellType();
              //System.out.println (int_val);

              DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
              cell = row.getCell(date_index);
              if(cell == null)continue;
              //string = Integer.toString((int)cell.getNumericCellValue());
              date = (Date)(cell.getDateCellValue());
              if(date == null)continue;
              Calendar calendar = Calendar.getInstance();
              calendar.setTime(date);
              //System.out.println(calendar.getTime());
              //System.out.println(sim.startDate);

              //System.out.println (string);
              //System.exit(0);

              cell = row.getCell(place_index);
              if(cell == null)continue;
              string = (String)(cell.getStringCellValue());
              stringNew = string.toLowerCase();
              stringNew = stringNew.replace(" ", "");

              //System.out.println (string);
              //System.out.println (stringNew);

              if(!sim.places.contains(stringNew))continue;
              md.place = string;

              cell = row.getCell(hpost_index);
              if(cell == null)continue;
              string = (String)(cell.getStringCellValue());
              stringNew = string.toLowerCase();
              stringNew = stringNew.replace(" ", "");

              //System.out.println (stringNew + " " + sim.healthPost);
              //System.out.println (stringNew + " " + md.place);

              if(!sim.healthPost.contains(stringNew))continue;
              md.hPost = string;


              md.setCalendar(calendar);
              sim.malariaData.add(md);
           }

        }
        catch(FileNotFoundException e)
            {
               System.out.println(e);
            }
        catch(IOException e)
            {
               System.out.println(e);
            }
        catch(InvalidFormatException e)
            {
               System.out.println(e);
            }

        //Checks the malaria incidence data
        //for(int i = 0; i < sim.malariaData.size(); i++)
        //{
        //   MalariaData md = sim.malariaData.get(i);

        //   System.out.println (
        //           "Date: " + md.getCalendar().getTime() +
        //           " Place: " + md.place +
        //           " Health Post: " + md.hPost
        //           );
        //}

        //System.exit(0);


   }

//====================================================
public void readAgeStructure(SimState state)
{
    AmaSim sim = (AmaSim)state;
    System.out.println("================================================");
    System.out.println ("Reading Age Structure    ......................");
    System.out.println(" ");

    List<Double> values = new ArrayList<Double>();
    double tot = 0.0;

try{
       Workbook workbook = WorkbookFactory.create(new FileInputStream("sim/app/AmaSim/input_data/ageStructureLoreto2014.xls") );


       int x0_5_index = 3;
       int x6_12_index = 4;
       int x13_17_index = 5;
       int x18_24_index = 6;
       int x25_39_index = 7;
       int x40_55_index = 8;
       int x56_index = 9;

       
       Sheet sheet = workbook.getSheetAt(0);
       Row row;
       Cell cell;
       String delims = "[ ]+";
       
       int numRows = sheet.getPhysicalNumberOfRows();
       //System.out.println ("numRows " + numRows);

          row = sheet.getRow(1);

         //int_val = cell.getCellType();
          //System.out.println (int_val);

          cell = row.getCell(x0_5_index);
          double val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x6_12_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x13_17_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x18_24_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x25_39_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x40_55_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          cell = row.getCell(x56_index);
          val = (double)(cell.getNumericCellValue());
          values.add(val);

          tot = tot + val;

          //md.print();
          //System.exit(0);

    }
    catch(FileNotFoundException e)
        {
           System.out.println(e);
        }
    catch(IOException e)
        {
           System.out.println(e);
        }
    catch(InvalidFormatException e)
        {
           System.out.println(e);
        }

    double val = values.get(0)/tot; 
    sim.ageStructure.put("0_5", val);

    val = val + values.get(1)/tot;
    sim.ageStructure.put("6_12", val);

    val = val + values.get(2)/tot;
    sim.ageStructure.put("13_17", val);

    val = val + values.get(3)/tot;
    sim.ageStructure.put("18_24", val);

    val = val + values.get(4)/tot;
    sim.ageStructure.put("25_39", val);

    val = val + values.get(5)/tot;
    sim.ageStructure.put("40_55", val);

    val = val + values.get(6)/tot;
    sim.ageStructure.put("56_", val);


    System.out.println("0 < Age < 5: " + sim.ageStructure.get("0_5"));
    System.out.println(" 6  < Age < 12: " + sim.ageStructure.get("6_12"));
    System.out.println(" 13 <  Age < 17: " + sim.ageStructure.get("13_17"));
    System.out.println(" 18 <  Age < 24: " + sim.ageStructure.get("18_24"));
    System.out.println(" 25 <  Age < 39: " + sim.ageStructure.get("25_39"));
    System.out.println(" 40 <  Age < 55: " + sim.ageStructure.get("40_55"));
    System.out.println(" Age > 56: " + sim.ageStructure.get("56_"));

    //System.exit(0);

}






}//End of file
