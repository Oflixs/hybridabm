/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ReadNapoRiverLevels {
   private static final long serialVersionUID = 1L;


   String stats[] = new String[2];
   public String[] getStats() { return stats; }

   public String out_stats = "";

   public AmaSim sim = null;

   public String input_file = "";
   public void setFile(String f)
   {
       input_file = f;
       String path = "sim/app/AmaSim/input_data/";
       input_file = path + input_file;

       //System.out.println(input_file);
       //System.exit(0);
   }


   //====================================================
   public ReadNapoRiverLevels(AmaSim sim)
   {

   } 


   //====================================================
   public void readLevels(AmaSim sim)
   {
      System.out.println("================================================");
      System.out.println ("Reading the river level    Data ...............");
      System.out.println(" ");

    try{
           Workbook workbook = WorkbookFactory.create(new FileInputStream(input_file));

           int date_index = 0;
           int level0 = 0;
           int level1 = 1;
           int level2 = 2;
           int level3 = 3;
           
           Sheet sheet = workbook.getSheetAt(0);
           Row row;
           Cell cell;
           String delims = "[ ]+";
           double val;
           int    int_val;
           double cat;
           String string = "";
           boolean write = false;
           Date date = null;
           double level = 0.0;
           double levelT = 0.0;
           int stats = 0;
           
           int numRows = sheet.getPhysicalNumberOfRows();
           //System.out.println ("numRows " + numRows);

           for(int i = 1; i < numRows; i = i + 2)
           { 
              write = false;

              row = sheet.getRow(i);

              //int_val = cell.getCellType();
              //System.out.println (int_val);

              DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
              cell = row.getCell(date_index);
              //string = Integer.toString((int)cell.getNumericCellValue());
              date = (Date)(cell.getDateCellValue());
              Calendar calendar = Calendar.getInstance();
              calendar.setTime(date);
              //date = format.parse(date);
              //System.out.println(date);
              //System.out.println(sim.startDate);

              //System.out.println (string);
              //System.exit(0);

              stats = 0;
              level = 0.0;
              row = sheet.getRow(i + 1);

              cell = row.getCell(level0);
              levelT = (Double)(cell.getNumericCellValue());
              if(levelT < 30)
              {
                  level = level + levelT;
                  stats++;
              }

              cell = row.getCell(level1);
              levelT = (Double)(cell.getNumericCellValue());
              if(levelT < 30)
              {
                  level = level + levelT;
                  stats++;
              }

              cell = row.getCell(level2);
              levelT = (Double)(cell.getNumericCellValue());
              if(levelT < 30)
              {
                  level = level + levelT;
                  stats++;
              }

              cell = row.getCell(level3);
              levelT = (Double)(cell.getNumericCellValue());
              if(levelT < 30)
              {
                  level = level + levelT;
                  stats++;
              }

              level = level / stats;
              //sim.napoLevels.add(level);
              //System.out.println (level);

              sim.dailyNapoLevel.put((Calendar)calendar, level);
           }

        }
        catch(FileNotFoundException e)
            {
               System.out.println(e);
            }
        catch(IOException e)
            {
               System.out.println(e);
            }
        catch(InvalidFormatException e)
            {
               System.out.println(e);
            }

       //int len = readed_xlsx.size();

       /*Collection col = sim.dailyNapoLevel.values();
       for (Iterator iterator = col.iterator(); iterator.hasNext();) {
           double dd = (Double) iterator.next();
           System.out.println(dd);


       }

       Set set = sim.dailyNapoLevel.keySet();
       for (Iterator iterator = set.iterator(); iterator.hasNext();) {
           Calendar dd = (Calendar) iterator.next();
         System.out.println(dd.getYear());
         System.out.println(dd.getMonth());
         if(dd.getYear() == 2011)
         System.out.println(dd);
         //if(dd == sim.endDate)System.out.println("Quiiiiiiiiiiiiii");
         //break;
       }
       System.exit(0);
       */


       //System.out.println("start Date:  " +  sim.startDate);
       //System.out.println("river level size: " +  sim.dailyNapoLevel.size());
       //double riverLevel = sim.dailyNapoLevel.get((Calendar)sim.startDate);
       //System.exit(0);

   }



   //determines the min and man river level ----------------
   //And resize level data to the interval : [0, 1] 
   public void transformData(AmaSim sim)
   {

       String date = "";

       sim.maxNapoLevel = -10000.0; 
       sim.minNapoLevel = 10000.0; 
       double lev = 0.0;
       int stats = 0;

       HashMap<Calendar, Double> tmp = new HashMap<Calendar, Double>();

       Iterator it = sim.dailyNapoLevel.entrySet().iterator();

       while (it.hasNext()) 
       { 
           Map.Entry pairs = (Map.Entry)it.next();

           lev = (Double)pairs.getValue();
           //System.out.println("date: " +  (String)pairs.getKey() +"lev: " + lev);
           if(lev >= sim.maxNapoLevel)
           {
               sim.maxNapoLevel = lev;
              // System.out.println("date: " +  (String)pairs.getKey() +"lev: " + lev);
           }
           if(lev <= sim.minNapoLevel)sim.minNapoLevel = lev;
           //System.out.println("date: " + (String)pairs.getKey());
           stats++;
       }

       System.out.println("Extreme Napo River levels. Max: " + sim.maxNapoLevel + " Min: " + sim.minNapoLevel);
       //System.out.println("Num of entries: " + stats);

       /*
       it = sim.dailyNapoLevel.entrySet().iterator();
       while (it.hasNext()) 
       { 
           Map.Entry pairs = (Map.Entry)it.next();

           lev = (Double)pairs.getValue();

           lev = (lev - sim.minNapoLevel)/(sim.maxNapoLevel - sim.minNapoLevel);

           
           tmp.put((Calendar)pairs.getKey(), lev);

       }

       sim.dailyNapoLevel = tmp;
       */ 

       //it = sim.dailyNapoLevel.entrySet().iterator();
       //it = tmp.entrySet().iterator();
       //while (it.hasNext()) 
       //{ 
       //    Map.Entry pairs = (Map.Entry)it.next();

       //    lev = (Double)pairs.getValue();
       //    System.out.println("Lev: " + lev);
       //    System.out.println("date: " + (String)pairs.getKey());
       //}
       //System.exit(0);


   }






}//End of file
