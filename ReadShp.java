/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.planargraph.Node;
import com.vividsolutions.jts.geom.Geometry;

import java.io.*;
import java.util.*;
import java.util.ArrayList;
import sim.field.grid.*;

import java.net.URL;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.io.geo.*;
import sim.util.*;

import sim.field.geo.GeomVectorField;
import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;
import sim.util.geo.GeomPlanarGraph;
import sim.util.geo.MasonGeometry;
import sim.util.geo.AttributeValue;
import sim.util.geo.GeometryUtilities;

public class ReadShp  
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    public boolean first = true;

    AmaSim sim = null;

    String dirCar = "" ;

    public ReadShp(SimState state)
    {
       sim = (AmaSim)state;

       if(sim.singleRun) dirCar = "input_data/" + sim.testCommunityName + "/";
       else dirCar = "input_data/" + sim.community.name  + "/";
    }

    /** How many agents in the simulation */ 
	//public int numAgents = 10;

    // where all the agents live
    //public GeomVectorField agents = new GeomVectorField(WIDTH, HEIGHT);

    // The Importer is responsible for reading in the GIS files.  If you have installed either 
    // GeoTools and/or OGR on your system, you can use those importers.  The ShapeFileImporter does 
    // not rely on external libraries.  
    //OGRImporter importer = new OGRImporter();
    //GeoToolsImporter importer = new GeoToolsImporter();
    //public ShapeFileImporter importer = new ShapeFileImporter();


    // Stores the vias network connections.  We represent the vias as a PlanarGraph, which allows 
    // easy selection of new waypoints for the agents.  

    //public int getNumAgents() { return numAgents; } 
    //public void setNumAgents(int n) { if (n > 0) numAgents = n; } 

    /** Add agents to the simulation and to the agent GeomVectorField.  Note that each agent does not have
     * any attributes.   */
    //void addAgents()
    //{
    //    for (int i = 0; i < numAgents; i++)
    //        {
    //            Agent a = new Agent();
    //            MasonGeometry mg = new MasonGeometry(a.getGeometry()); 
    //            mg.isMovable = true; 
    //            agents.addGeometry(mg);
    //            a.start(this);
    //            schedule.scheduleRepeating(a);
    //            
    //            // we can set the userData field of any MasonGeometry.  If the userData is inspectable, 
    //            // then the inspector will show this information 
    //            //if (i == 10) 
    //        }
    //}

    //====================================================
    public void read(SimState state)
    {
        String file = "";
        String dir  = "";

                // this Bag lets us only display certain fields in the Inspector, the non-masked fields
                // are not associated with the object at all
                //Bag masked = new Bag();
                //masked.add("NAME");
                //masked.add("FLOORS");
                //masked.add("ADDR_NUM");

//                System.out.println(System.getProperty("user.dir"));
                // We want to save the MBR so that we can ensure that all GeomFields
                // cover identical area.
                //System.out.println("reading houses layer --------");

                dir = dirCar;

                if(sim.singleRun) file = dir + "households" + sim.testCommunityName  + ".shp";
                else file = dir + "households" + sim.community.name  + ".shp";

                //ShapeFileEditor sfe = new ShapeFileEditor();
                //sfe.setup(state, file);
                System.out.println("Reading shp file: " + file);
                //System.exit(0);

    if(first)
    {
    System.out.println(" ");
    System.out.println("reading household layer ------------------------");
    }
                URL houseGeometry = AmaSim.class.getResource(file);
                //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
                //System.out.println("fdds");
                try{
                      ShapeFileImporter.read(houseGeometry, sim.homes);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }
                //System.exit(0);

                Envelope MBR = sim.homes.getMBR();

    if(first)
    {
    System.out.println(" ");
    System.out.println("reading social places layer --------------------");

    }

                dir = dirCar;

                file = dir + "socialPlaces.shp";
                System.out.println(file);

                URL socialPlacesGeometry = AmaSim.class.getResource(file);
                //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
                //System.out.println("fdds");
                try{
                      ShapeFileImporter.read(socialPlacesGeometry, sim.socialPlaces);
                      //MBR.expandToInclude(sim.socialPlaces.getMBR());
                   }catch (Exception e){//Catch exception if any
                       System.err.println("Error: " + e.getMessage());
                       System.out.println("File: " + file);
                     //System.exit(0);
                }


               //System.exit(0);

    if(first && (sim.numExcludedHotspots != 0))
    {
        System.out.println(" ");
        System.out.println("reading mov high risk clusters -----------------");

        if(sim.highRiskWhat == 2)
        {
            dir = dirCar;

            file = dir + "highRiskComm0HouseholdsProjected.shp";
            System.out.println(file);

            URL highRiskGeometry = AmaSim.class.getResource(file);
            //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
            //System.out.println("fdds");
            try{
                ShapeFileImporter.read(highRiskGeometry, sim.highRisk);
                //MBR.expandToInclude(sim.highRisk.getMBR());
            }catch (Exception e){//Catch exception if any
                System.err.println("Error: " + e.getMessage());
                System.out.println("File: " + file);
                System.exit(0);
            }
        
        }
        else if(sim.highRiskWhat == 1)
        {
            dir = dirCar;

            file = dir + "highRiskMovementProjected.shp";
            System.out.println(file);

            URL highRiskGeometry = AmaSim.class.getResource(file);
            //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
            //System.out.println("fdds");
            try{
                ShapeFileImporter.read(highRiskGeometry, sim.highRisk);
                //MBR.expandToInclude(sim.highRisk.getMBR());
            }catch (Exception e){//Catch exception if any
                System.err.println("Error: " + e.getMessage());
                System.out.println("File: " + file);
                System.exit(0);
            }
        
        }


        //System.exit(0);

    }





    if(first)
    {
    System.out.println(" ");
    System.out.println("reading farms         layer --------------------");

    }

                dir = dirCar;

                file = dir + "farms.shp";
                System.out.println(file);

                URL farmsGeometry = AmaSim.class.getResource(file);
                //ShapeFileImporter.read(houseGeometry, sim.homes, masked);
                //System.out.println("fdds");
                try{
                      ShapeFileImporter.read(farmsGeometry, sim.farms);
                      //MBR.expandToInclude(sim.farms.getMBR());
                   }catch (Exception e){//Catch exception if any
                       System.err.println("Error: " + e.getMessage());
                       System.out.println("File: " + file);
                     //System.exit(0);
                }


               //System.exit(0);


    if(first)
    {
    System.out.println("reading low water layer  -----------------------");
    }

                dir = dirCar;

                if(sim.singleRun) file = dir + "lowWaterM" + sim.testCommunityName  + ".shp";
                else file = dir + "lowWaterM" + sim.community.name  + ".shp";

                URL low_waterGeometry = AmaSim.class.getResource(file);
                try{
                      ShapeFileImporter.read(low_waterGeometry, sim.low_water);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }

                MBR.expandToInclude(sim.low_water.getMBR());


    if(first)
    {
    System.out.println("reading the rio internal part-------------------");
    }

                dir = dirCar;

                if(sim.singleRun) file = dir + "riverExcludedLowWaterM" + sim.testCommunityName  + ".shp";
                else file = dir + "riverExcludedLowWaterM" + sim.community.name  + ".shp";

                URL rioCutted_10mBufferGeometry = AmaSim.class.getResource(file);
                try{
                      ShapeFileImporter.read(rioCutted_10mBufferGeometry, sim.rioCutted_10mBuffer);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }

                MBR.expandToInclude(sim.rioCutted_10mBuffer.getMBR());



    if(first)
    {
    System.out.println("reading high water layer -----------------------");
    }

                dir = dirCar;
                //file =  dir + "riverHighLevel.shp";

                if(!sim.hybridExample)
                {
                    if(sim.singleRun) file = dir + "highWaterM" + sim.testCommunityName  + ".shp";
                    else file = dir + "highWaterM" + sim.community.name  + ".shp";
                }
                else
                {
                    if(sim.singleRun) file = dir + "exhighWaterM" + sim.testCommunityName  + ".shp";
                    else file = dir + "exhighWaterM" + sim.community.name  + ".shp";
                }

                URL high_waterGeometry = AmaSim.class.getResource(file);
                try{
                      ShapeFileImporter.read(high_waterGeometry, sim.high_water);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }

                MBR.expandToInclude(sim.high_water.getMBR());
    if(sim.humanLocalMovement == 3)
    {
        System.out.println("reading Roads layer  ---------------------------");


        dir = dirCar;
        //file =  dir + "riverHighLevel.shp";

        if(sim.singleRun) file = dir + "roadsM" + sim.testCommunityName  + ".shp";
        else file = dir + "roadMs" + sim.community.name  + ".shp";

        URL roadsGeometry = AmaSim.class.getResource(file);
        try{
            ShapeFileImporter.read(roadsGeometry, sim.roads);
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
	    System.out.println("File: " + file);
            System.exit(0);
        }

        MBR.expandToInclude(sim.roads.getMBR());
    }


    if(first)
    {
    System.out.println("reading Study area Boundaries  -----------------");
    }

                dir = dirCar;
                //file =  dir + "riverHighLevel.shp";

                if(sim.singleRun) file = dir + "StudyArea" + sim.testCommunityName  + ".shp";
                else file = dir + "StudyArea" + sim.community.name  + ".shp";

                URL study_areaGeometry = AmaSim.class.getResource(file);
                try{
                      ShapeFileImporter.read(study_areaGeometry, sim.study_area);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }

                MBR.expandToInclude(sim.study_area.getMBR());




    if(first)
    {
    System.out.println("reading elevation layer-------------------------");
    }
                //dir = dirCar + "/elevation/";

                if(sim.singleRun) file = dir + "elevation90m" + sim.testCommunityName  + ".shp";
                else file = dir + "elevation90m" + sim.community.name  + ".shp";

                URL elevationGeometry = AmaSim.class.getResource(file);
                try{
                      ShapeFileImporter.read(elevationGeometry, sim.elevation);
                   }catch (Exception e){//Catch exception if any
                     System.err.println("Error: " + e.getMessage());
                     System.out.println("File: " + file);
                     System.exit(0);
                }


                //double width = MBR.getWidth();
                //double height = MBR.getHeight();

                //width = width * 0.08;
                //MBR.expandBy(width);


                sim.low_water.setMBR(MBR);
                sim.rioCutted_10mBuffer.setMBR(MBR);
                sim.high_water.setMBR(MBR);
                sim.roads.setMBR(MBR);
                //sim.elevation.setMBR(MBR);
                sim.homes.setMBR(MBR);
                sim.globalMBR = MBR;
                //System.out.println(MBR.getWidth()  + " " + MBR.getHeight());

                if(!sim.elevation.getMBR().contains(MBR))
                {
                  System.out.println("The elevation shp extension must to contains");
                  System.out.println("The extension of all the other input shps");
                  System.exit(0);
                
                }


                //I try .........................
                //Bag bAlt = sim.elevation.getGeometries();

                //System.out.println("Num Ofertas: " + bofertas.numObjs);
                //for (int kk = 0; kk < bAlt.numObjs; kk++)
                //{
                //   MasonGeometry mg = (MasonGeometry)bAlt.get(kk);
                //   int pippo = mg.getIntegerAttribute("grid_code");
                //   System.out.println(pippo);
                //}
                //System.out.println("Number of Accommodation Offers: " + num_offers);

                //another try .................................
    	        //int num_homes = sim.homes.getGeometries().numObjs;

                //for (int ii = 0; ii < num_homes; ii++)
                //{
                //   MasonGeometry MGhome = (MasonGeometry)sim.homes.getGeometries().objs[ii];
                //   Point home = (Point)MGhome.getGeometry();
                //   System.out.println(home.toText());
                //}
                //System.exit(0);
                //I finish tring     .........................

    }


    //====================================================
    public void gridSetup(SimState state)
    {
        AmaSim sim = (AmaSim)state;

        System.out.println(" ");
        System.out.println("================================================");
        System.out.println("Grids setUp     --------------------------------");
        System.out.println(" ");

        read(state);
        first = false;

        sim.globalMBRWidth  = sim.globalMBR.getWidth() * sim.cutFactor;
        sim.globalMBRHeight = sim.globalMBR.getHeight() * sim.cutFactor;

        //System.out.println("GlobalMBRWidth: " + sim.globalMBR.getWidth());
        //System.out.println("GlobalMBRHeight: " + sim.globalMBR.getHeight());

        sim.globalMBRMinX   = sim.globalMBR.getMinX();
        sim.globalMBRMinY   = sim.globalMBR.getMinY();
        double diff = 0.5 * (sim.globalMBR.getWidth() - sim.globalMBRWidth);
        //System.out.println("Diff: " + diff);
        sim.globalMBRMinX   =  sim.globalMBRMinX + diff;
        diff = 0.5 * (sim.globalMBR.getHeight() - sim.globalMBRHeight);
        //System.out.println("Diff: " + diff);
        sim.globalMBRMinY   =  sim.globalMBRMinY + diff;


        double dnCellx = sim.globalMBRWidth / sim.mosquitoCellSize;
        double dnCelly = sim.globalMBRHeight / sim.mosquitoCellSize;

        int nCellx = (int)(dnCellx);
        int nCelly = (int)(dnCelly);


        //System.out.println("Mos cell size " + sim.mosquitoCellSize);
        //System.out.println(dnCellx  + " " + dnCelly);
        //System.out.println(nCellx  + " " + nCelly);
        //System.exit(0);

        sim.mosquitoGridWidth  = nCellx;
        sim.mosquitoGridHeight = nCelly;

        System.out.println("Sim mosquitos grid: " + nCellx  + " x " + nCelly + " cells");

        dnCellx = sim.globalMBRWidth / sim.geoCellSize;
        dnCelly = sim.globalMBRHeight / sim.geoCellSize;

        nCellx = (int)(dnCellx);
        nCelly = (int)(dnCelly);

        //System.out.println(dnCellx  + " " + dnCelly);
        //System.out.println(nCellx  + " " + nCelly);
        //System.exit(0);

        sim.geoGridWidth  = nCellx;
        sim.geoGridHeight = nCelly;
        System.out.println(" ");
        System.out.println("Sim geo grid: " + nCellx  + " x " + nCelly + " cells");

        boolean ok = false;
        ok = borderCorrections(state);

        System.out.println("Sim geo grid after corrections: " + sim.geoGridWidth  + " x " + sim.geoGridHeight + " cells");

        sim.mosquitoGrid  = new SparseGrid2D(sim.mosquitoGridWidth, sim.mosquitoGridHeight);
        sim.humanGrid  = new SparseGrid2D(sim.geoGridWidth, sim.geoGridHeight);

        sim.householdGrid = new SparseGrid2D(sim.geoGridWidth, sim.geoGridHeight);
        sim.socialPlacesGrid = new SparseGrid2D(sim.geoGridWidth, sim.geoGridHeight);
        sim.farmGrid = new SparseGrid2D(sim.geoGridWidth, sim.geoGridHeight);
        sim.geoGrid     = new ObjectGrid2D(sim.geoGridWidth, sim.geoGridHeight);
        sim.elevationGrid = new ObjectGrid2D(sim.geoGridWidth, sim.geoGridHeight);
        sim.pixelMosquitosGrid = new ObjectGrid2D(sim.geoGridWidth, sim.geoGridHeight);

        sim.homes         = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.low_water     = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.high_water    = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.study_area  = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.elevation     = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.rioCutted_10mBuffer  = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.farms  = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);
        sim.roads         = new GeomVectorField(sim.geoGridWidth,sim.geoGridHeight);


        read(state);

        checkHomes(state);

        setElevationGrid(state);
        setElevationLines(state);

        //System.exit(0);

    }

    //====================================================
    public void checkHomes(SimState state)
    {

        System.out.println(" ");
        System.out.println("Checking homes positions .......................");
        final AmaSim sim = (AmaSim)state;
        
        int num_homes = sim.homes.getGeometries().numObjs;
        boolean out = false;

        double extXGrid = sim.geoGridWidth  * sim.mosquitoCellSize;
        double extYGrid = sim.geoGridHeight * sim.mosquitoCellSize;

        int stats = 0;
        
        for (int ii = 0; ii < num_homes; ii++)
        {
           MasonGeometry MGhome = (MasonGeometry)sim.homes.getGeometries().objs[ii];
           Point pHome = (Point)MGhome.getGeometry();
           double pX = pHome.getX();
           double pY = pHome.getY();

           //System.out.println(pX  + " " + pY);
           //System.out.println((pX - sim.globalMBRMinX)  + " " + (pY - sim.globalMBRMinY));
           //System.out.println(extYGrid  + " " + extYGrid);

           if(extXGrid < (pX - sim.globalMBRMinX) || extYGrid < (pY - sim.globalMBRMinY)) 
           {
               System.out.println("First There is a home out of the grid!!!!");
               stats++;
               out = true;
               //break;
           }

           if((pX - sim.globalMBRMinX) < 0 || (pY - sim.globalMBRMinY) < 0) 
           {
               System.out.println("Second There is a home out of the grid!!!!");
               out = true;
               //break;
           }
 
 

        }
        System.out.println("Num homes: " + num_homes + "  house out of the grid: " + stats);

        if(out)
        {
           System.out.println("There is a home out of the grid!!!!");
           System.out.println("Expand the your geo cover please");
           System.out.println("In this case is better to have a water cover larger than");
           System.out.println("the homes cover");
           System.exit(0);
        }
        System.out.println("Homes positions inside the grid: Ok  ...........");
        //System.exit(0);

    }

    //====================================================
    public void setElevationGrid(SimState state)
    {

      System.out.println(" ");
      System.out.println("Configuring the elevation grid points  ---------");
      
      AmaSim sim = (AmaSim)state;

      double Width  = sim.elevation.getMBR().getWidth();
      double Height  = sim.elevation.getMBR().getHeight();

      int num_elev_points = sim.elevation.getGeometries().numObjs;

      //Get the min x and y coord in the elev cover
      double minX   = 2000000000;
      double minY   = 2000000000;
      for (int ii = 0; ii < num_elev_points; ii++)
      {
         MasonGeometry mgElev = (MasonGeometry)sim.elevation.getGeometries().objs[ii];
         Point point = (Point)mgElev.getGeometry();
         
         if(point.getX() <= minX)minX = point.getX(); 
         if(point.getY() <= minY)minY = point.getY(); 

      }


      //Get the number of point along the width------------------
      int NPointsHeight = 0;
      double x = minX;
      double scartoX = 10000.0;
      for (int ii = 0; ii < num_elev_points; ii++)
      {
         MasonGeometry mgElev = (MasonGeometry)sim.elevation.getGeometries().objs[ii];
         Point point = (Point)mgElev.getGeometry();
         //if(ii == 0){x = point.getX();}
         double dist = Math.sqrt((x - point.getX()) * (x - point.getX()));
         if(dist < 45.0)
         {
             NPointsHeight++;
             if(dist != 0 && dist < scartoX)scartoX = dist;
             //System.out.println(scartoX);
         }
         //System.out.println("Point x: " + x  + " " +  point.getX());
      }


      //Get the number of point along the height------------------
      int NPointsWidth = 0;
      double y = minY;
      double scartoY = 10000.0;
      for (int ii = 0; ii < num_elev_points; ii++)
      {
         MasonGeometry mgElev = (MasonGeometry)sim.elevation.getGeometries().objs[ii];
         Point point = (Point)mgElev.getGeometry();

         //Here a patch for this specific shp! change it if you 
         //change the shp....
         //int fid = mgElev.getIntegerAttribute("POINTID");
         //if(fid == 245564)
         //{
         //    minX = point.getX();
         //    minY = point.getY();
         //}

         //if(ii == 0){y = point.getY();}
         double dist = Math.sqrt((y - point.getY()) * (y - point.getY()));
         if(dist < 45.0)
         {
             NPointsWidth++;
             if(dist != 0 && dist < scartoY)scartoY = dist;
         }
      }

      System.out.println(" ");
      System.out.println("Width: " + Width);
      System.out.println("Height: " + Height);

      System.out.println(" ");
      System.out.println("Num input elevation points: " + num_elev_points);
      //System.out.println(" ");
      System.out.println("Num of elevation points along x: " + NPointsWidth);
      System.out.println("Num of elevation points along y: " + NPointsHeight);

      System.out.println("scartoX: " + scartoX);
      System.out.println("scartoY: " + scartoY);
      //System.exit(0);


      double cellWidth = Width/(NPointsWidth - 1);
      double cellHeight = Height/(NPointsHeight - 1);
      //cellWidth = cellHeight;

      System.out.println(" ");
      System.out.println("CellWidth: " + cellWidth);
      System.out.println("CellHeight: " + cellHeight);


      System.out.println(" ");
      System.out.println("minX: " + minX);
      System.out.println("minY: " + minY);


      double deltaX = cellWidth/(NPointsWidth);
      double deltaY = cellHeight/(NPointsHeight);
      sim.elevationGrid = new ObjectGrid2D(NPointsWidth, NPointsHeight);
      //System.exit(0);


      //puts the points on the grid--------------------------------
      int stats = 0;
      double sx = 0.0;
      double sy = 0.0;
      for (int i = 0; i < NPointsWidth; i++)
      {
         sx = sx + scartoX;
         x = minX + i * (cellWidth);
         sy = 0;
         for (int j = 0; j < NPointsHeight; j++)
         {
            sy = sy + scartoY;
            y = minY + j * (cellHeight);

out:
            for (int ii = 0; ii < num_elev_points; ii++)
            {
               MasonGeometry mgElev = (MasonGeometry)sim.elevation.getGeometries().objs[ii];
               Point point = (Point)mgElev.getGeometry();

               //System.out.println("----------------------------");
               //System.out.println("i: " + i + " j: " + j);
               //System.out.println("MinX: " + minX + " Miny: " + minY);
               //System.out.println("X: " + x + " y: " + y);
               //System.out.println("PointX: " + point.getX() + " Pointy: " + point.getY());
               //System.out.println("deltaX: " + deltaX + " deltaY: " + deltaY);

               if(
                      point.getX() <= (x + 44)
                   && point.getX() >= (x - 44)
                   && point.getY() <= (y + 44)
                   && point.getY() >= (y - 44)
                       )
               {
                   sim.elevationGrid.set(i, j, mgElev);
                   stats++;
                   //System.out.println("dentro----------------------");
                   //System.out.println("  " + i + "  " + j);
                   //System.out.println("PointX: " + point.getX() + " Pointy: " + point.getY());
                   //System.out.println("  " + x + "  " + y);
                   //System.out.println("Deltax:  " + (x - point.getX()) + " Deltay:  " + (y-point.getY()));

                   //System.out.println("ii " + ii);
                   //System.out.println("stats " + stats);
                   //System.exit(0);
                   break out;
               }
               //System.out.println("Fuori  ---------------------");
               //System.exit(0);
            }
            //System.exit(0);
         }
         //System.exit(0);
      }


      System.out.println(" ");
      if(stats == num_elev_points)
      {
         System.out.println("Num of placed elevation points: All ............");
      }
      else
      {
         System.out.println("Num of elevation placed points: " + stats);
         System.out.println("Tot num of elevation points: " + num_elev_points);
         System.exit(0);
      }
      //System.exit(0);
        
    }


    //====================================================
    public void setElevationLines(SimState state)
    {



    }


    //====================================================
    public boolean borderCorrections(SimState state)
    {
       AmaSim sim = (AmaSim)state;


       double wm = sim.mosquitoGridWidth * sim.mosquitoCellSize;
       double wg = sim.geoGridWidth * sim.geoCellSize;

       if((wg - wm) > sim.geoCellSize * 0.5)
       {
         System.out.println("Adjusting geoGrid Width");
         sim.geoGridWidth--;
         sim.geoGridWidth--;

       }

       double hm = sim.mosquitoGridHeight * sim.mosquitoCellSize;
       double hg = sim.geoGridHeight * sim.geoCellSize;

       if((hg - hm) > sim.geoCellSize * 0.5)
       {
         System.out.println("Adjusting geoGrid Height");
         sim.geoGridHeight--;
         sim.geoGridHeight--;
       }

       CoverPixel cp = null;
       Bag cps = sim.geoGrid.elements();
       int num_pixels = cps.size();

       for(int c = 0; c < num_pixels; c++)
       {
           cp = ((CoverPixel)cps.objs[c]);
           int i = cp.xcor;
           int j = cp.ycor;

           if(j >= sim.geoGridHeight || i >= sim.geoGridWidth)
           {
               cp.stopper.stop();         
           }
       }
 
       //System.exit(0);

       return true;

    }
}
