/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.HashMap;

public class ReadXlsMeteo
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    Bag readed_xlsx = new Bag();
    public Bag getReaded_xlsx() { return readed_xlsx; }

    //====================================================
    public ReadXlsMeteo(SimState state)
    {
 
    }

    //====================================================
    public void read(SimState state)
    {
      AmaSim sim = (AmaSim)state;
      System.out.println("================================================");
      System.out.println ("Reading the Metereological Data ...............");
      System.out.println(" ");

    try{
           Workbook workbook = WorkbookFactory.create(new FileInputStream("sim/app/AmaSim/input_data/meteoSantaClotilde.xls") );

           int date_index = 0;
           int rain_index0 = 8;
           int rain_index1 = 9;
           int maxT_index = 0;
           int minT_index = 1;
           
           Sheet sheet = workbook.getSheetAt(0);
           Row row;
           Cell cell;
           String delims = "[ ]+";
           double val;
           int    int_val;
           double cat;
           String string = "";
           boolean write = false;
           MeteoData md = new MeteoData(state);
           double rain0 = 0.0;
           double rain1 = 0.0;
           Date date = null;
           
           int numRows = sheet.getPhysicalNumberOfRows();
           //System.out.println ("numRows " + numRows);

           for(int i = 1; i < numRows; i = i + 2)
           { 
              md = new MeteoData(state);
              write = false;

              row = sheet.getRow(i);

             //int_val = cell.getCellType();
              //System.out.println (int_val);

              cell = row.getCell(date_index);
              //string = Integer.toString((int)cell.getNumericCellValue());
              date = (Date)(cell.getDateCellValue());
              Calendar calendar = Calendar.getInstance();
              calendar.setTime(date);

              md.setCalendar(calendar);
              //System.out.println (string);
              //System.exit(0);


              row = sheet.getRow(i + 1);

              cell = row.getCell(rain_index0);
              rain0 = (Double)(cell.getNumericCellValue());

              cell = row.getCell(rain_index1);
              if(cell == null){rain1 = rain0;}
              else rain1 = (Double)(cell.getNumericCellValue());


              if(rain0 < 45 && rain0 > 0
                      &&  rain1 < 45 && rain1 > 0){cat = (rain0 + rain1)/2;}
              else if(rain0 < 45 && rain0 > 0
                      &&  (rain1 > 45 || rain1 < 0)){cat = rain0;}
              else if(rain1 < 45 && rain1 > 0
                      &&  (rain0 > 45 || rain0 < 0)){cat = rain1;}
              else cat = 0.0;

              md.setRain(cat);

              cell = row.getCell(maxT_index);
              cat = (Double)(cell.getNumericCellValue());
              md.setMaxT(cat);

              cell = row.getCell(minT_index);
              cat = (Double)(cell.getNumericCellValue());
              md.setMinT(cat);

              sim.meteoData.add(md);
              //md.print();
              //System.exit(0);
           }

        }
        catch(FileNotFoundException e)
            {
               System.out.println(e);
            }
        catch(IOException e)
            {
               System.out.println(e);
            }
        catch(InvalidFormatException e)
            {
               System.out.println(e);
            }

       //int len = readed_xlsx.size();
       //System.out.println("len  " +  len);
       //System.exit(0);
    }


    //====================================================
    public void checkData(SimState state)
    {
       AmaSim sim = (AmaSim)state;
       int len = sim.meteoData.size();

       List<MeteoData> meteoData_tmp = new ArrayList<MeteoData>();
       MeteoData md = new MeteoData(state);
       MeteoData md_tmp = new MeteoData(state);
       double rain  = 0.0;
       double maxT  = 0.0;
       double minT  = 0.0;
       Date date = null;
       Calendar calendar = null;
       boolean passed = false;
       int avg_int;
       double avg = 10000000;

       //meteoData_tmp = sim.meteoData;

       for(int i = 0; i < len; i++)
       { 

          md   = sim.meteoData.get(i);  
          calendar = md.getCalendar();
          rain = md.getRain();
          maxT = md.getMaxT();
          minT = md.getMinT();

          md_tmp = new MeteoData(state);
          md_tmp.setCalendar(md.getCalendar());
          md_tmp.setMaxT(md.getMaxT());
          md_tmp.setRain(md.getRain());
          md_tmp.setMinT(md.getMinT());

          if(rain == -9999)
          {
              avg = 10000000;
              avg_int = 1;

              while(avg == 10000000)
              {
                 avg = correct(state, i, "rain", avg_int);
                 avg_int++;
              }
              md_tmp.setRain(avg);
          }

          if(maxT == -9999)
          {
              avg = 10000000;
              avg_int = 1;

              while(avg == 10000000)
              {
                 avg = correct(state, i, "maxT", avg_int);
                 avg_int++;
              }
              md_tmp.setMaxT(avg);
          }

          if(minT == -9999)
          {
              avg = 10000000;
              avg_int = 1;

              while(avg == 10000000)
              {
                 avg = correct(state, i, "minT", avg_int);
                 avg_int++;
              }
              md_tmp.setMinT(avg);
          }





          //if(maxT == -9999)md_tmp.setMaxT(correct(state, i, "maxT"));
          //if(minT == -9999)md_tmp.setMinT(correct(state, i, "minT"));

          meteoData_tmp.add(md_tmp);

          //System.out.println("d  " +  d);
          //System.out.println("string  " +  string);
          //System.exit(0);
       }

       sim.meteoData = meteoData_tmp;

       //for(int i = 0; i < len; i++)
       //{ 
       //   md   = sim.meteoData.get(i);  
       //   System.out.println(md.getDate()  + " -  " + md.getRain() + " -  " + md.getMaxT() + " -  "  + md.getMinT());

       //}


       //System.out.println("len meteo data: " + len);
       //System.exit(0);
       

    }



    //====================================================
    public double correct(SimState state, int m, String what, int avg_int)
    {

       AmaSim sim = (AmaSim)state;
       int len = sim.meteoData.size();

       MeteoData md = new MeteoData(state);
       double avg = 0.0;
       int stats = 0;

       for(int i = 0; i < len; i++)
       { 
          if( i >= m - avg_int && i <= m + avg_int )
          {
             md = sim.meteoData.get(i);  

             //System.out.println(i);

             if(what.equals("rain") && md.getRain() != -9999) 
             {
                avg = avg + md.getRain();
                stats++;
             }
             if(what.equals("minT") && md.getMinT() != -9999)
             {
                avg = avg + md.getMinT();

                stats++;
             }
             if(what.equals("maxT") && md.getMaxT() != -9999)
             {
                avg = avg + md.getMaxT();
                stats++;
             }

          }
       }

       if(stats != 0)
       {
          avg = avg / stats;
       }
       else
       {
         //System.out.println ("Warning " + (2*avg_int + 1) + " -9999 in series");
         //System.exit(0);
         return 10000000;
       }

       return avg;

    }

    //Rewrite data in meters and C degrees=================
    public void rescaleData(SimState state)
    {

       AmaSim sim = (AmaSim)state;
       int len = sim.meteoData.size();

       MeteoData md = new MeteoData(state);
       double avg = 0.0;
       int stats = 0;

       for(int i = 0; i < len; i++)
       { 
          md = sim.meteoData.get(i);  
          md.setRain(md.getRain()/10.0);
          md.setMaxT(md.getMaxT()/10.0);
          md.setMinT(md.getMinT()/10.0);
          sim.meteoData.set(i, md);

       }


    }



    //====================================================
    public void cutMeteoData(SimState state)
    {

       AmaSim sim = (AmaSim)state;
       List<MeteoData> meteoData_tmp = new ArrayList<MeteoData>();
       int len = sim.meteoData.size();
       boolean write = false;
       MeteoData md = new MeteoData(state);
       int stats = 0;

       sim.meteoDataComplete = sim.meteoData;

       for(int i = 0; i < len; i++)
       { 
           md = sim.meteoData.get(i);  
           if(md.getCalendar().equals(sim.startDate) )write = true;

           if(write)meteoData_tmp.add(md);

           if(md.getCalendar().equals(sim.endDate) )write   = false;
       }

       sim.meteoData = meteoData_tmp;

       //len = sim.meteoData.size();
       //for(int i = 0; i < len -9000; i++)
       //{ 
       //   md   = sim.meteoData.get(i);  
       //   System.out.println("Calendar: " + md.getCalendar().getTime()  + " Rain:  " + md.getRain() + " MaxT  " + md.getMaxT() + " MinT  "  + md.getMinT());
       //   if(md.getRain() > 5)stats++;

       //}

       //System.out.println(stats/ 24);
       //System.exit(0);

    }



    //Original meteo data where daily=====================
    //Here are transormed in huorly  =====================
    public void transformInHourly(SimState state)
    {
       AmaSim sim = (AmaSim)state;
       List<MeteoData> meteoData_tmp = new ArrayList<MeteoData>();
       int len = sim.meteoData.size();
       boolean write = false;
       MeteoData md = new MeteoData(state);


       for(int i = 0; i < len; i++)
       { 
           md = sim.meteoData.get(i);  

           for(int j = 0; j < 24; j++)
           { 
              meteoData_tmp.add(md);
           }

       }

       sim.meteoData = meteoData_tmp;
       //System.out.println("len meteo data: " + sim.meteoData.size());



    }


    //====================================================
    public void closeHoles(SimState state)
    {
       //In the original metereological data there are holes:
       //smoe dya or ent monnths are missing: here we close
       //therse holes
       AmaSim sim = (AmaSim)state;
       int len = sim.meteoData.size();

       List<MeteoData> meteoData_tmp = new ArrayList<MeteoData>();


       MeteoData md = new MeteoData(state);
       MeteoData md_tmp = new MeteoData(sim);

       Date date = null;
       Calendar calendar = null;
       boolean write = false;
       //meteoData_tmp = sim.meteoData;

       int firstYear = 0;
       int lastYear = 0;
      
       String start = "19960101";
       String end   = "19961231";
       List<Calendar> datesNorm = new ArrayList<Calendar>();
       List<Calendar> datesBis = new ArrayList<Calendar>();
       int stats = 0;


       for(int i = 0; i < len; i++)
       { 
          md   = sim.meteoData.get(i);  
          calendar = md.getCalendar();

          if(i == 0)firstYear = date.getYear();
          lastYear = date.getYear();


          if(date.equals(start)){write = true;}

          if(write){datesBis.add(calendar);}
          if(write && !calendar.equals("19960229")){datesNorm.add(calendar); stats++;}

          if(calendar.equals(end)){write = false;}
       }

       //Now I have the correct days sequence
       
       if(lastYear == 2014){lastYear--;}

       for(int i = firstYear; i <= lastYear; i++)
       { 
          if((i % 4) == 0)
          {
             //System.out.println(i);
             doClose(sim, i, datesBis);
          }
          else
          {
             doClose(sim, i, datesNorm);
          }
       }
 

       //sim.meteoData = meteoData_tmp;

       //check data
       /*

       //System.out.println("old len: " + len);

       len = sim.meteoData.size();

       //System.out.println("new len: " + len);

       List<String> tmp = new ArrayList<String>();
       List<String> tmp2 = new ArrayList<String>();
       

       for(int i = 0; i < len; i++)
       { 
          md   = sim.meteoData.get(i);  
          date = md.getCalendar();

          tmp.add(date);
          tmp2.add(date);
          //System.out.println(date + " " + i);
          //if(i>370)break;

       }

       for(int i = 0; i < len; i++)
       { 
           String date1 = tmp.get(i);

           for(int j = 0; j < len; j++)
           { 
              String date2 = tmp2.get(j);

              if(date1.equals(date2) && i != j)
              {
                 System.out.println(date1 + " " + date2 + " " + i + " " + j);
                 //System.exit(0);
              }

           }
       }
       System.exit(0);
       */


    }



    //====================================================
    public void doClose(AmaSim sim, Integer year, List<Calendar> ref)
    {

       int len = sim.meteoData.size();

       List<MeteoData> meteoData_tmp = new ArrayList<MeteoData>();

       MeteoData md = new MeteoData(sim);
       MeteoData md_tmp = new MeteoData(sim);

       boolean write = false;
       //meteoData_tmp = sim.meteoData;

       int firstYear = 0;
       int lastYear = 0;

       int refIndex = 0;
      
       String start = "19960101";
       String end   = "19961231";
       List<String> datesNorm = new ArrayList<String>();
       List<String> datesBis = new ArrayList<String>();
       int stats = 0;
       Calendar refDate = null;
       Calendar patch = null;
       Calendar calendar = null;

       //for(int i = 0; i < len;)
       int i = 0;
       while(i < len)    
       { 
          md   = sim.meteoData.get(i);  
          calendar = md.getCalendar();
          //System.out.println("-------------------");
          //System.out.println(i);
          //System.out.println(refIndex);

          if(year != calendar.YEAR)
          {
             meteoData_tmp.add(md);
             i++;
          }
          else
          {
              refDate = ref.get(refIndex);

              if(calendar.MONTH == (refDate.MONTH))
              {
                 meteoData_tmp.add(md);
                  stats++;
                 i++;
              }
              else
              {
                  md_tmp = new MeteoData(sim);
                  //Att modified for Sacha curaray
                  //patch = year + refDate.substring(4, 8);
                  //System.out.println(patch);
                  md_tmp.setCalendar(patch);
                  md_tmp.setMinT(md.getMinT());
                  md_tmp.setMaxT(md.getMaxT());
                  md_tmp.setRain(md.getRain());

                  meteoData_tmp.add(md_tmp);
                  stats++;

              }
              refIndex++;
          }
          
       }

       //System.out.println("stats " + stats);
       //System.exit(0);

       //System.out.println("Year " + year + " patched num: " + stats);
       //System.out.println("188 " + meteoData_tmp.get(188).getDate());
       sim.meteoData = meteoData_tmp;
       //System.out.println("188 " + sim.meteoData.get(188).getDate());
       //System.exit(0);
    }





}
