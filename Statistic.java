/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;
import java.text.DecimalFormat;
import java.util.*; 

import sim.field.grid.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class Statistic implements Steppable
{
   private static final long serialVersionUID = 1L;

   int count_remove = 0;
   public static int month = 0;
   public static int day = 0;
   public static int year = 0;
   public boolean write = false;


   public int month_old = 0;
   public int year_old = 0;

   public double weeklyAvgNumOfMosquitos = 0.0;

   public static double average_num_tourist_hotel = 0;

   public AmaSim sim;
   public SimState state;

   //====================================================
   public Statistic(AmaSim psim)
   {
       sim = psim;
       
       for(int i = 0; i < 4; i++)
       {
           sim.weeklyHIncidenceList.add(0.0);
           sim.weeklyHIncidenceListF.add(0.0);
           sim.weeklyHIncidenceListV.add(0.0);
       }

   }

   //====================================================
   public Stoppable stopper;
   public void step(SimState pstate)
   {
 
      //System.out.println ("Statistics starts");
      state = pstate;

      int nowi = sim.utils.getTime();
      long now = (long)sim.utils.getTime();

      int nowPrint = (int)state.schedule.getTime();  

      //Updrade weekly stats -----------------
      sim.weeklyHourStats++;

      MeteoData md = sim.meteoData.get(nowi);

      Calendar calendar = md.getCalendar();
      int year = calendar.YEAR;
      int month = calendar.MONTH;
      int day = calendar.DAY_OF_MONTH;
      String mDate = "";

      DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
      mDate = format.format(calendar.getTime());

      //System.out.println (calendar);
      //System.out.println (calendar.MONTH);
      //System.out.println (mDate);
      //System.exit(0);

      if(month != month_old && !sim.equilibration)
      {
         month_old = month; 
         

         sim.monthlyMalariaCases = 0.0;
         sim.monthlyMalariaCasesFalciparum = 0.0;
         sim.monthlyMalariaCasesVivax = 0.0;

      }

      if(year != year_old && !sim.equilibration)
      {
         year_old = year; 
         

         sim.yearlyMalariaCases = 0.0;
         sim.yearlyMalariaCasesFalciparum = 0.0;
         sim.yearlyMalariaCasesVivax = 0.0;
      }

      double avgTemp = md.getMinT() + md.getMaxT();
      sim.weeklyAvgTemp = sim.weeklyAvgTemp + avgTemp/2;

      sim.weeklyAvgFractInfectiousMosquitos = sim.weeklyAvgFractInfectiousMosquitos  + getFractInfectiousMosquitos();

      //Updrade weekly stats ended -----------

      if(nowPrint % sim.n_step_stats == 0)
      {
          print_output();
      }

      if(sim.upgradeCharts != 0 && (now % sim.upgradeCharts == 0) )
      {
          writeSeries();
      }

      if(nowPrint % 24 == 0)
      {
         double rain = md.getRain();
         sim.weeklyNumRainDays = sim.weeklyNumRainDays + rain;
 
         //Bites to humans------------------------
         sim.avgNumDailyBitePerHuman = sim.avgNumDailyBitePerHuman + sim.totNumDailyBites /sim.human_bag.size();

         sim.weeklyAvgNumBitesPerHuman = sim.weeklyAvgNumBitesPerHuman + sim.totNumDailyBites /sim.human_bag.size();

         //System.out.println ("+++++++++++++++++++++++++++++++++");
         //System.out.println ("Tot Num Daily Bites on humans = " + sim.totNumDailyBites);
         //System.out.println ("+++++++++++++++++++++++++++++++++");


         //Bites to animals ----------------------

         if(
                 (sim.totNumDailyBitesAnimals + sim.totNumDailyBites) > 0
           )
         {
             sim.avgDailyFractionBitesOnAnimals = sim.avgDailyFractionBitesOnAnimals + sim.totNumDailyBitesAnimals / ( sim.totNumDailyBitesAnimals + sim.totNumDailyBites);

             sim.weeklyAvgFractBitesOnAnimals = sim.weeklyAvgFractBitesOnAnimals + sim.totNumDailyBitesAnimals / ( sim.totNumDailyBitesAnimals + sim.totNumDailyBites);
         }
         else
         {
             sim.avgDailyFractionBitesOnAnimals = sim.avgDailyFractionBitesOnAnimals;

             sim.weeklyAvgFractBitesOnAnimals = sim.weeklyAvgFractBitesOnAnimals;
         }

         //System.out.println ("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
         //System.out.println (sim.avgDailyFractionBitesOnAnimals);
         //System.out.println (sim.weeklyAvgFractBitesOnAnimals);
         //System.out.println ("Animals: " + sim.totNumDailyBitesAnimals);
         //System.out.println ("Humans: " + sim.totNumDailyBites);
         //System.out.println (sim.totNumDailyBitesAnimals + sim.totNumDailyBites);
         //System.out.println (sim.avgDailyFractionBitesOnAnimals);
         //System.out.println("Average fraction of bites on animals per Day: " + sim.avgDailyFractionBitesOnAnimals/(sim.numDays ));

         sim.totNumDailyBites = 0;
         sim.totNumDailyBitesAnimals = 0;

         //System.exit(0);
         
         sim.totNumOfmosquitos = sim.totNumOfmosquitos  + sim.mosquito_bag.size();
         sim.totNumOfmosquitosCount++;

         sim.numDays++;
         sim.weeklyDayStats++;
         //System.out.println (sim.weeklyDayStats);

      }

      if(nowPrint % 720 == 0)
      {

         sim.monthlyMalariaIncidence[sim.month] = sim.malariaMonthlyCases/sim.human_bag.size();
         sim.malariaIncidenceSerie.add(sim.malariaMonthlyCases/sim.human_bag.size());
         sim.malariaMonthlyCases = 0;

         sim.month++;
         if(sim.month == 12 )sim.month = 0;

      }

      //Writes the weekly statistics --------------------------
      if(nowPrint % 168 == 0)
      {
          fractHumanAsympto( );

          sim.weeklyDataCounter++;

          sim.HmonthlyIncidence = sim.monthlyMalariaCases / sim.human_bag.size();
          sim.HmonthlyIncidenceFalciparum = sim.monthlyMalariaCasesFalciparum / sim.human_bag.size();
          sim.HmonthlyIncidenceVivax = sim.monthlyMalariaCasesVivax / sim.human_bag.size();

          sim.HyearlyIncidence = sim.yearlyMalariaCases / sim.human_bag.size();
          sim.HyearlyIncidenceFalciparum = sim.yearlyMalariaCasesFalciparum / sim.human_bag.size();
          sim.HyearlyIncidenceVivax = sim.yearlyMalariaCasesVivax / sim.human_bag.size();
   

          double riverLevel = sim.dailyNapoLevel.get(calendar);
          //double malariaBautista = sim.dailyMalariaPrevalence.get(calendar);
          double malariaObserved = getMalariaObserved(calendar);

          System.out.println(" ");
          System.out.println("--------------------------------- ");

          sim.HmonthlyIncidence = 0.0;
          sim.HmonthlyIncidenceVivax = 0.0;
          sim.HmonthlyIncidenceFalciparum = 0.0;

          System.out.println("sim.Weekly malaria cases " + sim.weeklyMalariaCases);

          sim.weeklyHIncidenceList.add(sim.weeklyMalariaCases);
          sim.weeklyHIncidenceListV.add(sim.weeklyMalariaCasesVivax);
          sim.weeklyHIncidenceListF.add(sim.weeklyMalariaCasesFalciparum);

          sim.weeklyHIncidenceList.remove(0);
          sim.weeklyHIncidenceListV.remove(0);
          sim.weeklyHIncidenceListF.remove(0);

          sim.weeklyMalariaCases = 0.0;
          sim.weeklyMalariaCasesFalciparum = 0.0;
          sim.weeklyMalariaCasesVivax = 0.0;

          for(int i = 0; i < 4; i++)
          {
             sim.HmonthlyIncidence = sim.HmonthlyIncidence + sim.weeklyHIncidenceList.get(i); 
             sim.HmonthlyIncidenceVivax = sim.HmonthlyIncidenceVivax + sim.weeklyHIncidenceListV.get(i); 
             sim.HmonthlyIncidenceFalciparum = sim.HmonthlyIncidenceFalciparum + sim.weeklyHIncidenceListF.get(i); 

             System.out.println("Weekly incidence " + i + ": " + sim.weeklyHIncidenceList.get(i));

          }

          sim.HmonthlyIncidence = sim.HmonthlyIncidence / sim.human_bag.size();
          sim.HmonthlyIncidenceVivax = sim.HmonthlyIncidenceVivax / sim.human_bag.size();
          sim.HmonthlyIncidenceFalciparum = sim.HmonthlyIncidenceFalciparum / sim.human_bag.size();

          System.out.println(" ");
          System.out.println("Monthly Incidence: " + sim.HmonthlyIncidence);
          //System.out.println("Observed Incidence: " + malariaBautista / 100);
          System.out.println("Observed Incidence: " + malariaObserved / 100);

          double numMos = (double)sim.mosquito_bag.size();
          double avgNumMos = (double)sim.totNumOfmosquitos / sim.totNumOfmosquitosCount;
          sim.totNumOfmosquitos = 0.0;
          sim.totNumOfmosquitosCount = 0;

          double lifespan = 0.0;
          if( sim.weeklyAvgMosquitoLifeSpanCount != 0)
          {
             lifespan = sim.weeklyAvgMosquitoLifeSpan / (sim.weeklyAvgMosquitoLifeSpanCount * 24);
          }

          double Fsporogonic = 0.0;
          if( sim.weeklyAvgSporogonicFalciparumCounter != 0)
          {
             Fsporogonic = sim.weeklyAvgSporogonicFalciparum / sim.weeklyAvgSporogonicFalciparumCounter;
          }

          double Vsporogonic = 0.0;
          if( sim.weeklyAvgSporogonicVivaxCounter != 0)
          {
             Vsporogonic = sim.weeklyAvgSporogonicVivax / sim.weeklyAvgSporogonicVivaxCounter;
          }

          double timeToGetBlood = 0.0;
          if(sim.weeklyTimeToGetBloodCounter != 0)
          {
              timeToGetBlood = sim.weeklyTimeToGetBlood / sim.weeklyTimeToGetBloodCounter;
          }

          double timeToLayEggs = 0.0;
          if(sim.weeklyTimeToLayEggsCounter != 0)
          {
              timeToLayEggs = sim.weeklyTimeToLayEggs / sim.weeklyTimeToLayEggsCounter;
          }

          double gonotrophic = timeToGetBlood + timeToLayEggs;

          double avgTimeToSymptoms = 0.0;
          if(sim.weeklyAvgTimeToSymptomsCounter != 0)
          {
             avgTimeToSymptoms = sim.weeklyAvgTimeToSymptoms / sim.weeklyAvgTimeToSymptomsCounter;
          }


          double numFailBite = 0;
          if( sim.weeklyNumBite != 0)
          {
             numFailBite = sim.weeklyNumFailBite/sim.weeklyNumBite;
          }

          sim.weeklyData.add(new Object[] {
              mDate, 
             
              //Humans Statisitcs -----------
              //malariaBautista / 100,
              malariaObserved,

              100 * getInfectedHumans()/sim.human_bag.size(),

              //4 * sim.HIncidence, 
              //4 * sim.HIncidenceVivax, 
              //4 * sim.HIncidenceFalciparum, 

              sim.HmonthlyIncidence, 
              sim.HmonthlyIncidenceVivax, 
              sim.HmonthlyIncidenceFalciparum, 

              sim.HyearlyIncidence, 
              sim.HyearlyIncidenceVivax, 
              sim.HyearlyIncidenceFalciparum, 

              sim.weeklyNumHumanInfected,
              sim.weeklyAvgNumBitesPerHuman /sim.weeklyDayStats, 
              sim.weeklyAvgFractBitesOnAnimals /sim.weeklyDayStats, 
              getSusceptibleHumans()/sim.human_bag.size(),
              getInfectiousHumans()/sim.human_bag.size(),
              getHumansInfectedPlasmodium("Falciparum")/sim.human_bag.size(),
              getHumansInfectedPlasmodium("Vivax")/sim.human_bag.size(),
              avgTimeToSymptoms,
              getFractHumanProtected(),
              numHouseholdInfected( ),
              fractHouseholdGotMalaria( ),
              fractHumanGotMalaria( ),
              ((double)getNumHumansProtectedForever())/sim.human_bag.size(),
              (double)sim.human_bag.size(),

              //Mosquitos Statisitcs -----------
              numMos, 
              avgNumMos, 
              sim.weeklyAvgFractInfectiousMosquitos / sim.weeklyHourStats,
              lifespan,
              Fsporogonic / 24,
              Vsporogonic / 24,
              gonotrophic / 24,
              timeToGetBlood / 24,
              timeToLayEggs / 24,
              numFailBite,

              //Environmental Statistics--------
              riverLevel, 
              sim.weeklyAvgTemp/sim.weeklyHourStats, 
              sim.weeklyNumRainDays/sim.weeklyDayStats, 
              sim.numPixelsCoveredByWater,
              sim.numProductivePixels,
              sim.maxAdultEmergent
              //sim.maxNumbAdultperArea

              //getNumOldMos( )
          });

          //System.out.println("Fail Bites: " + sim.weeklyNumFailBite);
          System.out.println("Fract Human Protected: " + getFractHumanProtected());
          System.out.println("Num Human Protected: " + getHumanOneMalaria()*sim.human_bag.size());
          System.out.println("Fract. of susceptible Humans: " + getSusceptibleHumans() / sim.human_bag.size());

          sim.weeklyHourStats = 0;
          sim.weeklyDayStats = 0;
          sim.weeklyAvgTemp = 0.0;
          sim.weeklyNumRainDays = 0.0;
          sim.weeklyAvgNumBitesPerHuman = 0.0;
          sim.weeklyAvgFractBitesOnAnimals = 0.0;
          sim.weeklyAvgFractInfectiousMosquitos = 0.0;

          sim.weeklyAvgMosquitoLifeSpan = 0.0;
          sim.weeklyAvgMosquitoLifeSpanCount = 0;

          sim.weeklyAvgSporogonicFalciparum = 0.0;
          sim.weeklyAvgSporogonicFalciparumCounter = 0;
          sim.weeklyAvgSporogonicVivax = 0.0;
          sim.weeklyAvgSporogonicVivaxCounter = 0;
          sim.weeklyNumHumanInfected = 0;

          sim.weeklyTimeToGetBlood = 0.0;
          sim.weeklyTimeToGetBloodCounter = 0;

          sim.weeklyTimeToLayEggs = 0.0;
          sim.weeklyTimeToLayEggsCounter = 0;

          sim.weeklyNumFailBite = 0;
          sim.weeklyNumBite = 0;

          sim.weeklyAvgTimeToSymptoms = 0;
          sim.weeklyAvgTimeToSymptomsCounter = 0;

          averagehouseholdIncidence();

          //See if the protection against malaria have to be activated-----------
          System.out.println("protectionActivationTreshold = " + sim.protectionActivationTreshold);
          System.out.println("HmonthlyIncidence = " + sim.HmonthlyIncidence);
          if((sim.HmonthlyIncidence) > sim.protectionActivationTreshold)
          {
              System.out.println("Increased the fract of protected humans!!!aaaZZZ");
              increaseProtection(sim.fractPeopleAddProtection); 
          }



          //System.exit(0);
      }

      //System.out.println ("Statistics stops");
   } 

   //====================================================
   public void print_output()
   {
       int now = sim.utils.getTime();
       int abs_time = sim.utils.getTime();
       //System.out.println ("now = " + now);

       //int now = (int)state.schedule.getTime();  
       //int abs_time = (int)state.schedule.getTime();  
       //System.out.println ("now = " + now);
       //System.out.println ("now print output = " + now);
       //now = (int)state.schedule.getTime();  
       //System.out.println ("now print output = " + now);
       //System.exit(0);

       //int rest = now % 8640;
       //year = (int)( ( now - rest )/ 8640 );
       //now = rest;
       //rest = now % 720;
       //month = (int)( ( now - rest )/ 720 );
       //now = rest;
       //rest = now % 24;
       //day = (int)( ( now - rest )/ 24 );

       MeteoData md = sim.meteoData.get(now);
       Calendar calendar = md.getCalendar();

       //DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
       //String mDate = format.format(calendar.getTime());

       int year = calendar.get(Calendar.YEAR);
       String month = sim.utils.getIntMonth(calendar.get(Calendar.MONTH));
       int day = calendar.get(Calendar.DAY_OF_MONTH);
       



          System.out.println ("===============================================================================");
          //System.out.format("%17s%5d%10s%4d%8s%4d%30s\n", "=========== Day: ",  day,  ",  Month: ",  month, ", Year: ", year, " ==============================");
          System.out.println("                 " +  day +  "       of " +  month + "  " + year + "    ");
          System.out.println ("===============================================================================");
          if(sim.equilibration)
          {
              System.out.println("--------------------------------------- ");
              System.out.println ("Equilibration: " + sim.equilibPeriodCounter / 24);
              System.out.println(" ");
          
          }
          System.out.println("Mosquitoes stats ---------------------- ");
          System.out.println("Number of Mosquitos in this time step: " + sim.mosquito_bag.size());
          System.out.println("Average Number of Mosquitos: " + sim.totNumOfmosquitos/ sim.totNumOfmosquitosCount);
          System.out.println("Fract. of infectious Mosquitos: " + getFractInfectiousMosquitos());

          System.out.println(" ");
          System.out.println("Average mosquitoes lifespan (days): " + (double)sim.totalLifespan/(24 * sim.numExistedMosquitos));
          System.out.println("Average sporogonic cycle Falciparum (days): " + sim.totalSporogonicFalciparum/(24 * sim.numSporogonicMosquitosFalciparum));
          System.out.println("Average sporogonic cycle Vivax (days): " + sim.totalSporogonicVivax/(24 * sim.numSporogonicMosquitosVivax));

          System.out.println(" ");
          System.out.println("Average gonotrophic cycle (days): " + ( ( (sim.timeToLayEggs )/(sim.numTimeToLayEggs) ) +  (sim.timeToGetBlood )/(sim.numTimeToGetBlood) ) /24 );
          System.out.println("Average time to get blood (days): " + ( (  (sim.timeToGetBlood )/(sim.numTimeToGetBlood) ) /24 ));
          System.out.println("Average time to lay eggs (days): " + ( (  (sim.timeToLayEggs )/(sim.numTimeToLayEggs) ) /24 ));
          System.out.println(" ");


          System.out.println("Humans stats -------------------------- ");
          System.out.println("Fract. of infectious Humans: " + getInfectiousHumans() / sim.human_bag.size());
          System.out.println("Fract. of infected Humans: " + getInfectedHumans() / sim.human_bag.size());
          System.out.println("Average asymptomaticity Human (min value (no asymptomaticiy), max value: 2): " + getAverageHumanasymptomaticity());

          System.out.println(" ");
          System.out.println("Fract. Human infected by P. Falciparum: " + getHumansInfectedPlasmodium("Falciparum") / sim.human_bag.size());
          System.out.println("Fract. Human infected by P. Vivax: " + getHumansInfectedPlasmodium("Vivax") / sim.human_bag.size());

          System.out.println(" ");
          System.out.println("Num humans infection blocking high risk: " + getNumHumansProtectedForever());

          double ddd = ((double)getNumHumansProtectedForever())/sim.human_bag.size();
          System.out.println("Fract humans infection blocking high risk: " + ddd);

          System.out.println(" ");
          System.out.println("Average num Bites per Human per Day: " + sim.avgNumDailyBitePerHuman/(sim.numDays ));

          System.out.println("Average fraction of bites on animals per Day: " + sim.avgDailyFractionBitesOnAnimals/(sim.numDays ));

          System.out.println(" ");
          System.out.println("CPU stats  ---------------------------- ");
          System.out.println("This time step took " + sim.runTime + " milliseconds to execute.");
          System.out.println("Average time per time step " + sim.timeTimeSteps/sim.timeTimeStepsCount + " milliseconds.");


         /*
         //sim.malariaIncidenceSerie.add(sim.malariaMonthlyCases/sim.human_bag.size() );

          System.out.println(" ");
          System.out.println("---------------------------");

          System.out.println("Malaria Monthly Incidence:");

          for(int i = 0; i < 12; i++)
          {
             if(sim.monthlyMalariaIncidence[i] == 0.0)continue;
             System.out.format( 
                     "%4d%1s",i+1, " "
             );
          
          }
          System.out.format("\n");

          for(int i = 0; i < 12; i++)
          {
             if(sim.monthlyMalariaIncidence[i] == 0.0)continue;
             System.out.format( 
                     "%.2f%1s",sim.monthlyMalariaIncidence[i], " "
             );
          
          }
          System.out.format("\n");


          //System.exit(0);



          System.out.println(" ");
          System.out.println("---------------------------");
          System.out.println("Biting Hours Histogram:");

          for(int i = 0; i < 24; i++)
          {
             if(i == 9)continue;
             if(i == 10)continue;
             if(i == 11)continue;
             if(i == 12)continue;
             if(i == 13)continue;
             if(i == 14)continue;
             if(i == 15)continue;
             if(i == 16)continue;
             System.out.format( 
                     "%4d%1s",i, " "
             );
          
          }
          System.out.format("\n");

          for(int i = 0; i < 24; i++)
          {
             if(i == 9)continue;
             if(i == 10)continue;
             if(i == 11)continue;
             if(i == 12)continue;
             if(i == 13)continue;
             if(i == 14)continue;
             if(i == 15)continue;
             if(i == 16)continue;
             System.out.format( 
                     "%.2f%1s",sim.bitingHours[i]/sim.totalNumBite, " "
             );
          
          }
          System.out.format("\n");


          //System.exit(0);


          System.out.println("---------------------------");
          */


        //int mb = 1024*1024;
         
        //Getting the runtime reference from system
        //Runtime runtime = Runtime.getRuntime();
        // 
        //System.out.println("##### Heap utilization statistics [MB] #####");
        // 
        //Print used memory
        //System.out.println("Used Memory:"
        //    + (runtime.totalMemory() - runtime.freeMemory()) / mb);
 
        //Print free memory
        //System.out.println("Free Memory:"
        //    + runtime.freeMemory() / mb);
        // 
        //Print total available memory
        //System.out.println("Total Memory:" + runtime.totalMemory() / mb);
 
        //Print Maximum available memory
        //System.out.println("Max Memory:" + runtime.maxMemory() / mb);

        //System.gc();



   }

   //====================================================
   public void writeSeries()
   {
       //Number of infectious nouseholds ------------------
       double ni = 0.0;   
       for(int x = 0; x < sim.household_bag.size(); x++)
       {
           Household hh = ((Household)sim.household_bag.objs[x]);
           
           if(hh.getFractInfectedPeople() > 0.0) ni++;
       }

       int now = sim.utils.getTime();
       double t = now/24;
       sim.num_infected_households_series.add(t, ni, true);
   }



   //====================================================
   public double getFractInfectiousMosquitos()
   {
       int size = sim.mosquito_bag.size();
       double f= 0.0;

       for(int x = 0; x < size; x++)
       {
           Mosquito m = ((Mosquito)sim.mosquito_bag.objs[x]);

           if(m.getInfectious())
           {
               f = f + 1.0;
           }

       }
       return f/size;
   }

   //====================================================
   public double getInfectiousHumans()
   {
       int size = sim.human_bag.size();
       double f= 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           if(h.getHInfectious() == true )
           {
               f = f + 1.0;
           }

       }
       return f/size;
   }

   //====================================================
   public double getInfectedHumans()
   {
       int size = sim.human_bag.size();
       double f= 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           if(h.getHInfected() == true)
           {
               f = f + 1.0;
           }

       }
       return f/size;
   }

   //====================================================
   public double getSusceptibleHumans()
   {
       int size = sim.human_bag.size();
       double stats = 0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           if(h.getSusceptible() == true)
           {
               stats++;
           }

       }
       return stats;
   }



   //====================================================
   public double getHumansInfectedPlasmodium(String what)
   {
       int size = sim.human_bag.size();
       double f= 0.0;

       double fract_vivax = 0.0;
       double fract_falciparum = 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           if(
                   h.getPlasmodium() != null
              &&   h.getPlasmodium().getType().equals("Vivax"))
           {
               fract_vivax = fract_vivax + 1.0;
           }
           else if(
                   h.getPlasmodium() != null
                && h.getPlasmodium().getType().equals("Falciparum"))
           {
               fract_falciparum = fract_falciparum + 1.0;
           }

       }

       if(what.equals("Falciparum"))
       {
           return fract_falciparum;
       }
       else if(what.equals("Vivax"))
       {
           return fract_vivax;
       }

       return 0.0;
   }

   //====================================================
   public double getHumanOneMalaria()
   {
       int size = sim.human_bag.size();

       double avgA = 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           if(h.getMalariaInOneYear())avgA++;
       }

       //System.out.println ("Ctot malaria humans: " + avgA);
       //System.out.println ("Ctot humans: " + size);

       return avgA/size;
   }

   //====================================================
   public double getFractHumanProtected()
   {
       int size = sim.human_bag.size();

       double avgA = 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           //if(h.getMalariaInOneYear())avgA++;
           if(h.getIsProtected())avgA++;
       }

       System.out.println ("Num humans protected: " + avgA);
       //System.out.println ("Ctot humans: " + size);
       //System.exit(0);

       return avgA/size;
   }




   //====================================================
   public double getAverageHumanasymptomaticity()
   {
       int size = sim.human_bag.size();
       double f= 0.0;

       double avgA = 0.0;

       for(int x = 0; x < size; x++)
       {
           Human h = ((Human)sim.human_bag.objs[x]);

           avgA = avgA + h.asymptomaticity.get("Falciparum");
           avgA = avgA + h.asymptomaticity.get("Vivax");

       }

       return avgA/size;
   }



   //====================================================
   public double getNumRBPixels()
   {
      Bag pixels = sim.geoGrid.elements();
      int len = pixels.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         CoverPixel cp = ((CoverPixel)pixels.objs[i]);

         if(cp.getIsRiverBank(state))stats++;
      }
      return stats;

    }

   //====================================================
   public double getNumOldMos()
   {
      Bag mos = sim.mosquito_bag;
      int len = mos.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Mosquito m = ((Mosquito)mos.objs[i]);

         if(m.getAge() >=  288)stats++;
      }
      return stats;



   }


   //====================================================
   public void averagehouseholdIncidence()
   {
      Bag hh = sim.household_bag;
      int len = hh.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Household h = ((Household)hh.objs[i]);

         int numCases = h.getWeeklyMalariaCases();
         h.setWeeklyMalariaCases(0);
         int people = h.getNumPeople();

         double inc = (double)((double)numCases/(double)people);

         h.increaseAvgMalariaIncidence(inc);
         h.increaseWeeksCounter();

         //if(numCases > 0)
         //{
         //   System.out.println ("-------------------------------");
         //   System.out.println ("Num cases: " + numCases);
         //   System.out.println ("Local inc: " + inc);
         //   System.out.println ("Hoseholsss incidence counter: " + h.getWeeksCounter());
         //   System.out.println ("Hoseholsss incidence : " + h.getAvgMalariaIncidence());
         
         //}

      }


   }

   //====================================================
   public double numHouseholdInfected()
   {
      Bag hh = sim.household_bag;
      int len = hh.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Household h = ((Household)hh.objs[i]);

         if(h.getNumInfectedPeople() > 0)stats++;
      }

      return stats;


   }


   //====================================================
   public double fractHouseholdGotMalaria()
   {
      Bag hh = sim.household_bag;
      int len = hh.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Household h = ((Household)hh.objs[i]);

         if(h.getGotMalaria())stats++;
      }

      return stats/len;


   }

   //====================================================
   public double fractHumanGotMalaria()
   {
      Bag hh = sim.human_bag;
      int len = hh.size();
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Human h = ((Human)hh.objs[i]);

         if(h.getGotMalaria())stats++;
      }

      return stats/len;


   }


   //====================================================
   public void increaseProtection(double fract)
   {
      Bag hh = sim.household_bag;
      int len = hh.size();
      int stats = 0;

      hh.shuffle(state.random);

      for(int i = 0; i < len; i++)
      {
         Household h = ((Household)hh.objs[i]);

         Bag people = h.getPeople();

         int size = people.size();

         double avgA = 0.0;

         for(int x = 0; x < size; x++)
         {
             Human human = ((Human)people.objs[x]);

             double hFract = getFractHumanProtected();

             if(!human.getMalariaInOneYear() 
                     && !human.getIsProtected()     
                     && hFract < 0.99)
             {
                 stats++;
                 human.setMIOYOn();
             }

             if(stats > Math.round(len * fract))return;
         }



         //Never have allk the humans protected: impossible (only 0.98)
      }

   
   }

   //====================================================
   public void fractHumanAsympto()
   {
      Bag hh = sim.human_bag;
      int len = hh.size();
      double statsV = 0;
      double statsF = 0;
      double stats = 0;

      for(int i = 0; i < len; i++)
      {
         Human h = ((Human)hh.objs[i]);

         if(h.getAsymptomatic() == 1)
         {
            if(h.getPlasmodium().getType().equals("Vivax"))
            {
                statsV++;
                stats++;
            }
            else if(h.getPlasmodium().getType().equals("Falciparum"))
            {
                statsF++;
                stats++;
            }
            else if(h.getPlasmodium().getType().equals("bothPlasmodia"))
            {
               statsF++;
               statsV++;
            }
            else 
            {
               System.out.println ("No plasmodium for asymptomatic human");
               System.exit(0);
            
            }

         }

      }

      System.out.println ("----------------------------------------");
      System.out.println ("Fract asymptomatic Vivax: " + statsV/len);
      System.out.println ("Fract asymptomatic Falciparum: " + statsF/len);
      System.out.println ("Fract asymptomatic: " + stats/len);
      //System.exit(0);


   }

   //====================================================
   public int getNumHumansProtectedForever()
   {
      Bag hh = sim.human_bag;
      int len = hh.size();
      int stats = 0;

      for(int i = 0; i < len; i++)
      {
         Human h = ((Human)hh.objs[i]);
         if(h.isProtectedForever)stats++;
      }

      return stats;
 
   }

   //====================================================
   public double getMalariaObserved(Calendar calendar)
   {
       //Get the observed monthly malaria incidence
       Calendar stop = (Calendar)calendar.clone();      
       Calendar start = (Calendar)calendar.clone();      
       start.add(start.HOUR, -720);

       int stats = 0;
       for(int i = 0; i < sim.malariaData.size(); i++)
       {
           MalariaData malD = sim.malariaData.get(i);


           if(malD.calendar.compareTo(start) > 0
                   && malD.calendar.compareTo(stop) <= 0)
           {
               stats++;
           }
       }

       //System.out.println ("Start: " + start.getTime());
       //System.out.println ("End: " + stop.getTime());
       //System.out.println ("Stats: " + stats);
       //System.out.println ((double)stats/sim.human_bag.size());
       //System.exit(0);
       return (double)stats/sim.human_bag.size();



   }


}
