/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import sim.util.geo.MasonGeometry;


//----------------------------------------------------
public class Utils 
{
   private static final long serialVersionUID = 1L;

   AmaSim sim = null;
   SimState state = null;

   //====================================================
   public Utils(SimState state)
   {
       sim = (AmaSim)state;
       state = (SimState)sim;
   }

   //=Periodic Boundary Conditions=======================
   public Integer[] pbc(SimState state, Integer coors[], String what, String grid)
   {
       int xtmp = coors[0];
       int ytmp = coors[1];

       int height = 0;
       int width = 0;
       if(grid.equals("geo"))
       {
          height = sim.geoGridHeight;
          width  = sim.geoGridWidth;
       }
       else if(grid.equals("mosquito"))
       {
          height = sim.mosquitoGridHeight;
          width  = sim.mosquitoGridWidth;
       }
       else
       {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("pbc utils");
          System.exit(0);
       }

       //Boolean doPbc = true;
       //if(!sim.pbcMosquito && grid.equals("mosquito"))doPbc = false;

       if(what.equals("toroidals"))
       {
          if(xtmp < 0){              xtmp = xtmp + width;}
          if(xtmp >= width){  xtmp = xtmp - width;}
   
          if(ytmp < 0){              ytmp = ytmp + height;}
          if(ytmp >= height){ ytmp = ytmp - height;}
   
          coors[0] = xtmp;
          coors[1] = ytmp;
       }
       //System.out.println ("Utils " + ytmp);

       return coors;
   }

   //=Get the integer coordinated on the discrete grid that
   //correspond to the geo coordinates of a point
   public Integer[] getDiscreteCoordinatesPoint(Point point, String grid)
   {
      Integer [] coors = {0, 0};

      int height = 0;
      int width = 0;
      if(grid.equals("geo"))
      {
         height = sim.geoGridHeight;
         width  = sim.geoGridWidth;
      }
      else if(grid.equals("mosquito"))
      {
         height = sim.mosquitoGridHeight;
         width  = sim.mosquitoGridWidth;
      }
      else
      {
         System.out.println ("Not defined grid grid type in:");
         System.out.println ("pbc utils");
         System.exit(0);
      }
         //System.out.println ("====================");


      for(int i = 0; i < width; i++)
      {
         for(int j = 0; j < height; j++)
         {
             CoverPixel cp = sim.utils.getCoverPixelFromCoords(sim, i, j, grid);
             MasonGeometry mg = cp.getMasonGeometry();
             Polygon square = (Polygon)mg.getGeometry();

             Point center = square.getCentroid();
/*
         System.out.println ("--------------------");
      System.out.println (
                point.getX() + " " +
                point.getY()
                );

      System.out.println (
                center.getX() + " " +
                center.getY()
                );

                */

             if(square.covers(point) || square.touches(point)
             || square.intersects(point))
             {
                coors[0] = cp.getXcor(); 
                coors[1] = cp.getYcor();
                return coors;

             }

         }
      }

      System.out.println (grid);
      System.out.println ("No cell grid square contains the given point");
      System.out.println ("You got a problem here......");
      System.out.println("Point coordinates:" + point.getX() + ", " + point.getY()); 

      System.out.println("MBR min - max x:" + sim.globalMBR.getMinX() + ", " + sim.globalMBR.getMaxX()); 
      System.out.println("MBR min - max y:" + sim.globalMBR.getMinY() + ", " + sim.globalMBR.getMaxY()); 

      System.exit(0);
       
      return coors;
   }

   //Returns the point that corresponds to the center of a grid cell
   public Point getCellCentralPoint(int i, int j, String grid)
   {
       //System.out.println ("grid " + grid);
       Coordinate coords = null;
       if(grid.equals("geo"))
       {
          coords  = getContinuosGeoCoords(i, j);
       }
       else if(grid.equals("mosquito"))
       {
          coords  = getContinuosMosquitoCoords(i, j);
       }
       else
       {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("getCellCentralPoint utils");
          System.exit(0);
       }

       Point point = new GeometryFactory().createPoint(coords);
       return point;
   }

   //Returns the geo coordinates that correspond to 
   //the a grid point of a grid cell
   public Coordinate getContinuosGeoCoords(int i, int j)
   {
       double x = sim.geoCellSize * i + sim.geoCellSize * 0.5;
       double y = sim.geoCellSize * j + sim.geoCellSize * 0.5;

       x = x + sim.globalMBRMinX;
       y = y + sim.globalMBRMinY;

       Coordinate coords  = new Coordinate(x, y);

       return coords;
   }

   //Returns the geo coordinates that correspond to 
   //the a grid point of a grid cell
   public Coordinate getContinuosMosquitoCoords(int i, int j)
   {
       double x = sim.mosquitoCellSize * i + sim.mosquitoCellSize * 0.5;
       double y = sim.mosquitoCellSize * j + sim.mosquitoCellSize * 0.5;

       x = x + sim.globalMBRMinX;
       y = y + sim.globalMBRMinY;

       Coordinate coords  = new Coordinate(x, y);

       return coords;
   }


   //Returns the geo coordinates that correspond to 
   //the a grid point of a grid cell
   public double[] getContinuosGeopos(int i, int j)
   {
       double x = sim.geoCellSize * i + sim.geoCellSize * 0.5;
       double y = sim.geoCellSize * j + sim.geoCellSize * 0.5;

       x = x + sim.globalMBRMinX;
       y = y + sim.globalMBRMinY;

       //Coordinate coords  = new Coordinate(x, y);
       double[] coords = {x, y};

       return coords;
   }




   //Creates a square of side side around the point center
   public Polygon getSquareAroundPoint(Point center, double side)
   {
       GeometryFactory fact =  new GeometryFactory();

       double x = center.getX();
       double y = center.getY();
       double lm = side/2;

       Coordinate[] coords  =
            new Coordinate[] {
                new Coordinate(x - lm, y - lm), 
                new Coordinate(x + lm, y - lm),
                new Coordinate(x + lm, y + lm), 
                new Coordinate(x - lm, y + lm), 
                new Coordinate(x - lm, y - lm) 
            };

       LinearRing linear = new GeometryFactory().createLinearRing(coords);
       Polygon poly = new Polygon(linear, null, fact);

       return poly;
   }


   //====================================================
   public Integer[] getCoordsToDisplay(final SimState state, int i, int j, String grid)
   {
       int height = 0;
       if(grid.equals("geo"))
       {
          height = sim.geoGridHeight;
       }
       else if(grid.equals("mosquito"))
       {
          height = sim.mosquitoGridHeight;
       }
       else
       {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("getCoordsToDisplay utils");
          System.exit(0);
       }

       Integer[] coords = {0, 0};
       coords[0] = i;
       coords[1] = -j + height - 1;
       return coords;
   }

   //====================================================
   public Integer[] getCoordsFromDisplay(final SimState state, int i, int j, String grid)
   {
       int height = 0;
       if(grid.equals("geo"))
       {
          height = sim.geoGridHeight;
       }
       else if(grid.equals("mosquito"))
       {
          height = sim.mosquitoGridHeight;
       }
       else
       {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("getCoordsToDisplay utils");
          System.exit(0);
       }

       Integer[] coords = {0, 0};
       coords[0] = i;
       coords[1] = -j + height - 1;
       return coords;
   }

   //====================================================
   public CoverPixel getCoverPixelFromCoords(final SimState state, int i, int j, String grid)
   {
      //return (CoverPixel)sim.geoGrid.get(i, (j ) );
      //System.out.println (i + " " + (-j + sim.gridHeight));
      if(grid.equals("geo"))
      {
         //System.out.println (getCoordsFromDisplay(state, i, j, "geo")[0] + "  " + 
         //getCoordsFromDisplay(state, i, j, "geo")[1] );
         return (CoverPixel)sim.geoGrid.get(
         getCoordsFromDisplay(state, i, j, "geo")[0], 
         getCoordsFromDisplay(state, i, j, "geo")[1] );
      }
      else if(grid.equals("mosquito"))
      {
         return (CoverPixel)sim.pixelMosquitosGrid.get(
         getCoordsFromDisplay(state, i, j, "mosquito")[0], 
         getCoordsFromDisplay(state, i, j, "mosquito")[1] );
      }
      else
      {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("getCoverPixelFromCoords utils");
          System.exit(0);

          //this only to add a return
          return (CoverPixel)sim.pixelMosquitosGrid.get(
          getCoordsFromDisplay(state, i, j, "mosquito")[0], 
          getCoordsFromDisplay(state, i, j, "mosquito")[1] );
      }
   }
/*
   public Integer[] getCellFromGeoToMosquitoGrid(SimState state, int xpos, int ypos)
   {
       //double[] ccoords = getContinuosGeopos(xpos, ypos);
       //System.out.println (coords.getX());

       Integer[] coords = {0, 0};

       double x = sim.geoCellSize * (xpos);
       double y = sim.geoCellSize * (ypos);

       coords[0] = (int)Math.floor(x/sim.mosquitoCellSize);
       coords[1] = (int)Math.floor(y/sim.mosquitoCellSize);

       //coords[0] = (int)Math.round(x/sim.mosquitoCellSize);
       //coords[1] = (int)Math.round(y/sim.mosquitoCellSize);

       return coords;
   }

   public Integer[] getCellFromMosquitoToGeoGrid(SimState state, int xpos, int ypos)
   {
       //double[] ccoords = getContinuosGeopos(xpos, ypos);
       //System.out.println (coords.getX());

       Integer[] coords = {0, 0};

       double x = sim.mosquitoCellSize * (xpos);
       double y = sim.mosquitoCellSize * (ypos);
       //System.out.println ("inside double: " + x + " "  + y);

       coords[0] = (int)Math.floor(x/sim.geoCellSize);
       coords[1] = (int)Math.floor(y/sim.geoCellSize);

       return coords;
   }

*/


   //====================================================
   public String getStringMonth(String m)
   {
       if(m.equals("01"))return "January";
       else if(m.equals("02"))return "February";
       else if(m.equals("03"))return "March";
       else if(m.equals("04"))return "April";
       else if(m.equals("05"))return "May";
       else if(m.equals("06"))return "June";
       else if(m.equals("07"))return "July";
       else if(m.equals("08"))return "August";
       else if(m.equals("09"))return "September";
       else if(m.equals("10"))return "October";
       else if(m.equals("11"))return "November";
       else if(m.equals("12"))return "December";
       else
       {
         System.out.println ("Months number betweeen 01 and 12");
         System.exit(0);
       }
       return " ";


   }

   //====================================================
   public String getIntMonth(Integer m)
   {
       if(m == 0)return "January";
       else if(m == 1)return "February";
       else if(m == 2)return "March";
       else if(m == 3)return "April";
       else if(m == 4)return "May";
       else if(m == 5)return "June";
       else if(m == 6)return "July";
       else if(m == 7)return "August";
       else if(m == 8)return "September";
       else if(m == 9)return "October";
       else if(m == 10)return "November";
       else if(m == 11)return "December";
       else
       {
         System.out.println ("Months number betweeen 1 and 12");
         System.out.println (m);
         System.exit(0);
       }
       return " ";


   }


   //=Periodic Boundary Conditions=======================
   public boolean pixelIsBorder(SimState state, CoverPixel cp, String grid)
   {

       int height = 0;
       int width = 0;
       if(grid.equals("geo"))
       {
          height = sim.geoGridHeight;
          width  = sim.geoGridWidth;
       }
       else if(grid.equals("mosquito"))
       {
          height = sim.mosquitoGridHeight;
          width  = sim.mosquitoGridWidth;
       }
       else
       {
          System.out.println ("Not defined grid grid type in:");
          System.out.println ("pbc utils");
          System.exit(0);
       }


       if(cp.getXcor() == 0 || cp.getXcor() == width - 1)return true;
       if(cp.getYcor() == 0 || cp.getYcor() == height - 1)return true;

       return false;

   }


   //=Get the Right time ------------------------------
   public int getTime()
   {
      //See if it is the equilibration period
      SimState state = (SimState)sim;
      int now;

      if(sim.equilibration)
      {
         now = 23;
      }
      else
      {
         now = (int)state.schedule.getTime();  
         now = now - sim.equilibPeriod;
      }
      //System.out.println (now);
      //System.exit(0);

      return now;
   }




//=Perfectly mixing human swap between household pairs
public void householdsSwapHumans()
{
    SimState state = (SimState)sim;
 
    Bag humans = sim.human_bag;
    int stats = 0;

    for(int i = 0; i < humans.size(); i++)
    {
        Human hzzz = ((Human)humans.objs[i]);

        //System.out.println (hss.getPeople().size());
        if(hzzz.getIsProtected())stats++;

    }

    System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println ("Num humans protected: " + stats);
    System.out.println ("Num humans : " + humans.size());
    System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++");
 
    Bag hh = sim.household_bag;

    for(int i = 0; i < hh.size(); i++)
    {
        Household  h = ((Household )hh.get(i));

        Bag hpeople = h.getPeople();
        int ri = state.random.nextInt(hpeople.size());
        Human human = ((Human)hpeople.get(ri));

        ri = state.random.nextInt(hh.size());

        Household  hS = ((Household )hh.get(ri));

        Bag hSpeople = hS.getPeople();
        ri = state.random.nextInt(hSpeople.size());
        Human humanS = ((Human)hSpeople.get(ri));

        Human humanTmp = new Human(state, human.getCpPosition(), h);

        h.removeHuman(human);
        h.addHuman(humanS);

        hS.removeHuman(humanS);
        hS.addHuman(human);

        humanTmp.setCpPosition(human.getCpPosition());
        human.setCpPosition(humanS.getCpPosition());
        humanS.setCpPosition(humanTmp.getCpPosition());

        humanTmp.setHousehold(human.getHousehold());
        human.setHousehold(humanS.getHousehold());
        humanS.setHousehold(humanTmp.getHousehold());

        //h.setPeople(hpeople);
        //hS.setPeople(hSpeople);

        if(sim.humanMixingType == 1 || sim.humanMixingType == 2) 
        {
            humanTmp.setIsProtected(human.getIsProtected());
            human.setIsProtected(humanS.getIsProtected());
            humanS.setIsProtected(humanTmp.getIsProtected());

            humanTmp.isProtectedInTheHousehold = human.isProtectedInTheHousehold;
            human.isProtectedInTheHousehold = humanS.isProtectedInTheHousehold;
            humanS.isProtectedInTheHousehold = humanTmp.isProtectedInTheHousehold;

            //System.out.println ("-------");
            //System.exit(0);
        }

        if(sim.humanMixingType == 2) 
        {
            humanTmp.setHInfected(human.getHInfected());
            human.setHInfected(humanS.getHInfected());
            humanS.setHInfected(humanTmp.getHInfected());

            humanTmp.setHInfectious(human.getHInfectious());
            human.setHInfectious(humanS.getHInfectious());
            humanS.setHInfectious(humanTmp.getHInfectious());

            humanTmp.setTreated(human.getTreated());
            human.setTreated(humanS.getTreated());
            humanS.setTreated(humanTmp.getTreated());

            humanTmp.setMalariaInOneYear(human.getMalariaInOneYear());
            human.setMalariaInOneYear(humanS.getMalariaInOneYear());
            humanS.setMalariaInOneYear(humanTmp.getMalariaInOneYear());

            humanTmp.setSusceptible(human.getSusceptible());
            human.setSusceptible(humanS.getSusceptible());
            humanS.setSusceptible(humanTmp.getSusceptible());

            humanTmp.setInfectiousPeriod(human.getInfectiousPeriod());
            human.setInfectiousPeriod(humanS.getInfectiousPeriod());
            humanS.setInfectiousPeriod(humanTmp.getInfectiousPeriod());

            humanTmp.setInfectionTimer(human.getInfectionTimer());
            human.setInfectionTimer(humanS.getInfectionTimer());
            humanS.setInfectionTimer(humanTmp.getInfectionTimer());

            humanTmp.setAsymptomatic(human.getAsymptomatic());
            human.setAsymptomatic(humanS.getAsymptomatic());
            humanS.setAsymptomatic(humanTmp.getAsymptomatic());

            humanTmp.setAsymptomaticity(human.getAsymptomaticity());
            human.setAsymptomaticity(humanS.getAsymptomaticity());
            humanS.setAsymptomaticity(humanTmp.getAsymptomaticity());

            humanTmp.setTimeToSymptoms(human.getTimeToSymptoms());
            human.setTimeToSymptoms(humanS.getTimeToSymptoms());
            humanS.setTimeToSymptoms(humanTmp.getTimeToSymptoms());

            humanTmp.setTimeToCommunicability(human.getTimeToCommunicability());
            human.setTimeToCommunicability(humanS.getTimeToCommunicability());
            humanS.setTimeToCommunicability(humanTmp.getTimeToCommunicability());

            humanTmp.setTimeToRecover(human.getTimeToRecover());
            human.setTimeToRecover(humanS.getTimeToRecover());
            humanS.setTimeToRecover(humanTmp.getTimeToRecover());

            humanTmp.setIdentity(human.getIdentity());
            human.setIdentity(humanS.getIdentity());
            humanS.setIdentity(humanTmp.getIdentity());

            humanTmp.setProbFail(human.getProbFail());
            human.setProbFail(humanS.getProbFail());
            humanS.setProbFail(humanTmp.getProbFail());

            humanTmp.setGotMalaria(human.getGotMalaria());
            human.setGotMalaria(humanS.getGotMalaria());
            humanS.setGotMalaria(humanTmp.getGotMalaria());

            humanTmp.setVivaxRecurrence(human.getVivaxRecurrence());
            human.setVivaxRecurrence(humanS.getVivaxRecurrence());
            humanS.setVivaxRecurrence(humanTmp.getVivaxRecurrence());

            humanTmp.setVivaxRecurrenceTime(human.getVivaxRecurrenceTime());
            human.setVivaxRecurrenceTime(humanS.getVivaxRecurrenceTime());
            humanS.setVivaxRecurrenceTime(humanTmp.getVivaxRecurrenceTime());

            humanTmp.setPlasmodium(human.getPlasmodium());
            human.setPlasmodium(humanS.getPlasmodium());
            humanS.setPlasmodium(humanTmp.getPlasmodium());

            humanTmp.setInfected(human.getInfected());
            human.setInfected(humanS.getInfected());
            humanS.setInfected(humanTmp.getInfected());

            humanTmp.setInfectious(human.getInfectious());
            human.setInfectious(humanS.getInfectious());
            humanS.setInfectious(humanTmp.getInfectious());

            humanTmp.setAge(human.getAge());
            human.setAge(humanS.getAge());
            humanS.setAge(humanTmp.getAge());
        }

        sim.human_bag.remove(humanTmp);
        h.removeHuman(humanTmp);
        humanTmp.stopper.stop();         
    }

    humans = sim.human_bag;
    stats = 0;

    for(int i = 0; i < humans.size(); i++)
    {
        Human hzzz = ((Human)humans.objs[i]);

        //System.out.println (hss.getPeople().size());
        if(hzzz.getIsProtected())stats++;

    }

    System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println ("Num humans protected: " + stats);
    System.out.println ("Num humans : " + humans.size());
    System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++");
 


 
    //Bag h = sim.human_bag;

    //for(int i = 0; i < h.size(); i++)
    //{
    //    Human  hu = ((Human)h.objs[i]);

    //    System.out.println (hu.cpPosition.getXcor() + " "  + hu.cpPosition.getYcor());

    //}
 
    //hsss = sim.household_bag;
    //stats = 0;

    //for(int i = 0; i < hsss.size(); i++)
    //{
    //    Household  hss = ((Household)hsss.objs[i]);

    //    //System.out.println (hss.getPeople().size());
    //    stats = stats + hss.getPeople().size();

    //}

    //System.out.println (stats);
 

    //System.exit(0);

}



//====================================================
public void getHumansInMovement()
{

    int stats = 0;
    stats = 0;
    for(int i = 0; i < sim.human_bag.size(); i++)
    {
        Human h = (Human)sim.human_bag.get(i);
        if(h.isMoving)stats++;
    }
    if(stats == 0)
    System.out.println ("Num humans moving: " + stats);

    
    
    else
    System.out.println ("Num humans moving: " + stats + " ====================================");

    //System.exit(0);
}

//====================================================
public int getDayHour(SimState state)
{
    int now = (int)state.schedule.getTime();  
    return (now %24);
}

//====================================================
public int getDayOfWeek(SimState state)
{
    int now = (int)state.schedule.getTime();  

    int week = now % 168;
    week = 1 + (week % 7);

    //System.out.println("Week day: " + week);

    //System.out.println ("Week day: " + week/168);

    return week;
}


   //=Get the integer coordinated on the discrete grid that
   //correspond to the geo coordinates of a point
   public Integer[] getDiscreteCoordinatesPointFarm(Point point, String grid)
   {
      Integer [] coors = {0, 0};

      int height = 0;
      int width = 0;
      if(grid.equals("geo"))
      {
         height = sim.geoGridHeight;
         width  = sim.geoGridWidth;
      }
      else if(grid.equals("mosquito"))
      {
         height = sim.mosquitoGridHeight;
         width  = sim.mosquitoGridWidth;
      }
      else
      {
         System.out.println ("Not defined grid grid type in:");
         System.out.println ("pbc utils");
         System.exit(0);
      }
         //System.out.println ("====================");


      for(int i = 0; i < width; i++)
      {
         for(int j = 0; j < height; j++)
         {
             CoverPixel cp = sim.utils.getCoverPixelFromCoords(sim, i, j, grid);
             MasonGeometry mg = cp.getMasonGeometry();
             Polygon square = (Polygon)mg.getGeometry();

             Point center = square.getCentroid();

             if(square.covers(point) || square.touches(point)
             || square.intersects(point))
             {
                coors[0] = cp.getXcor(); 
                coors[1] = cp.getYcor();
                return coors;

             }
         }
      }

      //System.out.println (grid);
      //System.out.println ("No cell grid square contains the given point");
      //System.out.println ("You got a problem here......");
      //System.out.println("Point coordinates:" + point.getX() + ", " + point.getY()); 

      //System.out.println("MBR min - max x:" + sim.globalMBR.getMinX() + ", " + sim.globalMBR.getMaxX()); 
      //System.out.println("MBR min - max y:" + sim.globalMBR.getMinY() + ", " + sim.globalMBR.getMaxY()); 

      //System.exit(0);
       
      return null;
   }



/*
//====================================================
public void deg2UTM(SimState state, double Lon, double Lat)
{
    double Easting;
    double Northing;
    int Zone;
    char Letter;

    Zone= (int) Math.floor(Lon/6+31);
    if (Lat<-72) 
        Letter='C';
    else if (Lat<-64) 
        Letter='D';
    else if (Lat<-56)
        Letter='E';
    else if (Lat<-48)
        Letter='F';
    else if (Lat<-40)
        Letter='G';
    else if (Lat<-32)
        Letter='H';
    else if (Lat<-24)
        Letter='J';
    else if (Lat<-16)
        Letter='K';
    else if (Lat<-8) 
        Letter='L';
    else if (Lat<0)
        Letter='M';
    else if (Lat<8)  
        Letter='N';
    else if (Lat<16) 
        Letter='P';
    else if (Lat<24) 
        Letter='Q';
    else if (Lat<32) 
        Letter='R';
    else if (Lat<40) 
        Letter='S';
    else if (Lat<48) 
        Letter='T';
    else if (Lat<56) 
        Letter='U';
    else if (Lat<64) 
        Letter='V';
    else if (Lat<72) 
        Letter='W';
    else
        Letter='X';
    Easting=0.5*Math.log((1+Math.cos(Lat*Math.PI/180)*Math.sin(Lon*Math.PI/180-(6*Zone-183)*Math.PI/180))/(1-Math.cos(Lat*Math.PI/180)*Math.sin(Lon*Math.PI/180-(6*Zone-183)*Math.PI/180)))*0.9996*6399593.62/Math.pow((1+Math.pow(0.0820944379, 2)*Math.pow(Math.cos(Lat*Math.PI/180), 2)), 0.5)*(1+ Math.pow(0.0820944379,2)/2*Math.pow((0.5*Math.log((1+Math.cos(Lat*Math.PI/180)*Math.sin(Lon*Math.PI/180-(6*Zone-183)*Math.PI/180))/(1-Math.cos(Lat*Math.PI/180)*Math.sin(Lon*Math.PI/180-(6*Zone-183)*Math.PI/180)))),2)*Math.pow(Math.cos(Lat*Math.PI/180),2)/3)+500000;
    Easting=Math.round(Easting*100)*0.01;
    Northing = (Math.atan(Math.tan(LatMath.PI/180)/Math.cos((Lon*Math.PI/180-(6*Zone -183)*Math.PI/180)))-Lat*Math.PI/180)*0.9996*6399593.625/Math.sqrt(1+0.006739496742*Math.pow(Math.cos(Lat*Math.PI/180),2))*(1+0.006739496742/2*Math.pow(0.5*Math.log((1+Math.cos(Lat*Math.PI/180)*Math.sin((Lon*Math.PI/180-(6*Zone -183)*Math.PI/180)))/(1-Math.cos(Lat*Math.PI/180)*Math.sin((Lon*Math.PI/180-(6*Zone -183)*Math.PI/180)))),2)*Math.pow(Math.cos(Lat*Math.PI/180),2))+0.9996*6399593.625*(Lat*Math.PI/180-0.005054622556*(Lat*Math.PI/180+Math.sin(2*Lat*Math.PI/180)/2)+4.258201531e-05*(3*(Lat*Math.PI/180+Math.sin(2*Lat*Math.PI/180)/2)+Math.sin(2*Lat*Math.PI/180)*Math.pow(Math.cos(Lat*Math.PI/180),2))/4-1.674057895e-07*(5*(3*(Lat*Math.PI/180+Math.sin(2*Lat*Math.PI/180)/2)+Math.sin(2*Lat*Math.PI/180)*Math.pow(Math.cos(Lat*Math.PI/180),2))/4+Math.sin(2*Lat*Math.PI/180)*Math.pow(Math.cos(Lat*Math.PI/180),2)*Math.pow(Math.cos(Lat*Math.PI/180),2))/3);
    if (Letter<'M')
        Northing = Northing + 10000000;
    Northing=Math.round(Northing*100)*0.01;

    System.out.println("-------------------------");
    System.out.println("Lat: " + Lat + " Lon: " + Lon);
    System.out.println("Easting: " + Easting);
    System.out.println("Northing: " + Northing);
    System.out.println("Zone: " + Zone);
    System.out.println("Letter: " + Letter);

}
*/




}


