/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.planargraph.Node;
import com.vividsolutions.jts.geom.Geometry;

import java.io.*;
import java.util.*;
import java.util.ArrayList;
import sim.field.grid.*;

import java.net.URL;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.io.geo.*;
import sim.util.*;

import sim.field.geo.GeomVectorField;
import sim.io.geo.ShapeFileExporter;
import sim.io.geo.ShapeFileImporter;
import sim.util.geo.GeomPlanarGraph;
import sim.util.geo.MasonGeometry;
import sim.util.geo.AttributeValue;
import sim.util.geo.GeometryUtilities;

import sim.engine.*;
import sim.util.*;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WriteShp implements Steppable
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    public GeomVectorField geoShp = null;
    public GeomVectorField mosShp = null;
    
    public String file_nameGeo = "";
    public String file_nameMos = "";

    public String dir_name = "";

    public WriteShp(AmaSim sim)
    {
       DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH-mm");
       Date date = new Date();
       dir_name = sim.outPath + "OutShps/";

       String tmp = "geoShp_" + dateFormat.format(date) + "_" + sim.outFileNumbering;
       file_nameGeo = dir_name.concat(tmp);

       tmp = "mosShp_" + dateFormat.format(date) + "_" + sim.outFileNumbering;
       file_nameMos = dir_name.concat(tmp);

       File theDir = new File(dir_name);

       // if the directory does not exist, create it
       if (!theDir.exists())
       {
           System.out.println("creating directory: " + dir_name);
           theDir.mkdir();
       }

 
    
    }

    //====================================================
    public void step(final SimState state)
    {

    }

    //====================================================
    public void write(SimState state)
    {
        AmaSim sim = (AmaSim)state;

        geoShp = new GeomVectorField(sim.geoGridWidth, sim.geoGridHeight);
        mosShp = new GeomVectorField(sim.mosquitoGridWidth, sim.mosquitoGridHeight);

        initGeoShp(state);
        initMosShp(state);

       
        System.out.println("mosShp: " + file_nameMos);
        System.out.println("geoShp: " + file_nameGeo);

        ShapeFileExporter.write(file_nameGeo, geoShp);
        ShapeFileExporter.write(file_nameMos, mosShp);
    }

   //====================================================
   public void initGeoShp(final SimState state)
   {
      AmaSim sim = (AmaSim)state;

      System.out.println("================================================");
      System.out.println("Writing the output shapeFiles ..................");
      System.out.println(" ");

      Bag cps = sim.geoGrid.elements();
      int num_pixels = cps.size();
      //System.out.println(num_pixels);
      //System.exit(0);

      for(int i =  0; i < num_pixels; i++)
      {
         CoverPixel cp = ((CoverPixel)cps.objs[i]);

         MasonGeometry mg = cp.getMasonGeometry();
         //System.out.println("Elevation from cp: " + cp.getElevation());

         int iele = (int)Math.round(10 * cp.getElevation());
         //System.out.println("iElevation: " + iele);
         double dele = (double)iele / (double)10;
         //System.out.println("dElevation: " +dele);
         //String sele = String.valueOf(cp.getElevation());

         //System.out.println("Elevation: " + iele);

         String sele = Double.toString(dele);
         //System.out.println("sElevation: " + sele);
         //sele = String.valueOf(dele);
         //System.out.println("sElevation: " + sele);
         //System.exit(0);

         //String nOvis = String.valueOf(cp.getNOvipositions());
         int nOvis = (int)cp.getNOvipositions();

         //String nMCases = String.valueOf(cp.getNMalariaCases());
         int nMCases = (int)cp.getNMalariaCasesF() + (int)cp.getNMalariaCasesV();

         //System.out.println("sNOvis: " + nOvis);
         //System.exit(0);

         int buffer = 0;
         if(cp.getIsBufferNoMos())buffer = 1; 
         String sbuffer = String.valueOf(buffer);

         if(!(nOvis > 0 || nMCases > 0 || buffer > 0))continue;

         mg.addIntegerAttribute("id", i);
         mg.addDoubleAttribute("elevation", dele);
         mg.addIntegerAttribute("nOvipos", nOvis);
         mg.addIntegerAttribute("nMalCases", nMCases);
         mg.addIntegerAttribute("buffer", buffer);

         //mg.addStringAttribute("elevation", dele);
         //mg.addStringAttribute("nOvipositions", nOvis);
         //mg.addStringAttribute("nMalariaCases", nMCases);
         //mg.addStringAttribute("buffer", sbuffer);

         geoShp.addGeometry(mg);
      }
   }

   //====================================================
   public void initMosShp(final SimState state)
   {
      AmaSim sim = (AmaSim)state;

      Bag cps = sim.pixelMosquitosGrid.elements();
      int num_pixels = cps.size();

      for(int i =  0; i < num_pixels; i++)
      {
         CoverPixel cp = ((CoverPixel)cps.objs[i]);

         MasonGeometry mg = cp.getMasonGeometry();

         //System.out.println("Elevation: " + iele);
         int nMoss = (int)cp.getAccumulatedNMosquitoes();
         int nMossLookingForBlood = (int)cp.getAccumulatedNMosquitoesLookingForBlood();
         int nMossBloodFeeded = (int)cp.getAccumulatedNMosquitoesBloodFeeded();

         mg.addIntegerAttribute("id", i);
         mg.addIntegerAttribute("NMosquitoes", nMoss);
         mg.addIntegerAttribute("NLookingForBlood", nMossLookingForBlood);
         mg.addIntegerAttribute("NBloodFeeded", nMossBloodFeeded);

         mosShp.addGeometry(mg);
      }
   }

   //====================================================
   public void read(SimState state)
   {
      AmaSim sim = (AmaSim)state;


   }



}
