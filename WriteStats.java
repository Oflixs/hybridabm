/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import java.io.FileWriter;
import java.io.*;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Point;

public class WriteStats implements Steppable
{
   private static final long serialVersionUID = 1L;

   //====================================================
   public WriteStats(AmaSim sim)
   {
      File theDir = new File(sim.outPath);
      int nMax = 0;
     
      // if the directory does not exist, create it
      if (!theDir.exists())
      {
          try{      
              Boolean bool = theDir.mkdirs();

              if(bool)
              {
                  System.out.println("creating directory: " + sim.outPath);
                  sim.outFileNumbering = "1"; 
              }
              else
              {
                  System.out.println ("outDir not created");
                  System.out.println ("outDir: " + sim.outPath);
                  System.exit(0);
              }


          }catch(Exception e){
              // if any error occurs
              e.printStackTrace();
          }

      }
      else
      {
          String [] directoryContents = theDir.list();

          List<String> fileLocations = new ArrayList<String>();

          for(String fileName: directoryContents) 
          {

              if(!fileName.startsWith("output"))continue;
              //System.out.println (fileName);

              String delims = "\\.";
              String[] words = fileName.split(delims);
              if(!words[1].equals("xls"))continue;

              System.out.println (fileName);

              delims = "_";
              words = words[0].split(delims);

              int len = words.length;

              //System.out.println ("Num words: " + len);
              //System.out.println (words[0]);
              if(!words[0].equals("output"))continue;
              int n = Integer.parseInt(words[len - 1]);
              if(n >= nMax)nMax = n;

          }
          nMax++;
          sim.outFileNumbering = "" + nMax;

          if(directoryContents.length == 0)
         {
            sim.outFileNumbering = "1";
         }
      }
      //System.out.println ("Numbering: " + sim.outFileNumbering);
      

      //System.exit(0);

   }

   //====================================================
   public void step(final SimState state)
   {
       final AmaSim sim = (AmaSim)state;
       //System.out.println ("Write Stas start -----");


       //System.out.println ("Write Stas start -----");
       //sim.utils.getHumansInMovement();


       int now = (int)state.schedule.getTime();  
       if(now % 168 == 0)
       {
           sim.writeXls.write(state);

           //System.out.println ("+++++++++++++++++++++++++++++++++");
           //System.out.println ("Global stats: = " + sim.statsGlobal);
           //System.out.println ("Global stats2: = " + sim.statsGlobal2);
           //System.out.println ("+++++++++++++++++++++++++++++++++");
       }


       //This writes only at the and of the simulation ----
       if(now == sim.num_step - 1 )
       {
          //writeOutputs(state);
           sim.writeXls.write(state);
           sim.writeXls.writeHousehold(state);
           sim.writeXls.writeHuman(state);
           sim.writeXls.writePixelsHuman(state);
           sim.writeXls.writePixelsHumanTime(state);
           sim.writeXls.writePixelsMosquito(state);

           for (Integer key : sim.householdsBites.keySet()) {
               System.out.println("key: " + key);
           }

           //writeDistMosAverages(state);
       }

       //Perfetc mix of humans swapping them between households
       if((now % 24 == 0) && sim.humanLocalMovement == 1)
       {
           sim.utils.householdsSwapHumans();

           //System.out.println (sim.household_bag.size());
           //System.out.println (sim.human_bag.size());

       }


       //System.out.println ("Write Stas stops -----");
      //System.exit(0);
   }

   //====================================================
   public void writeOutputs(SimState state)
   {
       final AmaSim sim = (AmaSim)state;
       writeArrayDouble(state, sim.monthlyMalariaIncidence, "monthlyMalariaIncidence");
       writeArrayListDouble(state, sim.malariaIncidenceSerie, "malariaIncidenceSerie");
   }

   //====================================================
   public void writeArrayListDouble(SimState state, List<Double>  arrayList, String name)
   {

      final AmaSim sim = (AmaSim)state;

      String file_name = name.concat(".dat");
      String tmp = file_name;
      file_name = "sim/app/AmaSim/outputs/";
      file_name = file_name.concat(tmp);
      String line = "";

      //System.out.println (file_name);

      File f = new File(file_name);

      if(!f.exists())
      {
         try
         {
            f.createNewFile();
         }
         catch(IOException e)
         {
            System.out.println ("Cannot create a file...");
         }
      }



      try
      {
         FileWriter file = new FileWriter(file_name, true);

         for(int i = 0; i < arrayList.size(); i++)
         {
            line = i + "  "  + arrayList.get(i) + "\n";
            file.write(line);
         }

         file.close();
      }
      catch (IOException e)
      {
         System.out.println ("exception opening file");
      }

      //System.exit(0);



   }


   //====================================================
   public void writeArrayDouble(SimState state, double[] array, String name)
   {

      final AmaSim sim = (AmaSim)state;

      String file_name = name.concat(".dat");
      String tmp = file_name;
      file_name = "sim/app/AmaSim/outputs/";
      file_name = file_name.concat(tmp);
      String line = "";

      //System.out.println (file_name);

      File f = new File(file_name);

      if(!f.exists())
      {
         try
         {
            f.createNewFile();
         }
         catch(IOException e)
         {
            System.out.println ("Cannot create a file...");
         }
      }



      try
      {
         FileWriter file = new FileWriter(file_name, true);

         for(int i = 0; i < array.length; i++)
         {
            line = i + "  "  + array[i] + "\n";
            file.write(line);
         }

         file.close();
      }
      catch (IOException e)
      {
         System.out.println ("exception opening file");
      }

      //System.exit(0);



   }


   //====================================================
   public void erase_all()
   {
       File dir =  new File("sim/app/AmaSim/outputs/");
       if(dir.isDirectory())
       {
           for(File c : dir.listFiles())
           {
              
              System.out.println ("Removing file: " + c.getName() + " from outputs directory..........");
              boolean success =  c.delete();
              //if(!success)
              //{
              //   System.out.println ("Cannot remove files from outputs directory");
              //   System.exit(0);
              //}
           
           }
       
       }
       

   }


   //====================================================
   public void write(String file_name, double x, double y)
   {
      file_name = file_name.concat(".out");
      String tmp = file_name;
      file_name = "sim/app/AmaSim/outputs/";
      file_name = file_name.concat(tmp);
      //System.out.println (file_name);

      File f = new File(file_name);

      if(!f.exists())
      {
         try
         {
            f.createNewFile();
         }
         catch(IOException e)
         {
            System.out.println ("Cannot create a file...");
         }
      }

      String line = x + "  "  + y + "\n";


      try
      {
         FileWriter file = new FileWriter(file_name, true);
         file.write(line);
         file.close();
      }
      catch (IOException e)
      {
         System.out.println ("exception opening file");
      }

      //System.exit(0);

   
   }


   //====================================================
   public void write_array(String file_name, double x)
   {
      file_name = file_name.concat(".out");
      String tmp = file_name;
      file_name = "sim/app/AmaSim/outputs/";
      file_name = file_name.concat(tmp);
      //System.out.println (file_name);

      File f = new File(file_name);

      if(!f.exists())
      {
         try
         {
            f.createNewFile();
         }
         catch(IOException e)
         {
            System.out.println ("Cannot create a file...");
         }
      }

      String line = x + "\n";


      try
      {
         FileWriter file = new FileWriter(file_name, true);
         file.write(line);
         file.close();
      }
      catch (IOException e)
      {
         System.out.println ("exception opening file");
      }

      //System.exit(0

   }


   //====================================================
   public void write_data(String file_name, double x, double y)
   {
      file_name = file_name.concat(".dat");
      String tmp = file_name;
      file_name = "sim/app/AmaSim/grace/";
      file_name = file_name.concat(tmp);
      //System.out.println (file_name);

      File f = new File(file_name);

      if(!f.exists())
      {
         try
         {
            f.createNewFile();
         }
         catch(IOException e)
         {
            System.out.println ("Cannot create a file...");
         }
      }

      String line = x + "  "  + y + "\n";


      try
      {
         FileWriter file = new FileWriter(file_name, true);
         file.write(line);
         file.close();
      }
      catch (IOException e)
      {
         System.out.println ("exception opening file");
      }

      //System.exit(0);

   
   }

   //====================================================
   public void erase_all_data()
   {
       File dir =  new File("sim/app/AmaSim/grace/");
       if(dir.isDirectory())
       {
           for(File c : dir.listFiles())
           {
              
              System.out.println ("Removing file: " + c.getName() + " from grace directory..........");
              boolean success =  c.delete();
              //if(!success)
              //{
              //   System.out.println ("Cannot remove files from outputs directory");
              //   System.exit(0);
              //}
           
           }
       
       }
       

   }

   //====================================================
   public void writeDistMosAverages(SimState state)
   {
      final AmaSim sim = (AmaSim)state;

      double dist = 0.0;
      double dist2 = 0.0;
      int stats = 0;

      double maxDist = -10000.0;
      double minDist = 10000.0;
      double d = 0.0;

      List<CoverPixel> trak = new ArrayList<CoverPixel>();
      Point centerStart =null;

      for(int m = 0; m < sim.mosTrajs.size(); m++)
      {
          trak = (ArrayList<CoverPixel>)sim.mosTrajs.get(m);

          for(int i = 0; i < trak.size(); i++)
          {
              if(i == 0)
              {
                  CoverPixel start = (CoverPixel)trak.get(i);
                  centerStart = start.square.getCentroid();
              }
              stats++;

              Point centerNow = (Point)trak.get(i).square.getCentroid();

              d = centerNow.distance(centerStart);

              if(d >= maxDist)maxDist = d;
              if(d <= minDist)minDist = d;

              dist = dist + d;
          }
      
      }

      double avgDist = dist / stats;
      dist = 0.0;
      stats = 0;

      //-------sd
      for(int m = 0; m < sim.mosTrajs.size(); m++)
      {
          trak = (ArrayList<CoverPixel>)sim.mosTrajs.get(m);

          for(int i = 0; i < trak.size(); i++)
          {
              if(i == 0)
              {
                  CoverPixel start = (CoverPixel)trak.get(i);
                  centerStart = start.square.getCentroid();
              }
              stats++;

              Point centerNow = (Point)trak.get(i).square.getCentroid();

              double dd = centerNow.distance(centerStart) - avgDist;

              dist = dist + dd * dd;
          }
      
      }

      double sd = Math.sqrt(dist/stats);



          System.out.println ("===============================================================================");
          System.out.println ("Average dist. traveld by Mosquitoes: " + avgDist);
          System.out.println ("SD of dist traveld by mosquitoes: " + sd);
          System.out.println ("Max traveld distance: " + maxDist);
          System.out.println ("Min traveld distance: " + minDist);
          System.out.println ("===============================================================================");


   }




}
