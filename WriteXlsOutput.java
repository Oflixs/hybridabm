/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import java.io.*;
import java.util.*;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory; // This is included in poi-ooxml-3.6-20091214.jar
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.util.logging.Level;
import java.util.logging.Logger;
import sim.engine.SimState;
import sim.util.*;

import java.util.List;
import java.util.ArrayList;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.vividsolutions.jts.geom.Point;

public class WriteXlsOutput
{
    //private static final long serialVersionUID = -4554882816749973618L;
    private static final long serialVersionUID = 1L;

    public String file_name;
    public String file_nameRootHuman;
    public String file_nameRootHumanTime;
    public String file_nameRootMosquito;

    public HashMap<String, Double> humanStats = new HashMap<String, Double>();
    public HashMap<String, Double> humanStatsMal = new HashMap<String, Double>();

    //====================================================
    public WriteXlsOutput(AmaSim sim)
    {
       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss-SSS");
       Date date = new Date();
       //System.out.println(dateFormat.format(date));

       String tmp = "output_" + dateFormat.format(date) + "_" + sim.outFileNumbering + ".xls";
       String tmpRootHuman = "pixelHuman_" + dateFormat.format(date) + "_" + sim.outFileNumbering + ".dat";
       String tmpRootHumanTime = "pixelHumanTime_" + dateFormat.format(date) + "_" + sim.outFileNumbering + ".dat";
       String tmpRootMosquito = "pixelMosquito_" + dateFormat.format(date) + "_" + sim.outFileNumbering + ".dat";

       file_name = sim.outPath;
       file_name = file_name.concat(tmp);

       file_nameRootHuman = sim.outPath;
       file_nameRootHuman = file_nameRootHuman.concat(tmpRootHuman);

       file_nameRootHumanTime = sim.outPath;
       file_nameRootHumanTime = file_nameRootHumanTime.concat(tmpRootHumanTime);

       file_nameRootMosquito = sim.outPath;
       file_nameRootMosquito = file_nameRootMosquito.concat(tmpRootMosquito);
      
       System.out.println(file_name);
       //System.exit(0);
    }

    //====================================================
    public void write(SimState state) 
    {
      AmaSim sim = (AmaSim)state;
 
      //erase_file();

      System.out.println("================================================");
      System.out.println ("Writing  outputs ..............................");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      File out_file = new File(file_name);

      try{
          FileInputStream file = new FileInputStream(out_file);
          try
          {

              workbook = new HSSFWorkbook(file);
              HSSFSheet sheet;

              //System.out.println("workbook number of sheets: " + workbook.getNumberOfSheets());
                  //System.out.println("Sheet number: " + workbook.getSheetIndex(sim.resSheet));

              sheet = workbook.getSheet(sim.resSheet);
              //If the sheet !exists a new sheet is created --------- 
              if(sheet == null)sheet = workbook.createSheet(sim.resSheet);

              workbook.setSheetOrder(sim.resSheet, 0);

              Cell cell = null;

              int lastRow = sheet.getLastRowNum();

              //System.out.println("Last row:" + lastRow);
              lastRow++;

              int size = sim.weeklyData.size();
              for(int i = 0; i < size; i++)
              {
                  Row row = sheet.createRow(lastRow++);

                  Object [] objArr = (Object[])sim.weeklyData.get(i);
                  //System.out.println(lastRow);
                  //System.out.println(objArr.length);

                  int cellnum = 0;
                  for (Object obj : objArr) 
                  {
                      //System.out.println("------------");
                      //System.out.println(obj);
                      cell = row.createCell(cellnum++);


                      if(obj instanceof Date) 
                      {
                          cell.setCellType(1);
                          cell.setCellValue((String)obj);
                          //System.out.println("Date....");
                      }
                      else if(obj instanceof String)
                      {
                          cell.setCellType(1);
                          cell.setCellValue((String)obj);
                          //System.out.println("String....");
                      }
                      else if(obj instanceof Double)
                      {
                          cell.setCellType(0);
                          cell.setCellValue((Double)obj);
                          //System.out.println("Double....");
                      }
                  }
              }
              //System.exit(0);

              sim.weeklyData = new ArrayList<Object[]>();
          }
          finally
          {
              file.close();
          }

      }
      catch (IOException ex){
          ex.printStackTrace();
      }


      //System.gc();
      //System.runFinalization();

      //Open the output file (same as the input)

      //deleteFile(file_name);

      try{

          //out_file.createNewFile();

          FileOutputStream out = new FileOutputStream(out_file, false);

          try{
              //FileOutputStream out = new FileOutputStream(out_file, true);

              workbook.write(out);

              //System.gc();
              //System.runFinalization();
              //try{
              //    Thread.sleep(300);
              //}
              //catch (InterruptedException ex){
              //    ex.printStackTrace();
              //}
          }
          finally
          {
              out.flush();
              out.close();
          }
      }
      catch (IOException ex){
          ex.printStackTrace();
      }

      System.out.println("Output data written successfully.....");
           

    }

   //====================================================
   public void erase_file()
   {

    	try{
 
    		File file = new File(file_name);
 
    		if(file.delete()){
    			System.out.println(file.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
 
    	}catch(Exception e){
 
    		e.printStackTrace();
 
    	}
 
 
   }

    //====================================================
    public void calcStatsHuman(SimState state) 
    {
        AmaSim sim = (AmaSim)state;
        //HashMap<String, double> humanStats = new HashMap<String, double>();
        
        //----------------------------
        for(int i = 0; i < sim.human_bag.size(); i++)
        {
            Human h = (Human)sim.human_bag.get(i);
            String aName;
            if(h.job != null)aName = h.job.name;
            else aName = h.ageSegment.name;

            //System.out.println("Job: " + aName);
            //System.out.println(humanStats.get(aName));
           
            if(humanStats.get(aName) == null)humanStats.put(aName, 1.0);
            else
            {
                //System.exit(0);
                double tmp = humanStats.get(aName);
                tmp = tmp + 1.0;
           
                humanStats.put(aName, tmp);
            }
        }

        //----------------------------
        for(int i = 0; i < sim.human_bag.size(); i++)
        {
            Human h = (Human)sim.human_bag.get(i);

            String aName;
            if(h.job != null)aName = h.job.name;
            else aName = h.ageSegment.name;
           
            if(humanStatsMal.get(aName) == null)humanStatsMal.put(aName, (double)h.numMalariaCases);
            else
            {
                double tmp = humanStatsMal.get(aName);
                tmp = tmp + h.numMalariaCases;
                humanStatsMal.put(aName, (tmp));
            }
        }

        //HashMap<String, double> humanStats = new HashMap<String, double>();


        //for (String key : humanStatsMal.keySet()) {
        //    //System.out.println("key: " + key + " value: " + humanStatsMal.get(key));

        //    double tmp = humanStatsMal.get(key);
        //    tmp = tmp / humanStats.get(key);

        //    humanStatsMal.put(key, tmp);
        //}

    }

    //====================================================
    public void writeHuman(SimState state) 
    {
      calcStatsHuman(state);
         
      AmaSim sim = (AmaSim)state;
 
      //erase_file();

      System.out.println("================================================");
      System.out.println ("Writing  outputs humans    ....................");
      System.out.println(" ");
      //System.exit(0);

      String sheetName = "Human";
      HSSFWorkbook workbookTmp = new HSSFWorkbook();

      try{
          FileInputStream file = new FileInputStream(file_name);
          try
          {

              HSSFWorkbook workbook = new HSSFWorkbook(file);
              HSSFSheet sheet;

              sheet = workbook.getSheet(sheetName);
              //If the sheet !exists a new sheet is created --------- 
              if(sheet == null)sheet = workbook.createSheet(sheetName);

              workbook.setSheetOrder(sheetName, 1);

              Cell cell = null;

              int lastRow = sheet.getLastRowNum();

              //System.out.println("Last row:" + lastRow);
              lastRow++;

              //Writes the name of the columns
              Row row = sheet.createRow(lastRow++);

              int cellnum = 0;

              //Id of the household
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Job");

              //Num people in the household
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Num Agents");

              //household xcoord
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Num Malaria Cases");

              //household xcoord
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Incidence");

              for (String key : humanStatsMal.keySet()) 
              {
                  row = sheet.createRow(lastRow++);

                  cellnum = 0;

                  cell = row.createCell(cellnum++);
                  cell.setCellType(1);
                  cell.setCellValue(key);

                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)humanStats.get(key));

                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)humanStatsMal.get(key));

                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)humanStatsMal.get(key)/humanStats.get(key));

              }


              workbookTmp = workbook;
          }
          finally
          {
              file.close();
          }

      }
      catch (IOException ex){
          ex.printStackTrace();
      }

      try{
          File out_file = new File(file_name);

          //out_file.createNewFile();

          FileOutputStream out = new FileOutputStream(out_file, false);

          try{
              //FileOutputStream out = new FileOutputStream(out_file, true);

              workbookTmp.write(out);
          }
          finally
          {
              out.flush();
              out.close();
          }
      }
      catch (IOException ex){
          ex.printStackTrace();
      }


      System.out.println("Output data human stats written successfully.....");
           

    }



    //====================================================
    public void writeInput(AmaSim sim)
    {
      ReadInput input = new ReadInput(sim);
      List<String> list = input.getInputList();

      System.out.println("================================================");
      System.out.println ("Writing  the input file into the output  ......");
      System.out.println(" ");

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet("Input File");
 
      int rownum = 0;
      int size = list.size();

      for(int i = 0; i < size; i++)
      {
          rownum++;
          Row row = sheet.createRow(rownum);

          String line = list.get(i);

          Cell cell = row.createCell(0);
          cell.setCellValue((String)line);

      }
 
      try {

          FileOutputStream out = 
             new FileOutputStream(new File(file_name));
          workbook.write(out);
          out.close();
          System.out.println("Excel written successfully..");
           
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }

      //System.exit(0);

    }

    //====================================================
    public void writeHousehold(SimState state) 
    {
      AmaSim sim = (AmaSim)state;
 
      //erase_file();

      System.out.println("================================================");
      System.out.println ("Writing  outputs households....................");
      System.out.println(" ");
      //System.exit(0);

      String sheetName = "Households";
      HSSFWorkbook workbookTmp = new HSSFWorkbook();


      try{
          FileInputStream file = new FileInputStream(file_name);
          try
          {

              HSSFWorkbook workbook = new HSSFWorkbook(file);
              HSSFSheet sheet;

              sheet = workbook.getSheet(sheetName);
              //If the sheet !exists a new sheet is created --------- 
              if(sheet == null)sheet = workbook.createSheet(sheetName);

              workbook.setSheetOrder(sheetName, 1);

              Cell cell = null;

              int lastRow = sheet.getLastRowNum();

              //System.out.println("Last row:" + lastRow);
              lastRow++;

              //Writes the name of the columns
              Row row = sheet.createRow(lastRow++);

              int cellnum = 0;

              //Id of the household
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Household Id");

              //Num people in the household
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Num people in the household");

              //household xcoord
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Household Xcoord");

              //household xcoord
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Household Ycoord");

              //household num falciparum cases during the length of the simulation
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Num. falciparum cases");

              //household num vivax cases during the length of the simulation
              cell = row.createCell(cellnum++);
              cell.setCellType(1);
              cell.setCellValue("Num. vivax cases");


              int size = sim.household_bag.size();
              for(int i = 0; i < size; i++)
              {
                  row = sheet.createRow(lastRow++);

                  Household hh = (Household)sim.household_bag.get(i);
                  //System.out.println(lastRow);
                  //System.out.println(objArr.length);

                  cellnum = 0;

                  //Id of the household
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getIdentity());

                  //Num people in the household
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getResidents().size());

                  //household xcoord
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getGeoPoint().getX());

                  //household xcoord
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getGeoPoint().getY());

                  //household num falciparum cases during the length of the simulation
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getNumFalciparumCases());

                  //household num vivax cases during the length of the simulation
                  cell = row.createCell(cellnum++);
                  cell.setCellType(0);
                  cell.setCellValue((double)hh.getNumVivaxCases());

              }
              //System.exit(0);

              sim.weeklyData = new ArrayList<Object[]>();

              workbookTmp = workbook;
          }
          finally
          {
              file.close();
          }

      }
      catch (IOException ex){
          ex.printStackTrace();
      }

      try{
          File out_file = new File(file_name);

          //out_file.createNewFile();

          FileOutputStream out = new FileOutputStream(out_file, false);

          try{
              //FileOutputStream out = new FileOutputStream(out_file, true);

              workbookTmp.write(out);
          }
          finally
          {
              out.flush();
              out.close();
          }
      }
      catch (IOException ex){
          ex.printStackTrace();
      }


      System.out.println("Output data written successfully.....");
           

    }

//==================================================
public void deleteFile(String file_name)
{
    File file;

    try {
        file = new File(file_name);

        // if file doesnt exists, then create it
        if (file.exists()) {
            if(file.delete())
            {
                System.out.println(file.getName() + " is deleted!");
            }
            else
            {
                System.out.println("Delete operation is failed.");
            }
        }

    } catch (Exception e) {
        e.printStackTrace();
    } 
}

//====================================================
public void writePixelsHuman(SimState state) 
{
    AmaSim sim = (AmaSim)state;

    //erase_file();

    System.out.println("================================================");
    System.out.println ("Writing  outputs Pixels Human..................");
    System.out.println(" ");
    //System.exit(0);

    int stats = 0;
    try{
        String txtFile = file_nameRootHuman;
        File newTxtFile = new File(txtFile);
        FileWriter fileWriter = new FileWriter(newTxtFile);

        String titles = "Pixel Id - Avg. Num person in the Pixel - Xcoor - Ycoor - N Falc cases - N Vivax cases";

        titles = titles + "\n";
        fileWriter.write(titles);

        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();
        System.out.println("Tot num. of geo grid Pixels: " + num_pixels);
        //System.exit(0);

        for(int i = 0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            if(cp.getAccumulatedNPeople() == 0)continue; 
            stats++;

            String write = i + " ";
            write = write + cp.getAccumulatedNPeople() + " ";

            Point center = cp.square.getCentroid();

            write = write + center.getX() + " ";
            write = write + center.getY() + " ";

            write = write + cp.getNMalariaCasesF() + " ";
            write = write + cp.getNMalariaCasesV() + " ";

            write = write + "\n";

            fileWriter.write(write);
        }
        //System.exit(0);
        fileWriter.close();
    } catch (IOException ex) {
        System.out.println(ex);

    }//End coords     file 




    System.out.println("Num pixel with visited by a person: " + stats);
    System.out.println("Output data written successfully.....");


}

//====================================================
public void writePixelsHumanTime(SimState state) 
{
    AmaSim sim = (AmaSim)state;

    //erase_file();

    System.out.println("================================================");
    System.out.println ("Writing  outputs Pixels Human Time.............");
    System.out.println(" ");
    //System.exit(0);

    int stats = 0;
    try{
        String txtFile = file_nameRootHumanTime;
        File newTxtFile = new File(txtFile);
        FileWriter fileWriter = new FileWriter(newTxtFile);

        String titles = "Pixel Id - Date - Avg. Num person in the Pixel - Xcoor - Ycoor - N cases";

        titles = titles + "\n";
        fileWriter.write(titles);

        Bag cps = sim.geoGrid.elements();
        int num_pixels = cps.size();
        //System.out.println("Tot num. of geo grid Pixels: " + num_pixels);
        //System.exit(0);

        for(int i = 0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            List<List<Object>> timePopCounts = cp.timePopCounts;
            List<List<Object>> timeMalariaCounts = cp.timeMalariaCounts;

            if(timeMalariaCounts.size() == 0)continue;

            for(int j = 0; j < timeMalariaCounts.size(); j++)
            {
                List<Object> tmpPop = timePopCounts.get(j);
                List<Object> tmpMalaria = timeMalariaCounts.get(j);

                stats++;

                String write = i + " ";

                write = write + (String)tmpMalaria.get(0) + " ";

                double d = (double)tmpPop.get(1);
                write = write + d + " ";

                Point center = cp.square.getCentroid();

                write = write + center.getX() + " ";
                write = write + center.getY() + " ";

                write = write + tmpMalaria.get(1) + " ";

                write = write + "\n";

                fileWriter.write(write);
            
            }

        }
        //System.exit(0);
        fileWriter.close();
    } catch (IOException ex) {
        System.out.println(ex);

    }//End coords     file 




    System.out.println("Num pixel with visited by a person: " + stats);
    System.out.println("Output data written successfully.....");


}



//====================================================
public void writePixelsMosquito(SimState state) 
{
    AmaSim sim = (AmaSim)state;

    //erase_file();

    System.out.println("================================================");
    System.out.println ("Writing  outputs Pixels Mosquitoes ............");
    System.out.println(" ");
    //System.exit(0);

    int stats = 0;
    try{
        String txtFile = file_nameRootMosquito;
        File newTxtFile = new File(txtFile);
        FileWriter fileWriter = new FileWriter(newTxtFile);

        String titles = "Pixel Id - Avg. Num person in the Pixel - Xcoor - Ycoor - N Falc cases - N Vivax cases";

        titles = titles + "\n";
        fileWriter.write(titles);

        Bag cps = sim.pixelMosquitosGrid.elements();
        int num_pixels = cps.size();
        System.out.println("Tot num. of mosquito grid Pixels: " + num_pixels);
        //System.exit(0);

        for(int i = 0; i < num_pixels; i++)
        {
            CoverPixel cp = ((CoverPixel)cps.objs[i]);

            if(cp.getAccumulatedNMosquitoes() == 0)continue; 
            stats++;

            String write = i + " ";
            write = write + cp.getAccumulatedNMosquitoes() + " ";

            Point center = cp.square.getCentroid();

            write = write + center.getX() + " ";
            write = write + center.getY() + " ";

            write = write + cp.getNMalariaCasesFMosquito() + " ";
            write = write + cp.getNMalariaCasesVMosquito() + " ";

            write = write + "\n";

            fileWriter.write(write);
        }
        //System.exit(0);
        fileWriter.close();
    } catch (IOException ex) {
        System.out.println(ex);

    }//End coords     file 




    System.out.println("Num pixel with visited by a mosquito: " + stats);
    System.out.println("Output data written successfully.....");


}






}
