#! /bin/tcsh
# Set MASON_HOME to the 'mason' directory
#setenv MASON_HOME       ${0:h}/..
#setenv MASON_HOME       /home/oflixs/Dropbox/Research/AG_Galapagos/Simulations/mason
#You have only to change next variable:
#setenv MASON_PATH /media/Backups/Dropbox_ubuntu/Dropbox/Research/AG_Galapagos/Simulations/mason/

setenv BASEDIR $PWD

cd ../../../
setenv MASON_PATH $PWD/
echo $MASON_PATH


setenv MASON_HOME        ${MASON_PATH}jar
setenv JTS_HOME          ${MASON_PATH}jts
setenv MASON_CHART_LIB   ${MASON_PATH}chart_lib
setenv GEO_HOME          ${MASON_PATH}geo_mason_lib
setenv CLONING           ${MASON_PATH}cloning
setenv POI               ${MASON_PATH}poi
setenv MASON_JAR         ${MASON_PATH}all_jar
#setenv MASON_HOME       /home/oflixs/Desktop/Dropbox/Research/AG_Galapagos/Simulations/mason

# Add MASON_HOME to the original classpath, even if the original is empty
setenv ORIGINAL_CLASSPATH `printenv CLASSPATH`
printenv   ORIGINAL_CLASSPATH 
setenv CLASSPATH ${MASON_PATH}

# Tack on jar files in the 'mason' directory.
# Turn off matching first so foreach doesn't freak on us.
# That will require testing for the existence of ${MASON_HOME}/\*\.jar, which
# will tell us that there were no jar files to be had.

set nonomatch=true
set jars=(${MASON_JAR}/*.jar)
if ("$jars" != "${CLONING}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif



set nonomatch=true
set jars=(${MASON_CHART_LIB}/*.jar)
if ("$jars" != "${MASON_CHART_LIB}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif

set nonomatch=true
set jars=(${CLONING}/*.jar)
if ("$jars" != "${CLONING}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif

set nonomatch=true
set jars=(${POI}/*.jar)
if ("$jars" != "${POI}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif



set nonomatch=true
set jars=(${MASON_HOME}/*.jar)
if ("$jars" != "${MASON_HOME}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif


set nonomatch=true
set jars=(${JTS_HOME}/*.jar)
if ("$jars" != "${MASON_HOME}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif

set nonomatch=true
set jars=(${GEO_HOME}/*.jar)
if ("$jars" != "${GEO_HOME}/\*\.jar") then
        foreach i ($jars)
                setenv CLASSPATH ${i}:${CLASSPATH}
        end
endif

echo ====================================================
printenv CLASSPATH
echo ====================================================

echo "Compile_and_run_analysis: compilation starts"

javac -Xmaxerrs 5 sim/app/AmaSim/*.java 

wait

echo "Compile_and_run_analysis: compilation ends"

wait

echo "Compile_and_run_analysis: Analysis starts"

java  -Xmx1800M   sim.app.AmaSim.AmaSim firstSim.params firstSim Comm0 noCalibration doAnalysis &

wait

echo "Compile_and_run_analysis: Analysis complete"

echo ====================================================

cd $BASEDIR
