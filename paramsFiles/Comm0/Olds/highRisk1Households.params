##Test Run     ##############################################
# Copyright 2014 by Francesco Pizzitutti
# Licensed under the Academic Free License version 3.0
# See the file "LICENSE" for more information
##############################################################
#Input to AmaSim simulation 
#Comment : lines starting with #, ignored lines with only whitespaces
#Format: parameter whitespace value whitespace value ....
# no '=' signs
#Parameters names are case sensitive
##############################################################
#The time step of this simulation is one hour. Every Day is 
#made by 24 hours, every week by 7 day = 168 hours
#evey month by 30 days = 720 hours
#every year by 12 month of 30 days = 8640 hours
##############################################################

##############################################################
#stats every_num_steps stop/continous (if stop write stats and stop) 
stats 96 cont
upgradeCharts 24

analysisShp 0
doWriteShp 0
analysisMosHotSpots 0

#=============================================================
#Number of step of one hour (only without UI) for every job
num_step 240000

#Simulation start date dd-MM-yyyy
startDate 01-01-2011
#Simulation end date dd-MM-yyyy
endDate 31-12-2012

#Starting Equilibration period in days
#if = 0 no equilibration period 
#equilibPeriod 350
equilibPeriod 266

#Num of jobs: every job the sim restart from the same
#initial condition and data are stored in the file file
num_job 1 sim/app/AmaSim/stat_output.out

#Name of the inhabitated places included in the simulation area
#comma separated 
places san luis tacsha curaray, tacshacuraray, santa maria de loreto, santa teresa, santa rosa, nueva chambira

#name(s) of the simulation area closest health place comma separated
healthPost san luis tacsha curaray, tacsha curaray

#Cover dynamics =============================================
#Cover Cell grid size in meters. 
mosquitoCellSize 30
geoCellSize 10

#mosquitoCellSize 80
#geoCellSize 80

#Acuatic habitats mas carrying capacity
#Max number of adult mosquitos agent per m2 of aquatic habitat
maxNumbAdultperArea 1.427
#maxNumbAdultperArea 0.0

#Pror factor between ovoposition and emergent adults
propOvopositionEmergentAdults 7.022
#propOvopositionEmergentAdults 0.0

#the low waters generate mosquito with a density per pixel
#that is the numMosPerHighWaterArea value divided by this
#factor
numMosHighLowWaterRatio 14.022

#To account for the mosquitoes zoophagy to every simulation pixel 
#is associated a prob that the mosquito meet an animal and feeds 
#from it
probBloodMealperPixel 0.0

#To account for the increased number of mammals (chickens, rats,
#pigs, dogs) around humans households 0
probBloodMealperPixelAroundHumans 0.418
#=Plasmodia parameters========================================
#Transmission efficiency infected human to no infected mosquito
SymptomaticFalciparumTEfficiencyHumanToMosquito 0.4
SymptomaticVivaxTEfficiencyHumanToMosquito 0.4

AsymptomaticFalciparumTEfficiencyHumanToMosquito 0.1
AsymptomaticVivaxTEfficiencyHumanToMosquito 0.1

#Transmission efficiency infected mosquito to no infected human
FalciparumTEfficiencyMosquitoToHuman 1.0
VivaxTEfficiencyMosquitoToHuman 1.0

#Plasmodium sporogonic cycle parameters ------------
#DD degree days to incubation
FalciparumDD 111
VivaxDD 105

#minTemp minimum incubation temperature 
FalciparumIncubationMinTemp 16.0
VivaxIncubationMinTemp 14.5

#Intrinsic (human) incubation period in days minVal maxVal ----
FalciparumIntrinsicIncubation 9 14
VivaxIntrinsicIncubation 12 17

#Human infectious period if not treated in days---------
FalciparumHumanInfectiousPeriodNotTreated 60
VivaxHumanInfectiousPeriodNotTreated 30

#Human infectious period if treated---in hours
#FalciparumHumanInfectiousPeriod 360
#VivaxHumanInfectiousPeriod 240
FalciparumHumanInfectiousPeriodTreated 160
VivaxHumanInfectiousPeriodTreated 24

#Start of the human communicability  period (infectious period)
#after the transmission from mosquito to human in days
FalciparumStartCommunicability 12
VivaxStartCommunicability 11


#Fract. of asynptomaticity gained per infection
#*****
FalciparumFractAsymptomaticityGainedPerInfection 0.1
VivaxFractAsymptomaticityGainedPerInfection 0.1

#Fract of Asyptomaticity lost per hour
#0.000114115 correspond to the complete lost of immunity in one year
FalciparumFractAsymptomaticityLostPerHour 0.000114115
VivaxFractAsymptomaticityLostPerHour 0.000114115

#=Mosquitos parameters========================================
#duration of the aquatic stage develpment of mosquitos in days
aquaticStageTime 15

#Time that need a female moquito to develop egg after blood-meal in hours
eggDevelpmentTime 48

#Daily survival probability
mosquitoDailySurvivalProbabilityRain 0.3731
mosquitoDailySurvivalProbabilityNoRain 0.8891

minRainWetDay 100
rainSurvFact 0.7

#Rest times -------
restingTimeAfterBite 4
restingTimeAfterLayEggs 0

#hour in 24h format
bitingPeak 0
#number of biting hours around the biting peak 
bitingPeakDev 6

#Mating time probability distribution ---------------
matingProbDist 24 0.1 36 0.5 48 0.3 60 0.1

#When moquito identifies the hosto to bite 
#there is a probability to fail the bite
probFailBite 0.0

probBloodMealFromOutdoorHuman 0.60

#=Households parameters ======================================

#Parameters of the age dependent force of infection
cAgeDepForceOfInfection 0.99
kAgeDepForceOfInfection 0.14


#Wheight random walk movement toward specified pixels
#such as cells with human presence and water breeding sites cells
#values < 0.5 a and > 0 mean that specified pixels are not favorite respet to 
#unspecified pixels
#values > 0.5 and < 1 mean that specified pixels are favorite
randomWalkWeight 0.85

#=Human Parameters ===========================================
fractProtectedPeople 0.892
probMosquitoDieBitingProtectedPerson 0.681
#Averge number of people per household with SD
avgPeoplePerHousehold 6 3
#
fractHumanTreated 1.0

fractHumanAsymptomatic 0.03
fractHumanAsymptomaticV 0.039
fractHumanAsymptomaticF 0.066

asymptoSuper 0
# readuction of infectivity of asymptomatic humans: 1.0 no reduction; 
# 0.0 no trasmission to mosquitoes from humans

#time (in days) after the malaria symptoms the human protect 
#himself and his family against mosquitos
#and related probFail of msquito bite during this period. probFail = 1 evey 
#bite attempt from mosquitos fails. probFail = 0 every bite attempt succeds
#The protection starts to decrease monotonically to zero after
#a protectionperiodFact * malariaProtection period is passed
malariaProtection 365
probFailMalariaProtection 1.0
protectionPeriodFact 0.4

#Recurrence average time in days and recurrence risk for Vivax malaria -----
vivaxTimeRecurrence 203
vivaxRiskRecurrence 0.3

#parameters regulatin the activation of protections against malaria when
#incidence is above the protectionActivationTreshold.The protection is increased 
#by a fractPeopleAddprotection every week
protectionActivationTreshold 10000.0
fractPeopleAddprotection 0.0
setProtOff 0.01

# originals riverPatch 89 - minRiver 82.75
riverPatch 89
minRiver 83.5

#Number of pixels river level marolevels
numRiverMacrolevels 10

#To set zero the mosDensity in a circular area of radis noMosHouseRadius meters
#If the radius is set 0.0 the radius the module is inactive
noMosHouseRadiusHigh 0.0
noMosHouseRadiusLow 0.0

##To include local human movement (inside the community)
#Values:
#0 no human movement at all
#1 perfect mixing: humans are swapped between houses every 24h
#2 Random walk with high weight on roads and households
#3 location-based social movement: go to only one focal point
#4 location-based social movement: go to school, to the church, to playground
#  farming, fishing
humanLocalMovement 4
#Only for humanLocalMovement = 0
humanMixingType 0
#Human protected when moving at the same level of the household protection
humanProtectedEverywhere 0
#Human protected where and when
# = 0 human movement complete not protected when moving but protected in house sleeping
# = 1 human movement complete protected as they were sleeping also when moving 
# = 2 human does not move and change protection state in the house pixel when not sleeping

#highRiskWaht change the protection of certain human agents
#0 no change
#1 hotspots
#2 households
#3 farmers
#4 Prot
highRiskWhat 2
numExcludedHotspots 1
numPeopleProtectedForever 0
