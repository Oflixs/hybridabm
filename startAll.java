/*
  Copyright 2011 by Francesco Pizzitutti
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package sim.app.AmaSim;

import sim.engine.*;
import sim.util.*;

import com.vividsolutions.jts.geom.Polygon;

public class startAll implements Steppable
{
   private static final long serialVersionUID = 1L;

   public AmaSim sim = null;
   public SimState state = null;

   //====================================================
   public startAll(AmaSim psim)
   {
      sim = psim;
      state = (SimState)sim;

      sim.utils = new Utils(sim);


      System.out.println("================================================");
      System.out.println ("== startAll ===================================");
      //Print the input file content to te output--------
      ReadInput input = new ReadInput(sim);
      //input.PrintInput();
      //System.exit(0);

      //Generates the two kind of Plasmodia ----------------
      System.out.println("Reading Plasmodia data -------------------------");
      sim.Falciparum         = new Plasmodium(sim, "Falciparum");
      sim.Vivax              = new Plasmodium(sim, "Vivax");
      sim.bothPlasmodia      = new Plasmodium(sim, "bothPlasmodia");

      sim.Falciparum.init(sim, input);
      sim.Vivax.init(sim, input);
      sim.bothPlasmodia.init(sim, input);


      //System.out.println ("numMosPerHigh " + sim.maxNumbAdultperArea);
      //System.out.println ("propOvo " + sim.propOvopositionEmergentAdults);
      //System.out.println ("HighLowRatio " + sim.numMosHighLowWaterRatio);
      //System.exit(0);

      
      //ReadLDAS readLDAS = new ReadLDAS(sim);
      //readLDAS.setFile("RioNapo20112012_MERRA2CHIRPS_NOAH36.nc");
      //readLDAS.read();
      //System.exit(0);

      //Read the metereological data -----------------------
      if(sim.withDates)
      {
         ReadXlsMeteo rxlm = new ReadXlsMeteo(sim);
         rxlm.read(sim);
         rxlm.checkData(sim);
         //rxlm.closeHoles(sim);
         //System.exit(0);

         //rxlm.rescaleData(sim);
         rxlm.transformInHourly(sim);
         rxlm.cutMeteoData(sim);
         sim.num_step = sim.meteoData.size() - 1;
         sim.num_step = sim.num_step + sim.equilibPeriod;
      }
      //System.out.println ("num_step = " + sim.num_step);
      //System.out.println ("N of simulation years = " + sim.num_step/(24 * 365));
      //System.exit(0);

      //Read the Nanay River level data--------------
      sim.readRiver.setFile("nivelNapoMazanBellavista2011-2013.xls");
      sim.readRiver.readLevels(sim);
      //sim.readRiver.rewriteData(sim);
      //System.exit(0);
      sim.readRiver.transformData(sim);
      //System.exit(0);

      //Read the malaria prevalence data-------------
      sim.readMalaria.setFile("MAZAN_NAPO_CORRECTED_2009-2012.xls");
      sim.readMalaria.readIncidence(sim);
      sim.readMalaria.readAgeStructure(sim);
      //System.exit(0);

      //sim.readMalaria.rewriteData(sim);
      //sim.readMalaria.transformData(sim);
      //System.exit(0);

      //Read the input geo covers----------------------------
      if(!sim.gridSetupDone)
      {
         ReadShp RShp = new ReadShp(sim);
         RShp.gridSetup(sim);
      }

      //System.exit(0);

      //Initialize the Geo grid  is important the order of calls---
      sim.initLayer = new InitializeLayers(sim);
      sim.initLayer.setupCpClusters();
      sim.initLayer.setWater(sim);
      //sim.initLayer.findRiverBanks(sim);
      sim.initLayer.findDryHigh(sim);
      sim.initLayer.checkLayers(sim);
      if(sim.highRiskWhat == 1 || sim.highRiskWhat == 2)sim.initLayer.setHighRisk(sim);

      //System.exit(0);

      //sim.initLayer.setWater(sim, "low");

      //sim.initLayer.setCover(sim);

      //Bicubic interpolator --->> not works
      //sim.bInterpolator = new BicubicInterpolator(sim);
      //Bilinear interpolator -
      sim.blinearInterpolator = new BilinearInterpolator(sim);
      sim.initLayer.setElevation(sim);
      //System.exit(0);
      sim.initLayer.findWaterMinMaxElevation(sim);
      sim.initLayer.setupCpMacrolevel(sim);
      //System.exit(0);

      LayersDynamics ld = new LayersDynamics(sim);
      //System.exit(0);
      

      //Initialize the Piuxel mosquito grid grid  ----------------
      sim.initPixelMosquitosGrid = new InitializePixelMosquitosGrid(sim);
      sim.initPixelMosquitosGrid.initAll(sim);
      sim.initPixelMosquitosGrid.linkTwoGrids(sim);
      //System.exit(0);

      //Select involved LDAS pixels and assemble the soilMoisture time series
      //sim.initLayer.findLDASPixels(state); 
      //readLDAS.getSoilMoisture();
      //System.exit(0);

      if(sim.humanLocalMovement != 0)
      {
          //Virtual cp where humans go when they are outside the community
          Polygon psquare = null;
          sim.outsideCp = new CoverPixel(sim, -10, -10, psquare);

          ReadDailyActivities readDailyAct = new ReadDailyActivities(sim);

          readDailyAct.read();

          readDailyAct.initSocial();

          readDailyAct.setCoverPixelsRoads();

          readDailyAct.initializePlaces();

          readDailyAct.initializeFarms();
      }

      //Generate Households----------------------------------
      sim.hhGen = new HouseholdsGenerator(sim);
      sim.hhGen.initHh(sim);
      //System.exit(0);

      //Generate Humans  -----------------------------------
      sim.hGen = new HumansGenerator();
      sim.hGen.generateHumans(sim);
      //System.exit(0);

      //Initialize the households and Breeding insede
      //mosquitos pixel grid pixels
      sim.initPixelMosquitosGrid.setHouseholds(sim);
      //sim.initPixelMosquitosGrid.setBreeding(sim);

      //To write outputs at the end of the simulation -------
      WriteStats ws = new WriteStats(sim);
      sim.schedule.scheduleRepeating(1.0, 9, ws);

      //System.exit(0);

 
      //Generate Mosquitos----------------------------------
      sim.mosGen = new MosquitosGenerator(sim);
      //sim.mosGen.initMos(sim);
      //sim.schedule.scheduleRepeating(sim.mosGen);

      sim.mosGen.generateMosquitosStart(sim);
      sim.mosGen.generateMosquitosStartEggs(sim);
      //System.out.println("end ---------");

      //Start the General Statistics module -------------------
      //System.exit(0);
      if(sim.stats)
      {
          Statistic statistics = new Statistic(sim);
          //Start repeat at 1.0 with ordering 10
          sim.schedule.scheduleRepeating(1.0, 10, statistics);
      }

      //For hybrid analysis -------
      sim.hybridAnalysis = false;
      if(sim.hybridAnalysis)
      {
          sim.hAnalysis = new HybridAnalysis(sim);
          //System.exit(0);
          sim.schedule.scheduleRepeating(1.0, 10, sim.hAnalysis);
      }

      if(sim.hybridDensity)sim.hInterface = new HybridInterface(sim);

      //To test all =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
      //System.exit(0);


   }

   //====================================================
   public void step(final SimState state)
   {
      final AmaSim sim = (AmaSim)state;
      sim.equilibPeriodCounter++;
      if(sim.equilibPeriodCounter == sim.equilibPeriod )
      {
         sim.equilibration = false;
         //sim.hhGen.setUpOneMalaria(sim);
      }
      
      //int now = sim.utils.getTime();
      //System.out.println ("now print output = " + now);
      //now = (int)state.schedule.getTime();  
      //System.out.println ("now print output = " + now);
      //System.exit(0);

      int now = (int)state.schedule.getTime();  
      sim.utils.getDayOfWeek(state);

      //Here to activate and desactivate write Shp
      if(now == (sim.num_step - 4) && sim.doWriteShp == 1)sim.writeShp.write(state);
      //sim.writeShp.read(state);
      //System.exit(0);
   }


//============================================================   
}
