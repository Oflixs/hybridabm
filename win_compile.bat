@ECHO OFF

rem Saving the classpath so we can restore it at the end.
SET OLDCLASSPATH=%CLASSPATH%

cp localJar/mosDensity.jar ../../../all_jar/

rem Assuming you kept this batch file in MASON_PATH\start
rem MASON_PATH is whatever .. expands to.
set STARTING_POINT=%CD%
cd ..
cd ..
cd ..
SET MASON_PATH=%CD%

REM cp all_jar/nome /sim/app/AmaSim/

SET MASON_JAR=%MASON_PATH%\all_jar
SET MASON_HOME=%MASON_PATH%\jar
SET JTS_PATH=%MASON_PATH%\jts
SET MASON_CHART_LIB=%MASON_PATH%\chart_lib
SET GEO_PATH=%MASON_PATH%\geo_mason_lib
SET CLONING=%MASON_PATH%\cloning
SET POI=%MASON_PATH%\poi

rem ECHO %MASON_PATH%

rem adding all jars in the jar directory to the classpath.

rem (for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 2>nul
(for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 

ECHO %MASON_JAR%
ECHO %CLASSPATH%

rem javac -cp  sim\app\AmaSim\*.java

rem javac -classpath "%MASON_PATH%;%MASON_PATH%\itext-1.2.jar;%MASON_PATH%\jcommon-1.0.16.jar;%MASON_PATH%\jfreechart-1.0.13.jar;%MASON_PATH%\jmf.jar"     sim\app\AmaSim\*.java


rem echo javac -cp "%MASON_HOME%\mason.16.jar;%JTS_PATH%\*.jar;%MASON_CHART_LIB%/*.jar;%GEO_PATH%\geomason.1.1.jar;%CLONING%\*.jar;%POI%\*.jar"     sim\app\AmaSim\*.java

rem echo javac -classpath "%MASON_PATH%;%MASON_PATH%/itext-1.2.jar;%MASON_PATH%/jcommon-1.0.16.jar;%MASON_PATH%;/jcommon-1.0.16.jar%MASON_PATH%/jfreechart-1.0.13.jar"     sim\app\AmaSim\*.java



cd %STARTING_POINT%
rem echo javac  sim\app\AmaSim\*.java
rem javac  *.java

"C:\Program Files\Java\jdk1.8.0_121\bin\javac.exe" -Xmaxerrs 5 -J-XX:+UseSerialGC  *.java

cd %MASON_PATH%

cd %STARTING_POINT%

rem Restoring the classpath.
SET CLASSPATH=%OLDCLASSPATH%
