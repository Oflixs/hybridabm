@ECHO OFF

rem Saving the classpath so we can restore it at the end.
SET OLDCLASSPATH=%CLASSPATH%

cp localJar/mosDensity.jar ../../../all_jar/

rem Assuming you kept this batch file in MASON_PATH\start
rem MASON_PATH is whatever .. expands to.
set STARTING_POINT=%CD%
cd ..
cd ..
cd ..
SET MASON_PATH=%CD%

SET MASON_JAR=%MASON_PATH%\all_jar
SET MASON_HOME=%MASON_PATH%\jar
SET JTS_PATH=%MASON_PATH%\jts
SET MASON_CHART_LIB=%MASON_PATH%\chart_lib
SET GEO_PATH=%MASON_PATH%\geo_mason_lib
SET CLONING=%MASON_PATH%\cloning
SET POI=%MASON_PATH%\poi

rem ECHO %MASON_PATH%

rem adding all jars in the jar directory to the classpath.

rem (for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 2>nul
(for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 

ECHO %MASON_JAR%
ECHO %CLASSPATH%

cd %STARTING_POINT%


rem ==================================================================
"C:\Program Files\Java\jdk1.8.0_121\bin\javac.exe" -Xmaxerrs 5 -J-XX:+UseSerialGC  *.java

cd %MASON_PATH%
ECHO compiled

rem ==================================================================
rem "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx7000M   sim.app.AmaSim.AmaSim parallel.params parallel Comm0 noCalibration
"C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx3000M   sim.app.AmaSim.AmaSim parallel.params test Comm16 noCalibration
rem "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx3200M   sim.app.AmaSim.AmaSim template.params test Comm16 noCalibration
rem "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx3200M sim.app.AmaSim.AmaSim template.params reference Comm16 noCalibration doAnalysis


cd %MASON_PATH%


cd %STARTING_POINT%

rem Restoring the classpath.
SET CLASSPATH=%OLDCLASSPATH%
