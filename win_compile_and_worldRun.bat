@ECHO OFF

rem Saving the classpath so we can restore it at the end.
SET OLDCLASSPATH=%CLASSPATH%


rem Assuming you kept this batch file in MASON_PATH\start
rem MASON_PATH is whatever .. expands to.
set STARTING_POINT=%CD%
cd ..
cd ..
cd ..
SET MASON_PATH=%CD%

SET MASON_JAR=%MASON_PATH%\all_jar
SET MASON_HOME=%MASON_PATH%\jar
SET JTS_PATH=%MASON_PATH%\jts
SET MASON_CHART_LIB=%MASON_PATH%\chart_lib
SET GEO_PATH=%MASON_PATH%\geo_mason_lib
SET CLONING=%MASON_PATH%\cloning
SET POI=%MASON_PATH%\poi


rem ECHO %MASON_PATH%

rem adding all jars in the jar directory to the classpath.

rem (for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 2>nul
(for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 

ECHO %MASON_JAR%
ECHO %CLASSPATH%


cd %STARTING_POINT%


echo -------------------------------------
echo -------------------------------------
echo current directory: 
pwd
echo starting point:
echo %starting_point%
echo -------------------------------------
echo -------------------------------------




rem ==================================================================
"C:\Program Files\Java\jdk1.8.0_121\bin\javac.exe" -Xmaxerrs 5 *.java
cd %MASON_PATH%

ECHO compiled

echo -------------------------------------
echo -------------------------------------
echo current directory: 
pwd
echo starting point:
echo %starting_point%
echo -------------------------------------
echo -------------------------------------





rem ==================================================================
rem "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe" -Dcom.sun.management.jmxremote -Xmx13000M  -XX:+UseSerialGC  sim.app.AmaSim.AmaSimWorld parallel.params parallel
"C:\Program Files\Java\jdk1.8.0_121\bin\java.exe" -Dcom.sun.management.jmxremote -Xmx10000M  sim.app.AmaSim.AmaSimWorld parallel.params parallel

cd %MASON_PATH%
REM "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx800M   sim.app.AmaSim.AmaSim paramsFiles\test.params test doAnalysis

rem "C:\Program Files\Java\jdk1.7.0_51\bin\java.exe"  -Xmx800M   sim.app.AmaSim.AmaSim AmaSim.params > out.out

rem java -Xmx800M   sim.app.AmaSim.ShowGraphs



rem java -Xmx800M   sim.app.AmaSim.ShowGraphs

rem java -Xmx200M sim.app.AmaSim.AmaSim >nul 2>nul 

cd %STARTING_POINT%

rem Restoring the classpath.
SET CLASSPATH=%OLDCLASSPATH%
