
@ECHO OFF

rem Saving the classpath so we can restore it at the end.
SET OLDCLASSPATH=%CLASSPATH%

set BATCH_DIR=%CD%

rem Assuming you kept this batch file in MASON_PATH\startxxx
rem MASON_PATH is whatever .. expands to.
set STARTING_POINT=%CD%
cd ..
cd ..
cd ..
SET MASON_PATH=%CD%

SET MASON_JAR=%MASON_PATH%\all_jar
SET MASON_HOME=%MASON_PATH%\jar
SET JTS_PATH=%MASON_PATH%\jts
SET MASON_CHART_LIB=%MASON_PATH%\chart_lib
SET GEO_PATH=%MASON_PATH%\geo_mason_lib
SET CLONING=%MASON_PATH%\cloning
SET POI=%MASON_PATH%\poi



(for /F %%f IN ('dir /b /a-d "%MASON_JAR%\*.jar"') do call ignoreme.bat %MASON_JAR%\%%f%) 

ECHO %MASON_JAR%
ECHO %CLASSPATH%

cd %STARTING_POINT%
"C:\Program Files\Java\jdk1.8.0_121\bin\javac.exe"  *.java

rem first sim -------------------------------------

rem run number 1
cd %MASON_PATH%

rem HERE START RUNS
start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx3200M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0 Comm0 noCalibration "

sleep 1

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

start /B cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

sleep 10

call cmd /c " "C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration " 

rem HERE START RUNS


rem analysis ---------------------------------------

sleep 10
cd %MASON_PATH%

"C:\Program Files\Java\jdk1.8.0_121\bin\javac.exe" -Xmaxerrs 5 -d .  *.java

rem HERE START ANALYSIS
"C:\Program Files\Java\jdk1.8.0_121\bin\java.exe"  -Xmx1500M   sim.app.AmaSim.AmaSim calibratedComm0.params calibratedComm0  Comm0  noCalibration doAnalysis  

rem HERE START ANALYSIS


SET CLASSPATH=%OLDCLASSPATH%

cd %STARTING_POINT%
cd %BATCH_DIR%
